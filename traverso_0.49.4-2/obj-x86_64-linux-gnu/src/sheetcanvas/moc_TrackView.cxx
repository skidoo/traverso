/****************************************************************************
** Meta object code from reading C++ file 'TrackView.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/sheetcanvas/TrackView.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TrackView.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_TrackView[] = {

 // content:
       6,       // revision
       0,       // classname
       4,   14, // classinfo
       6,   22, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // classinfo: key, value
      26,   10,
      57,   42,
      83,   72,
     109,   94,

 // slots: signature, parameters, type, tag, flags
     134,  124,  125,  124, 0x0a,
     152,  124,  125,  124, 0x0a,
     169,  124,  125,  124, 0x0a,
     182,  124,  125,  124, 0x0a,
     204,  199,  124,  124, 0x08,
     238,  199,  124,  124, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_TrackView[] = {
    "TrackView\0Edit properties\0edit_properties\0"
    "Add new Plugin\0add_new_plugin\0Select Bus\0"
    "select_bus\0Insert Silence\0insert_silence\0"
    "\0Command*\0edit_properties()\0"
    "add_new_plugin()\0select_bus()\0"
    "insert_silence()\0clip\0"
    "add_new_audioclipview(AudioClip*)\0"
    "remove_audioclipview(AudioClip*)\0"
};

void TrackView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        TrackView *_t = static_cast<TrackView *>(_o);
        switch (_id) {
        case 0: { Command* _r = _t->edit_properties();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 1: { Command* _r = _t->add_new_plugin();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 2: { Command* _r = _t->select_bus();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 3: { Command* _r = _t->insert_silence();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 4: _t->add_new_audioclipview((*reinterpret_cast< AudioClip*(*)>(_a[1]))); break;
        case 5: _t->remove_audioclipview((*reinterpret_cast< AudioClip*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData TrackView::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject TrackView::staticMetaObject = {
    { &ViewItem::staticMetaObject, qt_meta_stringdata_TrackView,
      qt_meta_data_TrackView, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &TrackView::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *TrackView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *TrackView::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_TrackView))
        return static_cast<void*>(const_cast< TrackView*>(this));
    return ViewItem::qt_metacast(_clname);
}

int TrackView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ViewItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
