/****************************************************************************
** Meta object code from reading C++ file 'AudioClipEditDialog.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../src/sheetcanvas/dialogs/AudioClipEditDialog.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'AudioClipEditDialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_AudioClipEditDialog[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      30,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      21,   20,   20,   20, 0x08,
      43,   20,   20,   20, 0x08,
      64,   20,   20,   20, 0x08,
      79,   20,   20,   20, 0x08,
      96,   20,   20,   20, 0x08,
     126,  120,   20,   20, 0x08,
     161,   20,   20,   20, 0x08,
     190,  185,   20,   20, 0x08,
     217,   20,   20,   20, 0x08,
     245,  239,   20,   20, 0x08,
     275,   20,   20,   20, 0x08,
     300,  120,   20,   20, 0x08,
     336,   20,   20,   20, 0x08,
     362,  120,   20,   20, 0x08,
     399,   20,   20,   20, 0x08,
     415,   20,   20,   20, 0x08,
     432,  185,   20,   20, 0x08,
     460,   20,   20,   20, 0x08,
     485,   20,   20,   20, 0x08,
     508,  239,   20,   20, 0x08,
     539,   20,   20,   20, 0x08,
     565,  120,   20,   20, 0x08,
     602,   20,   20,   20, 0x08,
     629,  120,   20,   20, 0x08,
     667,   20,   20,   20, 0x08,
     684,   20,   20,   20, 0x08,
     702,  185,   20,   20, 0x08,
     733,  185,   20,   20, 0x08,
     765,   20,   20,   20, 0x08,
     783,   20,   20,   20, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_AudioClipEditDialog[] = {
    "AudioClipEditDialog\0\0external_processing()\0"
    "clip_state_changed()\0save_changes()\0"
    "cancel_changes()\0clip_position_changed()\0"
    "value\0gain_spinbox_value_changed(double)\0"
    "fadein_length_changed()\0time\0"
    "fadein_edit_changed(QTime)\0"
    "fadein_mode_changed()\0index\0"
    "fadein_mode_edit_changed(int)\0"
    "fadein_bending_changed()\0"
    "fadein_bending_edit_changed(double)\0"
    "fadein_strength_changed()\0"
    "fadein_strength_edit_changed(double)\0"
    "fadein_linear()\0fadein_default()\0"
    "fadeout_edit_changed(QTime)\0"
    "fadeout_length_changed()\0"
    "fadeout_mode_changed()\0"
    "fadeout_mode_edit_changed(int)\0"
    "fadeout_bending_changed()\0"
    "fadeout_bending_edit_changed(double)\0"
    "fadeout_strength_changed()\0"
    "fadeout_strength_edit_changed(double)\0"
    "fadeout_linear()\0fadeout_default()\0"
    "clip_start_edit_changed(QTime)\0"
    "clip_length_edit_changed(QTime)\0"
    "update_clip_end()\0fade_curve_added()\0"
};

void AudioClipEditDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        AudioClipEditDialog *_t = static_cast<AudioClipEditDialog *>(_o);
        switch (_id) {
        case 0: _t->external_processing(); break;
        case 1: _t->clip_state_changed(); break;
        case 2: _t->save_changes(); break;
        case 3: _t->cancel_changes(); break;
        case 4: _t->clip_position_changed(); break;
        case 5: _t->gain_spinbox_value_changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 6: _t->fadein_length_changed(); break;
        case 7: _t->fadein_edit_changed((*reinterpret_cast< const QTime(*)>(_a[1]))); break;
        case 8: _t->fadein_mode_changed(); break;
        case 9: _t->fadein_mode_edit_changed((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 10: _t->fadein_bending_changed(); break;
        case 11: _t->fadein_bending_edit_changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 12: _t->fadein_strength_changed(); break;
        case 13: _t->fadein_strength_edit_changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 14: _t->fadein_linear(); break;
        case 15: _t->fadein_default(); break;
        case 16: _t->fadeout_edit_changed((*reinterpret_cast< const QTime(*)>(_a[1]))); break;
        case 17: _t->fadeout_length_changed(); break;
        case 18: _t->fadeout_mode_changed(); break;
        case 19: _t->fadeout_mode_edit_changed((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 20: _t->fadeout_bending_changed(); break;
        case 21: _t->fadeout_bending_edit_changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 22: _t->fadeout_strength_changed(); break;
        case 23: _t->fadeout_strength_edit_changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 24: _t->fadeout_linear(); break;
        case 25: _t->fadeout_default(); break;
        case 26: _t->clip_start_edit_changed((*reinterpret_cast< const QTime(*)>(_a[1]))); break;
        case 27: _t->clip_length_edit_changed((*reinterpret_cast< const QTime(*)>(_a[1]))); break;
        case 28: _t->update_clip_end(); break;
        case 29: _t->fade_curve_added(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData AudioClipEditDialog::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject AudioClipEditDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_AudioClipEditDialog,
      qt_meta_data_AudioClipEditDialog, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &AudioClipEditDialog::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *AudioClipEditDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *AudioClipEditDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_AudioClipEditDialog))
        return static_cast<void*>(const_cast< AudioClipEditDialog*>(this));
    if (!strcmp(_clname, "Ui::AudioClipEditDialog"))
        return static_cast< Ui::AudioClipEditDialog*>(const_cast< AudioClipEditDialog*>(this));
    return QDialog::qt_metacast(_clname);
}

int AudioClipEditDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 30)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 30;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
