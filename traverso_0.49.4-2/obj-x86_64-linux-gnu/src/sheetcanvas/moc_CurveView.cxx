/****************************************************************************
** Meta object code from reading C++ file 'CurveView.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/sheetcanvas/CurveView.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'CurveView.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_DragNode[] = {

 // content:
       6,       // revision
       0,       // classname
       2,   14, // classinfo
       2,   18, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // classinfo: key, value
      17,    9,
      35,   25,

 // slots: signature, parameters, type, tag, flags
      57,   46,   45,   45, 0x0a,
      71,   46,   45,   45, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_DragNode[] = {
    "DragNode\0Move Up\0move_up\0Move Down\0"
    "move_down\0\0autorepeat\0move_up(bool)\0"
    "move_down(bool)\0"
};

void DragNode::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        DragNode *_t = static_cast<DragNode *>(_o);
        switch (_id) {
        case 0: _t->move_up((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->move_down((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData DragNode::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject DragNode::staticMetaObject = {
    { &Command::staticMetaObject, qt_meta_stringdata_DragNode,
      qt_meta_data_DragNode, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &DragNode::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *DragNode::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *DragNode::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_DragNode))
        return static_cast<void*>(const_cast< DragNode*>(this));
    return Command::qt_metacast(_clname);
}

int DragNode::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Command::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}
static const uint qt_meta_data_CurveView[] = {

 // content:
       6,       // revision
       0,       // classname
       5,   14, // classinfo
      11,   24, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // classinfo: key, value
      19,   10,
      40,   28,
      69,   52,
      96,   86,
     132,  106,

 // signals: signature, parameters, type, tag, flags
     157,  156,  156,  156, 0x05,

 // slots: signature, parameters, type, tag, flags
     182,  156,  173,  156, 0x0a,
     193,  156,  173,  156, 0x0a,
     207,  156,  173,  156, 0x0a,
     226,  156,  173,  156, 0x0a,
     238,  156,  173,  156, 0x0a,
     269,  264,  156,  156, 0x08,
     300,  264,  156,  156, 0x08,
     334,  156,  156,  156, 0x08,
     347,  156,  156,  156, 0x08,
     363,  156,  156,  156, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_CurveView[] = {
    "CurveView\0New node\0add_node\0Remove node\0"
    "remove_node\0Remove all Nodes\0"
    "remove_all_nodes\0Move node\0drag_node\0"
    "Move node (vertical only)\0"
    "drag_node_vertical_only\0\0curveModified()\0"
    "Command*\0add_node()\0remove_node()\0"
    "remove_all_nodes()\0drag_node()\0"
    "drag_node_vertical_only()\0node\0"
    "add_curvenode_view(CurveNode*)\0"
    "remove_curvenode_view(CurveNode*)\0"
    "node_moved()\0set_view_mode()\0"
    "update_blink_color()\0"
};

void CurveView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        CurveView *_t = static_cast<CurveView *>(_o);
        switch (_id) {
        case 0: _t->curveModified(); break;
        case 1: { Command* _r = _t->add_node();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 2: { Command* _r = _t->remove_node();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 3: { Command* _r = _t->remove_all_nodes();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 4: { Command* _r = _t->drag_node();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 5: { Command* _r = _t->drag_node_vertical_only();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 6: _t->add_curvenode_view((*reinterpret_cast< CurveNode*(*)>(_a[1]))); break;
        case 7: _t->remove_curvenode_view((*reinterpret_cast< CurveNode*(*)>(_a[1]))); break;
        case 8: _t->node_moved(); break;
        case 9: _t->set_view_mode(); break;
        case 10: _t->update_blink_color(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData CurveView::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject CurveView::staticMetaObject = {
    { &ViewItem::staticMetaObject, qt_meta_stringdata_CurveView,
      qt_meta_data_CurveView, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &CurveView::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *CurveView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *CurveView::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_CurveView))
        return static_cast<void*>(const_cast< CurveView*>(this));
    return ViewItem::qt_metacast(_clname);
}

int CurveView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ViewItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    }
    return _id;
}

// SIGNAL 0
void CurveView::curveModified()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
QT_END_MOC_NAMESPACE
