/****************************************************************************
** Meta object code from reading C++ file 'TrackPanelView.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/sheetcanvas/TrackPanelView.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TrackPanelView.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_TrackPanelGain[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      25,   15,   16,   15, 0x0a,
      42,   15,   16,   15, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_TrackPanelGain[] = {
    "TrackPanelGain\0\0Command*\0gain_increment()\0"
    "gain_decrement()\0"
};

void TrackPanelGain::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        TrackPanelGain *_t = static_cast<TrackPanelGain *>(_o);
        switch (_id) {
        case 0: { Command* _r = _t->gain_increment();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 1: { Command* _r = _t->gain_decrement();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        default: ;
        }
    }
}

const QMetaObjectExtraData TrackPanelGain::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject TrackPanelGain::staticMetaObject = {
    { &ViewItem::staticMetaObject, qt_meta_stringdata_TrackPanelGain,
      qt_meta_data_TrackPanelGain, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &TrackPanelGain::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *TrackPanelGain::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *TrackPanelGain::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_TrackPanelGain))
        return static_cast<void*>(const_cast< TrackPanelGain*>(this));
    return ViewItem::qt_metacast(_clname);
}

int TrackPanelGain::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ViewItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}
static const uint qt_meta_data_TrackPanelPan[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      24,   14,   15,   14, 0x0a,
      35,   14,   15,   14, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_TrackPanelPan[] = {
    "TrackPanelPan\0\0Command*\0pan_left()\0"
    "pan_right()\0"
};

void TrackPanelPan::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        TrackPanelPan *_t = static_cast<TrackPanelPan *>(_o);
        switch (_id) {
        case 0: { Command* _r = _t->pan_left();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 1: { Command* _r = _t->pan_right();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        default: ;
        }
    }
}

const QMetaObjectExtraData TrackPanelPan::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject TrackPanelPan::staticMetaObject = {
    { &ViewItem::staticMetaObject, qt_meta_stringdata_TrackPanelPan,
      qt_meta_data_TrackPanelPan, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &TrackPanelPan::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *TrackPanelPan::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *TrackPanelPan::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_TrackPanelPan))
        return static_cast<void*>(const_cast< TrackPanelPan*>(this));
    return ViewItem::qt_metacast(_clname);
}

int TrackPanelPan::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ViewItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}
static const uint qt_meta_data_TrackPanelLed[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      20,   15,   14,   14, 0x0a,
      48,   14,   39,   14, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_TrackPanelLed[] = {
    "TrackPanelLed\0\0isOn\0ison_changed(bool)\0"
    "Command*\0toggle()\0"
};

void TrackPanelLed::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        TrackPanelLed *_t = static_cast<TrackPanelLed *>(_o);
        switch (_id) {
        case 0: _t->ison_changed((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: { Command* _r = _t->toggle();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        default: ;
        }
    }
}

const QMetaObjectExtraData TrackPanelLed::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject TrackPanelLed::staticMetaObject = {
    { &ViewItem::staticMetaObject, qt_meta_stringdata_TrackPanelLed,
      qt_meta_data_TrackPanelLed, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &TrackPanelLed::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *TrackPanelLed::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *TrackPanelLed::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_TrackPanelLed))
        return static_cast<void*>(const_cast< TrackPanelLed*>(this));
    return ViewItem::qt_metacast(_clname);
}

int TrackPanelLed::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ViewItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}
static const uint qt_meta_data_TrackPanelBus[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      15,   14,   14,   14, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_TrackPanelBus[] = {
    "TrackPanelBus\0\0bus_changed()\0"
};

void TrackPanelBus::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        TrackPanelBus *_t = static_cast<TrackPanelBus *>(_o);
        switch (_id) {
        case 0: _t->bus_changed(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData TrackPanelBus::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject TrackPanelBus::staticMetaObject = {
    { &ViewItem::staticMetaObject, qt_meta_stringdata_TrackPanelBus,
      qt_meta_data_TrackPanelBus, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &TrackPanelBus::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *TrackPanelBus::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *TrackPanelBus::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_TrackPanelBus))
        return static_cast<void*>(const_cast< TrackPanelBus*>(this));
    return ViewItem::qt_metacast(_clname);
}

int TrackPanelBus::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ViewItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}
static const uint qt_meta_data_TrackPanelView[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      16,   15,   15,   15, 0x08,
      30,   15,   15,   15, 0x08,
      43,   15,   15,   15, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_TrackPanelView[] = {
    "TrackPanelView\0\0update_gain()\0"
    "update_pan()\0update_track_name()\0"
};

void TrackPanelView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        TrackPanelView *_t = static_cast<TrackPanelView *>(_o);
        switch (_id) {
        case 0: _t->update_gain(); break;
        case 1: _t->update_pan(); break;
        case 2: _t->update_track_name(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData TrackPanelView::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject TrackPanelView::staticMetaObject = {
    { &ViewItem::staticMetaObject, qt_meta_stringdata_TrackPanelView,
      qt_meta_data_TrackPanelView, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &TrackPanelView::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *TrackPanelView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *TrackPanelView::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_TrackPanelView))
        return static_cast<void*>(const_cast< TrackPanelView*>(this));
    return ViewItem::qt_metacast(_clname);
}

int TrackPanelView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ViewItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
