/****************************************************************************
** Meta object code from reading C++ file 'SheetView.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/sheetcanvas/SheetView.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'SheetView.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_SheetView[] = {

 // content:
       6,       // revision
       0,       // classname
      17,   14, // classinfo
      30,   48, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // classinfo: key, value
      14,   10,
      20,   10,
      50,   38,
      63,   57,
      81,   76,
      96,   93,
     111,  106,
     131,  123,
     148,  139,
     166,  159,
     184,  175,
     203,  198,
     220,  198,
     248,  237,
     282,  259,
     319,  305,
     349,  342,

 // slots: signature, parameters, type, tag, flags
     366,  365,  365,  365, 0x0a,
     386,  365,  365,  365, 0x0a,
     406,  365,  365,  365, 0x0a,
     430,  365,  365,  365, 0x0a,
     455,  449,  365,  365, 0x0a,
     478,  365,  365,  365, 0x0a,
     512,  365,  503,  365, 0x0a,
     520,  365,  503,  365, 0x0a,
     540,  365,  503,  365, 0x0a,
     549,  365,  503,  365, 0x0a,
     564,  365,  503,  365, 0x0a,
     578,  365,  503,  365, 0x0a,
     590,  365,  503,  365, 0x0a,
     604,  365,  503,  365, 0x0a,
     614,  365,  503,  365, 0x0a,
     627,  365,  503,  365, 0x0a,
     638,  365,  503,  365, 0x0a,
     654,  365,  503,  365, 0x0a,
     673,  365,  503,  365, 0x0a,
     692,  365,  503,  365, 0x0a,
     705,  365,  503,  365, 0x0a,
     730,  365,  503,  365, 0x0a,
     755,  365,  503,  365, 0x0a,
     773,  365,  365,  365, 0x08,
     796,  365,  365,  365, 0x08,
     822,  365,  365,  365, 0x08,
     847,  365,  365,  365, 0x08,
     864,  365,  365,  365, 0x08,
     885,  365,  365,  365, 0x08,
     915,  365,  365,  365, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_SheetView[] = {
    "SheetView\0Set\0touch\0touch_play_cursor\0"
    "Center View\0center\0Right\0scroll_right\0"
    "Left\0scroll_left\0Up\0scroll_up\0Down\0"
    "scroll_down\0Shuttle\0shuttle\0To start\0"
    "goto_begin\0To end\0goto_end\0To Start\0"
    "play_to_begin\0Move\0play_cursor_move\0"
    "work_cursor_move\0Add Marker\0add_marker\0"
    "Add Marker at Playhead\0add_marker_at_playhead\0"
    "To workcursor\0playhead_to_workcursor\0"
    "Center\0center_playhead\0\0set_snap_range(int)\0"
    "update_scrollbars()\0stop_follow_play_head()\0"
    "follow_play_head()\0state\0"
    "set_follow_state(bool)\0transport_position_set()\0"
    "Command*\0touch()\0touch_play_cursor()\0"
    "center()\0scroll_right()\0scroll_left()\0"
    "scroll_up()\0scroll_down()\0shuttle()\0"
    "goto_begin()\0goto_end()\0play_to_begin()\0"
    "play_cursor_move()\0work_cursor_move()\0"
    "add_marker()\0add_marker_at_playhead()\0"
    "playhead_to_workcursor()\0center_playhead()\0"
    "scale_factor_changed()\0add_new_trackview(Track*)\0"
    "remove_trackview(Track*)\0update_shuttle()\0"
    "sheet_mode_changed()\0hscrollbar_value_changed(int)\0"
    "hscrollbar_action(int)\0"
};

void SheetView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        SheetView *_t = static_cast<SheetView *>(_o);
        switch (_id) {
        case 0: _t->set_snap_range((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->update_scrollbars(); break;
        case 2: _t->stop_follow_play_head(); break;
        case 3: _t->follow_play_head(); break;
        case 4: _t->set_follow_state((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 5: _t->transport_position_set(); break;
        case 6: { Command* _r = _t->touch();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 7: { Command* _r = _t->touch_play_cursor();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 8: { Command* _r = _t->center();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 9: { Command* _r = _t->scroll_right();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 10: { Command* _r = _t->scroll_left();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 11: { Command* _r = _t->scroll_up();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 12: { Command* _r = _t->scroll_down();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 13: { Command* _r = _t->shuttle();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 14: { Command* _r = _t->goto_begin();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 15: { Command* _r = _t->goto_end();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 16: { Command* _r = _t->play_to_begin();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 17: { Command* _r = _t->play_cursor_move();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 18: { Command* _r = _t->work_cursor_move();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 19: { Command* _r = _t->add_marker();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 20: { Command* _r = _t->add_marker_at_playhead();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 21: { Command* _r = _t->playhead_to_workcursor();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 22: { Command* _r = _t->center_playhead();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 23: _t->scale_factor_changed(); break;
        case 24: _t->add_new_trackview((*reinterpret_cast< Track*(*)>(_a[1]))); break;
        case 25: _t->remove_trackview((*reinterpret_cast< Track*(*)>(_a[1]))); break;
        case 26: _t->update_shuttle(); break;
        case 27: _t->sheet_mode_changed(); break;
        case 28: _t->hscrollbar_value_changed((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 29: _t->hscrollbar_action((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData SheetView::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject SheetView::staticMetaObject = {
    { &ViewItem::staticMetaObject, qt_meta_stringdata_SheetView,
      qt_meta_data_SheetView, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &SheetView::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *SheetView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *SheetView::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_SheetView))
        return static_cast<void*>(const_cast< SheetView*>(this));
    return ViewItem::qt_metacast(_clname);
}

int SheetView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ViewItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 30)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 30;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
