/****************************************************************************
** Meta object code from reading C++ file 'SheetWidget.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/sheetcanvas/SheetWidget.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'SheetWidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_SheetPanelGain[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      25,   15,   16,   15, 0x0a,
      42,   15,   16,   15, 0x0a,
      59,   15,   15,   15, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_SheetPanelGain[] = {
    "SheetPanelGain\0\0Command*\0gain_increment()\0"
    "gain_decrement()\0update_gain()\0"
};

void SheetPanelGain::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        SheetPanelGain *_t = static_cast<SheetPanelGain *>(_o);
        switch (_id) {
        case 0: { Command* _r = _t->gain_increment();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 1: { Command* _r = _t->gain_decrement();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 2: _t->update_gain(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData SheetPanelGain::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject SheetPanelGain::staticMetaObject = {
    { &ViewItem::staticMetaObject, qt_meta_stringdata_SheetPanelGain,
      qt_meta_data_SheetPanelGain, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &SheetPanelGain::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *SheetPanelGain::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *SheetPanelGain::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_SheetPanelGain))
        return static_cast<void*>(const_cast< SheetPanelGain*>(this));
    return ViewItem::qt_metacast(_clname);
}

int SheetPanelGain::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ViewItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
    return _id;
}
static const uint qt_meta_data_SheetPanelView[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_SheetPanelView[] = {
    "SheetPanelView\0"
};

void SheetPanelView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData SheetPanelView::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject SheetPanelView::staticMetaObject = {
    { &ViewItem::staticMetaObject, qt_meta_stringdata_SheetPanelView,
      qt_meta_data_SheetPanelView, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &SheetPanelView::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *SheetPanelView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *SheetPanelView::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_SheetPanelView))
        return static_cast<void*>(const_cast< SheetPanelView*>(this));
    return ViewItem::qt_metacast(_clname);
}

int SheetPanelView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ViewItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_SheetWidget[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      13,   12,   12,   12, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_SheetWidget[] = {
    "SheetWidget\0\0load_theme_data()\0"
};

void SheetWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        SheetWidget *_t = static_cast<SheetWidget *>(_o);
        switch (_id) {
        case 0: _t->load_theme_data(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData SheetWidget::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject SheetWidget::staticMetaObject = {
    { &QFrame::staticMetaObject, qt_meta_stringdata_SheetWidget,
      qt_meta_data_SheetWidget, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &SheetWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *SheetWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *SheetWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_SheetWidget))
        return static_cast<void*>(const_cast< SheetWidget*>(this));
    return QFrame::qt_metacast(_clname);
}

int SheetWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QFrame::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
