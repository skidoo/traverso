/****************************************************************************
** Meta object code from reading C++ file 'CurveNodeView.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/sheetcanvas/CurveNodeView.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'CurveNodeView.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_CurveNodeView[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      15,   14,   14,   14, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_CurveNodeView[] = {
    "CurveNodeView\0\0update_pos()\0"
};

void CurveNodeView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        CurveNodeView *_t = static_cast<CurveNodeView *>(_o);
        switch (_id) {
        case 0: _t->update_pos(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData CurveNodeView::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject CurveNodeView::staticMetaObject = {
    { &ViewItem::staticMetaObject, qt_meta_stringdata_CurveNodeView,
      qt_meta_data_CurveNodeView, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &CurveNodeView::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *CurveNodeView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *CurveNodeView::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_CurveNodeView))
        return static_cast<void*>(const_cast< CurveNodeView*>(this));
    if (!strcmp(_clname, "CurveNode"))
        return static_cast< CurveNode*>(const_cast< CurveNodeView*>(this));
    return ViewItem::qt_metacast(_clname);
}

int CurveNodeView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ViewItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
