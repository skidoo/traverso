/****************************************************************************
** Meta object code from reading C++ file 'AudioClipView.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/sheetcanvas/AudioClipView.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'AudioClipView.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_AudioClipView[] = {

 // content:
       6,       // revision
       0,       // classname
       8,   14, // classinfo
      19,   30, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // classinfo: key, value
      37,   14,
      66,   48,
      98,   79,
     130,  112,
     170,  151,
     208,  192,
     236,  219,
     267,  251,

 // slots: signature, parameters, type, tag, flags
     289,  284,  283,  283, 0x0a,
     318,  284,  283,  283, 0x0a,
     346,  283,  283,  283, 0x0a,
     356,  283,  283,  283, 0x0a,
     375,  283,  283,  283, 0x0a,
     403,  283,  394,  283, 0x0a,
     416,  283,  394,  283, 0x0a,
     431,  283,  394,  283, 0x0a,
     447,  283,  394,  283, 0x0a,
     470,  283,  394,  283, 0x0a,
     494,  283,  394,  283, 0x0a,
     507,  283,  394,  283, 0x0a,
     524,  283,  394,  283, 0x0a,
     551,  542,  283,  283, 0x08,
     577,  283,  283,  283, 0x08,
     602,  283,  283,  283, 0x08,
     620,  283,  283,  283, 0x08,
     639,  283,  283,  283, 0x08,
     658,  283,  283,  283, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_AudioClipView[] = {
    "AudioClipView\0Closest: Adjust Length\0"
    "fade_range\0In: Adjust Length\0clip_fade_in\0"
    "Out: Adjust Length\0clip_fade_out\0"
    "In: Select Preset\0select_fade_in_shape\0"
    "Out: Select Preset\0select_fade_out_shape\0"
    "Closest: Delete\0reset_fade\0Reset Audio File\0"
    "set_audio_file\0Edit Properties\0"
    "edit_properties\0\0fade\0"
    "add_new_fadeview(FadeCurve*)\0"
    "remove_fadeview(FadeCurve*)\0repaint()\0"
    "update_start_pos()\0position_changed()\0"
    "Command*\0fade_range()\0clip_fade_in()\0"
    "clip_fade_out()\0select_fade_in_shape()\0"
    "select_fade_out_shape()\0reset_fade()\0"
    "set_audio_file()\0edit_properties()\0"
    "progress\0update_progress_info(int)\0"
    "peak_creation_finished()\0start_recording()\0"
    "finish_recording()\0update_recording()\0"
    "clip_state_changed()\0"
};

void AudioClipView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        AudioClipView *_t = static_cast<AudioClipView *>(_o);
        switch (_id) {
        case 0: _t->add_new_fadeview((*reinterpret_cast< FadeCurve*(*)>(_a[1]))); break;
        case 1: _t->remove_fadeview((*reinterpret_cast< FadeCurve*(*)>(_a[1]))); break;
        case 2: _t->repaint(); break;
        case 3: _t->update_start_pos(); break;
        case 4: _t->position_changed(); break;
        case 5: { Command* _r = _t->fade_range();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 6: { Command* _r = _t->clip_fade_in();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 7: { Command* _r = _t->clip_fade_out();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 8: { Command* _r = _t->select_fade_in_shape();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 9: { Command* _r = _t->select_fade_out_shape();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 10: { Command* _r = _t->reset_fade();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 11: { Command* _r = _t->set_audio_file();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 12: { Command* _r = _t->edit_properties();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 13: _t->update_progress_info((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 14: _t->peak_creation_finished(); break;
        case 15: _t->start_recording(); break;
        case 16: _t->finish_recording(); break;
        case 17: _t->update_recording(); break;
        case 18: _t->clip_state_changed(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData AudioClipView::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject AudioClipView::staticMetaObject = {
    { &ViewItem::staticMetaObject, qt_meta_stringdata_AudioClipView,
      qt_meta_data_AudioClipView, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &AudioClipView::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *AudioClipView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *AudioClipView::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_AudioClipView))
        return static_cast<void*>(const_cast< AudioClipView*>(this));
    return ViewItem::qt_metacast(_clname);
}

int AudioClipView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ViewItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 19)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 19;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
