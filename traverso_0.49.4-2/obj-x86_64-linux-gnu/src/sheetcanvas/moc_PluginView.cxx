/****************************************************************************
** Meta object code from reading C++ file 'PluginView.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/sheetcanvas/PluginView.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'PluginView.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_PluginView[] = {

 // content:
       6,       // revision
       0,       // classname
       2,   14, // classinfo
       3,   18, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // classinfo: key, value
      19,   11,
      42,   35,

 // slots: signature, parameters, type, tag, flags
      66,   56,   57,   56, 0x0a,
      84,   56,   57,   56, 0x0a,
     100,   56,   56,   56, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_PluginView[] = {
    "PluginView\0Edit...\0edit_properties\0"
    "Remove\0remove_plugin\0\0Command*\0"
    "edit_properties()\0remove_plugin()\0"
    "repaint()\0"
};

void PluginView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        PluginView *_t = static_cast<PluginView *>(_o);
        switch (_id) {
        case 0: { Command* _r = _t->edit_properties();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 1: { Command* _r = _t->remove_plugin();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 2: _t->repaint(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData PluginView::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject PluginView::staticMetaObject = {
    { &ViewItem::staticMetaObject, qt_meta_stringdata_PluginView,
      qt_meta_data_PluginView, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &PluginView::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *PluginView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *PluginView::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_PluginView))
        return static_cast<void*>(const_cast< PluginView*>(this));
    return ViewItem::qt_metacast(_clname);
}

int PluginView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ViewItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
