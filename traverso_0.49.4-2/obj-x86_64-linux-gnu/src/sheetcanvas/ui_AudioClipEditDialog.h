/********************************************************************************
** Form generated from reading UI file 'AudioClipEditDialog.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_AUDIOCLIPEDITDIALOG_H
#define UI_AUDIOCLIPEDITDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QDoubleSpinBox>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QTabWidget>
#include <QtGui/QTimeEdit>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_AudioClipEditDialog
{
public:
    QVBoxLayout *vboxLayout;
    QTabWidget *tabWidget;
    QWidget *tab;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_14;
    QLabel *label;
    QLabel *label_4;
    QLabel *label_3;
    QLabel *label_2;
    QLabel *label_6;
    QVBoxLayout *verticalLayout;
    QLineEdit *sourceLineEdit;
    QLineEdit *clipNameLineEdit;
    QDoubleSpinBox *clipGainSpinBox;
    QTimeEdit *clipStartEdit;
    QTimeEdit *clipLengthEdit;
    QLabel *clipEndLineEdit;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *spacerItem;
    QPushButton *externalProcessingButton;
    QWidget *tab_2;
    QHBoxLayout *hboxLayout;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QTimeEdit *fadeInEdit;
    QDoubleSpinBox *fadeInStrengthBox;
    QDoubleSpinBox *fadeInBendingBox;
    QLabel *label_7;
    QLabel *label_10;
    QLabel *label_5;
    QComboBox *fadeInModeBox;
    QLabel *label_11;
    QPushButton *fadeInLinearButton;
    QPushButton *fadeInDefaultButton;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout1;
    QDoubleSpinBox *fadeOutStrengthBox;
    QLabel *label_12;
    QLabel *label_8;
    QComboBox *fadeOutModeBox;
    QLabel *label_9;
    QTimeEdit *fadeOutEdit;
    QLabel *label_13;
    QDoubleSpinBox *fadeOutBendingBox;
    QPushButton *fadeOutLinearButton;
    QPushButton *fadeOutDefaultButton;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *AudioClipEditDialog)
    {
        if (AudioClipEditDialog->objectName().isEmpty())
            AudioClipEditDialog->setObjectName(QString::fromUtf8("AudioClipEditDialog"));
        AudioClipEditDialog->resize(500, 285);
        vboxLayout = new QVBoxLayout(AudioClipEditDialog);
#ifndef Q_OS_MAC
        vboxLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        tabWidget = new QTabWidget(AudioClipEditDialog);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        verticalLayout_3 = new QVBoxLayout(tab);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        label_14 = new QLabel(tab);
        label_14->setObjectName(QString::fromUtf8("label_14"));
        label_14->setMinimumSize(QSize(100, 0));

        verticalLayout_2->addWidget(label_14);

        label = new QLabel(tab);
        label->setObjectName(QString::fromUtf8("label"));

        verticalLayout_2->addWidget(label);

        label_4 = new QLabel(tab);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        verticalLayout_2->addWidget(label_4);

        label_3 = new QLabel(tab);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        verticalLayout_2->addWidget(label_3);

        label_2 = new QLabel(tab);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        verticalLayout_2->addWidget(label_2);

        label_6 = new QLabel(tab);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        verticalLayout_2->addWidget(label_6);


        horizontalLayout->addLayout(verticalLayout_2);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        sourceLineEdit = new QLineEdit(tab);
        sourceLineEdit->setObjectName(QString::fromUtf8("sourceLineEdit"));
        sourceLineEdit->setReadOnly(false);

        verticalLayout->addWidget(sourceLineEdit);

        clipNameLineEdit = new QLineEdit(tab);
        clipNameLineEdit->setObjectName(QString::fromUtf8("clipNameLineEdit"));

        verticalLayout->addWidget(clipNameLineEdit);

        clipGainSpinBox = new QDoubleSpinBox(tab);
        clipGainSpinBox->setObjectName(QString::fromUtf8("clipGainSpinBox"));
        clipGainSpinBox->setMinimum(-120);
        clipGainSpinBox->setMaximum(30);
        clipGainSpinBox->setSingleStep(0.2);

        verticalLayout->addWidget(clipGainSpinBox);

        clipStartEdit = new QTimeEdit(tab);
        clipStartEdit->setObjectName(QString::fromUtf8("clipStartEdit"));

        verticalLayout->addWidget(clipStartEdit);

        clipLengthEdit = new QTimeEdit(tab);
        clipLengthEdit->setObjectName(QString::fromUtf8("clipLengthEdit"));

        verticalLayout->addWidget(clipLengthEdit);

        clipEndLineEdit = new QLabel(tab);
        clipEndLineEdit->setObjectName(QString::fromUtf8("clipEndLineEdit"));
        clipEndLineEdit->setFrameShape(QFrame::StyledPanel);
        clipEndLineEdit->setFrameShadow(QFrame::Sunken);

        verticalLayout->addWidget(clipEndLineEdit);


        horizontalLayout->addLayout(verticalLayout);


        verticalLayout_3->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        spacerItem = new QSpacerItem(261, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(spacerItem);

        externalProcessingButton = new QPushButton(tab);
        externalProcessingButton->setObjectName(QString::fromUtf8("externalProcessingButton"));

        horizontalLayout_2->addWidget(externalProcessingButton);


        verticalLayout_3->addLayout(horizontalLayout_2);

        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        hboxLayout = new QHBoxLayout(tab_2);
#ifndef Q_OS_MAC
        hboxLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout->setContentsMargins(9, 9, 9, 9);
#endif
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        groupBox = new QGroupBox(tab_2);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        gridLayout = new QGridLayout(groupBox);
#ifndef Q_OS_MAC
        gridLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        gridLayout->setContentsMargins(9, 9, 9, 9);
#endif
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        fadeInEdit = new QTimeEdit(groupBox);
        fadeInEdit->setObjectName(QString::fromUtf8("fadeInEdit"));
        fadeInEdit->setMaximumTime(QTime(22, 0, 0));

        gridLayout->addWidget(fadeInEdit, 0, 1, 1, 1);

        fadeInStrengthBox = new QDoubleSpinBox(groupBox);
        fadeInStrengthBox->setObjectName(QString::fromUtf8("fadeInStrengthBox"));
        fadeInStrengthBox->setMaximum(1);
        fadeInStrengthBox->setSingleStep(0.05);

        gridLayout->addWidget(fadeInStrengthBox, 3, 1, 1, 1);

        fadeInBendingBox = new QDoubleSpinBox(groupBox);
        fadeInBendingBox->setObjectName(QString::fromUtf8("fadeInBendingBox"));
        fadeInBendingBox->setMaximum(1);
        fadeInBendingBox->setSingleStep(0.05);

        gridLayout->addWidget(fadeInBendingBox, 2, 1, 1, 1);

        label_7 = new QLabel(groupBox);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        gridLayout->addWidget(label_7, 1, 0, 1, 1);

        label_10 = new QLabel(groupBox);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        gridLayout->addWidget(label_10, 2, 0, 1, 1);

        label_5 = new QLabel(groupBox);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        gridLayout->addWidget(label_5, 0, 0, 1, 1);

        fadeInModeBox = new QComboBox(groupBox);
        fadeInModeBox->setObjectName(QString::fromUtf8("fadeInModeBox"));

        gridLayout->addWidget(fadeInModeBox, 1, 1, 1, 1);

        label_11 = new QLabel(groupBox);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        gridLayout->addWidget(label_11, 3, 0, 1, 1);

        fadeInLinearButton = new QPushButton(groupBox);
        fadeInLinearButton->setObjectName(QString::fromUtf8("fadeInLinearButton"));

        gridLayout->addWidget(fadeInLinearButton, 4, 0, 1, 1);

        fadeInDefaultButton = new QPushButton(groupBox);
        fadeInDefaultButton->setObjectName(QString::fromUtf8("fadeInDefaultButton"));

        gridLayout->addWidget(fadeInDefaultButton, 4, 1, 1, 1);


        hboxLayout->addWidget(groupBox);

        groupBox_2 = new QGroupBox(tab_2);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        gridLayout1 = new QGridLayout(groupBox_2);
#ifndef Q_OS_MAC
        gridLayout1->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        gridLayout1->setContentsMargins(9, 9, 9, 9);
#endif
        gridLayout1->setObjectName(QString::fromUtf8("gridLayout1"));
        fadeOutStrengthBox = new QDoubleSpinBox(groupBox_2);
        fadeOutStrengthBox->setObjectName(QString::fromUtf8("fadeOutStrengthBox"));
        fadeOutStrengthBox->setMaximum(1);
        fadeOutStrengthBox->setSingleStep(0.05);

        gridLayout1->addWidget(fadeOutStrengthBox, 3, 1, 1, 1);

        label_12 = new QLabel(groupBox_2);
        label_12->setObjectName(QString::fromUtf8("label_12"));

        gridLayout1->addWidget(label_12, 2, 0, 1, 1);

        label_8 = new QLabel(groupBox_2);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        gridLayout1->addWidget(label_8, 1, 0, 1, 1);

        fadeOutModeBox = new QComboBox(groupBox_2);
        fadeOutModeBox->setObjectName(QString::fromUtf8("fadeOutModeBox"));

        gridLayout1->addWidget(fadeOutModeBox, 1, 1, 1, 1);

        label_9 = new QLabel(groupBox_2);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        gridLayout1->addWidget(label_9, 0, 0, 1, 1);

        fadeOutEdit = new QTimeEdit(groupBox_2);
        fadeOutEdit->setObjectName(QString::fromUtf8("fadeOutEdit"));

        gridLayout1->addWidget(fadeOutEdit, 0, 1, 1, 1);

        label_13 = new QLabel(groupBox_2);
        label_13->setObjectName(QString::fromUtf8("label_13"));

        gridLayout1->addWidget(label_13, 3, 0, 1, 1);

        fadeOutBendingBox = new QDoubleSpinBox(groupBox_2);
        fadeOutBendingBox->setObjectName(QString::fromUtf8("fadeOutBendingBox"));
        fadeOutBendingBox->setMaximum(1);
        fadeOutBendingBox->setSingleStep(0.05);

        gridLayout1->addWidget(fadeOutBendingBox, 2, 1, 1, 1);

        fadeOutLinearButton = new QPushButton(groupBox_2);
        fadeOutLinearButton->setObjectName(QString::fromUtf8("fadeOutLinearButton"));

        gridLayout1->addWidget(fadeOutLinearButton, 4, 0, 1, 1);

        fadeOutDefaultButton = new QPushButton(groupBox_2);
        fadeOutDefaultButton->setObjectName(QString::fromUtf8("fadeOutDefaultButton"));

        gridLayout1->addWidget(fadeOutDefaultButton, 4, 1, 1, 1);


        hboxLayout->addWidget(groupBox_2);

        tabWidget->addTab(tab_2, QString());

        vboxLayout->addWidget(tabWidget);

        buttonBox = new QDialogButtonBox(AudioClipEditDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        vboxLayout->addWidget(buttonBox);


        retranslateUi(AudioClipEditDialog);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(AudioClipEditDialog);
    } // setupUi

    void retranslateUi(QDialog *AudioClipEditDialog)
    {
        AudioClipEditDialog->setWindowTitle(QApplication::translate("AudioClipEditDialog", "Dialog", 0, QApplication::UnicodeUTF8));
        label_14->setText(QApplication::translate("AudioClipEditDialog", "Source", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("AudioClipEditDialog", "Name", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("AudioClipEditDialog", "Gain", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("AudioClipEditDialog", "Track start", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("AudioClipEditDialog", "Length", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("AudioClipEditDialog", "End", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        clipStartEdit->setToolTip(QApplication::translate("AudioClipEditDialog", "hh:mm:ss.sss", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        clipLengthEdit->setToolTip(QApplication::translate("AudioClipEditDialog", "hh:mm:ss.sss", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        clipEndLineEdit->setText(QApplication::translate("AudioClipEditDialog", "TextLabel", 0, QApplication::UnicodeUTF8));
        externalProcessingButton->setText(QApplication::translate("AudioClipEditDialog", "External Processing", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("AudioClipEditDialog", "Clip Parameters", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("AudioClipEditDialog", "Fade In", 0, QApplication::UnicodeUTF8));
        label_7->setText(QApplication::translate("AudioClipEditDialog", "Mode", 0, QApplication::UnicodeUTF8));
        label_10->setText(QApplication::translate("AudioClipEditDialog", "Bending", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("AudioClipEditDialog", "Length", 0, QApplication::UnicodeUTF8));
        label_11->setText(QApplication::translate("AudioClipEditDialog", "Strength", 0, QApplication::UnicodeUTF8));
        fadeInLinearButton->setText(QApplication::translate("AudioClipEditDialog", "&Linear", 0, QApplication::UnicodeUTF8));
        fadeInDefaultButton->setText(QApplication::translate("AudioClipEditDialog", "&Default", 0, QApplication::UnicodeUTF8));
        groupBox_2->setTitle(QApplication::translate("AudioClipEditDialog", "Fade Out", 0, QApplication::UnicodeUTF8));
        label_12->setText(QApplication::translate("AudioClipEditDialog", "Bending", 0, QApplication::UnicodeUTF8));
        label_8->setText(QApplication::translate("AudioClipEditDialog", "Mode", 0, QApplication::UnicodeUTF8));
        label_9->setText(QApplication::translate("AudioClipEditDialog", "Length", 0, QApplication::UnicodeUTF8));
        label_13->setText(QApplication::translate("AudioClipEditDialog", "Strength", 0, QApplication::UnicodeUTF8));
        fadeOutLinearButton->setText(QApplication::translate("AudioClipEditDialog", "&Linear", 0, QApplication::UnicodeUTF8));
        fadeOutDefaultButton->setText(QApplication::translate("AudioClipEditDialog", "&Default", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("AudioClipEditDialog", "Fades", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class AudioClipEditDialog: public Ui_AudioClipEditDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_AUDIOCLIPEDITDIALOG_H
