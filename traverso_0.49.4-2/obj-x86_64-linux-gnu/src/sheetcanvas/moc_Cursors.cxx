/****************************************************************************
** Meta object code from reading C++ file 'Cursors.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/sheetcanvas/Cursors.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Cursors.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_PlayHead[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      10,    9,    9,    9, 0x08,
      25,    9,    9,    9, 0x08,
      38,    9,    9,    9, 0x08,
      50,    9,    9,    9, 0x08,
      75,    9,    9,    9, 0x08,
      96,    9,    9,    9, 0x0a,
     114,    9,    9,    9, 0x0a,
     130,    9,    9,    9, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_PlayHead[] = {
    "PlayHead\0\0check_config()\0play_start()\0"
    "play_stop()\0set_animation_value(int)\0"
    "animation_finished()\0update_position()\0"
    "enable_follow()\0disable_follow()\0"
};

void PlayHead::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        PlayHead *_t = static_cast<PlayHead *>(_o);
        switch (_id) {
        case 0: _t->check_config(); break;
        case 1: _t->play_start(); break;
        case 2: _t->play_stop(); break;
        case 3: _t->set_animation_value((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->animation_finished(); break;
        case 5: _t->update_position(); break;
        case 6: _t->enable_follow(); break;
        case 7: _t->disable_follow(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData PlayHead::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject PlayHead::staticMetaObject = {
    { &ViewItem::staticMetaObject, qt_meta_stringdata_PlayHead,
      qt_meta_data_PlayHead, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &PlayHead::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *PlayHead::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *PlayHead::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_PlayHead))
        return static_cast<void*>(const_cast< PlayHead*>(this));
    return ViewItem::qt_metacast(_clname);
}

int PlayHead::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ViewItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    }
    return _id;
}
static const uint qt_meta_data_WorkCursor[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_WorkCursor[] = {
    "WorkCursor\0\0update_position()\0"
};

void WorkCursor::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        WorkCursor *_t = static_cast<WorkCursor *>(_o);
        switch (_id) {
        case 0: _t->update_position(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData WorkCursor::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject WorkCursor::staticMetaObject = {
    { &ViewItem::staticMetaObject, qt_meta_stringdata_WorkCursor,
      qt_meta_data_WorkCursor, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &WorkCursor::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *WorkCursor::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *WorkCursor::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_WorkCursor))
        return static_cast<void*>(const_cast< WorkCursor*>(this));
    return ViewItem::qt_metacast(_clname);
}

int WorkCursor::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ViewItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
