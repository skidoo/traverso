/****************************************************************************
** Meta object code from reading C++ file 'FadeView.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/sheetcanvas/FadeView.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'FadeView.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_FadeView[] = {

 // content:
       6,       // revision
       0,       // classname
       3,   14, // classinfo
       5,   20, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // classinfo: key, value
      21,    9,
      42,   26,
      65,   51,

 // signals: signature, parameters, type, tag, flags
      84,   83,   83,   83, 0x05,

 // slots: signature, parameters, type, tag, flags
      99,   83,   83,   83, 0x0a,
     124,   83,  115,   83, 0x0a,
     131,   83,  115,   83, 0x0a,
     142,   83,  115,   83, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_FadeView[] = {
    "FadeView\0Adjust Bend\0bend\0Adjust Strength\0"
    "strength\0Select Preset\0select_fade_shape\0"
    "\0fadeModified()\0state_changed()\0"
    "Command*\0bend()\0strength()\0"
    "select_fade_shape()\0"
};

void FadeView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        FadeView *_t = static_cast<FadeView *>(_o);
        switch (_id) {
        case 0: _t->fadeModified(); break;
        case 1: _t->state_changed(); break;
        case 2: { Command* _r = _t->bend();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 3: { Command* _r = _t->strength();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 4: { Command* _r = _t->select_fade_shape();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        default: ;
        }
    }
}

const QMetaObjectExtraData FadeView::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject FadeView::staticMetaObject = {
    { &ViewItem::staticMetaObject, qt_meta_stringdata_FadeView,
      qt_meta_data_FadeView, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &FadeView::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *FadeView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *FadeView::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_FadeView))
        return static_cast<void*>(const_cast< FadeView*>(this));
    return ViewItem::qt_metacast(_clname);
}

int FadeView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ViewItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
    return _id;
}

// SIGNAL 0
void FadeView::fadeModified()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
QT_END_MOC_NAMESPACE
