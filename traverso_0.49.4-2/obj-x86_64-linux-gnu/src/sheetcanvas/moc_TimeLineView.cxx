/****************************************************************************
** Meta object code from reading C++ file 'TimeLineView.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/sheetcanvas/TimeLineView.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TimeLineView.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_DragMarker[] = {

 // content:
       6,       // revision
       0,       // classname
       2,   14, // classinfo
       2,   18, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // classinfo: key, value
      21,   11,
      42,   31,

 // slots: signature, parameters, type, tag, flags
      65,   54,   53,   53, 0x0a,
      81,   54,   53,   53, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_DragMarker[] = {
    "DragMarker\0Move Left\0move_left\0"
    "Move right\0move_right\0\0autorepeat\0"
    "move_left(bool)\0move_right(bool)\0"
};

void DragMarker::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        DragMarker *_t = static_cast<DragMarker *>(_o);
        switch (_id) {
        case 0: _t->move_left((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->move_right((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData DragMarker::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject DragMarker::staticMetaObject = {
    { &Command::staticMetaObject, qt_meta_stringdata_DragMarker,
      qt_meta_data_DragMarker, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &DragMarker::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *DragMarker::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *DragMarker::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_DragMarker))
        return static_cast<void*>(const_cast< DragMarker*>(this));
    return Command::qt_metacast(_clname);
}

int DragMarker::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Command::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}
static const uint qt_meta_data_TimeLineView[] = {

 // content:
       6,       // revision
       0,       // classname
       6,   14, // classinfo
       9,   26, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // classinfo: key, value
      24,   13,
      58,   35,
      95,   81,
     121,  109,
     151,  133,
     184,  165,

 // slots: signature, parameters, type, tag, flags
     204,  203,  203,  203, 0x0a,
     229,  203,  220,  203, 0x0a,
     242,  203,  220,  203, 0x0a,
     267,  203,  220,  203, 0x0a,
     283,  203,  220,  203, 0x0a,
     297,  203,  220,  203, 0x0a,
     313,  203,  220,  203, 0x0a,
     341,  334,  203,  203, 0x08,
     370,  334,  203,  203, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_TimeLineView[] = {
    "TimeLineView\0Add Marker\0add_marker\0"
    "Add Marker at Playhead\0add_marker_at_playhead\0"
    "Remove Marker\0remove_marker\0Drag Marker\0"
    "drag_marker\0Clear all Markers\0"
    "clear_markers\0Playhead to Marker\0"
    "playhead_to_marker\0\0hzoom_changed()\0"
    "Command*\0add_marker()\0add_marker_at_playhead()\0"
    "remove_marker()\0drag_marker()\0"
    "clear_markers()\0playhead_to_marker()\0"
    "marker\0add_new_marker_view(Marker*)\0"
    "remove_marker_view(Marker*)\0"
};

void TimeLineView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        TimeLineView *_t = static_cast<TimeLineView *>(_o);
        switch (_id) {
        case 0: _t->hzoom_changed(); break;
        case 1: { Command* _r = _t->add_marker();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 2: { Command* _r = _t->add_marker_at_playhead();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 3: { Command* _r = _t->remove_marker();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 4: { Command* _r = _t->drag_marker();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 5: { Command* _r = _t->clear_markers();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 6: { Command* _r = _t->playhead_to_marker();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 7: _t->add_new_marker_view((*reinterpret_cast< Marker*(*)>(_a[1]))); break;
        case 8: _t->remove_marker_view((*reinterpret_cast< Marker*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData TimeLineView::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject TimeLineView::staticMetaObject = {
    { &ViewItem::staticMetaObject, qt_meta_stringdata_TimeLineView,
      qt_meta_data_TimeLineView, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &TimeLineView::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *TimeLineView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *TimeLineView::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_TimeLineView))
        return static_cast<void*>(const_cast< TimeLineView*>(this));
    return ViewItem::qt_metacast(_clname);
}

int TimeLineView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ViewItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
