/****************************************************************************
** Meta object code from reading C++ file 'PluginChainView.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/sheetcanvas/PluginChainView.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'PluginChainView.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_PluginChainView[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      24,   17,   16,   16, 0x0a,
      52,   17,   16,   16, 0x0a,
      85,   79,   16,   16, 0x0a,
     114,   16,   16,   16, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_PluginChainView[] = {
    "PluginChainView\0\0plugin\0"
    "add_new_pluginview(Plugin*)\0"
    "remove_pluginview(Plugin*)\0value\0"
    "scrollbar_value_changed(int)\0"
    "set_view_mode()\0"
};

void PluginChainView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        PluginChainView *_t = static_cast<PluginChainView *>(_o);
        switch (_id) {
        case 0: _t->add_new_pluginview((*reinterpret_cast< Plugin*(*)>(_a[1]))); break;
        case 1: _t->remove_pluginview((*reinterpret_cast< Plugin*(*)>(_a[1]))); break;
        case 2: _t->scrollbar_value_changed((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->set_view_mode(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData PluginChainView::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject PluginChainView::staticMetaObject = {
    { &ViewItem::staticMetaObject, qt_meta_stringdata_PluginChainView,
      qt_meta_data_PluginChainView, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &PluginChainView::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *PluginChainView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *PluginChainView::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_PluginChainView))
        return static_cast<void*>(const_cast< PluginChainView*>(this));
    return ViewItem::qt_metacast(_clname);
}

int PluginChainView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ViewItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
