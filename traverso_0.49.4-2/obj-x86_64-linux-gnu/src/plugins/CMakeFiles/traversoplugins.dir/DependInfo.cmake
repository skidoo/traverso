# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/tmp/traverso/src/plugins/LV2/LV2Plugin.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/CMakeFiles/traversoplugins.dir/LV2/LV2Plugin.o"
  "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/LV2/moc_LV2Plugin.cxx" "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/CMakeFiles/traversoplugins.dir/LV2/moc_LV2Plugin.o"
  "/tmp/traverso/src/plugins/Plugin.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/CMakeFiles/traversoplugins.dir/Plugin.o"
  "/tmp/traverso/src/plugins/PluginChain.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/CMakeFiles/traversoplugins.dir/PluginChain.o"
  "/tmp/traverso/src/plugins/PluginManager.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/CMakeFiles/traversoplugins.dir/PluginManager.o"
  "/tmp/traverso/src/plugins/PluginPropertiesDialog.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/CMakeFiles/traversoplugins.dir/PluginPropertiesDialog.o"
  "/tmp/traverso/src/plugins/PluginSlider.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/CMakeFiles/traversoplugins.dir/PluginSlider.o"
  "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/moc_Plugin.cxx" "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/CMakeFiles/traversoplugins.dir/moc_Plugin.o"
  "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/moc_PluginChain.cxx" "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/CMakeFiles/traversoplugins.dir/moc_PluginChain.o"
  "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/moc_PluginPropertiesDialog.cxx" "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/CMakeFiles/traversoplugins.dir/moc_PluginPropertiesDialog.o"
  "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/moc_PluginSlider.cxx" "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/CMakeFiles/traversoplugins.dir/moc_PluginSlider.o"
  "/tmp/traverso/src/plugins/native/CorrelationMeter.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/CMakeFiles/traversoplugins.dir/native/CorrelationMeter.o"
  "/tmp/traverso/src/plugins/native/GainEnvelope.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/CMakeFiles/traversoplugins.dir/native/GainEnvelope.o"
  "/tmp/traverso/src/plugins/native/SpectralMeter.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/CMakeFiles/traversoplugins.dir/native/SpectralMeter.o"
  "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/native/moc_CorrelationMeter.cxx" "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/CMakeFiles/traversoplugins.dir/native/moc_CorrelationMeter.o"
  "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/native/moc_GainEnvelope.cxx" "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/CMakeFiles/traversoplugins.dir/native/moc_GainEnvelope.o"
  "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/native/moc_SpectralMeter.cxx" "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/CMakeFiles/traversoplugins.dir/native/moc_SpectralMeter.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ALSA_SUPPORT"
  "HAVE_SYS_STAT_H"
  "HAVE_SYS_VFS_H"
  "JACK_SUPPORT"
  "LV2_SUPPORT"
  "MP3_DECODE_SUPPORT"
  "MP3_ENCODE_SUPPORT"
  "QT_CORE_LIB"
  "QT_GUI_LIB"
  "QT_NO_DEBUG"
  "SSE_OPTIMIZATIONS"
  "STATIC_BUILD"
  "USE_MLOCK"
  "USE_X86_64_ASM"
  "USE_XMMINTRIN"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "src/plugins"
  "../src/plugins"
  "/usr/include/lilv-0"
  "/usr/include/sratom-0"
  "/usr/include/sord-0"
  "/usr/include/serd-0"
  "../src/common"
  "../src/core"
  "../src/engine"
  "../src/commands"
  "../src/plugins/native"
  "../src/plugins/LV2"
  "/usr/include/qt4/QtXml"
  "/usr/include/qt4"
  "/usr/include/qt4/QtGui"
  "/usr/include/qt4/QtCore"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
