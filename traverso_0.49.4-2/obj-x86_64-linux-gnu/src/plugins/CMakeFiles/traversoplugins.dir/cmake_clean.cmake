file(REMOVE_RECURSE
  "moc_Plugin.cxx"
  "moc_PluginChain.cxx"
  "moc_PluginPropertiesDialog.cxx"
  "moc_PluginSlider.cxx"
  "native/moc_CorrelationMeter.cxx"
  "native/moc_GainEnvelope.cxx"
  "native/moc_SpectralMeter.cxx"
  "LV2/moc_LV2Plugin.cxx"
  "CMakeFiles/traversoplugins.dir/PluginChain.o"
  "CMakeFiles/traversoplugins.dir/Plugin.o"
  "CMakeFiles/traversoplugins.dir/PluginManager.o"
  "CMakeFiles/traversoplugins.dir/PluginSlider.o"
  "CMakeFiles/traversoplugins.dir/native/CorrelationMeter.o"
  "CMakeFiles/traversoplugins.dir/native/SpectralMeter.o"
  "CMakeFiles/traversoplugins.dir/native/GainEnvelope.o"
  "CMakeFiles/traversoplugins.dir/PluginPropertiesDialog.o"
  "CMakeFiles/traversoplugins.dir/LV2/LV2Plugin.o"
  "CMakeFiles/traversoplugins.dir/moc_Plugin.o"
  "CMakeFiles/traversoplugins.dir/moc_PluginChain.o"
  "CMakeFiles/traversoplugins.dir/moc_PluginPropertiesDialog.o"
  "CMakeFiles/traversoplugins.dir/moc_PluginSlider.o"
  "CMakeFiles/traversoplugins.dir/native/moc_CorrelationMeter.o"
  "CMakeFiles/traversoplugins.dir/native/moc_GainEnvelope.o"
  "CMakeFiles/traversoplugins.dir/native/moc_SpectralMeter.o"
  "CMakeFiles/traversoplugins.dir/LV2/moc_LV2Plugin.o"
  "../../lib/libtraversoplugins.pdb"
  "../../lib/libtraversoplugins.a"
)

# Per-language clean rules from dependency scanning.
foreach(lang CXX)
  include(CMakeFiles/traversoplugins.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
