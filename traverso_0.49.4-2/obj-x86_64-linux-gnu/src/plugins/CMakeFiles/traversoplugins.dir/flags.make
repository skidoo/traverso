# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.13

# compile CXX with /usr/bin/c++
CXX_FLAGS = -g -O2 -fdebug-prefix-map=/tmp/traverso=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -O2 -Wall -fPIC    -msse -mfpmath=sse  

CXX_DEFINES = -DALSA_SUPPORT -DHAVE_SYS_STAT_H -DHAVE_SYS_VFS_H -DJACK_SUPPORT -DLV2_SUPPORT -DMP3_DECODE_SUPPORT -DMP3_ENCODE_SUPPORT -DQT_CORE_LIB -DQT_GUI_LIB -DQT_NO_DEBUG -DSSE_OPTIMIZATIONS -DSTATIC_BUILD -DUSE_MLOCK -DUSE_X86_64_ASM -DUSE_XMMINTRIN

CXX_INCLUDES = -I/tmp/traverso/obj-x86_64-linux-gnu/src/plugins -I/tmp/traverso/src/plugins -I/usr/include/lilv-0 -I/usr/include/sratom-0 -I/usr/include/sord-0 -I/usr/include/serd-0 -I/tmp/traverso/src/common -I/tmp/traverso/src/core -I/tmp/traverso/src/engine -I/tmp/traverso/src/commands -I/tmp/traverso/src/plugins/native -I/tmp/traverso/src/plugins/LV2 -I/usr/include/qt4/QtXml -isystem /usr/include/qt4 -isystem /usr/include/qt4/QtGui -isystem /usr/include/qt4/QtCore 

