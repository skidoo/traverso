/****************************************************************************
** Meta object code from reading C++ file 'LV2Plugin.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../src/plugins/LV2/LV2Plugin.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'LV2Plugin.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_LV2Plugin[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      20,   10,   11,   10, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_LV2Plugin[] = {
    "LV2Plugin\0\0Command*\0toggle_bypass()\0"
};

void LV2Plugin::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        LV2Plugin *_t = static_cast<LV2Plugin *>(_o);
        switch (_id) {
        case 0: { Command* _r = _t->toggle_bypass();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        default: ;
        }
    }
}

const QMetaObjectExtraData LV2Plugin::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject LV2Plugin::staticMetaObject = {
    { &Plugin::staticMetaObject, qt_meta_stringdata_LV2Plugin,
      qt_meta_data_LV2Plugin, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &LV2Plugin::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *LV2Plugin::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *LV2Plugin::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_LV2Plugin))
        return static_cast<void*>(const_cast< LV2Plugin*>(this));
    return Plugin::qt_metacast(_clname);
}

int LV2Plugin::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Plugin::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
