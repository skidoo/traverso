/****************************************************************************
** Meta object code from reading C++ file 'DiskIO.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/core/DiskIO.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'DiskIO.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_DiskIO[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: signature, parameters, type, tag, flags
       8,    7,    7,    7, 0x05,
      23,    7,    7,    7, 0x05,
      50,    7,    7,    7, 0x05,

 // slots: signature, parameters, type, tag, flags
      77,    7,    7,    7, 0x0a,
      84,    7,    7,    7, 0x0a,
      95,    7,    7,    7, 0x0a,
     105,    7,    7,    7, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_DiskIO[] = {
    "DiskIO\0\0seekFinished()\0"
    "readSourceBufferUnderRun()\0"
    "writeSourceBufferOverRun()\0seek()\0"
    "start_io()\0stop_io()\0do_work()\0"
};

void DiskIO::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        DiskIO *_t = static_cast<DiskIO *>(_o);
        switch (_id) {
        case 0: _t->seekFinished(); break;
        case 1: _t->readSourceBufferUnderRun(); break;
        case 2: _t->writeSourceBufferOverRun(); break;
        case 3: _t->seek(); break;
        case 4: _t->start_io(); break;
        case 5: _t->stop_io(); break;
        case 6: _t->do_work(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData DiskIO::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject DiskIO::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_DiskIO,
      qt_meta_data_DiskIO, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &DiskIO::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *DiskIO::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *DiskIO::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_DiskIO))
        return static_cast<void*>(const_cast< DiskIO*>(this));
    return QObject::qt_metacast(_clname);
}

int DiskIO::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    }
    return _id;
}

// SIGNAL 0
void DiskIO::seekFinished()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void DiskIO::readSourceBufferUnderRun()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void DiskIO::writeSourceBufferOverRun()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}
QT_END_MOC_NAMESPACE
