/****************************************************************************
** Meta object code from reading C++ file 'Curve.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/core/Curve.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Curve.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Curve[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: signature, parameters, type, tag, flags
       7,    6,    6,    6, 0x05,
      22,    6,    6,    6, 0x05,
      44,    6,    6,    6, 0x05,
      68,    6,    6,    6, 0x05,

 // slots: signature, parameters, type, tag, flags
      90,    6,    6,    6, 0x09,
     109,  104,    6,    6, 0x08,
     138,  104,    6,    6, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_Curve[] = {
    "Curve\0\0stateChanged()\0nodeAdded(CurveNode*)\0"
    "nodeRemoved(CurveNode*)\0nodePositionChanged()\0"
    "set_changed()\0node\0private_add_node(CurveNode*)\0"
    "private_remove_node(CurveNode*)\0"
};

void Curve::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        Curve *_t = static_cast<Curve *>(_o);
        switch (_id) {
        case 0: _t->stateChanged(); break;
        case 1: _t->nodeAdded((*reinterpret_cast< CurveNode*(*)>(_a[1]))); break;
        case 2: _t->nodeRemoved((*reinterpret_cast< CurveNode*(*)>(_a[1]))); break;
        case 3: _t->nodePositionChanged(); break;
        case 4: _t->set_changed(); break;
        case 5: _t->private_add_node((*reinterpret_cast< CurveNode*(*)>(_a[1]))); break;
        case 6: _t->private_remove_node((*reinterpret_cast< CurveNode*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData Curve::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject Curve::staticMetaObject = {
    { &ContextItem::staticMetaObject, qt_meta_stringdata_Curve,
      qt_meta_data_Curve, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Curve::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Curve::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Curve::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Curve))
        return static_cast<void*>(const_cast< Curve*>(this));
    return ContextItem::qt_metacast(_clname);
}

int Curve::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ContextItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    }
    return _id;
}

// SIGNAL 0
void Curve::stateChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void Curve::nodeAdded(CurveNode * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Curve::nodeRemoved(CurveNode * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Curve::nodePositionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, 0);
}
QT_END_MOC_NAMESPACE
