/****************************************************************************
** Meta object code from reading C++ file 'FadeCurve.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/core/FadeCurve.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'FadeCurve.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_FadeCurve[] = {

 // content:
       6,       // revision
       0,       // classname
       4,   14, // classinfo
      10,   22, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // classinfo: key, value
      24,   10,
      50,   38,
      71,   59,
      91,   77,

 // signals: signature, parameters, type, tag, flags
     106,  105,  105,  105, 0x05,
     120,  105,  105,  105, 0x05,
     139,  105,  105,  105, 0x05,
     162,  105,  105,  105, 0x05,
     178,  105,  105,  105, 0x05,

 // slots: signature, parameters, type, tag, flags
     193,  105,  105,  105, 0x0a,
     225,  105,  216,  105, 0x0a,
     241,  105,  216,  105, 0x0a,
     252,  105,  216,  105, 0x0a,
     260,  105,  216,  105, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_FadeCurve[] = {
    "FadeCurve\0Toggle Bypass\0toggle_bypass\0"
    "Cycle Shape\0set_mode\0Remove Fade\0reset\0"
    "Toggle Raster\0toggle_raster\0\0modeChanged()\0"
    "bendValueChanged()\0strengthValueChanged()\0"
    "rasterChanged()\0rangeChanged()\0"
    "solve_node_positions()\0Command*\0"
    "toggle_bypass()\0set_mode()\0reset()\0"
    "toggle_raster()\0"
};

void FadeCurve::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        FadeCurve *_t = static_cast<FadeCurve *>(_o);
        switch (_id) {
        case 0: _t->modeChanged(); break;
        case 1: _t->bendValueChanged(); break;
        case 2: _t->strengthValueChanged(); break;
        case 3: _t->rasterChanged(); break;
        case 4: _t->rangeChanged(); break;
        case 5: _t->solve_node_positions(); break;
        case 6: { Command* _r = _t->toggle_bypass();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 7: { Command* _r = _t->set_mode();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 8: { Command* _r = _t->reset();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 9: { Command* _r = _t->toggle_raster();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        default: ;
        }
    }
}

const QMetaObjectExtraData FadeCurve::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject FadeCurve::staticMetaObject = {
    { &Curve::staticMetaObject, qt_meta_stringdata_FadeCurve,
      qt_meta_data_FadeCurve, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &FadeCurve::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *FadeCurve::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *FadeCurve::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_FadeCurve))
        return static_cast<void*>(const_cast< FadeCurve*>(this));
    if (!strcmp(_clname, "APILinkedListNode"))
        return static_cast< APILinkedListNode*>(const_cast< FadeCurve*>(this));
    return Curve::qt_metacast(_clname);
}

int FadeCurve::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Curve::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    }
    return _id;
}

// SIGNAL 0
void FadeCurve::modeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void FadeCurve::bendValueChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void FadeCurve::strengthValueChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}

// SIGNAL 3
void FadeCurve::rasterChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, 0);
}

// SIGNAL 4
void FadeCurve::rangeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, 0);
}
QT_END_MOC_NAMESPACE
