/****************************************************************************
** Meta object code from reading C++ file 'Sheet.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/core/Sheet.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Sheet.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Sheet[] = {

 // content:
       6,       // revision
       0,       // classname
      14,   14, // classinfo
      46,   42, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      18,       // signalCount

 // classinfo: key, value
      11,    6,
      34,   27,
      94,   69,
     138,  109,
     162,  157,
     172,  167,
     190,  177,
     215,  202,
     240,  227,
     264,  252,
     286,  275,
     315,  303,
     358,  332,
     394,  372,

 // signals: signature, parameters, type, tag, flags
     409,  408,  408,  408, 0x05,
     430,  408,  408,  408, 0x05,
     449,  408,  408,  408, 0x05,
     464,  408,  408,  408, 0x05,
     483,  408,  408,  408, 0x05,
     502,  408,  408,  408, 0x05,
     522,  408,  408,  408, 0x05,
     540,  408,  408,  408, 0x05,
     567,  408,  408,  408, 0x05,
     594,  408,  408,  408, 0x05,
     606,  408,  408,  408, 0x05,
     626,  620,  408,  408, 0x05,
     650,  408,  408,  408, 0x05,
     668,  408,  408,  408, 0x05,
     686,  408,  408,  408, 0x05,
     706,  408,  408,  408, 0x05,
     720,  408,  408,  408, 0x05,
     744,  408,  408,  408, 0x05,

 // slots: signature, parameters, type, tag, flags
     763,  408,  408,  408, 0x0a,
     779,  408,  408,  408, 0x0a,
     827,  408,  408,  408, 0x0a,
     849,  408,  408,  408, 0x0a,
     883,  878,  408,  408, 0x0a,
     908,  899,  408,  408, 0x0a,
     941,  408,  935,  408, 0x0a,
     952,  620,  408,  408, 0x0a,
     989,  408,  980,  408, 0x0a,
    1005,  408,  980,  408, 0x0a,
    1021,  408,  980,  408, 0x0a,
    1039,  408,  980,  408, 0x0a,
    1056,  408,  980,  408, 0x0a,
    1093,  408,  980,  408, 0x0a,
    1110,  408,  980,  408, 0x0a,
    1131,  408,  980,  408, 0x0a,
    1145,  408,  980,  408, 0x0a,
    1159,  408,  980,  408, 0x0a,
    1173,  408,  980,  408, 0x0a,
    1186,  408,  980,  408, 0x0a,
    1205,  408,  980,  408, 0x0a,
    1230, 1224,  408,  408, 0x08,
    1256, 1224,  408,  408, 0x08,
    1285,  408,  408,  408, 0x08,
    1321,  408,  408,  408, 0x08,
    1357,  408,  408,  408, 0x08,
    1382, 1377,  408,  408, 0x08,
    1418,  408,  408,  408, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_Sheet[] = {
    "Sheet\0Play\0start_transport\0Record\0"
    "set_recordable_and_start_transport\0"
    "Workcursor: To next edge\0work_next_edge\0"
    "Workcursor: To previous edge\0"
    "work_previous_edge\0Undo\0undo\0Redo\0"
    "redo\0Snap: On/Off\0toggle_snap\0"
    "Solo: On/Off\0toggle_solo\0Mute: On/Off\0"
    "toggle_mute\0Arm: On/Off\0toggle_arm\0"
    "Mode: Edit\0set_editing_mode\0Mode: Curve\0"
    "set_effects_mode\0To previous snap position\0"
    "prev_skip_pos\0To next snap position\0"
    "next_skip_pos\0\0trackRemoved(Track*)\0"
    "trackAdded(Track*)\0hzoomChanged()\0"
    "transportStarted()\0transportStopped()\0"
    "workingPosChanged()\0transportPosSet()\0"
    "firstVisibleFrameChanged()\0"
    "lastFramePositionChanged()\0seekStart()\0"
    "snapChanged()\0state\0tempFollowChanged(bool)\0"
    "propertyChanged()\0setCursorAtEdge()\0"
    "masterGainChanged()\0modeChanged()\0"
    "recordingStateChanged()\0prepareRecording()\0"
    "seek_finished()\0"
    "audiodevice_client_removed(TAudioDeviceClient*)\0"
    "audiodevice_started()\0"
    "audiodevice_params_changed()\0gain\0"
    "set_gain(float)\0location\0"
    "set_transport_pos(TimeRef)\0float\0"
    "get_gain()\0set_temp_follow_state(bool)\0"
    "Command*\0next_skip_pos()\0prev_skip_pos()\0"
    "start_transport()\0set_recordable()\0"
    "set_recordable_and_start_transport()\0"
    "work_next_edge()\0work_previous_edge()\0"
    "toggle_snap()\0toggle_solo()\0toggle_mute()\0"
    "toggle_arm()\0set_editing_mode()\0"
    "set_effects_mode()\0track\0"
    "private_add_track(Track*)\0"
    "private_remove_track(Track*)\0"
    "handle_diskio_writebuffer_overrun()\0"
    "handle_diskio_readbuffer_underrun()\0"
    "prepare_recording()\0clip\0"
    "clip_finished_recording(AudioClip*)\0"
    "config_changed()\0"
};

void Sheet::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        Sheet *_t = static_cast<Sheet *>(_o);
        switch (_id) {
        case 0: _t->trackRemoved((*reinterpret_cast< Track*(*)>(_a[1]))); break;
        case 1: _t->trackAdded((*reinterpret_cast< Track*(*)>(_a[1]))); break;
        case 2: _t->hzoomChanged(); break;
        case 3: _t->transportStarted(); break;
        case 4: _t->transportStopped(); break;
        case 5: _t->workingPosChanged(); break;
        case 6: _t->transportPosSet(); break;
        case 7: _t->firstVisibleFrameChanged(); break;
        case 8: _t->lastFramePositionChanged(); break;
        case 9: _t->seekStart(); break;
        case 10: _t->snapChanged(); break;
        case 11: _t->tempFollowChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 12: _t->propertyChanged(); break;
        case 13: _t->setCursorAtEdge(); break;
        case 14: _t->masterGainChanged(); break;
        case 15: _t->modeChanged(); break;
        case 16: _t->recordingStateChanged(); break;
        case 17: _t->prepareRecording(); break;
        case 18: _t->seek_finished(); break;
        case 19: _t->audiodevice_client_removed((*reinterpret_cast< TAudioDeviceClient*(*)>(_a[1]))); break;
        case 20: _t->audiodevice_started(); break;
        case 21: _t->audiodevice_params_changed(); break;
        case 22: _t->set_gain((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 23: _t->set_transport_pos((*reinterpret_cast< TimeRef(*)>(_a[1]))); break;
        case 24: { float _r = _t->get_gain();
            if (_a[0]) *reinterpret_cast< float*>(_a[0]) = _r; }  break;
        case 25: _t->set_temp_follow_state((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 26: { Command* _r = _t->next_skip_pos();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 27: { Command* _r = _t->prev_skip_pos();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 28: { Command* _r = _t->start_transport();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 29: { Command* _r = _t->set_recordable();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 30: { Command* _r = _t->set_recordable_and_start_transport();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 31: { Command* _r = _t->work_next_edge();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 32: { Command* _r = _t->work_previous_edge();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 33: { Command* _r = _t->toggle_snap();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 34: { Command* _r = _t->toggle_solo();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 35: { Command* _r = _t->toggle_mute();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 36: { Command* _r = _t->toggle_arm();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 37: { Command* _r = _t->set_editing_mode();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 38: { Command* _r = _t->set_effects_mode();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 39: _t->private_add_track((*reinterpret_cast< Track*(*)>(_a[1]))); break;
        case 40: _t->private_remove_track((*reinterpret_cast< Track*(*)>(_a[1]))); break;
        case 41: _t->handle_diskio_writebuffer_overrun(); break;
        case 42: _t->handle_diskio_readbuffer_underrun(); break;
        case 43: _t->prepare_recording(); break;
        case 44: _t->clip_finished_recording((*reinterpret_cast< AudioClip*(*)>(_a[1]))); break;
        case 45: _t->config_changed(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData Sheet::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject Sheet::staticMetaObject = {
    { &ContextItem::staticMetaObject, qt_meta_stringdata_Sheet,
      qt_meta_data_Sheet, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Sheet::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Sheet::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Sheet::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Sheet))
        return static_cast<void*>(const_cast< Sheet*>(this));
    if (!strcmp(_clname, "AudioProcessingItem"))
        return static_cast< AudioProcessingItem*>(const_cast< Sheet*>(this));
    return ContextItem::qt_metacast(_clname);
}

int Sheet::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ContextItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 46)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 46;
    }
    return _id;
}

// SIGNAL 0
void Sheet::trackRemoved(Track * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Sheet::trackAdded(Track * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Sheet::hzoomChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}

// SIGNAL 3
void Sheet::transportStarted()
{
    QMetaObject::activate(this, &staticMetaObject, 3, 0);
}

// SIGNAL 4
void Sheet::transportStopped()
{
    QMetaObject::activate(this, &staticMetaObject, 4, 0);
}

// SIGNAL 5
void Sheet::workingPosChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, 0);
}

// SIGNAL 6
void Sheet::transportPosSet()
{
    QMetaObject::activate(this, &staticMetaObject, 6, 0);
}

// SIGNAL 7
void Sheet::firstVisibleFrameChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, 0);
}

// SIGNAL 8
void Sheet::lastFramePositionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 8, 0);
}

// SIGNAL 9
void Sheet::seekStart()
{
    QMetaObject::activate(this, &staticMetaObject, 9, 0);
}

// SIGNAL 10
void Sheet::snapChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 10, 0);
}

// SIGNAL 11
void Sheet::tempFollowChanged(bool _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 11, _a);
}

// SIGNAL 12
void Sheet::propertyChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 12, 0);
}

// SIGNAL 13
void Sheet::setCursorAtEdge()
{
    QMetaObject::activate(this, &staticMetaObject, 13, 0);
}

// SIGNAL 14
void Sheet::masterGainChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 14, 0);
}

// SIGNAL 15
void Sheet::modeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 15, 0);
}

// SIGNAL 16
void Sheet::recordingStateChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 16, 0);
}

// SIGNAL 17
void Sheet::prepareRecording()
{
    QMetaObject::activate(this, &staticMetaObject, 17, 0);
}
QT_END_MOC_NAMESPACE
