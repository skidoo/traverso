/****************************************************************************
** Meta object code from reading C++ file 'ProjectConverter.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/core/ProjectConverter.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'ProjectConverter.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ProjectConverter[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       6,       // signalCount

 // signals: signature, parameters, type, tag, flags
      18,   17,   17,   17, 0x05,
      32,   17,   17,   17, 0x05,
      65,   60,   17,   17, 0x05,
      91,   60,   17,   17, 0x05,
     118,   17,   17,   17, 0x05,
     139,   17,   17,   17, 0x05,

 // slots: signature, parameters, type, tag, flags
     156,   17,   17,   17, 0x08,
     178,   60,   17,   17, 0x08,
     206,   60,   17,   17, 0x08,
     235,   17,   17,   17, 0x08,
     259,   17,   17,   17, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_ProjectConverter[] = {
    "ProjectConverter\0\0progress(int)\0"
    "actionProgressInfo(QString)\0file\0"
    "fileMergeStarted(QString)\0"
    "fileMergeFinished(QString)\0"
    "conversionFinished()\0message(QString)\0"
    "conversion_finished()\0file_merge_started(QString)\0"
    "file_merge_finished(QString)\0"
    "finish_2_3_conversion()\0processing_stopped()\0"
};

void ProjectConverter::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ProjectConverter *_t = static_cast<ProjectConverter *>(_o);
        switch (_id) {
        case 0: _t->progress((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->actionProgressInfo((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->fileMergeStarted((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 3: _t->fileMergeFinished((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 4: _t->conversionFinished(); break;
        case 5: _t->message((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 6: _t->conversion_finished(); break;
        case 7: _t->file_merge_started((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 8: _t->file_merge_finished((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 9: _t->finish_2_3_conversion(); break;
        case 10: _t->processing_stopped(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ProjectConverter::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ProjectConverter::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_ProjectConverter,
      qt_meta_data_ProjectConverter, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ProjectConverter::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ProjectConverter::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ProjectConverter::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ProjectConverter))
        return static_cast<void*>(const_cast< ProjectConverter*>(this));
    return QObject::qt_metacast(_clname);
}

int ProjectConverter::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    }
    return _id;
}

// SIGNAL 0
void ProjectConverter::progress(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void ProjectConverter::actionProgressInfo(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void ProjectConverter::fileMergeStarted(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void ProjectConverter::fileMergeFinished(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void ProjectConverter::conversionFinished()
{
    QMetaObject::activate(this, &staticMetaObject, 4, 0);
}

// SIGNAL 5
void ProjectConverter::message(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}
QT_END_MOC_NAMESPACE
