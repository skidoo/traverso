/****************************************************************************
** Meta object code from reading C++ file 'AudioClipManager.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/core/AudioClipManager.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'AudioClipManager.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_AudioClipManager[] = {

 // content:
       6,       // revision
       0,       // classname
       2,   14, // classinfo
       9,   18, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // classinfo: key, value
      28,   17,
      52,   45,

 // slots: signature, parameters, type, tag, flags
      80,   75,   74,   74, 0x0a,
     101,   75,   74,   74, 0x0a,
     125,   75,   74,   74, 0x0a,
     149,   75,   74,   74, 0x0a,
     177,   75,   74,   74, 0x0a,
     211,   75,   74,   74, 0x0a,
     240,   74,   74,   74, 0x0a,
     269,   74,  260,   74, 0x0a,
     288,   74,  260,   74, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_AudioClipManager[] = {
    "AudioClipManager\0Select all\0"
    "select_all_clips\0Invert\0invert_clip_selection\0"
    "\0clip\0add_clip(AudioClip*)\0"
    "remove_clip(AudioClip*)\0select_clip(AudioClip*)\0"
    "toggle_selected(AudioClip*)\0"
    "remove_from_selection(AudioClip*)\0"
    "add_to_selection(AudioClip*)\0"
    "update_last_frame()\0Command*\0"
    "select_all_clips()\0invert_clip_selection()\0"
};

void AudioClipManager::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        AudioClipManager *_t = static_cast<AudioClipManager *>(_o);
        switch (_id) {
        case 0: _t->add_clip((*reinterpret_cast< AudioClip*(*)>(_a[1]))); break;
        case 1: _t->remove_clip((*reinterpret_cast< AudioClip*(*)>(_a[1]))); break;
        case 2: _t->select_clip((*reinterpret_cast< AudioClip*(*)>(_a[1]))); break;
        case 3: _t->toggle_selected((*reinterpret_cast< AudioClip*(*)>(_a[1]))); break;
        case 4: _t->remove_from_selection((*reinterpret_cast< AudioClip*(*)>(_a[1]))); break;
        case 5: _t->add_to_selection((*reinterpret_cast< AudioClip*(*)>(_a[1]))); break;
        case 6: _t->update_last_frame(); break;
        case 7: { Command* _r = _t->select_all_clips();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 8: { Command* _r = _t->invert_clip_selection();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        default: ;
        }
    }
}

const QMetaObjectExtraData AudioClipManager::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject AudioClipManager::staticMetaObject = {
    { &ContextItem::staticMetaObject, qt_meta_stringdata_AudioClipManager,
      qt_meta_data_AudioClipManager, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &AudioClipManager::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *AudioClipManager::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *AudioClipManager::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_AudioClipManager))
        return static_cast<void*>(const_cast< AudioClipManager*>(this));
    return ContextItem::qt_metacast(_clname);
}

int AudioClipManager::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ContextItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
