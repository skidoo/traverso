/****************************************************************************
** Meta object code from reading C++ file 'Project.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/core/Project.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Project.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Project[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       8,       // signalCount

 // signals: signature, parameters, type, tag, flags
       9,    8,    8,    8, 0x05,
      37,    8,    8,    8, 0x05,
      56,    8,    8,    8, 0x05,
      77,    8,    8,    8, 0x05,
     109,    8,    8,    8, 0x05,
     143,    8,    8,    8, 0x05,
     160,    8,    8,    8, 0x05,
     190,    8,    8,    8, 0x05,

 // slots: signature, parameters, type, tag, flags
     221,    8,  212,    8, 0x0a,
     236,  230,    8,    8, 0x08,
     262,  230,    8,    8, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_Project[] = {
    "Project\0\0currentSheetChanged(Sheet*)\0"
    "sheetAdded(Sheet*)\0sheetRemoved(Sheet*)\0"
    "sheetExportProgressChanged(int)\0"
    "overallExportProgressChanged(int)\0"
    "exportFinished()\0exportStartedForSheet(Sheet*)\0"
    "projectLoadFinished()\0Command*\0select()\0"
    "sheet\0private_add_sheet(Sheet*)\0"
    "private_remove_sheet(Sheet*)\0"
};

void Project::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        Project *_t = static_cast<Project *>(_o);
        switch (_id) {
        case 0: _t->currentSheetChanged((*reinterpret_cast< Sheet*(*)>(_a[1]))); break;
        case 1: _t->sheetAdded((*reinterpret_cast< Sheet*(*)>(_a[1]))); break;
        case 2: _t->sheetRemoved((*reinterpret_cast< Sheet*(*)>(_a[1]))); break;
        case 3: _t->sheetExportProgressChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->overallExportProgressChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->exportFinished(); break;
        case 6: _t->exportStartedForSheet((*reinterpret_cast< Sheet*(*)>(_a[1]))); break;
        case 7: _t->projectLoadFinished(); break;
        case 8: { Command* _r = _t->select();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 9: _t->private_add_sheet((*reinterpret_cast< Sheet*(*)>(_a[1]))); break;
        case 10: _t->private_remove_sheet((*reinterpret_cast< Sheet*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData Project::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject Project::staticMetaObject = {
    { &ContextItem::staticMetaObject, qt_meta_stringdata_Project,
      qt_meta_data_Project, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Project::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Project::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Project::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Project))
        return static_cast<void*>(const_cast< Project*>(this));
    return ContextItem::qt_metacast(_clname);
}

int Project::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ContextItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    }
    return _id;
}

// SIGNAL 0
void Project::currentSheetChanged(Sheet * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Project::sheetAdded(Sheet * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Project::sheetRemoved(Sheet * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Project::sheetExportProgressChanged(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void Project::overallExportProgressChanged(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void Project::exportFinished()
{
    QMetaObject::activate(this, &staticMetaObject, 5, 0);
}

// SIGNAL 6
void Project::exportStartedForSheet(Sheet * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void Project::projectLoadFinished()
{
    QMetaObject::activate(this, &staticMetaObject, 7, 0);
}
QT_END_MOC_NAMESPACE
