/****************************************************************************
** Meta object code from reading C++ file 'TimeLine.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/core/TimeLine.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TimeLine.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_TimeLine[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: signature, parameters, type, tag, flags
      10,    9,    9,    9, 0x05,
      31,    9,    9,    9, 0x05,
      54,    9,    9,    9, 0x05,

 // slots: signature, parameters, type, tag, flags
      85,   78,    9,    9, 0x08,
     113,   78,    9,    9, 0x08,
     144,    9,    9,    9, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_TimeLine[] = {
    "TimeLine\0\0markerAdded(Marker*)\0"
    "markerRemoved(Marker*)\0markerPositionChanged()\0"
    "marker\0private_add_marker(Marker*)\0"
    "private_remove_marker(Marker*)\0"
    "marker_position_changed()\0"
};

void TimeLine::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        TimeLine *_t = static_cast<TimeLine *>(_o);
        switch (_id) {
        case 0: _t->markerAdded((*reinterpret_cast< Marker*(*)>(_a[1]))); break;
        case 1: _t->markerRemoved((*reinterpret_cast< Marker*(*)>(_a[1]))); break;
        case 2: _t->markerPositionChanged(); break;
        case 3: _t->private_add_marker((*reinterpret_cast< Marker*(*)>(_a[1]))); break;
        case 4: _t->private_remove_marker((*reinterpret_cast< Marker*(*)>(_a[1]))); break;
        case 5: _t->marker_position_changed(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData TimeLine::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject TimeLine::staticMetaObject = {
    { &ContextItem::staticMetaObject, qt_meta_stringdata_TimeLine,
      qt_meta_data_TimeLine, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &TimeLine::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *TimeLine::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *TimeLine::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_TimeLine))
        return static_cast<void*>(const_cast< TimeLine*>(this));
    return ContextItem::qt_metacast(_clname);
}

int TimeLine::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ContextItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    }
    return _id;
}

// SIGNAL 0
void TimeLine::markerAdded(Marker * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void TimeLine::markerRemoved(Marker * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void TimeLine::markerPositionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}
QT_END_MOC_NAMESPACE
