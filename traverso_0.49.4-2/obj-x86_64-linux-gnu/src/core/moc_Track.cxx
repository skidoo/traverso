/****************************************************************************
** Meta object code from reading C++ file 'Track.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/core/Track.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Track.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Track[] = {

 // content:
       6,       // revision
       0,       // classname
       4,   14, // classinfo
      22,   22, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      13,       // signalCount

 // classinfo: key, value
      11,    6,
      31,   16,
      47,   42,
      73,   52,

 // signals: signature, parameters, type, tag, flags
      94,   89,   88,   88, 0x05,
     121,   89,   88,   88, 0x05,
     150,   88,   88,   88, 0x05,
     174,  166,   88,   88, 0x05,
     199,  192,   88,   88, 0x05,
     225,  217,   88,   88, 0x05,
     253,  244,   88,   88, 0x05,
     271,   88,   88,   88, 0x05,
     285,   88,   88,   88, 0x05,
     298,   88,   88,   88, 0x05,
     313,   88,   88,   88, 0x05,
     335,   88,   88,   88, 0x05,
     350,   88,   88,   88, 0x05,

 // slots: signature, parameters, type, tag, flags
     371,  366,   88,   88, 0x0a,
     387,   89,   88,   88, 0x0a,
     427,   88,  421,   88, 0x0a,
     447,   88,  438,   88, 0x0a,
     454,   88,  438,   88, 0x0a,
     467,   88,  438,   88, 0x0a,
     474,   88,  438,   88, 0x0a,
     491,   89,   88,   88, 0x08,
     520,   89,   88,   88, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_Track[] = {
    "Track\0Mute\0mute\0Record: On/Off\0"
    "toggle_arm\0Solo\0solo\0Silence other tracks\0"
    "silence_others\0\0clip\0audioClipAdded(AudioClip*)\0"
    "audioClipRemoved(AudioClip*)\0"
    "heightChanged()\0isMuted\0muteChanged(bool)\0"
    "isSolo\0soloChanged(bool)\0isArmed\0"
    "armedChanged(bool)\0isLocked\0"
    "lockChanged(bool)\0gainChanged()\0"
    "panChanged()\0stateChanged()\0"
    "audibleStateChanged()\0inBusChanged()\0"
    "outBusChanged()\0gain\0set_gain(float)\0"
    "clip_position_changed(AudioClip*)\0"
    "float\0get_gain()\0Command*\0mute()\0"
    "toggle_arm()\0solo()\0silence_others()\0"
    "private_add_clip(AudioClip*)\0"
    "private_remove_clip(AudioClip*)\0"
};

void Track::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        Track *_t = static_cast<Track *>(_o);
        switch (_id) {
        case 0: _t->audioClipAdded((*reinterpret_cast< AudioClip*(*)>(_a[1]))); break;
        case 1: _t->audioClipRemoved((*reinterpret_cast< AudioClip*(*)>(_a[1]))); break;
        case 2: _t->heightChanged(); break;
        case 3: _t->muteChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 4: _t->soloChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 5: _t->armedChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 6: _t->lockChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 7: _t->gainChanged(); break;
        case 8: _t->panChanged(); break;
        case 9: _t->stateChanged(); break;
        case 10: _t->audibleStateChanged(); break;
        case 11: _t->inBusChanged(); break;
        case 12: _t->outBusChanged(); break;
        case 13: _t->set_gain((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 14: _t->clip_position_changed((*reinterpret_cast< AudioClip*(*)>(_a[1]))); break;
        case 15: { float _r = _t->get_gain();
            if (_a[0]) *reinterpret_cast< float*>(_a[0]) = _r; }  break;
        case 16: { Command* _r = _t->mute();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 17: { Command* _r = _t->toggle_arm();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 18: { Command* _r = _t->solo();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 19: { Command* _r = _t->silence_others();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 20: _t->private_add_clip((*reinterpret_cast< AudioClip*(*)>(_a[1]))); break;
        case 21: _t->private_remove_clip((*reinterpret_cast< AudioClip*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData Track::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject Track::staticMetaObject = {
    { &ContextItem::staticMetaObject, qt_meta_stringdata_Track,
      qt_meta_data_Track, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Track::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Track::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Track::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Track))
        return static_cast<void*>(const_cast< Track*>(this));
    if (!strcmp(_clname, "AudioProcessingItem"))
        return static_cast< AudioProcessingItem*>(const_cast< Track*>(this));
    return ContextItem::qt_metacast(_clname);
}

int Track::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ContextItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 22)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 22;
    }
    return _id;
}

// SIGNAL 0
void Track::audioClipAdded(AudioClip * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Track::audioClipRemoved(AudioClip * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Track::heightChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}

// SIGNAL 3
void Track::muteChanged(bool _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void Track::soloChanged(bool _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void Track::armedChanged(bool _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void Track::lockChanged(bool _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void Track::gainChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, 0);
}

// SIGNAL 8
void Track::panChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 8, 0);
}

// SIGNAL 9
void Track::stateChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 9, 0);
}

// SIGNAL 10
void Track::audibleStateChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 10, 0);
}

// SIGNAL 11
void Track::inBusChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 11, 0);
}

// SIGNAL 12
void Track::outBusChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 12, 0);
}
QT_END_MOC_NAMESPACE
