/****************************************************************************
** Meta object code from reading C++ file 'ProjectManager.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/core/ProjectManager.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'ProjectManager.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ProjectManager[] = {

 // content:
       6,       // revision
       0,       // classname
       2,   14, // classinfo
      12,   18, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       7,       // signalCount

 // classinfo: key, value
      28,   15,
      58,   41,

 // signals: signature, parameters, type, tag, flags
      64,   63,   63,   63, 0x05,
      88,   63,   63,   63, 0x05,
     110,   63,   63,   63, 0x05,
     137,   63,   63,   63, 0x05,
     175,   63,   63,   63, 0x05,
     204,  202,   63,   63, 0x05,
     239,  202,   63,   63, 0x05,

 // slots: signature, parameters, type, tag, flags
     292,   63,  283,   63, 0x0a,
     307,   63,  283,   63, 0x0a,
     314,   63,  283,   63, 0x0a,
     321,   63,  283,   63, 0x0a,
     336,  328,   63,   63, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_ProjectManager[] = {
    "ProjectManager\0Save Project\0save_project\0"
    "Exit application\0exit\0\0projectLoaded(Project*)\0"
    "aboutToDelete(Sheet*)\0currentProjectDirChanged()\0"
    "unsupportedProjectDirChangeDetected()\0"
    "projectDirChangeDetected()\0,\0"
    "projectLoadFailed(QString,QString)\0"
    "projectFileVersionMismatch(QString,QString)\0"
    "Command*\0save_project()\0exit()\0undo()\0"
    "redo()\0dirname\0project_dir_rename_detected(QString)\0"
};

void ProjectManager::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ProjectManager *_t = static_cast<ProjectManager *>(_o);
        switch (_id) {
        case 0: _t->projectLoaded((*reinterpret_cast< Project*(*)>(_a[1]))); break;
        case 1: _t->aboutToDelete((*reinterpret_cast< Sheet*(*)>(_a[1]))); break;
        case 2: _t->currentProjectDirChanged(); break;
        case 3: _t->unsupportedProjectDirChangeDetected(); break;
        case 4: _t->projectDirChangeDetected(); break;
        case 5: _t->projectLoadFailed((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 6: _t->projectFileVersionMismatch((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 7: { Command* _r = _t->save_project();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 8: { Command* _r = _t->exit();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 9: { Command* _r = _t->undo();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 10: { Command* _r = _t->redo();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 11: _t->project_dir_rename_detected((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ProjectManager::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ProjectManager::staticMetaObject = {
    { &ContextItem::staticMetaObject, qt_meta_stringdata_ProjectManager,
      qt_meta_data_ProjectManager, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ProjectManager::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ProjectManager::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ProjectManager::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ProjectManager))
        return static_cast<void*>(const_cast< ProjectManager*>(this));
    return ContextItem::qt_metacast(_clname);
}

int ProjectManager::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ContextItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 12)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 12;
    }
    return _id;
}

// SIGNAL 0
void ProjectManager::projectLoaded(Project * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void ProjectManager::aboutToDelete(Sheet * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void ProjectManager::currentProjectDirChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}

// SIGNAL 3
void ProjectManager::unsupportedProjectDirChangeDetected()
{
    QMetaObject::activate(this, &staticMetaObject, 3, 0);
}

// SIGNAL 4
void ProjectManager::projectDirChangeDetected()
{
    QMetaObject::activate(this, &staticMetaObject, 4, 0);
}

// SIGNAL 5
void ProjectManager::projectLoadFailed(QString _t1, QString _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void ProjectManager::projectFileVersionMismatch(QString _t1, QString _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}
QT_END_MOC_NAMESPACE
