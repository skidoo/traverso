/****************************************************************************
** Meta object code from reading C++ file 'Marker.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/core/Marker.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Marker.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Marker[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: signature, parameters, type, tag, flags
       8,    7,    7,    7, 0x05,
      26,    7,    7,    7, 0x05,
      47,    7,    7,    7, 0x05,

 // slots: signature, parameters, type, tag, flags
      67,   62,    7,    7, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_Marker[] = {
    "Marker\0\0positionChanged()\0"
    "descriptionChanged()\0indexChanged()\0"
    "when\0set_when(TimeRef)\0"
};

void Marker::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        Marker *_t = static_cast<Marker *>(_o);
        switch (_id) {
        case 0: _t->positionChanged(); break;
        case 1: _t->descriptionChanged(); break;
        case 2: _t->indexChanged(); break;
        case 3: _t->set_when((*reinterpret_cast< const TimeRef(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData Marker::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject Marker::staticMetaObject = {
    { &ContextItem::staticMetaObject, qt_meta_stringdata_Marker,
      qt_meta_data_Marker, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Marker::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Marker::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Marker::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Marker))
        return static_cast<void*>(const_cast< Marker*>(this));
    if (!strcmp(_clname, "Snappable"))
        return static_cast< Snappable*>(const_cast< Marker*>(this));
    return ContextItem::qt_metacast(_clname);
}

int Marker::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ContextItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}

// SIGNAL 0
void Marker::positionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void Marker::descriptionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void Marker::indexChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}
QT_END_MOC_NAMESPACE
