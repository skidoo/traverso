/****************************************************************************
** Meta object code from reading C++ file 'AudioClip.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/core/AudioClip.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'AudioClip.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_AudioClip[] = {

 // content:
       6,       // revision
       0,       // classname
       6,   14, // classinfo
      25,   26, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       7,       // signalCount

 // classinfo: key, value
      15,   10,
      31,   20,
      57,   45,
      85,   72,
     111,  101,
     126,  121,

 // signals: signature, parameters, type, tag, flags
     132,  131,  131,  131, 0x05,
     147,  131,  131,  131, 0x05,
     161,  131,  131,  131, 0x05,
     175,  131,  131,  131, 0x05,
     193,  131,  131,  131, 0x05,
     215,  131,  131,  131, 0x05,
     239,  131,  131,  131, 0x05,

 // slots: signature, parameters, type, tag, flags
     269,  131,  131,  131, 0x0a,
     288,  131,  131,  131, 0x0a,
     326,  310,  131,  131, 0x0a,
     366,  349,  131,  131, 0x0a,
     390,  131,  131,  131, 0x0a,
     420,  131,  131,  131, 0x0a,
     434,  131,  131,  131, 0x0a,
     450,  448,  131,  131, 0x0a,
     472,  131,  466,  131, 0x0a,
     492,  131,  483,  131, 0x0a,
     499,  131,  483,  131, 0x0a,
     515,  131,  483,  131, 0x0a,
     532,  131,  483,  131, 0x0a,
     550,  131,  483,  131, 0x0a,
     562,  131,  483,  131, 0x0a,
     574,  569,  131,  131, 0x08,
     603,  569,  131,  131, 0x08,
     635,  131,  131,  131, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_AudioClip[] = {
    "AudioClip\0Mute\0mute\0In: Remove\0"
    "reset_fade_in\0Out: Remove\0reset_fade_out\0"
    "Both: Remove\0reset_fade_both\0Normalize\0"
    "normalize\0Lock\0lock\0\0stateChanged()\0"
    "muteChanged()\0lockChanged()\0"
    "positionChanged()\0fadeAdded(FadeCurve*)\0"
    "fadeRemoved(FadeCurve*)\0"
    "recordingFinished(AudioClip*)\0"
    "finish_recording()\0finish_write_source()\0"
    "newLeftLocation\0set_left_edge(TimeRef)\0"
    "newRightLocation\0set_right_edge(TimeRef)\0"
    "track_audible_state_changed()\0"
    "toggle_mute()\0toggle_lock()\0g\0"
    "set_gain(float)\0float\0get_gain()\0"
    "Command*\0mute()\0reset_fade_in()\0"
    "reset_fade_out()\0reset_fade_both()\0"
    "normalize()\0lock()\0fade\0"
    "private_add_fade(FadeCurve*)\0"
    "private_remove_fade(FadeCurve*)\0"
    "get_capture_bus()\0"
};

void AudioClip::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        AudioClip *_t = static_cast<AudioClip *>(_o);
        switch (_id) {
        case 0: _t->stateChanged(); break;
        case 1: _t->muteChanged(); break;
        case 2: _t->lockChanged(); break;
        case 3: _t->positionChanged(); break;
        case 4: _t->fadeAdded((*reinterpret_cast< FadeCurve*(*)>(_a[1]))); break;
        case 5: _t->fadeRemoved((*reinterpret_cast< FadeCurve*(*)>(_a[1]))); break;
        case 6: _t->recordingFinished((*reinterpret_cast< AudioClip*(*)>(_a[1]))); break;
        case 7: _t->finish_recording(); break;
        case 8: _t->finish_write_source(); break;
        case 9: _t->set_left_edge((*reinterpret_cast< TimeRef(*)>(_a[1]))); break;
        case 10: _t->set_right_edge((*reinterpret_cast< TimeRef(*)>(_a[1]))); break;
        case 11: _t->track_audible_state_changed(); break;
        case 12: _t->toggle_mute(); break;
        case 13: _t->toggle_lock(); break;
        case 14: _t->set_gain((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 15: { float _r = _t->get_gain();
            if (_a[0]) *reinterpret_cast< float*>(_a[0]) = _r; }  break;
        case 16: { Command* _r = _t->mute();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 17: { Command* _r = _t->reset_fade_in();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 18: { Command* _r = _t->reset_fade_out();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 19: { Command* _r = _t->reset_fade_both();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 20: { Command* _r = _t->normalize();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 21: { Command* _r = _t->lock();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 22: _t->private_add_fade((*reinterpret_cast< FadeCurve*(*)>(_a[1]))); break;
        case 23: _t->private_remove_fade((*reinterpret_cast< FadeCurve*(*)>(_a[1]))); break;
        case 24: _t->get_capture_bus(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData AudioClip::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject AudioClip::staticMetaObject = {
    { &ContextItem::staticMetaObject, qt_meta_stringdata_AudioClip,
      qt_meta_data_AudioClip, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &AudioClip::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *AudioClip::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *AudioClip::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_AudioClip))
        return static_cast<void*>(const_cast< AudioClip*>(this));
    if (!strcmp(_clname, "AudioProcessingItem"))
        return static_cast< AudioProcessingItem*>(const_cast< AudioClip*>(this));
    if (!strcmp(_clname, "Snappable"))
        return static_cast< Snappable*>(const_cast< AudioClip*>(this));
    return ContextItem::qt_metacast(_clname);
}

int AudioClip::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ContextItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 25)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 25;
    }
    return _id;
}

// SIGNAL 0
void AudioClip::stateChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void AudioClip::muteChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void AudioClip::lockChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}

// SIGNAL 3
void AudioClip::positionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, 0);
}

// SIGNAL 4
void AudioClip::fadeAdded(FadeCurve * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void AudioClip::fadeRemoved(FadeCurve * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void AudioClip::recordingFinished(AudioClip * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}
QT_END_MOC_NAMESPACE
