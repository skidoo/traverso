# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/tmp/traverso/src/audiofileio/decode/AbstractAudioReader.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/audiofileio/CMakeFiles/traversoaudiofileio.dir/decode/AbstractAudioReader.o"
  "/tmp/traverso/src/audiofileio/decode/FlacAudioReader.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/audiofileio/CMakeFiles/traversoaudiofileio.dir/decode/FlacAudioReader.o"
  "/tmp/traverso/src/audiofileio/decode/MadAudioReader.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/audiofileio/CMakeFiles/traversoaudiofileio.dir/decode/MadAudioReader.o"
  "/tmp/traverso/src/audiofileio/decode/ResampleAudioReader.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/audiofileio/CMakeFiles/traversoaudiofileio.dir/decode/ResampleAudioReader.o"
  "/tmp/traverso/src/audiofileio/decode/SFAudioReader.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/audiofileio/CMakeFiles/traversoaudiofileio.dir/decode/SFAudioReader.o"
  "/tmp/traverso/src/audiofileio/decode/VorbisAudioReader.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/audiofileio/CMakeFiles/traversoaudiofileio.dir/decode/VorbisAudioReader.o"
  "/tmp/traverso/src/audiofileio/decode/WPAudioReader.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/audiofileio/CMakeFiles/traversoaudiofileio.dir/decode/WPAudioReader.o"
  "/tmp/traverso/src/audiofileio/encode/AbstractAudioWriter.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/audiofileio/CMakeFiles/traversoaudiofileio.dir/encode/AbstractAudioWriter.o"
  "/tmp/traverso/src/audiofileio/encode/FlacAudioWriter.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/audiofileio/CMakeFiles/traversoaudiofileio.dir/encode/FlacAudioWriter.o"
  "/tmp/traverso/src/audiofileio/encode/LameAudioWriter.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/audiofileio/CMakeFiles/traversoaudiofileio.dir/encode/LameAudioWriter.o"
  "/tmp/traverso/src/audiofileio/encode/SFAudioWriter.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/audiofileio/CMakeFiles/traversoaudiofileio.dir/encode/SFAudioWriter.o"
  "/tmp/traverso/src/audiofileio/encode/VorbisAudioWriter.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/audiofileio/CMakeFiles/traversoaudiofileio.dir/encode/VorbisAudioWriter.o"
  "/tmp/traverso/src/audiofileio/encode/WPAudioWriter.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/audiofileio/CMakeFiles/traversoaudiofileio.dir/encode/WPAudioWriter.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ALSA_SUPPORT"
  "HAVE_SYS_STAT_H"
  "HAVE_SYS_VFS_H"
  "JACK_SUPPORT"
  "LV2_SUPPORT"
  "MP3_DECODE_SUPPORT"
  "MP3_ENCODE_SUPPORT"
  "QT_CORE_LIB"
  "QT_GUI_LIB"
  "QT_NO_DEBUG"
  "SSE_OPTIMIZATIONS"
  "STATIC_BUILD"
  "USE_MLOCK"
  "USE_X86_64_ASM"
  "USE_XMMINTRIN"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "src/audiofileio"
  "../src/audiofileio"
  "/usr/include/lilv-0"
  "/usr/include/sratom-0"
  "/usr/include/sord-0"
  "/usr/include/serd-0"
  "../src/common"
  "../src/audiofileio/decode"
  "../src/audiofileio/encode"
  "/usr/include/qt4"
  "/usr/include/qt4/QtGui"
  "/usr/include/qt4/QtCore"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
