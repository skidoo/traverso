/********************************************************************************
** Form generated from reading UI file 'ProjectManagerDialog.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PROJECTMANAGERDIALOG_H
#define UI_PROJECTMANAGERDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QTabWidget>
#include <QtGui/QTextEdit>
#include <QtGui/QTreeWidget>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ProjectManagerDialog
{
public:
    QVBoxLayout *vboxLayout;
    QTabWidget *tabWidget;
    QWidget *tab_4;
    QGridLayout *gridLayout;
    QGroupBox *groupBox_3;
    QVBoxLayout *vboxLayout1;
    QHBoxLayout *hboxLayout;
    QLabel *label_3;
    QLineEdit *lineEditTitle;
    QHBoxLayout *hboxLayout1;
    QLabel *label;
    QTextEdit *descriptionTextEdit;
    QHBoxLayout *hboxLayout2;
    QLabel *label_2;
    QLineEdit *lineEdit;
    QGroupBox *groupBox;
    QVBoxLayout *vboxLayout2;
    QPushButton *sheetsExportButton;
    QPushButton *exportTemplateButton;
    QSpacerItem *spacerItem;
    QSpacerItem *spacerItem1;
    QWidget *tab;
    QHBoxLayout *hboxLayout3;
    QTreeWidget *treeSheetWidget;
    QVBoxLayout *vboxLayout3;
    QGroupBox *groupBox_4;
    QVBoxLayout *vboxLayout4;
    QLineEdit *selectedSheetName;
    QHBoxLayout *hboxLayout4;
    QPushButton *deleteSheetButton;
    QSpacerItem *spacerItem2;
    QPushButton *renameSheetButton;
    QGroupBox *groupBox_2;
    QHBoxLayout *hboxLayout5;
    QHBoxLayout *hboxLayout6;
    QPushButton *createSheetButton;
    QSpacerItem *spacerItem3;
    QWidget *cdtext;
    QGridLayout *gridLayout1;
    QLabel *label_7;
    QLineEdit *lineEditPerformer;
    QLabel *label_4;
    QLineEdit *lineEditId;
    QLabel *label_6;
    QLineEdit *lineEditUPC;
    QLabel *label_5;
    QComboBox *comboBoxGenre;
    QSpacerItem *spacerItem4;
    QLabel *label_8;
    QLineEdit *lineEditArranger;
    QLabel *label_9;
    QLineEdit *lineEditSongwriter;
    QLabel *label_10;
    QLineEdit *lineEditMessage;
    QHBoxLayout *hboxLayout7;
    QHBoxLayout *hboxLayout8;
    QPushButton *undoButton;
    QPushButton *redoButton;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *ProjectManagerDialog)
    {
        if (ProjectManagerDialog->objectName().isEmpty())
            ProjectManagerDialog->setObjectName(QString::fromUtf8("ProjectManagerDialog"));
        ProjectManagerDialog->resize(581, 308);
        ProjectManagerDialog->setMaximumSize(QSize(600, 500));
        vboxLayout = new QVBoxLayout(ProjectManagerDialog);
#ifndef Q_OS_MAC
        vboxLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        tabWidget = new QTabWidget(ProjectManagerDialog);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tab_4 = new QWidget();
        tab_4->setObjectName(QString::fromUtf8("tab_4"));
        gridLayout = new QGridLayout(tab_4);
#ifndef Q_OS_MAC
        gridLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        gridLayout->setContentsMargins(9, 9, 9, 9);
#endif
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        groupBox_3 = new QGroupBox(tab_4);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        vboxLayout1 = new QVBoxLayout(groupBox_3);
#ifndef Q_OS_MAC
        vboxLayout1->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout1->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout1->setObjectName(QString::fromUtf8("vboxLayout1"));
        hboxLayout = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout->setContentsMargins(0, 0, 0, 0);
#endif
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        label_3 = new QLabel(groupBox_3);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setMinimumSize(QSize(100, 0));

        hboxLayout->addWidget(label_3);

        lineEditTitle = new QLineEdit(groupBox_3);
        lineEditTitle->setObjectName(QString::fromUtf8("lineEditTitle"));
        lineEditTitle->setEnabled(true);
        lineEditTitle->setAcceptDrops(true);

        hboxLayout->addWidget(lineEditTitle);


        vboxLayout1->addLayout(hboxLayout);

        hboxLayout1 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout1->setSpacing(6);
#endif
        hboxLayout1->setContentsMargins(0, 0, 0, 0);
        hboxLayout1->setObjectName(QString::fromUtf8("hboxLayout1"));
        label = new QLabel(groupBox_3);
        label->setObjectName(QString::fromUtf8("label"));
        label->setMinimumSize(QSize(100, 0));

        hboxLayout1->addWidget(label);

        descriptionTextEdit = new QTextEdit(groupBox_3);
        descriptionTextEdit->setObjectName(QString::fromUtf8("descriptionTextEdit"));
        descriptionTextEdit->setMaximumSize(QSize(16777215, 50));

        hboxLayout1->addWidget(descriptionTextEdit);


        vboxLayout1->addLayout(hboxLayout1);

        hboxLayout2 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout2->setSpacing(6);
#endif
        hboxLayout2->setContentsMargins(0, 0, 0, 0);
        hboxLayout2->setObjectName(QString::fromUtf8("hboxLayout2"));
        label_2 = new QLabel(groupBox_3);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setMinimumSize(QSize(100, 0));

        hboxLayout2->addWidget(label_2);

        lineEdit = new QLineEdit(groupBox_3);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));

        hboxLayout2->addWidget(lineEdit);


        vboxLayout1->addLayout(hboxLayout2);


        gridLayout->addWidget(groupBox_3, 0, 0, 2, 1);

        groupBox = new QGroupBox(tab_4);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setMinimumSize(QSize(150, 0));
        vboxLayout2 = new QVBoxLayout(groupBox);
        vboxLayout2->setSpacing(9);
#ifndef Q_OS_MAC
        vboxLayout2->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout2->setObjectName(QString::fromUtf8("vboxLayout2"));
        sheetsExportButton = new QPushButton(groupBox);
        sheetsExportButton->setObjectName(QString::fromUtf8("sheetsExportButton"));

        vboxLayout2->addWidget(sheetsExportButton);

        exportTemplateButton = new QPushButton(groupBox);
        exportTemplateButton->setObjectName(QString::fromUtf8("exportTemplateButton"));

        vboxLayout2->addWidget(exportTemplateButton);


        gridLayout->addWidget(groupBox, 0, 1, 1, 1);

        spacerItem = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(spacerItem, 1, 1, 2, 1);

        spacerItem1 = new QSpacerItem(385, 20, QSizePolicy::Minimum, QSizePolicy::Maximum);

        gridLayout->addItem(spacerItem1, 2, 0, 1, 1);

        tabWidget->addTab(tab_4, QString());
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        hboxLayout3 = new QHBoxLayout(tab);
#ifndef Q_OS_MAC
        hboxLayout3->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout3->setContentsMargins(9, 9, 9, 9);
#endif
        hboxLayout3->setObjectName(QString::fromUtf8("hboxLayout3"));
        treeSheetWidget = new QTreeWidget(tab);
        treeSheetWidget->setObjectName(QString::fromUtf8("treeSheetWidget"));
        QSizePolicy sizePolicy(static_cast<QSizePolicy::Policy>(0), static_cast<QSizePolicy::Policy>(0));
        sizePolicy.setHorizontalStretch(10);
        sizePolicy.setVerticalStretch(5);
        sizePolicy.setHeightForWidth(treeSheetWidget->sizePolicy().hasHeightForWidth());
        treeSheetWidget->setSizePolicy(sizePolicy);
        treeSheetWidget->setMinimumSize(QSize(330, 120));

        hboxLayout3->addWidget(treeSheetWidget);

        vboxLayout3 = new QVBoxLayout();
#ifndef Q_OS_MAC
        vboxLayout3->setSpacing(6);
#endif
        vboxLayout3->setContentsMargins(0, 0, 0, 0);
        vboxLayout3->setObjectName(QString::fromUtf8("vboxLayout3"));
        groupBox_4 = new QGroupBox(tab);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        vboxLayout4 = new QVBoxLayout(groupBox_4);
#ifndef Q_OS_MAC
        vboxLayout4->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout4->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout4->setObjectName(QString::fromUtf8("vboxLayout4"));
        selectedSheetName = new QLineEdit(groupBox_4);
        selectedSheetName->setObjectName(QString::fromUtf8("selectedSheetName"));
        selectedSheetName->setMinimumSize(QSize(0, 0));

        vboxLayout4->addWidget(selectedSheetName);

        hboxLayout4 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout4->setSpacing(6);
#endif
        hboxLayout4->setContentsMargins(0, 0, 0, 0);
        hboxLayout4->setObjectName(QString::fromUtf8("hboxLayout4"));
        deleteSheetButton = new QPushButton(groupBox_4);
        deleteSheetButton->setObjectName(QString::fromUtf8("deleteSheetButton"));
        deleteSheetButton->setAutoDefault(false);

        hboxLayout4->addWidget(deleteSheetButton);

        spacerItem2 = new QSpacerItem(1, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        hboxLayout4->addItem(spacerItem2);

        renameSheetButton = new QPushButton(groupBox_4);
        renameSheetButton->setObjectName(QString::fromUtf8("renameSheetButton"));

        hboxLayout4->addWidget(renameSheetButton);


        vboxLayout4->addLayout(hboxLayout4);


        vboxLayout3->addWidget(groupBox_4);

        groupBox_2 = new QGroupBox(tab);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        hboxLayout5 = new QHBoxLayout(groupBox_2);
#ifndef Q_OS_MAC
        hboxLayout5->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout5->setContentsMargins(9, 9, 9, 9);
#endif
        hboxLayout5->setObjectName(QString::fromUtf8("hboxLayout5"));
        hboxLayout6 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout6->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout6->setContentsMargins(0, 0, 0, 0);
#endif
        hboxLayout6->setObjectName(QString::fromUtf8("hboxLayout6"));
        createSheetButton = new QPushButton(groupBox_2);
        createSheetButton->setObjectName(QString::fromUtf8("createSheetButton"));

        hboxLayout6->addWidget(createSheetButton);


        hboxLayout5->addLayout(hboxLayout6);


        vboxLayout3->addWidget(groupBox_2);

        spacerItem3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        vboxLayout3->addItem(spacerItem3);


        hboxLayout3->addLayout(vboxLayout3);

        tabWidget->addTab(tab, QString());
        cdtext = new QWidget();
        cdtext->setObjectName(QString::fromUtf8("cdtext"));
        gridLayout1 = new QGridLayout(cdtext);
#ifndef Q_OS_MAC
        gridLayout1->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        gridLayout1->setContentsMargins(9, 9, 9, 9);
#endif
        gridLayout1->setObjectName(QString::fromUtf8("gridLayout1"));
        label_7 = new QLabel(cdtext);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setMinimumSize(QSize(100, 0));

        gridLayout1->addWidget(label_7, 0, 0, 1, 1);

        lineEditPerformer = new QLineEdit(cdtext);
        lineEditPerformer->setObjectName(QString::fromUtf8("lineEditPerformer"));

        gridLayout1->addWidget(lineEditPerformer, 0, 1, 1, 2);

        label_4 = new QLabel(cdtext);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setMinimumSize(QSize(100, 0));

        gridLayout1->addWidget(label_4, 1, 0, 1, 1);

        lineEditId = new QLineEdit(cdtext);
        lineEditId->setObjectName(QString::fromUtf8("lineEditId"));

        gridLayout1->addWidget(lineEditId, 1, 1, 1, 2);

        label_6 = new QLabel(cdtext);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setMinimumSize(QSize(100, 0));

        gridLayout1->addWidget(label_6, 2, 0, 1, 1);

        lineEditUPC = new QLineEdit(cdtext);
        lineEditUPC->setObjectName(QString::fromUtf8("lineEditUPC"));

        gridLayout1->addWidget(lineEditUPC, 2, 1, 1, 2);

        label_5 = new QLabel(cdtext);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setMinimumSize(QSize(100, 0));

        gridLayout1->addWidget(label_5, 3, 0, 1, 1);

        comboBoxGenre = new QComboBox(cdtext);
        comboBoxGenre->setObjectName(QString::fromUtf8("comboBoxGenre"));

        gridLayout1->addWidget(comboBoxGenre, 3, 1, 1, 1);

        spacerItem4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout1->addItem(spacerItem4, 3, 2, 1, 1);

        label_8 = new QLabel(cdtext);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        gridLayout1->addWidget(label_8, 4, 0, 1, 1);

        lineEditArranger = new QLineEdit(cdtext);
        lineEditArranger->setObjectName(QString::fromUtf8("lineEditArranger"));

        gridLayout1->addWidget(lineEditArranger, 4, 1, 1, 2);

        label_9 = new QLabel(cdtext);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        gridLayout1->addWidget(label_9, 5, 0, 1, 1);

        lineEditSongwriter = new QLineEdit(cdtext);
        lineEditSongwriter->setObjectName(QString::fromUtf8("lineEditSongwriter"));

        gridLayout1->addWidget(lineEditSongwriter, 5, 1, 1, 2);

        label_10 = new QLabel(cdtext);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        gridLayout1->addWidget(label_10, 6, 0, 1, 1);

        lineEditMessage = new QLineEdit(cdtext);
        lineEditMessage->setObjectName(QString::fromUtf8("lineEditMessage"));

        gridLayout1->addWidget(lineEditMessage, 6, 1, 1, 2);

        tabWidget->addTab(cdtext, QString());

        vboxLayout->addWidget(tabWidget);

        hboxLayout7 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout7->setSpacing(6);
#endif
        hboxLayout7->setContentsMargins(0, 0, 0, 0);
        hboxLayout7->setObjectName(QString::fromUtf8("hboxLayout7"));
        hboxLayout8 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout8->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout8->setContentsMargins(0, 0, 0, 0);
#endif
        hboxLayout8->setObjectName(QString::fromUtf8("hboxLayout8"));
        undoButton = new QPushButton(ProjectManagerDialog);
        undoButton->setObjectName(QString::fromUtf8("undoButton"));
        undoButton->setMaximumSize(QSize(16777215, 22));

        hboxLayout8->addWidget(undoButton);

        redoButton = new QPushButton(ProjectManagerDialog);
        redoButton->setObjectName(QString::fromUtf8("redoButton"));
        redoButton->setMaximumSize(QSize(16777215, 22));

        hboxLayout8->addWidget(redoButton);


        hboxLayout7->addLayout(hboxLayout8);

        buttonBox = new QDialogButtonBox(ProjectManagerDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::NoButton|QDialogButtonBox::Ok);

        hboxLayout7->addWidget(buttonBox);


        vboxLayout->addLayout(hboxLayout7);


        retranslateUi(ProjectManagerDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), ProjectManagerDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), ProjectManagerDialog, SLOT(reject()));

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(ProjectManagerDialog);
    } // setupUi

    void retranslateUi(QDialog *ProjectManagerDialog)
    {
        ProjectManagerDialog->setWindowTitle(QApplication::translate("ProjectManagerDialog", "Dialog", 0, QApplication::UnicodeUTF8));
        groupBox_3->setTitle(QApplication::translate("ProjectManagerDialog", "Informational", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("ProjectManagerDialog", "Title", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("ProjectManagerDialog", "Description", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("ProjectManagerDialog", "Engineer", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("ProjectManagerDialog", "Export", 0, QApplication::UnicodeUTF8));
        sheetsExportButton->setText(QApplication::translate("ProjectManagerDialog", "Sheet(s)", 0, QApplication::UnicodeUTF8));
        exportTemplateButton->setText(QApplication::translate("ProjectManagerDialog", "Template", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(tab_4), QApplication::translate("ProjectManagerDialog", "Project", 0, QApplication::UnicodeUTF8));
        QTreeWidgetItem *___qtreewidgetitem = treeSheetWidget->headerItem();
        ___qtreewidgetitem->setText(2, QApplication::translate("ProjectManagerDialog", "Length", 0, QApplication::UnicodeUTF8));
        ___qtreewidgetitem->setText(1, QApplication::translate("ProjectManagerDialog", "Tracks", 0, QApplication::UnicodeUTF8));
        ___qtreewidgetitem->setText(0, QApplication::translate("ProjectManagerDialog", "Sheet Name", 0, QApplication::UnicodeUTF8));
        groupBox_4->setTitle(QApplication::translate("ProjectManagerDialog", "Selected Sheet", 0, QApplication::UnicodeUTF8));
        deleteSheetButton->setText(QApplication::translate("ProjectManagerDialog", "Delete", 0, QApplication::UnicodeUTF8));
        renameSheetButton->setText(QApplication::translate("ProjectManagerDialog", "Rename", 0, QApplication::UnicodeUTF8));
        groupBox_2->setTitle(QApplication::translate("ProjectManagerDialog", "New Sheet", 0, QApplication::UnicodeUTF8));
        createSheetButton->setText(QApplication::translate("ProjectManagerDialog", "Create new Sheet", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("ProjectManagerDialog", "Sheets", 0, QApplication::UnicodeUTF8));
        label_7->setText(QApplication::translate("ProjectManagerDialog", "Performer", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("ProjectManagerDialog", "Disc ID:", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("ProjectManagerDialog", "UPC EAN:", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("ProjectManagerDialog", "Genre:", 0, QApplication::UnicodeUTF8));
        comboBoxGenre->clear();
        comboBoxGenre->insertItems(0, QStringList()
         << QApplication::translate("ProjectManagerDialog", "Unused", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("ProjectManagerDialog", "Undefined", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("ProjectManagerDialog", "Adult Contemporary", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("ProjectManagerDialog", "Alternative Rock", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("ProjectManagerDialog", "Childrens", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("ProjectManagerDialog", "Classical", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("ProjectManagerDialog", "Contemporary Christian", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("ProjectManagerDialog", "Country", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("ProjectManagerDialog", "Dance", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("ProjectManagerDialog", "Easy Listening", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("ProjectManagerDialog", "Erotic", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("ProjectManagerDialog", "Folk", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("ProjectManagerDialog", "Gospel", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("ProjectManagerDialog", "Hip Hop", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("ProjectManagerDialog", "Jazz", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("ProjectManagerDialog", "Latin", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("ProjectManagerDialog", "Musical", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("ProjectManagerDialog", "New Age", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("ProjectManagerDialog", "Opera", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("ProjectManagerDialog", "Operette", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("ProjectManagerDialog", "Pop\302\240Music", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("ProjectManagerDialog", "Rap", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("ProjectManagerDialog", "Reggae", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("ProjectManagerDialog", "Rock Music", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("ProjectManagerDialog", "Rhythm and Blues", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("ProjectManagerDialog", "Sound Effects", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("ProjectManagerDialog", "Spoken Word", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("ProjectManagerDialog", "World Music", 0, QApplication::UnicodeUTF8)
        );
        label_8->setText(QApplication::translate("ProjectManagerDialog", "Arranger", 0, QApplication::UnicodeUTF8));
        label_9->setText(QApplication::translate("ProjectManagerDialog", "Songwriter", 0, QApplication::UnicodeUTF8));
        label_10->setText(QApplication::translate("ProjectManagerDialog", "Message", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(cdtext), QApplication::translate("ProjectManagerDialog", "CD\302\240Text", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        undoButton->setToolTip(QApplication::translate("ProjectManagerDialog", "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Undo last change</p></body></html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        undoButton->setText(QString());
#ifndef QT_NO_TOOLTIP
        redoButton->setToolTip(QApplication::translate("ProjectManagerDialog", "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Redo last change</p></body></html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        redoButton->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class ProjectManagerDialog: public Ui_ProjectManagerDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PROJECTMANAGERDIALOG_H
