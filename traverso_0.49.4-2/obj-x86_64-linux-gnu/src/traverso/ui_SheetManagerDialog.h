/********************************************************************************
** Form generated from reading UI file 'SheetManagerDialog.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SHEETMANAGERDIALOG_H
#define UI_SHEETMANAGERDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QTabWidget>
#include <QtGui/QTreeWidget>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ProjectManagerDialog
{
public:
    QVBoxLayout *vboxLayout;
    QTabWidget *tabWidget;
    QWidget *tab_4;
    QWidget *tab;
    QHBoxLayout *hboxLayout;
    QTreeWidget *treeSheetWidget;
    QVBoxLayout *vboxLayout1;
    QGroupBox *groupBox_4;
    QVBoxLayout *vboxLayout2;
    QLineEdit *selectedSheetName;
    QHBoxLayout *hboxLayout1;
    QPushButton *deleteSheetButton;
    QSpacerItem *spacerItem;
    QPushButton *renameSheetButton;
    QGroupBox *groupBox_2;
    QHBoxLayout *hboxLayout2;
    QHBoxLayout *hboxLayout3;
    QPushButton *createSheetButton;
    QSpacerItem *spacerItem1;
    QHBoxLayout *hboxLayout4;
    QHBoxLayout *hboxLayout5;
    QPushButton *undoPushButton;
    QPushButton *redoPushButton;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *ProjectManagerDialog)
    {
        if (ProjectManagerDialog->objectName().isEmpty())
            ProjectManagerDialog->setObjectName(QString::fromUtf8("ProjectManagerDialog"));
        ProjectManagerDialog->resize(600, 309);
        ProjectManagerDialog->setMaximumSize(QSize(600, 500));
        vboxLayout = new QVBoxLayout(ProjectManagerDialog);
#ifndef Q_OS_MAC
        vboxLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        tabWidget = new QTabWidget(ProjectManagerDialog);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tab_4 = new QWidget();
        tab_4->setObjectName(QString::fromUtf8("tab_4"));
        tabWidget->addTab(tab_4, QString());
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        hboxLayout = new QHBoxLayout(tab);
#ifndef Q_OS_MAC
        hboxLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout->setContentsMargins(9, 9, 9, 9);
#endif
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        treeSheetWidget = new QTreeWidget(tab);
        treeSheetWidget->setObjectName(QString::fromUtf8("treeSheetWidget"));
        QSizePolicy sizePolicy(static_cast<QSizePolicy::Policy>(7), static_cast<QSizePolicy::Policy>(7));
        sizePolicy.setHorizontalStretch(10);
        sizePolicy.setVerticalStretch(5);
        sizePolicy.setHeightForWidth(treeSheetWidget->sizePolicy().hasHeightForWidth());
        treeSheetWidget->setSizePolicy(sizePolicy);
        treeSheetWidget->setMinimumSize(QSize(250, 120));

        hboxLayout->addWidget(treeSheetWidget);

        vboxLayout1 = new QVBoxLayout();
#ifndef Q_OS_MAC
        vboxLayout1->setSpacing(6);
#endif
        vboxLayout1->setContentsMargins(0, 0, 0, 0);
        vboxLayout1->setObjectName(QString::fromUtf8("vboxLayout1"));
        groupBox_4 = new QGroupBox(tab);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        vboxLayout2 = new QVBoxLayout(groupBox_4);
#ifndef Q_OS_MAC
        vboxLayout2->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout2->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout2->setObjectName(QString::fromUtf8("vboxLayout2"));
        selectedSheetName = new QLineEdit(groupBox_4);
        selectedSheetName->setObjectName(QString::fromUtf8("selectedSheetName"));
        selectedSheetName->setMinimumSize(QSize(0, 0));

        vboxLayout2->addWidget(selectedSheetName);

        hboxLayout1 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout1->setSpacing(6);
#endif
        hboxLayout1->setContentsMargins(0, 0, 0, 0);
        hboxLayout1->setObjectName(QString::fromUtf8("hboxLayout1"));
        deleteSheetButton = new QPushButton(groupBox_4);
        deleteSheetButton->setObjectName(QString::fromUtf8("deleteSheetButton"));
        deleteSheetButton->setAutoDefault(false);

        hboxLayout1->addWidget(deleteSheetButton);

        spacerItem = new QSpacerItem(10, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        hboxLayout1->addItem(spacerItem);

        renameSheetButton = new QPushButton(groupBox_4);
        renameSheetButton->setObjectName(QString::fromUtf8("renameSheetButton"));

        hboxLayout1->addWidget(renameSheetButton);


        vboxLayout2->addLayout(hboxLayout1);


        vboxLayout1->addWidget(groupBox_4);

        groupBox_2 = new QGroupBox(tab);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        hboxLayout2 = new QHBoxLayout(groupBox_2);
#ifndef Q_OS_MAC
        hboxLayout2->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout2->setContentsMargins(9, 9, 9, 9);
#endif
        hboxLayout2->setObjectName(QString::fromUtf8("hboxLayout2"));
        hboxLayout3 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout3->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout3->setContentsMargins(0, 0, 0, 0);
#endif
        hboxLayout3->setObjectName(QString::fromUtf8("hboxLayout3"));
        createSheetButton = new QPushButton(groupBox_2);
        createSheetButton->setObjectName(QString::fromUtf8("createSheetButton"));

        hboxLayout3->addWidget(createSheetButton);


        hboxLayout2->addLayout(hboxLayout3);


        vboxLayout1->addWidget(groupBox_2);

        spacerItem1 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        vboxLayout1->addItem(spacerItem1);


        hboxLayout->addLayout(vboxLayout1);

        tabWidget->addTab(tab, QString());

        vboxLayout->addWidget(tabWidget);

        hboxLayout4 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout4->setSpacing(6);
#endif
        hboxLayout4->setContentsMargins(0, 0, 0, 0);
        hboxLayout4->setObjectName(QString::fromUtf8("hboxLayout4"));
        hboxLayout5 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout5->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout5->setContentsMargins(0, 0, 0, 0);
#endif
        hboxLayout5->setObjectName(QString::fromUtf8("hboxLayout5"));
        undoPushButton = new QPushButton(ProjectManagerDialog);
        undoPushButton->setObjectName(QString::fromUtf8("undoPushButton"));
        undoPushButton->setMaximumSize(QSize(16777215, 22));

        hboxLayout5->addWidget(undoPushButton);

        redoPushButton = new QPushButton(ProjectManagerDialog);
        redoPushButton->setObjectName(QString::fromUtf8("redoPushButton"));
        redoPushButton->setMaximumSize(QSize(16777215, 22));

        hboxLayout5->addWidget(redoPushButton);


        hboxLayout4->addLayout(hboxLayout5);

        buttonBox = new QDialogButtonBox(ProjectManagerDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Close);

        hboxLayout4->addWidget(buttonBox);


        vboxLayout->addLayout(hboxLayout4);


        retranslateUi(ProjectManagerDialog);
        QObject::connect(buttonBox, SIGNAL(clicked(QAbstractButton*)), ProjectManagerDialog, SLOT(accept()));
        QObject::connect(selectedSheetName, SIGNAL(returnPressed()), renameSheetButton, SLOT(animateClick()));

        tabWidget->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(ProjectManagerDialog);
    } // setupUi

    void retranslateUi(QDialog *ProjectManagerDialog)
    {
        ProjectManagerDialog->setWindowTitle(QApplication::translate("ProjectManagerDialog", "Dialog", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(tab_4), QApplication::translate("ProjectManagerDialog", "Project", 0, QApplication::UnicodeUTF8));
        groupBox_4->setTitle(QApplication::translate("ProjectManagerDialog", "Selected Sheet", 0, QApplication::UnicodeUTF8));
        deleteSheetButton->setText(QApplication::translate("ProjectManagerDialog", "Delete", 0, QApplication::UnicodeUTF8));
        renameSheetButton->setText(QApplication::translate("ProjectManagerDialog", "Rename", 0, QApplication::UnicodeUTF8));
        groupBox_2->setTitle(QApplication::translate("ProjectManagerDialog", "New Sheet", 0, QApplication::UnicodeUTF8));
        createSheetButton->setText(QApplication::translate("ProjectManagerDialog", "Create new Sheet", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("ProjectManagerDialog", "Sheets", 0, QApplication::UnicodeUTF8));
        undoPushButton->setText(QApplication::translate("ProjectManagerDialog", "undotext", 0, QApplication::UnicodeUTF8));
        redoPushButton->setText(QApplication::translate("ProjectManagerDialog", "redotext", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class ProjectManagerDialog: public Ui_ProjectManagerDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SHEETMANAGERDIALOG_H
