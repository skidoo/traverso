/****************************************************************************
** Meta object code from reading C++ file 'MessageWidget.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../src/traverso/widgets/MessageWidget.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'MessageWidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MessageWidgetPrivate[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      22,   21,   21,   21, 0x0a,
      48,   21,   21,   21, 0x0a,
      71,   21,   21,   21, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_MessageWidgetPrivate[] = {
    "MessageWidgetPrivate\0\0queue_message(InfoStruct)\0"
    "dequeue_messagequeue()\0show_history()\0"
};

void MessageWidgetPrivate::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        MessageWidgetPrivate *_t = static_cast<MessageWidgetPrivate *>(_o);
        switch (_id) {
        case 0: _t->queue_message((*reinterpret_cast< InfoStruct(*)>(_a[1]))); break;
        case 1: _t->dequeue_messagequeue(); break;
        case 2: _t->show_history(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData MessageWidgetPrivate::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject MessageWidgetPrivate::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_MessageWidgetPrivate,
      qt_meta_data_MessageWidgetPrivate, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MessageWidgetPrivate::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MessageWidgetPrivate::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MessageWidgetPrivate::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MessageWidgetPrivate))
        return static_cast<void*>(const_cast< MessageWidgetPrivate*>(this));
    return QWidget::qt_metacast(_clname);
}

int MessageWidgetPrivate::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
