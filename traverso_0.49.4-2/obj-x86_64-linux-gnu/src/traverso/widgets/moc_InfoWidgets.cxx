/****************************************************************************
** Meta object code from reading C++ file 'InfoWidgets.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../src/traverso/widgets/InfoWidgets.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'InfoWidgets.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_InfoWidget[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x09,
      30,   11,   11,   11, 0x09,

       0        // eod
};

static const char qt_meta_stringdata_InfoWidget[] = {
    "InfoWidget\0\0set_sheet(Sheet*)\0"
    "set_project(Project*)\0"
};

void InfoWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        InfoWidget *_t = static_cast<InfoWidget *>(_o);
        switch (_id) {
        case 0: _t->set_sheet((*reinterpret_cast< Sheet*(*)>(_a[1]))); break;
        case 1: _t->set_project((*reinterpret_cast< Project*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData InfoWidget::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject InfoWidget::staticMetaObject = {
    { &QFrame::staticMetaObject, qt_meta_stringdata_InfoWidget,
      qt_meta_data_InfoWidget, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &InfoWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *InfoWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *InfoWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_InfoWidget))
        return static_cast<void*>(const_cast< InfoWidget*>(this));
    return QFrame::qt_metacast(_clname);
}

int InfoWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QFrame::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}
static const uint qt_meta_data_SystemResources[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      17,   16,   16,   16, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_SystemResources[] = {
    "SystemResources\0\0update_status()\0"
};

void SystemResources::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        SystemResources *_t = static_cast<SystemResources *>(_o);
        switch (_id) {
        case 0: _t->update_status(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData SystemResources::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject SystemResources::staticMetaObject = {
    { &InfoWidget::staticMetaObject, qt_meta_stringdata_SystemResources,
      qt_meta_data_SystemResources, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &SystemResources::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *SystemResources::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *SystemResources::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_SystemResources))
        return static_cast<void*>(const_cast< SystemResources*>(this));
    return InfoWidget::qt_metacast(_clname);
}

int SystemResources::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = InfoWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}
static const uint qt_meta_data_DriverInfo[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x08,
      33,   11,   11,   11, 0x08,
      52,   11,   11,   11, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_DriverInfo[] = {
    "DriverInfo\0\0update_driver_info()\0"
    "update_xrun_info()\0show_driver_config_dialog()\0"
};

void DriverInfo::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        DriverInfo *_t = static_cast<DriverInfo *>(_o);
        switch (_id) {
        case 0: _t->update_driver_info(); break;
        case 1: _t->update_xrun_info(); break;
        case 2: _t->show_driver_config_dialog(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData DriverInfo::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject DriverInfo::staticMetaObject = {
    { &InfoWidget::staticMetaObject, qt_meta_stringdata_DriverInfo,
      qt_meta_data_DriverInfo, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &DriverInfo::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *DriverInfo::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *DriverInfo::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_DriverInfo))
        return static_cast<void*>(const_cast< DriverInfo*>(this));
    return InfoWidget::qt_metacast(_clname);
}

int DriverInfo::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = InfoWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
    return _id;
}
static const uint qt_meta_data_HDDSpaceInfo[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      14,   13,   13,   13, 0x08,
      32,   13,   13,   13, 0x08,
      48,   13,   13,   13, 0x08,
      64,   13,   13,   13, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_HDDSpaceInfo[] = {
    "HDDSpaceInfo\0\0set_sheet(Sheet*)\0"
    "update_status()\0sheet_started()\0"
    "sheet_stopped()\0"
};

void HDDSpaceInfo::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        HDDSpaceInfo *_t = static_cast<HDDSpaceInfo *>(_o);
        switch (_id) {
        case 0: _t->set_sheet((*reinterpret_cast< Sheet*(*)>(_a[1]))); break;
        case 1: _t->update_status(); break;
        case 2: _t->sheet_started(); break;
        case 3: _t->sheet_stopped(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData HDDSpaceInfo::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject HDDSpaceInfo::staticMetaObject = {
    { &InfoWidget::staticMetaObject, qt_meta_stringdata_HDDSpaceInfo,
      qt_meta_data_HDDSpaceInfo, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &HDDSpaceInfo::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *HDDSpaceInfo::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *HDDSpaceInfo::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_HDDSpaceInfo))
        return static_cast<void*>(const_cast< HDDSpaceInfo*>(this));
    return InfoWidget::qt_metacast(_clname);
}

int HDDSpaceInfo::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = InfoWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}
static const uint qt_meta_data_SystemValueBar[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_SystemValueBar[] = {
    "SystemValueBar\0"
};

void SystemValueBar::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData SystemValueBar::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject SystemValueBar::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_SystemValueBar,
      qt_meta_data_SystemValueBar, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &SystemValueBar::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *SystemValueBar::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *SystemValueBar::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_SystemValueBar))
        return static_cast<void*>(const_cast< SystemValueBar*>(this));
    return QWidget::qt_metacast(_clname);
}

int SystemValueBar::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_ProgressToolBar[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      17,   16,   16,   16, 0x0a,
      35,   16,   16,   16, 0x0a,
      54,   16,   16,   16, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_ProgressToolBar[] = {
    "ProgressToolBar\0\0set_progress(int)\0"
    "set_label(QString)\0set_num_files(int)\0"
};

void ProgressToolBar::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ProgressToolBar *_t = static_cast<ProgressToolBar *>(_o);
        switch (_id) {
        case 0: _t->set_progress((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->set_label((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->set_num_files((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ProgressToolBar::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ProgressToolBar::staticMetaObject = {
    { &QToolBar::staticMetaObject, qt_meta_stringdata_ProgressToolBar,
      qt_meta_data_ProgressToolBar, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ProgressToolBar::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ProgressToolBar::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ProgressToolBar::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ProgressToolBar))
        return static_cast<void*>(const_cast< ProgressToolBar*>(this));
    return QToolBar::qt_metacast(_clname);
}

int ProgressToolBar::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QToolBar::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
