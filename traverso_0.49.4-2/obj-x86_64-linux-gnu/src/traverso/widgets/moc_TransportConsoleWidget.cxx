/****************************************************************************
** Meta object code from reading C++ file 'TransportConsoleWidget.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../src/traverso/widgets/TransportConsoleWidget.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TransportConsoleWidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_TransportConsoleWidget[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      24,   23,   23,   23, 0x09,
      46,   23,   23,   23, 0x09,
      64,   23,   23,   23, 0x08,
      75,   23,   23,   23, 0x08,
      85,   23,   23,   23, 0x08,
      99,   23,   23,   23, 0x08,
     114,   23,   23,   23, 0x08,
     123,   23,   23,   23, 0x08,
     134,   23,   23,   23, 0x08,
     154,   23,   23,   23, 0x08,
     174,   23,   23,   23, 0x08,
     199,   23,   23,   23, 0x08,
     214,   23,   23,   23, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_TransportConsoleWidget[] = {
    "TransportConsoleWidget\0\0set_project(Project*)\0"
    "set_sheet(Sheet*)\0to_start()\0to_left()\0"
    "rec_toggled()\0play_toggled()\0to_end()\0"
    "to_right()\0transport_started()\0"
    "transport_stopped()\0update_recording_state()\0"
    "update_label()\0update_layout()\0"
};

void TransportConsoleWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        TransportConsoleWidget *_t = static_cast<TransportConsoleWidget *>(_o);
        switch (_id) {
        case 0: _t->set_project((*reinterpret_cast< Project*(*)>(_a[1]))); break;
        case 1: _t->set_sheet((*reinterpret_cast< Sheet*(*)>(_a[1]))); break;
        case 2: _t->to_start(); break;
        case 3: _t->to_left(); break;
        case 4: _t->rec_toggled(); break;
        case 5: _t->play_toggled(); break;
        case 6: _t->to_end(); break;
        case 7: _t->to_right(); break;
        case 8: _t->transport_started(); break;
        case 9: _t->transport_stopped(); break;
        case 10: _t->update_recording_state(); break;
        case 11: _t->update_label(); break;
        case 12: _t->update_layout(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData TransportConsoleWidget::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject TransportConsoleWidget::staticMetaObject = {
    { &QToolBar::staticMetaObject, qt_meta_stringdata_TransportConsoleWidget,
      qt_meta_data_TransportConsoleWidget, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &TransportConsoleWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *TransportConsoleWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *TransportConsoleWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_TransportConsoleWidget))
        return static_cast<void*>(const_cast< TransportConsoleWidget*>(this));
    return QToolBar::qt_metacast(_clname);
}

int TransportConsoleWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QToolBar::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
