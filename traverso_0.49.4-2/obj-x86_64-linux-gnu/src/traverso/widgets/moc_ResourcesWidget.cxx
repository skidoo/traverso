/****************************************************************************
** Meta object code from reading C++ file 'ResourcesWidget.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../src/traverso/widgets/ResourcesWidget.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'ResourcesWidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_FileWidget[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      18,   12,   11,   11, 0x08,
      52,   11,   11,   11, 0x08,
      76,   11,   11,   11, 0x08,
     103,  101,   11,   11, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_FileWidget[] = {
    "FileWidget\0\0index\0dirview_item_clicked(QModelIndex)\0"
    "dir_up_button_clicked()\0"
    "refresh_button_clicked()\0i\0box_actived(int)\0"
};

void FileWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        FileWidget *_t = static_cast<FileWidget *>(_o);
        switch (_id) {
        case 0: _t->dirview_item_clicked((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 1: _t->dir_up_button_clicked(); break;
        case 2: _t->refresh_button_clicked(); break;
        case 3: _t->box_actived((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData FileWidget::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject FileWidget::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_FileWidget,
      qt_meta_data_FileWidget, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &FileWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *FileWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *FileWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_FileWidget))
        return static_cast<void*>(const_cast< FileWidget*>(this));
    return QWidget::qt_metacast(_clname);
}

int FileWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}
static const uint qt_meta_data_ClipTreeItem[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      14,   13,   13,   13, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_ClipTreeItem[] = {
    "ClipTreeItem\0\0clip_state_changed()\0"
};

void ClipTreeItem::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ClipTreeItem *_t = static_cast<ClipTreeItem *>(_o);
        switch (_id) {
        case 0: _t->clip_state_changed(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData ClipTreeItem::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ClipTreeItem::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_ClipTreeItem,
      qt_meta_data_ClipTreeItem, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ClipTreeItem::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ClipTreeItem::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ClipTreeItem::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ClipTreeItem))
        return static_cast<void*>(const_cast< ClipTreeItem*>(this));
    if (!strcmp(_clname, "QTreeWidgetItem"))
        return static_cast< QTreeWidgetItem*>(const_cast< ClipTreeItem*>(this));
    return QObject::qt_metacast(_clname);
}

int ClipTreeItem::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}
static const uint qt_meta_data_SourceTreeItem[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      16,   15,   15,   15, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_SourceTreeItem[] = {
    "SourceTreeItem\0\0source_state_changed()\0"
};

void SourceTreeItem::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        SourceTreeItem *_t = static_cast<SourceTreeItem *>(_o);
        switch (_id) {
        case 0: _t->source_state_changed(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData SourceTreeItem::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject SourceTreeItem::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_SourceTreeItem,
      qt_meta_data_SourceTreeItem, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &SourceTreeItem::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *SourceTreeItem::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *SourceTreeItem::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_SourceTreeItem))
        return static_cast<void*>(const_cast< SourceTreeItem*>(this));
    if (!strcmp(_clname, "QTreeWidgetItem"))
        return static_cast< QTreeWidgetItem*>(const_cast< SourceTreeItem*>(this));
    return QObject::qt_metacast(_clname);
}

int SourceTreeItem::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}
static const uint qt_meta_data_ResourcesWidget[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      25,   17,   16,   16, 0x08,
      47,   16,   16,   16, 0x08,
      77,   71,   16,   16, 0x08,
     111,   71,   16,   16, 0x08,
     152,  146,   16,   16, 0x08,
     172,  146,   16,   16, 0x08,
     194,  146,   16,   16, 0x08,
     225,  220,   16,   16, 0x08,
     246,  220,   16,   16, 0x08,
     277,  270,   16,   16, 0x08,
     301,  270,   16,   16, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_ResourcesWidget[] = {
    "ResourcesWidget\0\0project\0set_project(Project*)\0"
    "project_load_finished()\0index\0"
    "view_combo_box_index_changed(int)\0"
    "sheet_combo_box_index_changed(int)\0"
    "sheet\0sheet_added(Sheet*)\0"
    "sheet_removed(Sheet*)\0set_current_sheet(Sheet*)\0"
    "clip\0add_clip(AudioClip*)\0"
    "remove_clip(AudioClip*)\0source\0"
    "add_source(ReadSource*)\0"
    "remove_source(ReadSource*)\0"
};

void ResourcesWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ResourcesWidget *_t = static_cast<ResourcesWidget *>(_o);
        switch (_id) {
        case 0: _t->set_project((*reinterpret_cast< Project*(*)>(_a[1]))); break;
        case 1: _t->project_load_finished(); break;
        case 2: _t->view_combo_box_index_changed((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->sheet_combo_box_index_changed((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->sheet_added((*reinterpret_cast< Sheet*(*)>(_a[1]))); break;
        case 5: _t->sheet_removed((*reinterpret_cast< Sheet*(*)>(_a[1]))); break;
        case 6: _t->set_current_sheet((*reinterpret_cast< Sheet*(*)>(_a[1]))); break;
        case 7: _t->add_clip((*reinterpret_cast< AudioClip*(*)>(_a[1]))); break;
        case 8: _t->remove_clip((*reinterpret_cast< AudioClip*(*)>(_a[1]))); break;
        case 9: _t->add_source((*reinterpret_cast< ReadSource*(*)>(_a[1]))); break;
        case 10: _t->remove_source((*reinterpret_cast< ReadSource*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ResourcesWidget::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ResourcesWidget::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_ResourcesWidget,
      qt_meta_data_ResourcesWidget, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ResourcesWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ResourcesWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ResourcesWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ResourcesWidget))
        return static_cast<void*>(const_cast< ResourcesWidget*>(this));
    if (!strcmp(_clname, "Ui::ResourcesWidget"))
        return static_cast< Ui::ResourcesWidget*>(const_cast< ResourcesWidget*>(this));
    return QWidget::qt_metacast(_clname);
}

int ResourcesWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
