/****************************************************************************
** Meta object code from reading C++ file 'CorrelationMeterWidget.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../src/traverso/widgets/CorrelationMeterWidget.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'CorrelationMeterWidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_CorrelationMeterView[] = {

 // content:
       6,       // revision
       0,       // classname
       1,   14, // classinfo
       3,   16, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // classinfo: key, value
      42,   21,

 // slots: signature, parameters, type, tag, flags
      52,   51,   51,   51, 0x08,
      72,   66,   51,   51, 0x08,
      99,   51,   90,   51, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_CorrelationMeterView[] = {
    "CorrelationMeterView\0Toggle display range\0"
    "set_mode\0\0update_data()\0sheet\0"
    "set_sheet(Sheet*)\0Command*\0set_mode()\0"
};

void CorrelationMeterView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        CorrelationMeterView *_t = static_cast<CorrelationMeterView *>(_o);
        switch (_id) {
        case 0: _t->update_data(); break;
        case 1: _t->set_sheet((*reinterpret_cast< Sheet*(*)>(_a[1]))); break;
        case 2: { Command* _r = _t->set_mode();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        default: ;
        }
    }
}

const QMetaObjectExtraData CorrelationMeterView::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject CorrelationMeterView::staticMetaObject = {
    { &MeterView::staticMetaObject, qt_meta_stringdata_CorrelationMeterView,
      qt_meta_data_CorrelationMeterView, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &CorrelationMeterView::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *CorrelationMeterView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *CorrelationMeterView::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_CorrelationMeterView))
        return static_cast<void*>(const_cast< CorrelationMeterView*>(this));
    return MeterView::qt_metacast(_clname);
}

int CorrelationMeterView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = MeterView::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
