/****************************************************************************
** Meta object code from reading C++ file 'MeterWidget.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../src/traverso/widgets/MeterWidget.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'MeterWidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MeterView[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      11,   10,   10,   10, 0x08,
      33,   10,   10,   10, 0x08,
      47,   10,   10,   10, 0x08,
      67,   10,   10,   10, 0x08,
      87,   10,   10,   10, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_MeterView[] = {
    "MeterView\0\0set_project(Project*)\0"
    "update_data()\0transport_started()\0"
    "transport_stopped()\0delay_timeout()\0"
};

void MeterView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        MeterView *_t = static_cast<MeterView *>(_o);
        switch (_id) {
        case 0: _t->set_project((*reinterpret_cast< Project*(*)>(_a[1]))); break;
        case 1: _t->update_data(); break;
        case 2: _t->transport_started(); break;
        case 3: _t->transport_stopped(); break;
        case 4: _t->delay_timeout(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData MeterView::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject MeterView::staticMetaObject = {
    { &ViewItem::staticMetaObject, qt_meta_stringdata_MeterView,
      qt_meta_data_MeterView, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MeterView::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MeterView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MeterView::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MeterView))
        return static_cast<void*>(const_cast< MeterView*>(this));
    return ViewItem::qt_metacast(_clname);
}

int MeterView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ViewItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
