/****************************************************************************
** Meta object code from reading C++ file 'SpectralMeterWidget.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../src/traverso/widgets/SpectralMeterWidget.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'SpectralMeterWidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_SpectralMeterConfigWidget[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      27,   26,   26,   26, 0x05,

 // slots: signature, parameters, type, tag, flags
      43,   26,   26,   26, 0x08,
      68,   26,   26,   26, 0x08,
      93,   26,   26,   26, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_SpectralMeterConfigWidget[] = {
    "SpectralMeterConfigWidget\0\0configChanged()\0"
    "on_buttonClose_clicked()\0"
    "on_buttonApply_clicked()\0"
    "advancedButton_toggled(bool)\0"
};

void SpectralMeterConfigWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        SpectralMeterConfigWidget *_t = static_cast<SpectralMeterConfigWidget *>(_o);
        switch (_id) {
        case 0: _t->configChanged(); break;
        case 1: _t->on_buttonClose_clicked(); break;
        case 2: _t->on_buttonApply_clicked(); break;
        case 3: _t->advancedButton_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData SpectralMeterConfigWidget::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject SpectralMeterConfigWidget::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_SpectralMeterConfigWidget,
      qt_meta_data_SpectralMeterConfigWidget, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &SpectralMeterConfigWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *SpectralMeterConfigWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *SpectralMeterConfigWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_SpectralMeterConfigWidget))
        return static_cast<void*>(const_cast< SpectralMeterConfigWidget*>(this));
    return QDialog::qt_metacast(_clname);
}

int SpectralMeterConfigWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}

// SIGNAL 0
void SpectralMeterConfigWidget::configChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
static const uint qt_meta_data_SpectralMeterView[] = {

 // content:
       6,       // revision
       0,       // classname
       5,   14, // classinfo
       8,   24, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // classinfo: key, value
      30,   18,
      67,   46,
      96,   76,
     123,  102,
     159,  144,

 // slots: signature, parameters, type, tag, flags
     175,  174,  174,  174, 0x08,
     189,  174,  174,  174, 0x0a,
     216,  210,  174,  174, 0x0a,
     243,  174,  234,  174, 0x0a,
     261,  174,  234,  174, 0x0a,
     272,  174,  234,  174, 0x0a,
     280,  174,  234,  174, 0x0a,
     303,  174,  234,  174, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_SpectralMeterView[] = {
    "SpectralMeterView\0Settings...\0"
    "edit_properties\0Toggle avarage curve\0"
    "set_mode\0Reset average curve\0reset\0"
    "Export avarage curve\0export_avarage_curve\0"
    "Capture Screen\0screen_capture\0\0"
    "update_data()\0load_configuration()\0"
    "sheet\0set_sheet(Sheet*)\0Command*\0"
    "edit_properties()\0set_mode()\0reset()\0"
    "export_avarage_curve()\0screen_capture()\0"
};

void SpectralMeterView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        SpectralMeterView *_t = static_cast<SpectralMeterView *>(_o);
        switch (_id) {
        case 0: _t->update_data(); break;
        case 1: _t->load_configuration(); break;
        case 2: _t->set_sheet((*reinterpret_cast< Sheet*(*)>(_a[1]))); break;
        case 3: { Command* _r = _t->edit_properties();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 4: { Command* _r = _t->set_mode();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 5: { Command* _r = _t->reset();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 6: { Command* _r = _t->export_avarage_curve();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 7: { Command* _r = _t->screen_capture();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        default: ;
        }
    }
}

const QMetaObjectExtraData SpectralMeterView::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject SpectralMeterView::staticMetaObject = {
    { &MeterView::staticMetaObject, qt_meta_stringdata_SpectralMeterView,
      qt_meta_data_SpectralMeterView, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &SpectralMeterView::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *SpectralMeterView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *SpectralMeterView::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_SpectralMeterView))
        return static_cast<void*>(const_cast< SpectralMeterView*>(this));
    return MeterView::qt_metacast(_clname);
}

int SpectralMeterView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = MeterView::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
