/****************************************************************************
** Meta object code from reading C++ file 'VUMeter.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../src/traverso/widgets/VUMeter.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'VUMeter.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_VUMeterRuler[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_VUMeterRuler[] = {
    "VUMeterRuler\0"
};

void VUMeterRuler::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData VUMeterRuler::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject VUMeterRuler::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_VUMeterRuler,
      qt_meta_data_VUMeterRuler, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &VUMeterRuler::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *VUMeterRuler::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *VUMeterRuler::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_VUMeterRuler))
        return static_cast<void*>(const_cast< VUMeterRuler*>(this));
    return QWidget::qt_metacast(_clname);
}

int VUMeterRuler::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_VUMeter[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
       9,    8,    8,    8, 0x08,
      35,    8,    8,    8, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_VUMeter[] = {
    "VUMeter\0\0peak_monitoring_stopped()\0"
    "peak_monitoring_started()\0"
};

void VUMeter::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        VUMeter *_t = static_cast<VUMeter *>(_o);
        switch (_id) {
        case 0: _t->peak_monitoring_stopped(); break;
        case 1: _t->peak_monitoring_started(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData VUMeter::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject VUMeter::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_VUMeter,
      qt_meta_data_VUMeter, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &VUMeter::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *VUMeter::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *VUMeter::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_VUMeter))
        return static_cast<void*>(const_cast< VUMeter*>(this));
    return QWidget::qt_metacast(_clname);
}

int VUMeter::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}
static const uint qt_meta_data_VUMeterOverLed[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      18,   16,   15,   15, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_VUMeterOverLed[] = {
    "VUMeterOverLed\0\0b\0set_active(bool)\0"
};

void VUMeterOverLed::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        VUMeterOverLed *_t = static_cast<VUMeterOverLed *>(_o);
        switch (_id) {
        case 0: _t->set_active((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData VUMeterOverLed::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject VUMeterOverLed::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_VUMeterOverLed,
      qt_meta_data_VUMeterOverLed, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &VUMeterOverLed::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *VUMeterOverLed::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *VUMeterOverLed::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_VUMeterOverLed))
        return static_cast<void*>(const_cast< VUMeterOverLed*>(this));
    return QWidget::qt_metacast(_clname);
}

int VUMeterOverLed::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}
static const uint qt_meta_data_VUMeterLevel[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      14,   13,   13,   13, 0x05,

 // slots: signature, parameters, type, tag, flags
      38,   13,   13,   13, 0x08,
      45,   13,   13,   13, 0x08,
      53,   13,   13,   13, 0x08,
      67,   13,   13,   13, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_VUMeterLevel[] = {
    "VUMeterLevel\0\0activate_over_led(bool)\0"
    "stop()\0start()\0update_peak()\0"
    "reset_peak_hold_value()\0"
};

void VUMeterLevel::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        VUMeterLevel *_t = static_cast<VUMeterLevel *>(_o);
        switch (_id) {
        case 0: _t->activate_over_led((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->stop(); break;
        case 2: _t->start(); break;
        case 3: _t->update_peak(); break;
        case 4: _t->reset_peak_hold_value(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData VUMeterLevel::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject VUMeterLevel::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_VUMeterLevel,
      qt_meta_data_VUMeterLevel, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &VUMeterLevel::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *VUMeterLevel::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *VUMeterLevel::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_VUMeterLevel))
        return static_cast<void*>(const_cast< VUMeterLevel*>(this));
    return QWidget::qt_metacast(_clname);
}

int VUMeterLevel::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
    return _id;
}

// SIGNAL 0
void VUMeterLevel::activate_over_led(bool _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
