/********************************************************************************
** Form generated from reading UI file 'RestoreProjectBackupDialog.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_RESTOREPROJECTBACKUPDIALOG_H
#define UI_RESTOREPROJECTBACKUPDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QTreeWidget>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_RestoreProjectBackupDialog
{
public:
    QVBoxLayout *vboxLayout;
    QLabel *label;
    QHBoxLayout *hboxLayout;
    QVBoxLayout *vboxLayout1;
    QLabel *label_2;
    QLabel *label_3;
    QVBoxLayout *vboxLayout2;
    QLabel *currentDateLable;
    QLabel *lastBackupLable;
    QTreeWidget *dateTreeWidget;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *RestoreProjectBackupDialog)
    {
        if (RestoreProjectBackupDialog->objectName().isEmpty())
            RestoreProjectBackupDialog->setObjectName(QString::fromUtf8("RestoreProjectBackupDialog"));
        RestoreProjectBackupDialog->resize(323, 358);
        vboxLayout = new QVBoxLayout(RestoreProjectBackupDialog);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        label = new QLabel(RestoreProjectBackupDialog);
        label->setObjectName(QString::fromUtf8("label"));

        vboxLayout->addWidget(label);

        hboxLayout = new QHBoxLayout();
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        vboxLayout1 = new QVBoxLayout();
        vboxLayout1->setObjectName(QString::fromUtf8("vboxLayout1"));
        label_2 = new QLabel(RestoreProjectBackupDialog);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        vboxLayout1->addWidget(label_2);

        label_3 = new QLabel(RestoreProjectBackupDialog);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        vboxLayout1->addWidget(label_3);


        hboxLayout->addLayout(vboxLayout1);

        vboxLayout2 = new QVBoxLayout();
        vboxLayout2->setObjectName(QString::fromUtf8("vboxLayout2"));
        currentDateLable = new QLabel(RestoreProjectBackupDialog);
        currentDateLable->setObjectName(QString::fromUtf8("currentDateLable"));

        vboxLayout2->addWidget(currentDateLable);

        lastBackupLable = new QLabel(RestoreProjectBackupDialog);
        lastBackupLable->setObjectName(QString::fromUtf8("lastBackupLable"));

        vboxLayout2->addWidget(lastBackupLable);


        hboxLayout->addLayout(vboxLayout2);


        vboxLayout->addLayout(hboxLayout);

        dateTreeWidget = new QTreeWidget(RestoreProjectBackupDialog);
        dateTreeWidget->setObjectName(QString::fromUtf8("dateTreeWidget"));

        vboxLayout->addWidget(dateTreeWidget);

        buttonBox = new QDialogButtonBox(RestoreProjectBackupDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::NoButton|QDialogButtonBox::Ok);

        vboxLayout->addWidget(buttonBox);


        retranslateUi(RestoreProjectBackupDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), RestoreProjectBackupDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), RestoreProjectBackupDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(RestoreProjectBackupDialog);
    } // setupUi

    void retranslateUi(QDialog *RestoreProjectBackupDialog)
    {
        RestoreProjectBackupDialog->setWindowTitle(QApplication::translate("RestoreProjectBackupDialog", "Restore from backup ", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("RestoreProjectBackupDialog", "Set the date to restore the selected backup.", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("RestoreProjectBackupDialog", "Current date and time:", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("RestoreProjectBackupDialog", "Last backup:", 0, QApplication::UnicodeUTF8));
        currentDateLable->setText(QApplication::translate("RestoreProjectBackupDialog", "-", 0, QApplication::UnicodeUTF8));
        lastBackupLable->setText(QApplication::translate("RestoreProjectBackupDialog", "-", 0, QApplication::UnicodeUTF8));
        QTreeWidgetItem *___qtreewidgetitem = dateTreeWidget->headerItem();
        ___qtreewidgetitem->setText(1, QApplication::translate("RestoreProjectBackupDialog", "Time", 0, QApplication::UnicodeUTF8));
        ___qtreewidgetitem->setText(0, QApplication::translate("RestoreProjectBackupDialog", "Date", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class RestoreProjectBackupDialog: public Ui_RestoreProjectBackupDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_RESTOREPROJECTBACKUPDIALOG_H
