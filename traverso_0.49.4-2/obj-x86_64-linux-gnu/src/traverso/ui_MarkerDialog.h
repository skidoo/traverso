/********************************************************************************
** Form generated from reading UI file 'MarkerDialog.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MARKERDIALOG_H
#define UI_MARKERDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QDialog>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QTabWidget>
#include <QtGui/QToolButton>
#include <QtGui/QTreeWidget>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MarkerDialog
{
public:
    QVBoxLayout *vboxLayout;
    QTreeWidget *markersTreeWidget;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QLineEdit *lineEditIsrc;
    QLabel *label_6;
    QToolButton *toolButtonPEmphAll;
    QToolButton *toolButtonCopyAll;
    QCheckBox *checkBoxPreEmph;
    QCheckBox *checkBoxCopy;
    QLabel *label_9;
    QLineEdit *lineEditPosition;
    QTabWidget *tabWidget;
    QWidget *tab;
    QGridLayout *gridLayout1;
    QLabel *label;
    QLineEdit *lineEditTitle;
    QToolButton *toolButtonTitleAll;
    QLabel *label_2;
    QLineEdit *lineEditPerformer;
    QToolButton *toolButtonPerformerAll;
    QLabel *label_3;
    QLineEdit *lineEditComposer;
    QToolButton *toolButtonComposerAll;
    QWidget *tab_2;
    QGridLayout *gridLayout2;
    QLabel *label_4;
    QLineEdit *lineEditSongwriter;
    QLabel *label_5;
    QLineEdit *lineEditArranger;
    QLabel *label_7;
    QLineEdit *lineEditMessage;
    QToolButton *toolButtonMessageAll;
    QToolButton *toolButtonArrangerAll;
    QToolButton *toolButtonSongWriterAll;
    QHBoxLayout *hboxLayout;
    QPushButton *pushButtonRemove;
    QPushButton *pushButtonExport;
    QSpacerItem *spacerItem;
    QPushButton *pushButtonOk;
    QPushButton *pushButtonCancel;

    void setupUi(QDialog *MarkerDialog)
    {
        if (MarkerDialog->objectName().isEmpty())
            MarkerDialog->setObjectName(QString::fromUtf8("MarkerDialog"));
        MarkerDialog->resize(400, 558);
        vboxLayout = new QVBoxLayout(MarkerDialog);
#ifndef Q_OS_MAC
        vboxLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        markersTreeWidget = new QTreeWidget(MarkerDialog);
        markersTreeWidget->setObjectName(QString::fromUtf8("markersTreeWidget"));
        markersTreeWidget->setEditTriggers(QAbstractItemView::AnyKeyPressed|QAbstractItemView::DoubleClicked|QAbstractItemView::EditKeyPressed);
        markersTreeWidget->setRootIsDecorated(false);

        vboxLayout->addWidget(markersTreeWidget);

        groupBox = new QGroupBox(MarkerDialog);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        gridLayout = new QGridLayout(groupBox);
#ifndef Q_OS_MAC
        gridLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        gridLayout->setContentsMargins(9, 9, 9, 9);
#endif
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        lineEditIsrc = new QLineEdit(groupBox);
        lineEditIsrc->setObjectName(QString::fromUtf8("lineEditIsrc"));

        gridLayout->addWidget(lineEditIsrc, 1, 1, 1, 2);

        label_6 = new QLabel(groupBox);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        gridLayout->addWidget(label_6, 1, 0, 1, 1);

        toolButtonPEmphAll = new QToolButton(groupBox);
        toolButtonPEmphAll->setObjectName(QString::fromUtf8("toolButtonPEmphAll"));
        QIcon icon;
        icon.addFile(QString::fromUtf8("../../../../../../../usr/share/icons/crystalsvg/16x16/actions/tab_duplicate.png"), QSize(), QIcon::Normal, QIcon::Off);
        toolButtonPEmphAll->setIcon(icon);

        gridLayout->addWidget(toolButtonPEmphAll, 3, 2, 1, 1);

        toolButtonCopyAll = new QToolButton(groupBox);
        toolButtonCopyAll->setObjectName(QString::fromUtf8("toolButtonCopyAll"));
        toolButtonCopyAll->setIcon(icon);

        gridLayout->addWidget(toolButtonCopyAll, 2, 2, 1, 1);

        checkBoxPreEmph = new QCheckBox(groupBox);
        checkBoxPreEmph->setObjectName(QString::fromUtf8("checkBoxPreEmph"));

        gridLayout->addWidget(checkBoxPreEmph, 3, 0, 1, 2);

        checkBoxCopy = new QCheckBox(groupBox);
        checkBoxCopy->setObjectName(QString::fromUtf8("checkBoxCopy"));

        gridLayout->addWidget(checkBoxCopy, 2, 0, 1, 2);

        label_9 = new QLabel(groupBox);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        gridLayout->addWidget(label_9, 0, 0, 1, 1);

        lineEditPosition = new QLineEdit(groupBox);
        lineEditPosition->setObjectName(QString::fromUtf8("lineEditPosition"));

        gridLayout->addWidget(lineEditPosition, 0, 1, 1, 2);


        vboxLayout->addWidget(groupBox);

        tabWidget = new QTabWidget(MarkerDialog);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        gridLayout1 = new QGridLayout(tab);
#ifndef Q_OS_MAC
        gridLayout1->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        gridLayout1->setContentsMargins(9, 9, 9, 9);
#endif
        gridLayout1->setObjectName(QString::fromUtf8("gridLayout1"));
        label = new QLabel(tab);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout1->addWidget(label, 0, 0, 1, 1);

        lineEditTitle = new QLineEdit(tab);
        lineEditTitle->setObjectName(QString::fromUtf8("lineEditTitle"));

        gridLayout1->addWidget(lineEditTitle, 0, 1, 1, 1);

        toolButtonTitleAll = new QToolButton(tab);
        toolButtonTitleAll->setObjectName(QString::fromUtf8("toolButtonTitleAll"));
        toolButtonTitleAll->setIcon(icon);

        gridLayout1->addWidget(toolButtonTitleAll, 0, 2, 1, 1);

        label_2 = new QLabel(tab);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout1->addWidget(label_2, 1, 0, 1, 1);

        lineEditPerformer = new QLineEdit(tab);
        lineEditPerformer->setObjectName(QString::fromUtf8("lineEditPerformer"));

        gridLayout1->addWidget(lineEditPerformer, 1, 1, 1, 1);

        toolButtonPerformerAll = new QToolButton(tab);
        toolButtonPerformerAll->setObjectName(QString::fromUtf8("toolButtonPerformerAll"));
        toolButtonPerformerAll->setIcon(icon);

        gridLayout1->addWidget(toolButtonPerformerAll, 1, 2, 1, 1);

        label_3 = new QLabel(tab);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout1->addWidget(label_3, 2, 0, 1, 1);

        lineEditComposer = new QLineEdit(tab);
        lineEditComposer->setObjectName(QString::fromUtf8("lineEditComposer"));

        gridLayout1->addWidget(lineEditComposer, 2, 1, 1, 1);

        toolButtonComposerAll = new QToolButton(tab);
        toolButtonComposerAll->setObjectName(QString::fromUtf8("toolButtonComposerAll"));
        toolButtonComposerAll->setIcon(icon);

        gridLayout1->addWidget(toolButtonComposerAll, 2, 2, 1, 1);

        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        gridLayout2 = new QGridLayout(tab_2);
#ifndef Q_OS_MAC
        gridLayout2->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        gridLayout2->setContentsMargins(9, 9, 9, 9);
#endif
        gridLayout2->setObjectName(QString::fromUtf8("gridLayout2"));
        label_4 = new QLabel(tab_2);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout2->addWidget(label_4, 0, 0, 1, 1);

        lineEditSongwriter = new QLineEdit(tab_2);
        lineEditSongwriter->setObjectName(QString::fromUtf8("lineEditSongwriter"));

        gridLayout2->addWidget(lineEditSongwriter, 0, 1, 1, 1);

        label_5 = new QLabel(tab_2);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        gridLayout2->addWidget(label_5, 1, 0, 1, 1);

        lineEditArranger = new QLineEdit(tab_2);
        lineEditArranger->setObjectName(QString::fromUtf8("lineEditArranger"));

        gridLayout2->addWidget(lineEditArranger, 1, 1, 1, 1);

        label_7 = new QLabel(tab_2);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        gridLayout2->addWidget(label_7, 2, 0, 1, 1);

        lineEditMessage = new QLineEdit(tab_2);
        lineEditMessage->setObjectName(QString::fromUtf8("lineEditMessage"));

        gridLayout2->addWidget(lineEditMessage, 2, 1, 1, 1);

        toolButtonMessageAll = new QToolButton(tab_2);
        toolButtonMessageAll->setObjectName(QString::fromUtf8("toolButtonMessageAll"));
        toolButtonMessageAll->setIcon(icon);

        gridLayout2->addWidget(toolButtonMessageAll, 2, 2, 1, 1);

        toolButtonArrangerAll = new QToolButton(tab_2);
        toolButtonArrangerAll->setObjectName(QString::fromUtf8("toolButtonArrangerAll"));
        toolButtonArrangerAll->setIcon(icon);

        gridLayout2->addWidget(toolButtonArrangerAll, 1, 2, 1, 1);

        toolButtonSongWriterAll = new QToolButton(tab_2);
        toolButtonSongWriterAll->setObjectName(QString::fromUtf8("toolButtonSongWriterAll"));
        toolButtonSongWriterAll->setIcon(icon);

        gridLayout2->addWidget(toolButtonSongWriterAll, 0, 2, 1, 1);

        tabWidget->addTab(tab_2, QString());

        vboxLayout->addWidget(tabWidget);

        hboxLayout = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout->setSpacing(6);
#endif
        hboxLayout->setContentsMargins(0, 0, 0, 0);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        pushButtonRemove = new QPushButton(MarkerDialog);
        pushButtonRemove->setObjectName(QString::fromUtf8("pushButtonRemove"));

        hboxLayout->addWidget(pushButtonRemove);

        pushButtonExport = new QPushButton(MarkerDialog);
        pushButtonExport->setObjectName(QString::fromUtf8("pushButtonExport"));

        hboxLayout->addWidget(pushButtonExport);

        spacerItem = new QSpacerItem(281, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacerItem);

        pushButtonOk = new QPushButton(MarkerDialog);
        pushButtonOk->setObjectName(QString::fromUtf8("pushButtonOk"));
        pushButtonOk->setAutoDefault(false);

        hboxLayout->addWidget(pushButtonOk);

        pushButtonCancel = new QPushButton(MarkerDialog);
        pushButtonCancel->setObjectName(QString::fromUtf8("pushButtonCancel"));

        hboxLayout->addWidget(pushButtonCancel);


        vboxLayout->addLayout(hboxLayout);

        QWidget::setTabOrder(markersTreeWidget, checkBoxCopy);
        QWidget::setTabOrder(checkBoxCopy, toolButtonCopyAll);
        QWidget::setTabOrder(toolButtonCopyAll, checkBoxPreEmph);
        QWidget::setTabOrder(checkBoxPreEmph, toolButtonPEmphAll);
        QWidget::setTabOrder(toolButtonPEmphAll, lineEditIsrc);
        QWidget::setTabOrder(lineEditIsrc, tabWidget);
        QWidget::setTabOrder(tabWidget, lineEditTitle);
        QWidget::setTabOrder(lineEditTitle, toolButtonTitleAll);
        QWidget::setTabOrder(toolButtonTitleAll, lineEditPerformer);
        QWidget::setTabOrder(lineEditPerformer, toolButtonPerformerAll);
        QWidget::setTabOrder(toolButtonPerformerAll, lineEditComposer);
        QWidget::setTabOrder(lineEditComposer, toolButtonComposerAll);
        QWidget::setTabOrder(toolButtonComposerAll, lineEditSongwriter);
        QWidget::setTabOrder(lineEditSongwriter, lineEditArranger);
        QWidget::setTabOrder(lineEditArranger, toolButtonArrangerAll);
        QWidget::setTabOrder(toolButtonArrangerAll, lineEditMessage);
        QWidget::setTabOrder(lineEditMessage, toolButtonMessageAll);
        QWidget::setTabOrder(toolButtonMessageAll, pushButtonOk);

        retranslateUi(MarkerDialog);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MarkerDialog);
    } // setupUi

    void retranslateUi(QDialog *MarkerDialog)
    {
        MarkerDialog->setWindowTitle(QApplication::translate("MarkerDialog", "Markers", 0, QApplication::UnicodeUTF8));
        QTreeWidgetItem *___qtreewidgetitem = markersTreeWidget->headerItem();
        ___qtreewidgetitem->setText(1, QApplication::translate("MarkerDialog", "Title", 0, QApplication::UnicodeUTF8));
        ___qtreewidgetitem->setText(0, QApplication::translate("MarkerDialog", "Position", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("MarkerDialog", "Options", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("MarkerDialog", "ISRC:", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        toolButtonPEmphAll->setToolTip(QApplication::translate("MarkerDialog", "Apply to all", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        toolButtonPEmphAll->setText(QApplication::translate("MarkerDialog", "...", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        toolButtonCopyAll->setToolTip(QApplication::translate("MarkerDialog", "Apply to all", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        toolButtonCopyAll->setText(QApplication::translate("MarkerDialog", "...", 0, QApplication::UnicodeUTF8));
        checkBoxPreEmph->setText(QApplication::translate("MarkerDialog", "Pre-Emphasis", 0, QApplication::UnicodeUTF8));
        checkBoxCopy->setText(QApplication::translate("MarkerDialog", "Copy protection", 0, QApplication::UnicodeUTF8));
        label_9->setText(QApplication::translate("MarkerDialog", "Position: (MM:SS:75ths)", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        tabWidget->setToolTip(QString());
#endif // QT_NO_TOOLTIP
        label->setText(QApplication::translate("MarkerDialog", "Title:", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        toolButtonTitleAll->setToolTip(QApplication::translate("MarkerDialog", "Apply to all", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        toolButtonTitleAll->setText(QApplication::translate("MarkerDialog", "...", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("MarkerDialog", "Performer:", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        toolButtonPerformerAll->setToolTip(QApplication::translate("MarkerDialog", "Apply to all", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        toolButtonPerformerAll->setText(QApplication::translate("MarkerDialog", "...", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("MarkerDialog", "Composer:", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        toolButtonComposerAll->setToolTip(QApplication::translate("MarkerDialog", "Apply to all", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        toolButtonComposerAll->setText(QApplication::translate("MarkerDialog", "...", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("MarkerDialog", "CD-Text", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("MarkerDialog", "Songwriter", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("MarkerDialog", "Arranger:", 0, QApplication::UnicodeUTF8));
        label_7->setText(QApplication::translate("MarkerDialog", "Message:", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        toolButtonMessageAll->setToolTip(QApplication::translate("MarkerDialog", "Apply to all", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        toolButtonMessageAll->setText(QApplication::translate("MarkerDialog", "...", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        toolButtonArrangerAll->setToolTip(QApplication::translate("MarkerDialog", "Apply to all", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        toolButtonArrangerAll->setText(QApplication::translate("MarkerDialog", "...", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        toolButtonSongWriterAll->setToolTip(QApplication::translate("MarkerDialog", "Apply to all", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        toolButtonSongWriterAll->setText(QApplication::translate("MarkerDialog", "...", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("MarkerDialog", "CD-Text optional", 0, QApplication::UnicodeUTF8));
        pushButtonRemove->setText(QApplication::translate("MarkerDialog", "&Remove", 0, QApplication::UnicodeUTF8));
        pushButtonExport->setText(QApplication::translate("MarkerDialog", "&Export", 0, QApplication::UnicodeUTF8));
        pushButtonOk->setText(QApplication::translate("MarkerDialog", "&Ok", 0, QApplication::UnicodeUTF8));
        pushButtonCancel->setText(QApplication::translate("MarkerDialog", "&Cancel", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MarkerDialog: public Ui_MarkerDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MARKERDIALOG_H
