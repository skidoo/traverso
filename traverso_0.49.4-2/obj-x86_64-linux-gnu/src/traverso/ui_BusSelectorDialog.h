/********************************************************************************
** Form generated from reading UI file 'BusSelectorDialog.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_BUSSELECTORDIALOG_H
#define UI_BUSSELECTORDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QListWidget>
#include <QtGui/QRadioButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_BusSelectorDialog
{
public:
    QVBoxLayout *vboxLayout;
    QHBoxLayout *hboxLayout;
    QLabel *label_3;
    QSpacerItem *spacerItem;
    QComboBox *trackComboBox;
    QGroupBox *groupBox_2;
    QHBoxLayout *hboxLayout1;
    QListWidget *busesListWidget;
    QVBoxLayout *vboxLayout1;
    QLabel *label_2;
    QRadioButton *radioBoth;
    QRadioButton *radioLeftOnly;
    QRadioButton *radioRightOnly;
    QSpacerItem *spacerItem1;
    QGroupBox *playbackBusesGroupBox;
    QHBoxLayout *hboxLayout2;
    QListWidget *busesListWidgetPlayback;
    QVBoxLayout *vboxLayout2;
    QLabel *label_5;
    QRadioButton *radioBothPlayback;
    QRadioButton *radioLeftOnlyPlayback;
    QRadioButton *radioRightOnlyPlayback;
    QSpacerItem *spacerItem2;
    QDialogButtonBox *buttonBox;

    void setupUi(QWidget *BusSelectorDialog)
    {
        if (BusSelectorDialog->objectName().isEmpty())
            BusSelectorDialog->setObjectName(QString::fromUtf8("BusSelectorDialog"));
        BusSelectorDialog->resize(321, 389);
        vboxLayout = new QVBoxLayout(BusSelectorDialog);
#ifndef Q_OS_MAC
        vboxLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        hboxLayout = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout->setContentsMargins(0, 0, 0, 0);
#endif
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        label_3 = new QLabel(BusSelectorDialog);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        QSizePolicy sizePolicy(static_cast<QSizePolicy::Policy>(0), static_cast<QSizePolicy::Policy>(0));
        sizePolicy.setHorizontalStretch(2);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(label_3->sizePolicy().hasHeightForWidth());
        label_3->setSizePolicy(sizePolicy);

        hboxLayout->addWidget(label_3);

        spacerItem = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacerItem);

        trackComboBox = new QComboBox(BusSelectorDialog);
        trackComboBox->setObjectName(QString::fromUtf8("trackComboBox"));
        QSizePolicy sizePolicy1(static_cast<QSizePolicy::Policy>(5), static_cast<QSizePolicy::Policy>(0));
        sizePolicy1.setHorizontalStretch(4);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(trackComboBox->sizePolicy().hasHeightForWidth());
        trackComboBox->setSizePolicy(sizePolicy1);

        hboxLayout->addWidget(trackComboBox);


        vboxLayout->addLayout(hboxLayout);

        groupBox_2 = new QGroupBox(BusSelectorDialog);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        hboxLayout1 = new QHBoxLayout(groupBox_2);
#ifndef Q_OS_MAC
        hboxLayout1->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout1->setContentsMargins(9, 9, 9, 9);
#endif
        hboxLayout1->setObjectName(QString::fromUtf8("hboxLayout1"));
        busesListWidget = new QListWidget(groupBox_2);
        busesListWidget->setObjectName(QString::fromUtf8("busesListWidget"));

        hboxLayout1->addWidget(busesListWidget);

        vboxLayout1 = new QVBoxLayout();
#ifndef Q_OS_MAC
        vboxLayout1->setSpacing(6);
#endif
        vboxLayout1->setContentsMargins(0, 0, 0, 0);
        vboxLayout1->setObjectName(QString::fromUtf8("vboxLayout1"));
        label_2 = new QLabel(groupBox_2);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        vboxLayout1->addWidget(label_2);

        radioBoth = new QRadioButton(groupBox_2);
        radioBoth->setObjectName(QString::fromUtf8("radioBoth"));
        radioBoth->setChecked(true);

        vboxLayout1->addWidget(radioBoth);

        radioLeftOnly = new QRadioButton(groupBox_2);
        radioLeftOnly->setObjectName(QString::fromUtf8("radioLeftOnly"));

        vboxLayout1->addWidget(radioLeftOnly);

        radioRightOnly = new QRadioButton(groupBox_2);
        radioRightOnly->setObjectName(QString::fromUtf8("radioRightOnly"));

        vboxLayout1->addWidget(radioRightOnly);

        spacerItem1 = new QSpacerItem(20, 1, QSizePolicy::Minimum, QSizePolicy::Expanding);

        vboxLayout1->addItem(spacerItem1);


        hboxLayout1->addLayout(vboxLayout1);


        vboxLayout->addWidget(groupBox_2);

        playbackBusesGroupBox = new QGroupBox(BusSelectorDialog);
        playbackBusesGroupBox->setObjectName(QString::fromUtf8("playbackBusesGroupBox"));
        hboxLayout2 = new QHBoxLayout(playbackBusesGroupBox);
#ifndef Q_OS_MAC
        hboxLayout2->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout2->setContentsMargins(9, 9, 9, 9);
#endif
        hboxLayout2->setObjectName(QString::fromUtf8("hboxLayout2"));
        busesListWidgetPlayback = new QListWidget(playbackBusesGroupBox);
        busesListWidgetPlayback->setObjectName(QString::fromUtf8("busesListWidgetPlayback"));

        hboxLayout2->addWidget(busesListWidgetPlayback);

        vboxLayout2 = new QVBoxLayout();
#ifndef Q_OS_MAC
        vboxLayout2->setSpacing(6);
#endif
        vboxLayout2->setContentsMargins(0, 0, 0, 0);
        vboxLayout2->setObjectName(QString::fromUtf8("vboxLayout2"));
        label_5 = new QLabel(playbackBusesGroupBox);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        vboxLayout2->addWidget(label_5);

        radioBothPlayback = new QRadioButton(playbackBusesGroupBox);
        radioBothPlayback->setObjectName(QString::fromUtf8("radioBothPlayback"));
        radioBothPlayback->setChecked(true);

        vboxLayout2->addWidget(radioBothPlayback);

        radioLeftOnlyPlayback = new QRadioButton(playbackBusesGroupBox);
        radioLeftOnlyPlayback->setObjectName(QString::fromUtf8("radioLeftOnlyPlayback"));

        vboxLayout2->addWidget(radioLeftOnlyPlayback);

        radioRightOnlyPlayback = new QRadioButton(playbackBusesGroupBox);
        radioRightOnlyPlayback->setObjectName(QString::fromUtf8("radioRightOnlyPlayback"));

        vboxLayout2->addWidget(radioRightOnlyPlayback);

        spacerItem2 = new QSpacerItem(20, 1, QSizePolicy::Minimum, QSizePolicy::Expanding);

        vboxLayout2->addItem(spacerItem2);


        hboxLayout2->addLayout(vboxLayout2);


        vboxLayout->addWidget(playbackBusesGroupBox);

        buttonBox = new QDialogButtonBox(BusSelectorDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Close|QDialogButtonBox::NoButton|QDialogButtonBox::Ok);

        vboxLayout->addWidget(buttonBox);


        retranslateUi(BusSelectorDialog);

        QMetaObject::connectSlotsByName(BusSelectorDialog);
    } // setupUi

    void retranslateUi(QWidget *BusSelectorDialog)
    {
        BusSelectorDialog->setWindowTitle(QApplication::translate("BusSelectorDialog", "Bus Selector", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("BusSelectorDialog", "Track", 0, QApplication::UnicodeUTF8));
        groupBox_2->setTitle(QApplication::translate("BusSelectorDialog", "Capture Buses", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("BusSelectorDialog", "Channels", 0, QApplication::UnicodeUTF8));
        radioBoth->setText(QApplication::translate("BusSelectorDialog", "Both", 0, QApplication::UnicodeUTF8));
        radioLeftOnly->setText(QApplication::translate("BusSelectorDialog", "Left", 0, QApplication::UnicodeUTF8));
        radioRightOnly->setText(QApplication::translate("BusSelectorDialog", "Right", 0, QApplication::UnicodeUTF8));
        playbackBusesGroupBox->setTitle(QApplication::translate("BusSelectorDialog", "Playback Buses", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("BusSelectorDialog", "Channels", 0, QApplication::UnicodeUTF8));
        radioBothPlayback->setText(QApplication::translate("BusSelectorDialog", "Both", 0, QApplication::UnicodeUTF8));
        radioLeftOnlyPlayback->setText(QApplication::translate("BusSelectorDialog", "Left", 0, QApplication::UnicodeUTF8));
        radioRightOnlyPlayback->setText(QApplication::translate("BusSelectorDialog", "Right", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class BusSelectorDialog: public Ui_BusSelectorDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_BUSSELECTORDIALOG_H
