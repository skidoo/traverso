/********************************************************************************
** Form generated from reading UI file 'OpenProjectDialog.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_OPENPROJECTDIALOG_H
#define UI_OPENPROJECTDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QTreeWidget>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_OpenProjectDialog
{
public:
    QVBoxLayout *vboxLayout;
    QTreeWidget *projectListView;
    QGroupBox *groupBox_2;
    QVBoxLayout *vboxLayout1;
    QHBoxLayout *hboxLayout;
    QLineEdit *selectedProjectName;
    QPushButton *loadProjectButton;
    QHBoxLayout *hboxLayout1;
    QPushButton *deleteProjectbutton;
    QSpacerItem *spacerItem;
    QHBoxLayout *hboxLayout2;
    QPushButton *projectDirSelectButton;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *OpenProjectDialog)
    {
        if (OpenProjectDialog->objectName().isEmpty())
            OpenProjectDialog->setObjectName(QString::fromUtf8("OpenProjectDialog"));
        OpenProjectDialog->resize(269, 356);
        vboxLayout = new QVBoxLayout(OpenProjectDialog);
#ifndef Q_OS_MAC
        vboxLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        projectListView = new QTreeWidget(OpenProjectDialog);
        projectListView->setObjectName(QString::fromUtf8("projectListView"));

        vboxLayout->addWidget(projectListView);

        groupBox_2 = new QGroupBox(OpenProjectDialog);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setMinimumSize(QSize(16, 90));
        vboxLayout1 = new QVBoxLayout(groupBox_2);
#ifndef Q_OS_MAC
        vboxLayout1->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout1->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout1->setObjectName(QString::fromUtf8("vboxLayout1"));
        hboxLayout = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout->setContentsMargins(0, 0, 0, 0);
#endif
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        selectedProjectName = new QLineEdit(groupBox_2);
        selectedProjectName->setObjectName(QString::fromUtf8("selectedProjectName"));
        selectedProjectName->setEnabled(false);
        QSizePolicy sizePolicy(static_cast<QSizePolicy::Policy>(7), static_cast<QSizePolicy::Policy>(0));
        sizePolicy.setHorizontalStretch(1);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(selectedProjectName->sizePolicy().hasHeightForWidth());
        selectedProjectName->setSizePolicy(sizePolicy);
        selectedProjectName->setMinimumSize(QSize(50, 0));
        selectedProjectName->setMouseTracking(true);
        selectedProjectName->setReadOnly(false);

        hboxLayout->addWidget(selectedProjectName);

        loadProjectButton = new QPushButton(groupBox_2);
        loadProjectButton->setObjectName(QString::fromUtf8("loadProjectButton"));

        hboxLayout->addWidget(loadProjectButton);


        vboxLayout1->addLayout(hboxLayout);

        hboxLayout1 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout1->setSpacing(6);
#endif
        hboxLayout1->setContentsMargins(0, 0, 0, 0);
        hboxLayout1->setObjectName(QString::fromUtf8("hboxLayout1"));
        deleteProjectbutton = new QPushButton(groupBox_2);
        deleteProjectbutton->setObjectName(QString::fromUtf8("deleteProjectbutton"));

        hboxLayout1->addWidget(deleteProjectbutton);

        spacerItem = new QSpacerItem(61, 27, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout1->addItem(spacerItem);


        vboxLayout1->addLayout(hboxLayout1);


        vboxLayout->addWidget(groupBox_2);

        hboxLayout2 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout2->setSpacing(6);
#endif
        hboxLayout2->setContentsMargins(0, 0, 0, 0);
        hboxLayout2->setObjectName(QString::fromUtf8("hboxLayout2"));
        projectDirSelectButton = new QPushButton(OpenProjectDialog);
        projectDirSelectButton->setObjectName(QString::fromUtf8("projectDirSelectButton"));

        hboxLayout2->addWidget(projectDirSelectButton);

        buttonBox = new QDialogButtonBox(OpenProjectDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Close);

        hboxLayout2->addWidget(buttonBox);


        vboxLayout->addLayout(hboxLayout2);


        retranslateUi(OpenProjectDialog);
        QObject::connect(buttonBox, SIGNAL(clicked(QAbstractButton*)), OpenProjectDialog, SLOT(accept()));
        QObject::connect(projectListView, SIGNAL(activated(QModelIndex)), loadProjectButton, SLOT(click()));

        QMetaObject::connectSlotsByName(OpenProjectDialog);
    } // setupUi

    void retranslateUi(QDialog *OpenProjectDialog)
    {
        OpenProjectDialog->setWindowTitle(QApplication::translate("OpenProjectDialog", "Open Project", 0, QApplication::UnicodeUTF8));
        groupBox_2->setTitle(QApplication::translate("OpenProjectDialog", "Selected Project", 0, QApplication::UnicodeUTF8));
        loadProjectButton->setText(QApplication::translate("OpenProjectDialog", "Load", 0, QApplication::UnicodeUTF8));
        deleteProjectbutton->setText(QApplication::translate("OpenProjectDialog", "Delete", 0, QApplication::UnicodeUTF8));
        projectDirSelectButton->setText(QApplication::translate("OpenProjectDialog", "Select Project Dir", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class OpenProjectDialog: public Ui_OpenProjectDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_OPENPROJECTDIALOG_H
