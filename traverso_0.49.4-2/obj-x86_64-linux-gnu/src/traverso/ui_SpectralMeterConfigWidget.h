/********************************************************************************
** Form generated from reading UI file 'SpectralMeterConfigWidget.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SPECTRALMETERCONFIGWIDGET_H
#define UI_SPECTRALMETERCONFIGWIDGET_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QComboBox>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QSpinBox>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SpectralMeterConfigWidget
{
public:
    QVBoxLayout *vboxLayout;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QCheckBox *checkBoxAverage;
    QSpinBox *spinBoxNumBands;
    QLabel *label_2;
    QSpinBox *spinBoxLowerDb;
    QLabel *label_6;
    QSpinBox *spinBoxUpperDb;
    QLabel *label_7;
    QSpinBox *spinBoxLowerFreq;
    QLabel *label;
    QSpinBox *spinBoxUpperFreq;
    QLabel *label_3;
    QGroupBox *groupBoxAdvanced;
    QGridLayout *gridLayout1;
    QLabel *label_4;
    QComboBox *comboBoxFftSize;
    QComboBox *comboBoxWindowing;
    QLabel *label_5;
    QHBoxLayout *hboxLayout;
    QPushButton *buttonAdvanced;
    QSpacerItem *spacerItem;
    QPushButton *buttonApply;
    QPushButton *buttonClose;

    void setupUi(QWidget *SpectralMeterConfigWidget)
    {
        if (SpectralMeterConfigWidget->objectName().isEmpty())
            SpectralMeterConfigWidget->setObjectName(QString::fromUtf8("SpectralMeterConfigWidget"));
        SpectralMeterConfigWidget->resize(377, 364);
        vboxLayout = new QVBoxLayout(SpectralMeterConfigWidget);
#ifndef Q_OS_MAC
        vboxLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        groupBox = new QGroupBox(SpectralMeterConfigWidget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        gridLayout = new QGridLayout(groupBox);
#ifndef Q_OS_MAC
        gridLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        gridLayout->setContentsMargins(9, 9, 9, 9);
#endif
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        checkBoxAverage = new QCheckBox(groupBox);
        checkBoxAverage->setObjectName(QString::fromUtf8("checkBoxAverage"));

        gridLayout->addWidget(checkBoxAverage, 5, 0, 1, 2);

        spinBoxNumBands = new QSpinBox(groupBox);
        spinBoxNumBands->setObjectName(QString::fromUtf8("spinBoxNumBands"));
        spinBoxNumBands->setMaximum(2048);
        spinBoxNumBands->setMinimum(4);
        spinBoxNumBands->setValue(16);

        gridLayout->addWidget(spinBoxNumBands, 4, 1, 1, 1);

        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout->addWidget(label_2, 4, 0, 1, 1);

        spinBoxLowerDb = new QSpinBox(groupBox);
        spinBoxLowerDb->setObjectName(QString::fromUtf8("spinBoxLowerDb"));
        spinBoxLowerDb->setMaximum(0);
        spinBoxLowerDb->setMinimum(-140);
        spinBoxLowerDb->setValue(-140);

        gridLayout->addWidget(spinBoxLowerDb, 3, 1, 1, 1);

        label_6 = new QLabel(groupBox);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        gridLayout->addWidget(label_6, 3, 0, 1, 1);

        spinBoxUpperDb = new QSpinBox(groupBox);
        spinBoxUpperDb->setObjectName(QString::fromUtf8("spinBoxUpperDb"));
        spinBoxUpperDb->setMaximum(12);
        spinBoxUpperDb->setMinimum(-24);

        gridLayout->addWidget(spinBoxUpperDb, 2, 1, 1, 1);

        label_7 = new QLabel(groupBox);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        gridLayout->addWidget(label_7, 2, 0, 1, 1);

        spinBoxLowerFreq = new QSpinBox(groupBox);
        spinBoxLowerFreq->setObjectName(QString::fromUtf8("spinBoxLowerFreq"));
        spinBoxLowerFreq->setMaximum(20000);
        spinBoxLowerFreq->setMinimum(1);
        spinBoxLowerFreq->setValue(20);

        gridLayout->addWidget(spinBoxLowerFreq, 1, 1, 1, 1);

        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 1, 0, 1, 1);

        spinBoxUpperFreq = new QSpinBox(groupBox);
        spinBoxUpperFreq->setObjectName(QString::fromUtf8("spinBoxUpperFreq"));
        spinBoxUpperFreq->setMaximum(96000);
        spinBoxUpperFreq->setMinimum(100);
        spinBoxUpperFreq->setValue(20000);

        gridLayout->addWidget(spinBoxUpperFreq, 0, 1, 1, 1);

        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout->addWidget(label_3, 0, 0, 1, 1);


        vboxLayout->addWidget(groupBox);

        groupBoxAdvanced = new QGroupBox(SpectralMeterConfigWidget);
        groupBoxAdvanced->setObjectName(QString::fromUtf8("groupBoxAdvanced"));
        gridLayout1 = new QGridLayout(groupBoxAdvanced);
#ifndef Q_OS_MAC
        gridLayout1->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        gridLayout1->setContentsMargins(9, 9, 9, 9);
#endif
        gridLayout1->setObjectName(QString::fromUtf8("gridLayout1"));
        label_4 = new QLabel(groupBoxAdvanced);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout1->addWidget(label_4, 0, 0, 1, 1);

        comboBoxFftSize = new QComboBox(groupBoxAdvanced);
        comboBoxFftSize->setObjectName(QString::fromUtf8("comboBoxFftSize"));

        gridLayout1->addWidget(comboBoxFftSize, 0, 1, 1, 1);

        comboBoxWindowing = new QComboBox(groupBoxAdvanced);
        comboBoxWindowing->setObjectName(QString::fromUtf8("comboBoxWindowing"));

        gridLayout1->addWidget(comboBoxWindowing, 1, 1, 1, 1);

        label_5 = new QLabel(groupBoxAdvanced);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        gridLayout1->addWidget(label_5, 1, 0, 1, 1);


        vboxLayout->addWidget(groupBoxAdvanced);

        hboxLayout = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout->setSpacing(6);
#endif
        hboxLayout->setContentsMargins(0, 0, 0, 0);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        buttonAdvanced = new QPushButton(SpectralMeterConfigWidget);
        buttonAdvanced->setObjectName(QString::fromUtf8("buttonAdvanced"));
        buttonAdvanced->setCheckable(true);

        hboxLayout->addWidget(buttonAdvanced);

        spacerItem = new QSpacerItem(271, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacerItem);

        buttonApply = new QPushButton(SpectralMeterConfigWidget);
        buttonApply->setObjectName(QString::fromUtf8("buttonApply"));
        buttonApply->setAutoDefault(true);

        hboxLayout->addWidget(buttonApply);

        buttonClose = new QPushButton(SpectralMeterConfigWidget);
        buttonClose->setObjectName(QString::fromUtf8("buttonClose"));

        hboxLayout->addWidget(buttonClose);


        vboxLayout->addLayout(hboxLayout);

        QWidget::setTabOrder(spinBoxUpperFreq, spinBoxLowerFreq);
        QWidget::setTabOrder(spinBoxLowerFreq, spinBoxUpperDb);
        QWidget::setTabOrder(spinBoxUpperDb, spinBoxLowerDb);
        QWidget::setTabOrder(spinBoxLowerDb, spinBoxNumBands);
        QWidget::setTabOrder(spinBoxNumBands, checkBoxAverage);
        QWidget::setTabOrder(checkBoxAverage, comboBoxFftSize);
        QWidget::setTabOrder(comboBoxFftSize, comboBoxWindowing);
        QWidget::setTabOrder(comboBoxWindowing, buttonApply);
        QWidget::setTabOrder(buttonApply, buttonClose);
        QWidget::setTabOrder(buttonClose, buttonAdvanced);

        retranslateUi(SpectralMeterConfigWidget);
        QObject::connect(checkBoxAverage, SIGNAL(clicked()), buttonApply, SLOT(animateClick()));

        comboBoxFftSize->setCurrentIndex(3);


        QMetaObject::connectSlotsByName(SpectralMeterConfigWidget);
    } // setupUi

    void retranslateUi(QWidget *SpectralMeterConfigWidget)
    {
        SpectralMeterConfigWidget->setWindowTitle(QApplication::translate("SpectralMeterConfigWidget", "FFT-Spectrum Configuration", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("SpectralMeterConfigWidget", "Frequency Range", 0, QApplication::UnicodeUTF8));
        checkBoxAverage->setText(QApplication::translate("SpectralMeterConfigWidget", "Show average spectrum", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("SpectralMeterConfigWidget", "Number of bands:", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("SpectralMeterConfigWidget", "Lower dB value:", 0, QApplication::UnicodeUTF8));
        label_7->setText(QApplication::translate("SpectralMeterConfigWidget", "Upper dB value:", 0, QApplication::UnicodeUTF8));
        spinBoxLowerFreq->setSuffix(QApplication::translate("SpectralMeterConfigWidget", " Hz", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("SpectralMeterConfigWidget", "Lower Limit:", 0, QApplication::UnicodeUTF8));
        spinBoxUpperFreq->setSuffix(QApplication::translate("SpectralMeterConfigWidget", " Hz", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("SpectralMeterConfigWidget", "Upper Limit:", 0, QApplication::UnicodeUTF8));
        groupBoxAdvanced->setTitle(QApplication::translate("SpectralMeterConfigWidget", "Advanced FFT Options", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("SpectralMeterConfigWidget", "FFT Size:", 0, QApplication::UnicodeUTF8));
        comboBoxFftSize->clear();
        comboBoxFftSize->insertItems(0, QStringList()
         << QApplication::translate("SpectralMeterConfigWidget", "256", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("SpectralMeterConfigWidget", "512", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("SpectralMeterConfigWidget", "1024", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("SpectralMeterConfigWidget", "2048", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("SpectralMeterConfigWidget", "4096", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("SpectralMeterConfigWidget", "8192", 0, QApplication::UnicodeUTF8)
        );
        comboBoxWindowing->clear();
        comboBoxWindowing->insertItems(0, QStringList()
         << QApplication::translate("SpectralMeterConfigWidget", "Rectangle", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("SpectralMeterConfigWidget", "Hanning", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("SpectralMeterConfigWidget", "Hamming", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("SpectralMeterConfigWidget", "Blackman", 0, QApplication::UnicodeUTF8)
        );
        label_5->setText(QApplication::translate("SpectralMeterConfigWidget", "Windowing function:", 0, QApplication::UnicodeUTF8));
        buttonAdvanced->setText(QApplication::translate("SpectralMeterConfigWidget", "Advanced", 0, QApplication::UnicodeUTF8));
        buttonApply->setText(QApplication::translate("SpectralMeterConfigWidget", "Apply", 0, QApplication::UnicodeUTF8));
        buttonClose->setText(QApplication::translate("SpectralMeterConfigWidget", "&Close", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class SpectralMeterConfigWidget: public Ui_SpectralMeterConfigWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SPECTRALMETERCONFIGWIDGET_H
