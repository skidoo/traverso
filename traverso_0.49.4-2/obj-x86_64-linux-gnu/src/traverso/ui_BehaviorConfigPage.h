/********************************************************************************
** Form generated from reading UI file 'BehaviorConfigPage.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_BEHAVIORCONFIGPAGE_H
#define UI_BEHAVIORCONFIGPAGE_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QComboBox>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QRadioButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QSpinBox>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_BehaviorConfigPage
{
public:
    QVBoxLayout *verticalLayout;
    QGroupBox *groupBox;
    QVBoxLayout *vboxLayout;
    QHBoxLayout *hboxLayout;
    QLabel *label_2;
    QSpacerItem *spacerItem;
    QRadioButton *saveRadioButton;
    QRadioButton *askRadioButton;
    QRadioButton *neverRadioButton;
    QCheckBox *loadLastProjectCheckBox;
    QGroupBox *groupBox_2;
    QVBoxLayout *vboxLayout1;
    QHBoxLayout *hboxLayout1;
    QLabel *label_4;
    QSpinBox *numberOfTrackSpinBox;
    QGroupBox *groupBox_4;
    QVBoxLayout *vboxLayout2;
    QCheckBox *lockClipsCheckBox;
    QGroupBox *groupBox_3;
    QVBoxLayout *vboxLayout3;
    QHBoxLayout *hboxLayout2;
    QCheckBox *keepCursorVisibleCheckBox;
    QComboBox *scrollModeComboBox;
    QCheckBox *resyncAudioCheckBox;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *BehaviorConfigPage)
    {
        if (BehaviorConfigPage->objectName().isEmpty())
            BehaviorConfigPage->setObjectName(QString::fromUtf8("BehaviorConfigPage"));
        BehaviorConfigPage->resize(336, 412);
        verticalLayout = new QVBoxLayout(BehaviorConfigPage);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        groupBox = new QGroupBox(BehaviorConfigPage);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        vboxLayout = new QVBoxLayout(groupBox);
#ifndef Q_OS_MAC
        vboxLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        hboxLayout = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout->setContentsMargins(0, 0, 0, 0);
#endif
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        hboxLayout->addWidget(label_2);

        spacerItem = new QSpacerItem(20, 23, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacerItem);

        saveRadioButton = new QRadioButton(groupBox);
        saveRadioButton->setObjectName(QString::fromUtf8("saveRadioButton"));
        saveRadioButton->setChecked(true);

        hboxLayout->addWidget(saveRadioButton);

        askRadioButton = new QRadioButton(groupBox);
        askRadioButton->setObjectName(QString::fromUtf8("askRadioButton"));

        hboxLayout->addWidget(askRadioButton);

        neverRadioButton = new QRadioButton(groupBox);
        neverRadioButton->setObjectName(QString::fromUtf8("neverRadioButton"));

        hboxLayout->addWidget(neverRadioButton);


        vboxLayout->addLayout(hboxLayout);

        loadLastProjectCheckBox = new QCheckBox(groupBox);
        loadLastProjectCheckBox->setObjectName(QString::fromUtf8("loadLastProjectCheckBox"));

        vboxLayout->addWidget(loadLastProjectCheckBox);


        verticalLayout->addWidget(groupBox);

        groupBox_2 = new QGroupBox(BehaviorConfigPage);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        vboxLayout1 = new QVBoxLayout(groupBox_2);
#ifndef Q_OS_MAC
        vboxLayout1->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout1->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout1->setObjectName(QString::fromUtf8("vboxLayout1"));
        hboxLayout1 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout1->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout1->setContentsMargins(0, 0, 0, 0);
#endif
        hboxLayout1->setObjectName(QString::fromUtf8("hboxLayout1"));
        label_4 = new QLabel(groupBox_2);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        hboxLayout1->addWidget(label_4);

        numberOfTrackSpinBox = new QSpinBox(groupBox_2);
        numberOfTrackSpinBox->setObjectName(QString::fromUtf8("numberOfTrackSpinBox"));
        numberOfTrackSpinBox->setMinimum(1);
        numberOfTrackSpinBox->setMaximum(1024);
        numberOfTrackSpinBox->setValue(1);

        hboxLayout1->addWidget(numberOfTrackSpinBox);


        vboxLayout1->addLayout(hboxLayout1);


        verticalLayout->addWidget(groupBox_2);

        groupBox_4 = new QGroupBox(BehaviorConfigPage);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        vboxLayout2 = new QVBoxLayout(groupBox_4);
#ifndef Q_OS_MAC
        vboxLayout2->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout2->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout2->setObjectName(QString::fromUtf8("vboxLayout2"));
        lockClipsCheckBox = new QCheckBox(groupBox_4);
        lockClipsCheckBox->setObjectName(QString::fromUtf8("lockClipsCheckBox"));

        vboxLayout2->addWidget(lockClipsCheckBox);


        verticalLayout->addWidget(groupBox_4);

        groupBox_3 = new QGroupBox(BehaviorConfigPage);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        vboxLayout3 = new QVBoxLayout(groupBox_3);
#ifndef Q_OS_MAC
        vboxLayout3->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout3->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout3->setObjectName(QString::fromUtf8("vboxLayout3"));
        hboxLayout2 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout2->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout2->setContentsMargins(0, 0, 0, 0);
#endif
        hboxLayout2->setObjectName(QString::fromUtf8("hboxLayout2"));
        keepCursorVisibleCheckBox = new QCheckBox(groupBox_3);
        keepCursorVisibleCheckBox->setObjectName(QString::fromUtf8("keepCursorVisibleCheckBox"));
        keepCursorVisibleCheckBox->setChecked(true);

        hboxLayout2->addWidget(keepCursorVisibleCheckBox);

        scrollModeComboBox = new QComboBox(groupBox_3);
        scrollModeComboBox->setObjectName(QString::fromUtf8("scrollModeComboBox"));

        hboxLayout2->addWidget(scrollModeComboBox);


        vboxLayout3->addLayout(hboxLayout2);

        resyncAudioCheckBox = new QCheckBox(groupBox_3);
        resyncAudioCheckBox->setObjectName(QString::fromUtf8("resyncAudioCheckBox"));
        resyncAudioCheckBox->setChecked(false);

        vboxLayout3->addWidget(resyncAudioCheckBox);


        verticalLayout->addWidget(groupBox_3);

        verticalSpacer = new QSpacerItem(20, 51, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);


        retranslateUi(BehaviorConfigPage);
        QObject::connect(keepCursorVisibleCheckBox, SIGNAL(toggled(bool)), scrollModeComboBox, SLOT(setEnabled(bool)));

        QMetaObject::connectSlotsByName(BehaviorConfigPage);
    } // setupUi

    void retranslateUi(QWidget *BehaviorConfigPage)
    {
        BehaviorConfigPage->setWindowTitle(QApplication::translate("BehaviorConfigPage", "Form", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("BehaviorConfigPage", "Project Settings", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("BehaviorConfigPage", "On close:", 0, QApplication::UnicodeUTF8));
        saveRadioButton->setText(QApplication::translate("BehaviorConfigPage", "Save", 0, QApplication::UnicodeUTF8));
        askRadioButton->setText(QApplication::translate("BehaviorConfigPage", "Ask", 0, QApplication::UnicodeUTF8));
        neverRadioButton->setText(QApplication::translate("BehaviorConfigPage", "Don't save", 0, QApplication::UnicodeUTF8));
        loadLastProjectCheckBox->setText(QApplication::translate("BehaviorConfigPage", "Load last used project at startup", 0, QApplication::UnicodeUTF8));
        groupBox_2->setTitle(QApplication::translate("BehaviorConfigPage", "New Sheet Settings", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("BehaviorConfigPage", "Number of tracks", 0, QApplication::UnicodeUTF8));
        groupBox_4->setTitle(QApplication::translate("BehaviorConfigPage", "Audio Clip Settings", 0, QApplication::UnicodeUTF8));
        lockClipsCheckBox->setText(QApplication::translate("BehaviorConfigPage", "Lock Audio Clips by default", 0, QApplication::UnicodeUTF8));
        groupBox_3->setTitle(QApplication::translate("BehaviorConfigPage", "Playback Settings", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        keepCursorVisibleCheckBox->setToolTip(QApplication::translate("BehaviorConfigPage", "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Keep the play cursor in view while playing or recording.</p></body></html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        keepCursorVisibleCheckBox->setText(QApplication::translate("BehaviorConfigPage", "Scroll playback", 0, QApplication::UnicodeUTF8));
        scrollModeComboBox->clear();
        scrollModeComboBox->insertItems(0, QStringList()
         << QApplication::translate("BehaviorConfigPage", "Jump", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("BehaviorConfigPage", "Stay Centered", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("BehaviorConfigPage", "Animated", 0, QApplication::UnicodeUTF8)
        );
        resyncAudioCheckBox->setText(QApplication::translate("BehaviorConfigPage", "Continuously adjust audio while dragging", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class BehaviorConfigPage: public Ui_BehaviorConfigPage {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_BEHAVIORCONFIGPAGE_H
