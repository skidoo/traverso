/********************************************************************************
** Form generated from reading UI file 'ProjectConverterDialog.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PROJECTCONVERTERDIALOG_H
#define UI_PROJECTCONVERTERDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QProgressBar>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QTextBrowser>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_ProjectConverterDialog
{
public:
    QVBoxLayout *vboxLayout;
    QLabel *projectNameLable;
    QTextBrowser *conversionInfoText;
    QLabel *taskLable;
    QTextBrowser *taskTextBrowswer;
    QLabel *progressLable;
    QProgressBar *progressBar;
    QHBoxLayout *hboxLayout;
    QSpacerItem *spacerItem;
    QPushButton *startButton;
    QPushButton *stopConversionButton;
    QPushButton *loadProjectButton;
    QPushButton *closeButton;

    void setupUi(QDialog *ProjectConverterDialog)
    {
        if (ProjectConverterDialog->objectName().isEmpty())
            ProjectConverterDialog->setObjectName(QString::fromUtf8("ProjectConverterDialog"));
        ProjectConverterDialog->resize(452, 480);
        vboxLayout = new QVBoxLayout(ProjectConverterDialog);
#ifndef Q_OS_MAC
        vboxLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        projectNameLable = new QLabel(ProjectConverterDialog);
        projectNameLable->setObjectName(QString::fromUtf8("projectNameLable"));

        vboxLayout->addWidget(projectNameLable);

        conversionInfoText = new QTextBrowser(ProjectConverterDialog);
        conversionInfoText->setObjectName(QString::fromUtf8("conversionInfoText"));
        QSizePolicy sizePolicy(static_cast<QSizePolicy::Policy>(7), static_cast<QSizePolicy::Policy>(7));
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(5);
        sizePolicy.setHeightForWidth(conversionInfoText->sizePolicy().hasHeightForWidth());
        conversionInfoText->setSizePolicy(sizePolicy);

        vboxLayout->addWidget(conversionInfoText);

        taskLable = new QLabel(ProjectConverterDialog);
        taskLable->setObjectName(QString::fromUtf8("taskLable"));

        vboxLayout->addWidget(taskLable);

        taskTextBrowswer = new QTextBrowser(ProjectConverterDialog);
        taskTextBrowswer->setObjectName(QString::fromUtf8("taskTextBrowswer"));
        QSizePolicy sizePolicy1(static_cast<QSizePolicy::Policy>(7), static_cast<QSizePolicy::Policy>(7));
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(1);
        sizePolicy1.setHeightForWidth(taskTextBrowswer->sizePolicy().hasHeightForWidth());
        taskTextBrowswer->setSizePolicy(sizePolicy1);

        vboxLayout->addWidget(taskTextBrowswer);

        progressLable = new QLabel(ProjectConverterDialog);
        progressLable->setObjectName(QString::fromUtf8("progressLable"));

        vboxLayout->addWidget(progressLable);

        progressBar = new QProgressBar(ProjectConverterDialog);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setValue(0);
        progressBar->setOrientation(Qt::Horizontal);

        vboxLayout->addWidget(progressBar);

        hboxLayout = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout->setSpacing(6);
#endif
        hboxLayout->setContentsMargins(0, 0, 0, 0);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        spacerItem = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacerItem);

        startButton = new QPushButton(ProjectConverterDialog);
        startButton->setObjectName(QString::fromUtf8("startButton"));

        hboxLayout->addWidget(startButton);

        stopConversionButton = new QPushButton(ProjectConverterDialog);
        stopConversionButton->setObjectName(QString::fromUtf8("stopConversionButton"));

        hboxLayout->addWidget(stopConversionButton);

        loadProjectButton = new QPushButton(ProjectConverterDialog);
        loadProjectButton->setObjectName(QString::fromUtf8("loadProjectButton"));

        hboxLayout->addWidget(loadProjectButton);

        closeButton = new QPushButton(ProjectConverterDialog);
        closeButton->setObjectName(QString::fromUtf8("closeButton"));

        hboxLayout->addWidget(closeButton);


        vboxLayout->addLayout(hboxLayout);


        retranslateUi(ProjectConverterDialog);
        QObject::connect(closeButton, SIGNAL(clicked()), ProjectConverterDialog, SLOT(reject()));
        QObject::connect(startButton, SIGNAL(clicked()), ProjectConverterDialog, SLOT(accept()));

        QMetaObject::connectSlotsByName(ProjectConverterDialog);
    } // setupUi

    void retranslateUi(QDialog *ProjectConverterDialog)
    {
        ProjectConverterDialog->setWindowTitle(QApplication::translate("ProjectConverterDialog", "Project Converter", 0, QApplication::UnicodeUTF8));
        projectNameLable->setText(QApplication::translate("ProjectConverterDialog", "Project XXX (no translation needed)", 0, QApplication::UnicodeUTF8));
        taskLable->setText(QApplication::translate("ProjectConverterDialog", "Conversion information", 0, QApplication::UnicodeUTF8));
        progressLable->setText(QApplication::translate("ProjectConverterDialog", "Conversion progress", 0, QApplication::UnicodeUTF8));
        startButton->setText(QApplication::translate("ProjectConverterDialog", "Start conversion", 0, QApplication::UnicodeUTF8));
        stopConversionButton->setText(QApplication::translate("ProjectConverterDialog", "Stop conversion", 0, QApplication::UnicodeUTF8));
        loadProjectButton->setText(QApplication::translate("ProjectConverterDialog", "Load Project", 0, QApplication::UnicodeUTF8));
        closeButton->setText(QApplication::translate("ProjectConverterDialog", "Close", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class ProjectConverterDialog: public Ui_ProjectConverterDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PROJECTCONVERTERDIALOG_H
