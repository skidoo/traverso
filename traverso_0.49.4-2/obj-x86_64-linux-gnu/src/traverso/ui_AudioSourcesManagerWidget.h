/********************************************************************************
** Form generated from reading UI file 'AudioSourcesManagerWidget.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_AUDIOSOURCESMANAGERWIDGET_H
#define UI_AUDIOSOURCESMANAGERWIDGET_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QTreeWidget>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_AudioSourcesManagerWidget
{
public:
    QGridLayout *gridLayout;
    QSpacerItem *spacerItem;
    QSpacerItem *spacerItem1;
    QGridLayout *gridLayout1;
    QSpacerItem *spacerItem2;
    QGroupBox *groupBox;
    QGridLayout *gridLayout2;
    QPushButton *removeSourcesButton;
    QPushButton *removeAllSourcesButton;
    QPushButton *removeUnusedSourcesButton;
    QLabel *AudioSourcesLabel;
    QTreeWidget *treeAudioSourcesWidget;

    void setupUi(QWidget *AudioSourcesManagerWidget)
    {
        if (AudioSourcesManagerWidget->objectName().isEmpty())
            AudioSourcesManagerWidget->setObjectName(QString::fromUtf8("AudioSourcesManagerWidget"));
        AudioSourcesManagerWidget->resize(778, 459);
        gridLayout = new QGridLayout(AudioSourcesManagerWidget);
#ifndef Q_OS_MAC
        gridLayout->setSpacing(6);
#endif
        gridLayout->setContentsMargins(8, 8, 8, 8);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        spacerItem = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(spacerItem, 1, 0, 1, 1);

        spacerItem1 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(spacerItem1, 0, 1, 1, 1);

        gridLayout1 = new QGridLayout();
#ifndef Q_OS_MAC
        gridLayout1->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        gridLayout1->setContentsMargins(0, 0, 0, 0);
#endif
        gridLayout1->setObjectName(QString::fromUtf8("gridLayout1"));
        spacerItem2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout1->addItem(spacerItem2, 2, 1, 1, 1);

        groupBox = new QGroupBox(AudioSourcesManagerWidget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        gridLayout2 = new QGridLayout(groupBox);
#ifndef Q_OS_MAC
        gridLayout2->setSpacing(6);
#endif
        gridLayout2->setContentsMargins(8, 8, 8, 8);
        gridLayout2->setObjectName(QString::fromUtf8("gridLayout2"));
        removeSourcesButton = new QPushButton(groupBox);
        removeSourcesButton->setObjectName(QString::fromUtf8("removeSourcesButton"));

        gridLayout2->addWidget(removeSourcesButton, 0, 1, 1, 1);

        removeAllSourcesButton = new QPushButton(groupBox);
        removeAllSourcesButton->setObjectName(QString::fromUtf8("removeAllSourcesButton"));

        gridLayout2->addWidget(removeAllSourcesButton, 0, 2, 1, 1);

        removeUnusedSourcesButton = new QPushButton(groupBox);
        removeUnusedSourcesButton->setObjectName(QString::fromUtf8("removeUnusedSourcesButton"));

        gridLayout2->addWidget(removeUnusedSourcesButton, 0, 0, 1, 1);


        gridLayout1->addWidget(groupBox, 2, 0, 1, 1);

        AudioSourcesLabel = new QLabel(AudioSourcesManagerWidget);
        AudioSourcesLabel->setObjectName(QString::fromUtf8("AudioSourcesLabel"));

        gridLayout1->addWidget(AudioSourcesLabel, 0, 0, 1, 1);

        treeAudioSourcesWidget = new QTreeWidget(AudioSourcesManagerWidget);
        treeAudioSourcesWidget->setObjectName(QString::fromUtf8("treeAudioSourcesWidget"));
        treeAudioSourcesWidget->setMinimumSize(QSize(700, 0));

        gridLayout1->addWidget(treeAudioSourcesWidget, 1, 0, 1, 2);


        gridLayout->addLayout(gridLayout1, 0, 0, 1, 1);


        retranslateUi(AudioSourcesManagerWidget);

        QMetaObject::connectSlotsByName(AudioSourcesManagerWidget);
    } // setupUi

    void retranslateUi(QWidget *AudioSourcesManagerWidget)
    {
        AudioSourcesManagerWidget->setWindowTitle(QApplication::translate("AudioSourcesManagerWidget", "Form", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("AudioSourcesManagerWidget", "Remove sources", 0, QApplication::UnicodeUTF8));
        removeSourcesButton->setText(QApplication::translate("AudioSourcesManagerWidget", "Remove source", 0, QApplication::UnicodeUTF8));
        removeAllSourcesButton->setText(QApplication::translate("AudioSourcesManagerWidget", "Remove all sources", 0, QApplication::UnicodeUTF8));
        removeUnusedSourcesButton->setText(QApplication::translate("AudioSourcesManagerWidget", "Remove unused sources", 0, QApplication::UnicodeUTF8));
        AudioSourcesLabel->setText(QApplication::translate("AudioSourcesManagerWidget", "AudioSources", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class AudioSourcesManagerWidget: public Ui_AudioSourcesManagerWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_AUDIOSOURCESMANAGERWIDGET_H
