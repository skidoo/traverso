/****************************************************************************
** Meta object code from reading C++ file 'MarkerDialog.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../src/traverso/dialogs/MarkerDialog.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'MarkerDialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MarkerDialog[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      24,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      14,   13,   13,   13, 0x08,
      41,   39,   13,   13, 0x08,
      89,   13,   13,   13, 0x08,
     118,   13,   13,   13, 0x08,
     144,   13,   13,   13, 0x08,
     160,   13,   13,   13, 0x08,
     173,   13,   13,   13, 0x08,
     181,   13,   13,   13, 0x08,
     190,   13,   13,   13, 0x08,
     204,   13,   13,   13, 0x08,
     221,   13,   13,   13, 0x08,
     239,   13,   13,   13, 0x08,
     256,   13,   13,   13, 0x08,
     273,   13,   13,   13, 0x08,
     293,   13,   13,   13, 0x08,
     309,   13,   13,   13, 0x08,
     322,   13,   13,   13, 0x08,
     334,   13,   13,   13, 0x08,
     350,   13,   13,   13, 0x08,
     365,   13,   13,   13, 0x08,
     380,   13,   13,   13, 0x08,
     397,   13,   13,   13, 0x08,
     411,   13,   13,   13, 0x08,
     422,   13,   13,   13, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_MarkerDialog[] = {
    "MarkerDialog\0\0update_marker_treeview()\0"
    ",\0item_changed(QTreeWidgetItem*,QTreeWidgetItem*)\0"
    "description_changed(QString)\0"
    "position_changed(QString)\0remove_marker()\0"
    "export_toc()\0apply()\0cancel()\0"
    "title_enter()\0position_enter()\0"
    "performer_enter()\0composer_enter()\0"
    "arranger_enter()\0sheetwriter_enter()\0"
    "message_enter()\0isrc_enter()\0title_all()\0"
    "performer_all()\0composer_all()\0"
    "arranger_all()\0songwriter_all()\0"
    "message_all()\0copy_all()\0pemph_all()\0"
};

void MarkerDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        MarkerDialog *_t = static_cast<MarkerDialog *>(_o);
        switch (_id) {
        case 0: _t->update_marker_treeview(); break;
        case 1: _t->item_changed((*reinterpret_cast< QTreeWidgetItem*(*)>(_a[1])),(*reinterpret_cast< QTreeWidgetItem*(*)>(_a[2]))); break;
        case 2: _t->description_changed((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 3: _t->position_changed((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 4: _t->remove_marker(); break;
        case 5: _t->export_toc(); break;
        case 6: _t->apply(); break;
        case 7: _t->cancel(); break;
        case 8: _t->title_enter(); break;
        case 9: _t->position_enter(); break;
        case 10: _t->performer_enter(); break;
        case 11: _t->composer_enter(); break;
        case 12: _t->arranger_enter(); break;
        case 13: _t->sheetwriter_enter(); break;
        case 14: _t->message_enter(); break;
        case 15: _t->isrc_enter(); break;
        case 16: _t->title_all(); break;
        case 17: _t->performer_all(); break;
        case 18: _t->composer_all(); break;
        case 19: _t->arranger_all(); break;
        case 20: _t->songwriter_all(); break;
        case 21: _t->message_all(); break;
        case 22: _t->copy_all(); break;
        case 23: _t->pemph_all(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData MarkerDialog::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject MarkerDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_MarkerDialog,
      qt_meta_data_MarkerDialog, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MarkerDialog::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MarkerDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MarkerDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MarkerDialog))
        return static_cast<void*>(const_cast< MarkerDialog*>(this));
    if (!strcmp(_clname, "Ui::MarkerDialog"))
        return static_cast< Ui::MarkerDialog*>(const_cast< MarkerDialog*>(this));
    return QDialog::qt_metacast(_clname);
}

int MarkerDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 24)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 24;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
