/****************************************************************************
** Meta object code from reading C++ file 'CDWritingDialog.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../src/traverso/dialogs/CDWritingDialog.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'CDWritingDialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_CDWritingDialog[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      25,   17,   16,   16, 0x08,
      47,   16,   16,   16, 0x08,
      77,   71,   16,   16, 0x08,
     102,   16,   16,   16, 0x08,
     123,   16,   16,   16, 0x08,
     143,   16,   16,   16, 0x08,
     166,   16,   16,   16, 0x08,
     211,  191,   16,   16, 0x08,
     261,   16,   16,   16, 0x08,
     291,  282,   16,   16, 0x08,
     315,   16,   16,   16, 0x08,
     331,   16,   16,   16, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_CDWritingDialog[] = {
    "CDWritingDialog\0\0project\0set_project(Project*)\0"
    "on_stopButton_clicked()\0state\0"
    "export_only_changed(int)\0start_burn_process()\0"
    "stop_burn_process()\0read_standard_output()\0"
    "cdrdao_process_started()\0exitcode,exitstatus\0"
    "cdrdao_process_finished(int,QProcess::ExitStatus)\0"
    "cd_export_finished()\0progress\0"
    "cd_export_progress(int)\0query_devices()\0"
    "reject()\0"
};

void CDWritingDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        CDWritingDialog *_t = static_cast<CDWritingDialog *>(_o);
        switch (_id) {
        case 0: _t->set_project((*reinterpret_cast< Project*(*)>(_a[1]))); break;
        case 1: _t->on_stopButton_clicked(); break;
        case 2: _t->export_only_changed((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->start_burn_process(); break;
        case 4: _t->stop_burn_process(); break;
        case 5: _t->read_standard_output(); break;
        case 6: _t->cdrdao_process_started(); break;
        case 7: _t->cdrdao_process_finished((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QProcess::ExitStatus(*)>(_a[2]))); break;
        case 8: _t->cd_export_finished(); break;
        case 9: _t->cd_export_progress((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 10: _t->query_devices(); break;
        case 11: _t->reject(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData CDWritingDialog::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject CDWritingDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_CDWritingDialog,
      qt_meta_data_CDWritingDialog, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &CDWritingDialog::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *CDWritingDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *CDWritingDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_CDWritingDialog))
        return static_cast<void*>(const_cast< CDWritingDialog*>(this));
    if (!strcmp(_clname, "Ui::CDWritingDialog"))
        return static_cast< Ui::CDWritingDialog*>(const_cast< CDWritingDialog*>(this));
    return QDialog::qt_metacast(_clname);
}

int CDWritingDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 12)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 12;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
