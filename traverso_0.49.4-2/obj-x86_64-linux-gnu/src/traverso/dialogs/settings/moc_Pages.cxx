/****************************************************************************
** Meta object code from reading C++ file 'Pages.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../src/traverso/dialogs/settings/Pages.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Pages.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ConfigPage[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_ConfigPage[] = {
    "ConfigPage\0"
};

void ConfigPage::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData ConfigPage::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ConfigPage::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_ConfigPage,
      qt_meta_data_ConfigPage, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ConfigPage::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ConfigPage::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ConfigPage::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ConfigPage))
        return static_cast<void*>(const_cast< ConfigPage*>(this));
    return QWidget::qt_metacast(_clname);
}

int ConfigPage::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_AudioDriverConfigPage[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      23,   22,   22,   22, 0x08,
      49,   22,   22,   22, 0x08,
      86,   22,   22,   22, 0x08,
     125,   22,   22,   22, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_AudioDriverConfigPage[] = {
    "AudioDriverConfigPage\0\0update_latency_combobox()\0"
    "rate_combobox_index_changed(QString)\0"
    "driver_combobox_index_changed(QString)\0"
    "restart_driver_button_clicked()\0"
};

void AudioDriverConfigPage::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        AudioDriverConfigPage *_t = static_cast<AudioDriverConfigPage *>(_o);
        switch (_id) {
        case 0: _t->update_latency_combobox(); break;
        case 1: _t->rate_combobox_index_changed((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->driver_combobox_index_changed((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 3: _t->restart_driver_button_clicked(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData AudioDriverConfigPage::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject AudioDriverConfigPage::staticMetaObject = {
    { &ConfigPage::staticMetaObject, qt_meta_stringdata_AudioDriverConfigPage,
      qt_meta_data_AudioDriverConfigPage, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &AudioDriverConfigPage::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *AudioDriverConfigPage::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *AudioDriverConfigPage::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_AudioDriverConfigPage))
        return static_cast<void*>(const_cast< AudioDriverConfigPage*>(this));
    return ConfigPage::qt_metacast(_clname);
}

int AudioDriverConfigPage::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ConfigPage::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}
static const uint qt_meta_data_AppearenceConfigPage[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      22,   21,   21,   21, 0x08,
      54,   49,   21,   21, 0x08,
      89,   83,   21,   21, 0x08,
     118,   21,   21,   21, 0x08,
     168,   21,   21,   21, 0x08,
     197,   21,   21,   21, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_AppearenceConfigPage[] = {
    "AppearenceConfigPage\0\0dirselect_button_clicked()\0"
    "text\0style_index_changed(QString)\0"
    "theme\0theme_index_changed(QString)\0"
    "use_selected_styles_pallet_checkbox_toggled(bool)\0"
    "color_adjustbox_changed(int)\0"
    "theme_option_changed()\0"
};

void AppearenceConfigPage::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        AppearenceConfigPage *_t = static_cast<AppearenceConfigPage *>(_o);
        switch (_id) {
        case 0: _t->dirselect_button_clicked(); break;
        case 1: _t->style_index_changed((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 2: _t->theme_index_changed((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 3: _t->use_selected_styles_pallet_checkbox_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 4: _t->color_adjustbox_changed((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->theme_option_changed(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData AppearenceConfigPage::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject AppearenceConfigPage::staticMetaObject = {
    { &ConfigPage::staticMetaObject, qt_meta_stringdata_AppearenceConfigPage,
      qt_meta_data_AppearenceConfigPage, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &AppearenceConfigPage::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *AppearenceConfigPage::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *AppearenceConfigPage::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_AppearenceConfigPage))
        return static_cast<void*>(const_cast< AppearenceConfigPage*>(this));
    return ConfigPage::qt_metacast(_clname);
}

int AppearenceConfigPage::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ConfigPage::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    }
    return _id;
}
static const uint qt_meta_data_BehaviorConfigPage[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      20,   19,   19,   19, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_BehaviorConfigPage[] = {
    "BehaviorConfigPage\0\0update_follow()\0"
};

void BehaviorConfigPage::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        BehaviorConfigPage *_t = static_cast<BehaviorConfigPage *>(_o);
        switch (_id) {
        case 0: _t->update_follow(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData BehaviorConfigPage::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject BehaviorConfigPage::staticMetaObject = {
    { &ConfigPage::staticMetaObject, qt_meta_stringdata_BehaviorConfigPage,
      qt_meta_data_BehaviorConfigPage, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &BehaviorConfigPage::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *BehaviorConfigPage::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *BehaviorConfigPage::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_BehaviorConfigPage))
        return static_cast<void*>(const_cast< BehaviorConfigPage*>(this));
    return ConfigPage::qt_metacast(_clname);
}

int BehaviorConfigPage::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ConfigPage::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}
static const uint qt_meta_data_KeyboardConfigPage[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      27,   20,   19,   19, 0x08,
      57,   19,   19,   19, 0x08,
      79,   19,   19,   19, 0x08,
     105,   19,   19,   19, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_KeyboardConfigPage[] = {
    "KeyboardConfigPage\0\0keymap\0"
    "keymap_index_changed(QString)\0"
    "update_keymap_combo()\0on_exportButton_clicked()\0"
    "on_printButton_clicked()\0"
};

void KeyboardConfigPage::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        KeyboardConfigPage *_t = static_cast<KeyboardConfigPage *>(_o);
        switch (_id) {
        case 0: _t->keymap_index_changed((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: _t->update_keymap_combo(); break;
        case 2: _t->on_exportButton_clicked(); break;
        case 3: _t->on_printButton_clicked(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData KeyboardConfigPage::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject KeyboardConfigPage::staticMetaObject = {
    { &ConfigPage::staticMetaObject, qt_meta_stringdata_KeyboardConfigPage,
      qt_meta_data_KeyboardConfigPage, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &KeyboardConfigPage::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *KeyboardConfigPage::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *KeyboardConfigPage::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_KeyboardConfigPage))
        return static_cast<void*>(const_cast< KeyboardConfigPage*>(this));
    return ConfigPage::qt_metacast(_clname);
}

int KeyboardConfigPage::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ConfigPage::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}
static const uint qt_meta_data_RecordingConfigPage[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      27,   21,   20,   20, 0x08,
      61,   55,   20,   20, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_RecordingConfigPage[] = {
    "RecordingConfigPage\0\0index\0"
    "encoding_index_changed(int)\0state\0"
    "use_onthefly_resampling_checkbox_changed(int)\0"
};

void RecordingConfigPage::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        RecordingConfigPage *_t = static_cast<RecordingConfigPage *>(_o);
        switch (_id) {
        case 0: _t->encoding_index_changed((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->use_onthefly_resampling_checkbox_changed((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData RecordingConfigPage::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject RecordingConfigPage::staticMetaObject = {
    { &ConfigPage::staticMetaObject, qt_meta_stringdata_RecordingConfigPage,
      qt_meta_data_RecordingConfigPage, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &RecordingConfigPage::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *RecordingConfigPage::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *RecordingConfigPage::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_RecordingConfigPage))
        return static_cast<void*>(const_cast< RecordingConfigPage*>(this));
    return ConfigPage::qt_metacast(_clname);
}

int RecordingConfigPage::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ConfigPage::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
