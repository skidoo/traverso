/****************************************************************************
** Meta object code from reading C++ file 'NewSheetDialog.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../src/traverso/dialogs/project/NewSheetDialog.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'NewSheetDialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_NewSheetDialog[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      16,   15,   15,   15, 0x08,
      25,   15,   15,   15, 0x08,
      42,   34,   15,   15, 0x08,
      64,   15,   15,   15, 0x08,
      94,   88,   15,   15, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_NewSheetDialog[] = {
    "NewSheetDialog\0\0accept()\0reject()\0"
    "project\0set_project(Project*)\0"
    "update_template_combo()\0state\0"
    "use_template_checkbox_state_changed(int)\0"
};

void NewSheetDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        NewSheetDialog *_t = static_cast<NewSheetDialog *>(_o);
        switch (_id) {
        case 0: _t->accept(); break;
        case 1: _t->reject(); break;
        case 2: _t->set_project((*reinterpret_cast< Project*(*)>(_a[1]))); break;
        case 3: _t->update_template_combo(); break;
        case 4: _t->use_template_checkbox_state_changed((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData NewSheetDialog::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject NewSheetDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_NewSheetDialog,
      qt_meta_data_NewSheetDialog, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &NewSheetDialog::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *NewSheetDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *NewSheetDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_NewSheetDialog))
        return static_cast<void*>(const_cast< NewSheetDialog*>(this));
    if (!strcmp(_clname, "Ui::NewSheetDialog"))
        return static_cast< Ui::NewSheetDialog*>(const_cast< NewSheetDialog*>(this));
    return QDialog::qt_metacast(_clname);
}

int NewSheetDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
