/****************************************************************************
** Meta object code from reading C++ file 'OpenProjectDialog.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../src/traverso/dialogs/project/OpenProjectDialog.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'OpenProjectDialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_OpenProjectDialog[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      19,   18,   18,   18, 0x08,
      42,   18,   18,   18, 0x08,
      73,   18,   18,   18, 0x08,
     106,   18,   18,   18, 0x08,
     144,  142,   18,   18, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_OpenProjectDialog[] = {
    "OpenProjectDialog\0\0update_projects_list()\0"
    "on_loadProjectButton_clicked()\0"
    "on_deleteProjectbutton_clicked()\0"
    "on_projectDirSelectButton_clicked()\0"
    ",\0projectitem_clicked(QTreeWidgetItem*,int)\0"
};

void OpenProjectDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        OpenProjectDialog *_t = static_cast<OpenProjectDialog *>(_o);
        switch (_id) {
        case 0: _t->update_projects_list(); break;
        case 1: _t->on_loadProjectButton_clicked(); break;
        case 2: _t->on_deleteProjectbutton_clicked(); break;
        case 3: _t->on_projectDirSelectButton_clicked(); break;
        case 4: _t->projectitem_clicked((*reinterpret_cast< QTreeWidgetItem*(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData OpenProjectDialog::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject OpenProjectDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_OpenProjectDialog,
      qt_meta_data_OpenProjectDialog, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &OpenProjectDialog::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *OpenProjectDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *OpenProjectDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_OpenProjectDialog))
        return static_cast<void*>(const_cast< OpenProjectDialog*>(this));
    if (!strcmp(_clname, "Ui::OpenProjectDialog"))
        return static_cast< Ui::OpenProjectDialog*>(const_cast< OpenProjectDialog*>(this));
    return QDialog::qt_metacast(_clname);
}

int OpenProjectDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
