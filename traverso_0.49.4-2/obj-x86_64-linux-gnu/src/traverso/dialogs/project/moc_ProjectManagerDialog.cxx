/****************************************************************************
** Meta object code from reading C++ file 'ProjectManagerDialog.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../src/traverso/dialogs/project/ProjectManagerDialog.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'ProjectManagerDialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ProjectManagerDialog[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      14,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      22,   21,   21,   21, 0x08,
      50,   42,   21,   21, 0x08,
      78,   72,   21,   21, 0x08,
     118,   21,   21,   21, 0x08,
     149,   21,   21,   21, 0x08,
     180,   21,   21,   21, 0x08,
     216,  211,   21,   21, 0x08,
     243,  211,   21,   21, 0x08,
     270,   21,   21,   21, 0x08,
     294,   21,   21,   21, 0x08,
     318,   21,   21,   21, 0x08,
     350,   21,   21,   21, 0x08,
     384,   21,   21,   21, 0x08,
     393,   21,   21,   21, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_ProjectManagerDialog[] = {
    "ProjectManagerDialog\0\0update_sheet_list()\0"
    "project\0set_project(Project*)\0item,\0"
    "sheetitem_clicked(QTreeWidgetItem*,int)\0"
    "on_renameSheetButton_clicked()\0"
    "on_deleteSheetButton_clicked()\0"
    "on_createSheetButton_clicked()\0text\0"
    "redo_text_changed(QString)\0"
    "undo_text_changed(QString)\0"
    "on_undoButton_clicked()\0on_redoButton_clicked()\0"
    "on_sheetsExportButton_clicked()\0"
    "on_exportTemplateButton_clicked()\0"
    "accept()\0reject()\0"
};

void ProjectManagerDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ProjectManagerDialog *_t = static_cast<ProjectManagerDialog *>(_o);
        switch (_id) {
        case 0: _t->update_sheet_list(); break;
        case 1: _t->set_project((*reinterpret_cast< Project*(*)>(_a[1]))); break;
        case 2: _t->sheetitem_clicked((*reinterpret_cast< QTreeWidgetItem*(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 3: _t->on_renameSheetButton_clicked(); break;
        case 4: _t->on_deleteSheetButton_clicked(); break;
        case 5: _t->on_createSheetButton_clicked(); break;
        case 6: _t->redo_text_changed((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 7: _t->undo_text_changed((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 8: _t->on_undoButton_clicked(); break;
        case 9: _t->on_redoButton_clicked(); break;
        case 10: _t->on_sheetsExportButton_clicked(); break;
        case 11: _t->on_exportTemplateButton_clicked(); break;
        case 12: _t->accept(); break;
        case 13: _t->reject(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ProjectManagerDialog::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ProjectManagerDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_ProjectManagerDialog,
      qt_meta_data_ProjectManagerDialog, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ProjectManagerDialog::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ProjectManagerDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ProjectManagerDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ProjectManagerDialog))
        return static_cast<void*>(const_cast< ProjectManagerDialog*>(this));
    if (!strcmp(_clname, "Ui::ProjectManagerDialog"))
        return static_cast< Ui::ProjectManagerDialog*>(const_cast< ProjectManagerDialog*>(this));
    return QDialog::qt_metacast(_clname);
}

int ProjectManagerDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 14)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
