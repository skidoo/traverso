/********************************************************************************
** Form generated from reading UI file 'InsertSilenceDialog.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_INSERTSILENCEDIALOG_H
#define UI_INSERTSILENCEDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QDoubleSpinBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_InsertSilenceDialog
{
public:
    QVBoxLayout *vboxLayout;
    QHBoxLayout *hboxLayout;
    QLabel *label;
    QDoubleSpinBox *lengthSpinBox;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *InsertSilenceDialog)
    {
        if (InsertSilenceDialog->objectName().isEmpty())
            InsertSilenceDialog->setObjectName(QString::fromUtf8("InsertSilenceDialog"));
        InsertSilenceDialog->resize(261, 87);
        vboxLayout = new QVBoxLayout(InsertSilenceDialog);
#ifndef Q_OS_MAC
        vboxLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        hboxLayout = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout->setContentsMargins(0, 0, 0, 0);
#endif
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        label = new QLabel(InsertSilenceDialog);
        label->setObjectName(QString::fromUtf8("label"));

        hboxLayout->addWidget(label);

        lengthSpinBox = new QDoubleSpinBox(InsertSilenceDialog);
        lengthSpinBox->setObjectName(QString::fromUtf8("lengthSpinBox"));
        lengthSpinBox->setDecimals(3);
        lengthSpinBox->setMaximum(3600);
        lengthSpinBox->setMinimum(0.001);
        lengthSpinBox->setValue(10);

        hboxLayout->addWidget(lengthSpinBox);


        vboxLayout->addLayout(hboxLayout);

        buttonBox = new QDialogButtonBox(InsertSilenceDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::NoButton|QDialogButtonBox::Ok);

        vboxLayout->addWidget(buttonBox);


        retranslateUi(InsertSilenceDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), InsertSilenceDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), InsertSilenceDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(InsertSilenceDialog);
    } // setupUi

    void retranslateUi(QDialog *InsertSilenceDialog)
    {
        InsertSilenceDialog->setWindowTitle(QApplication::translate("InsertSilenceDialog", "Insert Silence", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("InsertSilenceDialog", "Insert Silence (seconds):", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class InsertSilenceDialog: public Ui_InsertSilenceDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_INSERTSILENCEDIALOG_H
