/********************************************************************************
** Form generated from reading UI file 'ExportDialog.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EXPORTDIALOG_H
#define UI_EXPORTDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QProgressBar>
#include <QtGui/QPushButton>
#include <QtGui/QRadioButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_ExportDialog
{
public:
    QVBoxLayout *vboxLayout;
    QGroupBox *generalOptionsGroupBox;
    QVBoxLayout *vboxLayout1;
    QHBoxLayout *hboxLayout;
    QRadioButton *currentSheetButton;
    QRadioButton *allSheetsButton;
    QSpacerItem *spacerItem;
    QHBoxLayout *hboxLayout1;
    QLabel *label;
    QLineEdit *exportDirName;
    QPushButton *fileSelectButton;
    QGroupBox *ExportStateGroupBox;
    QVBoxLayout *vboxLayout2;
    QHBoxLayout *hboxLayout2;
    QLabel *currentProcessingSheetName;
    QSpacerItem *spacerItem1;
    QProgressBar *progressBar;
    QHBoxLayout *hboxLayout3;
    QSpacerItem *spacerItem2;
    QPushButton *abortButton;
    QPushButton *startButton;
    QPushButton *closeButton;

    void setupUi(QDialog *ExportDialog)
    {
        if (ExportDialog->objectName().isEmpty())
            ExportDialog->setObjectName(QString::fromUtf8("ExportDialog"));
        ExportDialog->resize(453, 258);
        QSizePolicy sizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(1);
        sizePolicy.setHeightForWidth(ExportDialog->sizePolicy().hasHeightForWidth());
        ExportDialog->setSizePolicy(sizePolicy);
        vboxLayout = new QVBoxLayout(ExportDialog);
        vboxLayout->setSpacing(9);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        vboxLayout->setContentsMargins(9, 9, 9, 9);
        generalOptionsGroupBox = new QGroupBox(ExportDialog);
        generalOptionsGroupBox->setObjectName(QString::fromUtf8("generalOptionsGroupBox"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Maximum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(generalOptionsGroupBox->sizePolicy().hasHeightForWidth());
        generalOptionsGroupBox->setSizePolicy(sizePolicy1);
        generalOptionsGroupBox->setMaximumSize(QSize(16777215, 350));
        vboxLayout1 = new QVBoxLayout(generalOptionsGroupBox);
        vboxLayout1->setSpacing(6);
        vboxLayout1->setObjectName(QString::fromUtf8("vboxLayout1"));
        vboxLayout1->setContentsMargins(9, 9, 9, 9);
        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        hboxLayout->setContentsMargins(0, 0, 0, 0);
        currentSheetButton = new QRadioButton(generalOptionsGroupBox);
        currentSheetButton->setObjectName(QString::fromUtf8("currentSheetButton"));
        currentSheetButton->setChecked(true);

        hboxLayout->addWidget(currentSheetButton);

        allSheetsButton = new QRadioButton(generalOptionsGroupBox);
        allSheetsButton->setObjectName(QString::fromUtf8("allSheetsButton"));

        hboxLayout->addWidget(allSheetsButton);

        spacerItem = new QSpacerItem(0, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacerItem);


        vboxLayout1->addLayout(hboxLayout);

        hboxLayout1 = new QHBoxLayout();
        hboxLayout1->setSpacing(6);
        hboxLayout1->setObjectName(QString::fromUtf8("hboxLayout1"));
        hboxLayout1->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(generalOptionsGroupBox);
        label->setObjectName(QString::fromUtf8("label"));
        label->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        hboxLayout1->addWidget(label);

        exportDirName = new QLineEdit(generalOptionsGroupBox);
        exportDirName->setObjectName(QString::fromUtf8("exportDirName"));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(5);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(exportDirName->sizePolicy().hasHeightForWidth());
        exportDirName->setSizePolicy(sizePolicy2);

        hboxLayout1->addWidget(exportDirName);

        fileSelectButton = new QPushButton(generalOptionsGroupBox);
        fileSelectButton->setObjectName(QString::fromUtf8("fileSelectButton"));

        hboxLayout1->addWidget(fileSelectButton);


        vboxLayout1->addLayout(hboxLayout1);


        vboxLayout->addWidget(generalOptionsGroupBox);

        ExportStateGroupBox = new QGroupBox(ExportDialog);
        ExportStateGroupBox->setObjectName(QString::fromUtf8("ExportStateGroupBox"));
        sizePolicy1.setHeightForWidth(ExportStateGroupBox->sizePolicy().hasHeightForWidth());
        ExportStateGroupBox->setSizePolicy(sizePolicy1);
        vboxLayout2 = new QVBoxLayout(ExportStateGroupBox);
        vboxLayout2->setSpacing(6);
        vboxLayout2->setObjectName(QString::fromUtf8("vboxLayout2"));
        vboxLayout2->setContentsMargins(9, 9, 9, 9);
        hboxLayout2 = new QHBoxLayout();
        hboxLayout2->setSpacing(6);
        hboxLayout2->setObjectName(QString::fromUtf8("hboxLayout2"));
        hboxLayout2->setContentsMargins(0, 0, 0, 0);
        currentProcessingSheetName = new QLabel(ExportStateGroupBox);
        currentProcessingSheetName->setObjectName(QString::fromUtf8("currentProcessingSheetName"));

        hboxLayout2->addWidget(currentProcessingSheetName);

        spacerItem1 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout2->addItem(spacerItem1);


        vboxLayout2->addLayout(hboxLayout2);

        progressBar = new QProgressBar(ExportStateGroupBox);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setValue(0);
        progressBar->setOrientation(Qt::Horizontal);

        vboxLayout2->addWidget(progressBar);


        vboxLayout->addWidget(ExportStateGroupBox);

        hboxLayout3 = new QHBoxLayout();
        hboxLayout3->setSpacing(6);
        hboxLayout3->setObjectName(QString::fromUtf8("hboxLayout3"));
        hboxLayout3->setContentsMargins(0, 0, 0, 0);
        spacerItem2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout3->addItem(spacerItem2);

        abortButton = new QPushButton(ExportDialog);
        abortButton->setObjectName(QString::fromUtf8("abortButton"));
        abortButton->setMinimumSize(QSize(200, 0));

        hboxLayout3->addWidget(abortButton);

        startButton = new QPushButton(ExportDialog);
        startButton->setObjectName(QString::fromUtf8("startButton"));
        startButton->setMinimumSize(QSize(140, 0));

        hboxLayout3->addWidget(startButton);

        closeButton = new QPushButton(ExportDialog);
        closeButton->setObjectName(QString::fromUtf8("closeButton"));

        hboxLayout3->addWidget(closeButton);


        vboxLayout->addLayout(hboxLayout3);


        retranslateUi(ExportDialog);

        QMetaObject::connectSlotsByName(ExportDialog);
    } // setupUi

    void retranslateUi(QDialog *ExportDialog)
    {
        ExportDialog->setWindowTitle(QApplication::translate("ExportDialog", "Export", 0, QApplication::UnicodeUTF8));
        generalOptionsGroupBox->setTitle(QApplication::translate("ExportDialog", "General Options", 0, QApplication::UnicodeUTF8));
        currentSheetButton->setText(QApplication::translate("ExportDialog", "Export current Sheet", 0, QApplication::UnicodeUTF8));
        allSheetsButton->setText(QApplication::translate("ExportDialog", "Export all Sheets", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("ExportDialog", "Export directory", 0, QApplication::UnicodeUTF8));
        fileSelectButton->setText(QString());
        ExportStateGroupBox->setTitle(QApplication::translate("ExportDialog", "Export status", 0, QApplication::UnicodeUTF8));
        currentProcessingSheetName->setText(QApplication::translate("ExportDialog", "-", 0, QApplication::UnicodeUTF8));
        abortButton->setText(QApplication::translate("ExportDialog", "Abort Export", 0, QApplication::UnicodeUTF8));
        startButton->setText(QApplication::translate("ExportDialog", "Start Export", 0, QApplication::UnicodeUTF8));
        closeButton->setText(QApplication::translate("ExportDialog", "Close", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class ExportDialog: public Ui_ExportDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EXPORTDIALOG_H
