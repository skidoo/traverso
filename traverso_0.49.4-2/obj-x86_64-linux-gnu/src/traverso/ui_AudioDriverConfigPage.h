/********************************************************************************
** Form generated from reading UI file 'AudioDriverConfigPage.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_AUDIODRIVERCONFIGPAGE_H
#define UI_AUDIODRIVERCONFIGPAGE_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QComboBox>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_AudioDriverConfigPage
{
public:
    QVBoxLayout *vboxLayout;
    QGroupBox *groupBox;
    QHBoxLayout *hboxLayout;
    QLabel *label;
    QComboBox *driverCombo;
    QGroupBox *driverConfigGroupBox;
    QVBoxLayout *vboxLayout1;
    QHBoxLayout *hboxLayout1;
    QLabel *label_2;
    QComboBox *duplexComboBox;
    QHBoxLayout *hboxLayout2;
    QLabel *label_2_2;
    QComboBox *rateComboBox;
    QHBoxLayout *hboxLayout3;
    QLabel *label_7;
    QComboBox *latencyComboBox;
    QHBoxLayout *hboxLayout4;
    QSpacerItem *spacerItem;
    QPushButton *restartDriverButton;
    QGroupBox *jackGroupBox;
    QVBoxLayout *vboxLayout2;
    QCheckBox *jackTransportCheckBox;
    QSpacerItem *spacerItem1;

    void setupUi(QWidget *AudioDriverConfigPage)
    {
        if (AudioDriverConfigPage->objectName().isEmpty())
            AudioDriverConfigPage->setObjectName(QString::fromUtf8("AudioDriverConfigPage"));
        AudioDriverConfigPage->resize(250, 344);
        vboxLayout = new QVBoxLayout(AudioDriverConfigPage);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        groupBox = new QGroupBox(AudioDriverConfigPage);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        hboxLayout = new QHBoxLayout(groupBox);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));

        hboxLayout->addWidget(label);

        driverCombo = new QComboBox(groupBox);
        driverCombo->setObjectName(QString::fromUtf8("driverCombo"));

        hboxLayout->addWidget(driverCombo);


        vboxLayout->addWidget(groupBox);

        driverConfigGroupBox = new QGroupBox(AudioDriverConfigPage);
        driverConfigGroupBox->setObjectName(QString::fromUtf8("driverConfigGroupBox"));
        vboxLayout1 = new QVBoxLayout(driverConfigGroupBox);
        vboxLayout1->setObjectName(QString::fromUtf8("vboxLayout1"));
        hboxLayout1 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout1->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout1->setContentsMargins(0, 0, 0, 0);
#endif
        hboxLayout1->setObjectName(QString::fromUtf8("hboxLayout1"));
        label_2 = new QLabel(driverConfigGroupBox);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        hboxLayout1->addWidget(label_2);

        duplexComboBox = new QComboBox(driverConfigGroupBox);
        duplexComboBox->setObjectName(QString::fromUtf8("duplexComboBox"));

        hboxLayout1->addWidget(duplexComboBox);


        vboxLayout1->addLayout(hboxLayout1);

        hboxLayout2 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout2->setSpacing(6);
#endif
        hboxLayout2->setContentsMargins(0, 0, 0, 0);
        hboxLayout2->setObjectName(QString::fromUtf8("hboxLayout2"));
        label_2_2 = new QLabel(driverConfigGroupBox);
        label_2_2->setObjectName(QString::fromUtf8("label_2_2"));

        hboxLayout2->addWidget(label_2_2);

        rateComboBox = new QComboBox(driverConfigGroupBox);
        rateComboBox->setObjectName(QString::fromUtf8("rateComboBox"));

        hboxLayout2->addWidget(rateComboBox);


        vboxLayout1->addLayout(hboxLayout2);

        hboxLayout3 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout3->setSpacing(6);
#endif
        hboxLayout3->setContentsMargins(0, 0, 0, 0);
        hboxLayout3->setObjectName(QString::fromUtf8("hboxLayout3"));
        label_7 = new QLabel(driverConfigGroupBox);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        hboxLayout3->addWidget(label_7);

        latencyComboBox = new QComboBox(driverConfigGroupBox);
        latencyComboBox->setObjectName(QString::fromUtf8("latencyComboBox"));

        hboxLayout3->addWidget(latencyComboBox);


        vboxLayout1->addLayout(hboxLayout3);

        hboxLayout4 = new QHBoxLayout();
        hboxLayout4->setObjectName(QString::fromUtf8("hboxLayout4"));
        spacerItem = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout4->addItem(spacerItem);

        restartDriverButton = new QPushButton(driverConfigGroupBox);
        restartDriverButton->setObjectName(QString::fromUtf8("restartDriverButton"));

        hboxLayout4->addWidget(restartDriverButton);


        vboxLayout1->addLayout(hboxLayout4);


        vboxLayout->addWidget(driverConfigGroupBox);

        jackGroupBox = new QGroupBox(AudioDriverConfigPage);
        jackGroupBox->setObjectName(QString::fromUtf8("jackGroupBox"));
        vboxLayout2 = new QVBoxLayout(jackGroupBox);
#ifndef Q_OS_MAC
        vboxLayout2->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout2->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout2->setObjectName(QString::fromUtf8("vboxLayout2"));
        jackTransportCheckBox = new QCheckBox(jackGroupBox);
        jackTransportCheckBox->setObjectName(QString::fromUtf8("jackTransportCheckBox"));

        vboxLayout2->addWidget(jackTransportCheckBox);


        vboxLayout->addWidget(jackGroupBox);

        spacerItem1 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        vboxLayout->addItem(spacerItem1);


        retranslateUi(AudioDriverConfigPage);

        QMetaObject::connectSlotsByName(AudioDriverConfigPage);
    } // setupUi

    void retranslateUi(QWidget *AudioDriverConfigPage)
    {
        AudioDriverConfigPage->setWindowTitle(QApplication::translate("AudioDriverConfigPage", "Form", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("AudioDriverConfigPage", "Driver Selection", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("AudioDriverConfigPage", "Driver:", 0, QApplication::UnicodeUTF8));
        driverConfigGroupBox->setTitle(QApplication::translate("AudioDriverConfigPage", "Configure driver", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        label_2->setToolTip(QApplication::translate("AudioDriverConfigPage", "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">Duplex mode:</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Defines if both the Playback and Capture buses </p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">of your soundcard are to be used, </p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">or only the Playback or Capture bus(es).</p></body></html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        label_2->setText(QApplication::translate("AudioDriverConfigPage", "Duplex mode", 0, QApplication::UnicodeUTF8));
        duplexComboBox->clear();
        duplexComboBox->insertItems(0, QStringList()
         << QApplication::translate("AudioDriverConfigPage", "Full", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("AudioDriverConfigPage", "Playback", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("AudioDriverConfigPage", "Capture", 0, QApplication::UnicodeUTF8)
        );
#ifndef QT_NO_TOOLTIP
        label_2_2->setToolTip(QApplication::translate("AudioDriverConfigPage", "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">Sample rate:</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">The sample rate used by the audio card.</p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">New projects will use this samplerate as </p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block"
                        "-indent:0; text-indent:0px;\">the project's sample rate on creation.</p></body></html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        label_2_2->setText(QApplication::translate("AudioDriverConfigPage", "Sample rate", 0, QApplication::UnicodeUTF8));
        rateComboBox->clear();
        rateComboBox->insertItems(0, QStringList()
         << QApplication::translate("AudioDriverConfigPage", "22050", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("AudioDriverConfigPage", "32000", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("AudioDriverConfigPage", "44100", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("AudioDriverConfigPage", "48000", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("AudioDriverConfigPage", "88200", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("AudioDriverConfigPage", "96000", 0, QApplication::UnicodeUTF8)
        );
#ifndef QT_NO_TOOLTIP
        label_7->setToolTip(QApplication::translate("AudioDriverConfigPage", "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">Buffer latency:</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">The latency introduced by the size of the audio buffers.</p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Some people need low latencies, if you don't need it, </p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left"
                        ":0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">or don't know what it means, please leave the default setting!</p></body></html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        label_7->setText(QApplication::translate("AudioDriverConfigPage", "Buffer latency (ms)", 0, QApplication::UnicodeUTF8));
        restartDriverButton->setText(QApplication::translate("AudioDriverConfigPage", "Restart Driver", 0, QApplication::UnicodeUTF8));
        jackGroupBox->setTitle(QApplication::translate("AudioDriverConfigPage", "Jack", 0, QApplication::UnicodeUTF8));
        jackTransportCheckBox->setText(QApplication::translate("AudioDriverConfigPage", "Enable Jack transport control", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class AudioDriverConfigPage: public Ui_AudioDriverConfigPage {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_AUDIODRIVERCONFIGPAGE_H
