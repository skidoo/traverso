/********************************************************************************
** Form generated from reading UI file 'NewProjectDialog.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_NEWPROJECTDIALOG_H
#define UI_NEWPROJECTDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QComboBox>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QRadioButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QSpinBox>
#include <QtGui/QStackedWidget>
#include <QtGui/QTextEdit>
#include <QtGui/QToolButton>
#include <QtGui/QTreeWidget>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_NewProjectDialog
{
public:
    QVBoxLayout *vboxLayout;
    QGridLayout *gridLayout;
    QLabel *label_2_2;
    QLineEdit *newProjectName;
    QLabel *label;
    QTextEdit *descriptionTextEdit;
    QLabel *label_3_2;
    QLineEdit *newProjectEngineer;
    QHBoxLayout *hboxLayout;
    QRadioButton *radioButtonImport;
    QRadioButton *radioButtonEmpty;
    QStackedWidget *stackedWidget;
    QWidget *page;
    QGridLayout *gridLayout1;
    QGroupBox *groupBox;
    QVBoxLayout *vboxLayout1;
    QHBoxLayout *hboxLayout1;
    QToolButton *buttonAdd;
    QToolButton *buttonRemove;
    QSpacerItem *spacerItem;
    QToolButton *buttonUp;
    QToolButton *buttonDown;
    QHBoxLayout *hboxLayout2;
    QTreeWidget *treeWidgetFiles;
    QCheckBox *checkBoxCopy;
    QWidget *page_2;
    QGridLayout *gridLayout2;
    QGroupBox *groupBox_2;
    QVBoxLayout *vboxLayout2;
    QHBoxLayout *hboxLayout3;
    QLabel *label_2_2_2;
    QSpinBox *sheetCountSpinBox;
    QHBoxLayout *hboxLayout4;
    QLabel *label_2;
    QSpinBox *trackCountSpinBox;
    QHBoxLayout *hboxLayout5;
    QCheckBox *useTemplateCheckBox;
    QComboBox *templateComboBox;
    QSpacerItem *spacerItem1;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *NewProjectDialog)
    {
        if (NewProjectDialog->objectName().isEmpty())
            NewProjectDialog->setObjectName(QString::fromUtf8("NewProjectDialog"));
        NewProjectDialog->resize(414, 454);
        vboxLayout = new QVBoxLayout(NewProjectDialog);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label_2_2 = new QLabel(NewProjectDialog);
        label_2_2->setObjectName(QString::fromUtf8("label_2_2"));
        label_2_2->setMinimumSize(QSize(70, 0));

        gridLayout->addWidget(label_2_2, 0, 0, 1, 1);

        newProjectName = new QLineEdit(NewProjectDialog);
        newProjectName->setObjectName(QString::fromUtf8("newProjectName"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(2);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(newProjectName->sizePolicy().hasHeightForWidth());
        newProjectName->setSizePolicy(sizePolicy);

        gridLayout->addWidget(newProjectName, 0, 1, 1, 1);

        label = new QLabel(NewProjectDialog);
        label->setObjectName(QString::fromUtf8("label"));
        label->setMinimumSize(QSize(0, 0));

        gridLayout->addWidget(label, 1, 0, 1, 1);

        descriptionTextEdit = new QTextEdit(NewProjectDialog);
        descriptionTextEdit->setObjectName(QString::fromUtf8("descriptionTextEdit"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(descriptionTextEdit->sizePolicy().hasHeightForWidth());
        descriptionTextEdit->setSizePolicy(sizePolicy1);
        descriptionTextEdit->setMaximumSize(QSize(16777215, 50));

        gridLayout->addWidget(descriptionTextEdit, 1, 1, 1, 1);

        label_3_2 = new QLabel(NewProjectDialog);
        label_3_2->setObjectName(QString::fromUtf8("label_3_2"));
        label_3_2->setMinimumSize(QSize(70, 0));

        gridLayout->addWidget(label_3_2, 2, 0, 1, 1);

        newProjectEngineer = new QLineEdit(NewProjectDialog);
        newProjectEngineer->setObjectName(QString::fromUtf8("newProjectEngineer"));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Preferred);
        sizePolicy2.setHorizontalStretch(2);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(newProjectEngineer->sizePolicy().hasHeightForWidth());
        newProjectEngineer->setSizePolicy(sizePolicy2);

        gridLayout->addWidget(newProjectEngineer, 2, 1, 1, 1);


        vboxLayout->addLayout(gridLayout);

        hboxLayout = new QHBoxLayout();
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        radioButtonImport = new QRadioButton(NewProjectDialog);
        radioButtonImport->setObjectName(QString::fromUtf8("radioButtonImport"));
        radioButtonImport->setChecked(true);

        hboxLayout->addWidget(radioButtonImport);

        radioButtonEmpty = new QRadioButton(NewProjectDialog);
        radioButtonEmpty->setObjectName(QString::fromUtf8("radioButtonEmpty"));

        hboxLayout->addWidget(radioButtonEmpty);


        vboxLayout->addLayout(hboxLayout);

        stackedWidget = new QStackedWidget(NewProjectDialog);
        stackedWidget->setObjectName(QString::fromUtf8("stackedWidget"));
        sizePolicy1.setHeightForWidth(stackedWidget->sizePolicy().hasHeightForWidth());
        stackedWidget->setSizePolicy(sizePolicy1);
        page = new QWidget();
        page->setObjectName(QString::fromUtf8("page"));
        gridLayout1 = new QGridLayout(page);
        gridLayout1->setObjectName(QString::fromUtf8("gridLayout1"));
        groupBox = new QGroupBox(page);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        vboxLayout1 = new QVBoxLayout(groupBox);
        vboxLayout1->setObjectName(QString::fromUtf8("vboxLayout1"));
        hboxLayout1 = new QHBoxLayout();
        hboxLayout1->setObjectName(QString::fromUtf8("hboxLayout1"));
        buttonAdd = new QToolButton(groupBox);
        buttonAdd->setObjectName(QString::fromUtf8("buttonAdd"));

        hboxLayout1->addWidget(buttonAdd);

        buttonRemove = new QToolButton(groupBox);
        buttonRemove->setObjectName(QString::fromUtf8("buttonRemove"));

        hboxLayout1->addWidget(buttonRemove);

        spacerItem = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout1->addItem(spacerItem);

        buttonUp = new QToolButton(groupBox);
        buttonUp->setObjectName(QString::fromUtf8("buttonUp"));

        hboxLayout1->addWidget(buttonUp);

        buttonDown = new QToolButton(groupBox);
        buttonDown->setObjectName(QString::fromUtf8("buttonDown"));

        hboxLayout1->addWidget(buttonDown);


        vboxLayout1->addLayout(hboxLayout1);

        hboxLayout2 = new QHBoxLayout();
        hboxLayout2->setObjectName(QString::fromUtf8("hboxLayout2"));
        treeWidgetFiles = new QTreeWidget(groupBox);
        treeWidgetFiles->setObjectName(QString::fromUtf8("treeWidgetFiles"));
        treeWidgetFiles->setSelectionMode(QAbstractItemView::ExtendedSelection);
        treeWidgetFiles->setRootIsDecorated(false);
        treeWidgetFiles->setColumnCount(2);

        hboxLayout2->addWidget(treeWidgetFiles);


        vboxLayout1->addLayout(hboxLayout2);

        checkBoxCopy = new QCheckBox(groupBox);
        checkBoxCopy->setObjectName(QString::fromUtf8("checkBoxCopy"));

        vboxLayout1->addWidget(checkBoxCopy);


        gridLayout1->addWidget(groupBox, 0, 0, 1, 1);

        stackedWidget->addWidget(page);
        page_2 = new QWidget();
        page_2->setObjectName(QString::fromUtf8("page_2"));
        gridLayout2 = new QGridLayout(page_2);
        gridLayout2->setObjectName(QString::fromUtf8("gridLayout2"));
        groupBox_2 = new QGroupBox(page_2);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        vboxLayout2 = new QVBoxLayout(groupBox_2);
        vboxLayout2->setObjectName(QString::fromUtf8("vboxLayout2"));
        hboxLayout3 = new QHBoxLayout();
        hboxLayout3->setSpacing(6);
        hboxLayout3->setObjectName(QString::fromUtf8("hboxLayout3"));
        hboxLayout3->setContentsMargins(0, 0, 0, 0);
        label_2_2_2 = new QLabel(groupBox_2);
        label_2_2_2->setObjectName(QString::fromUtf8("label_2_2_2"));
        QSizePolicy sizePolicy3(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy3.setHorizontalStretch(2);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(label_2_2_2->sizePolicy().hasHeightForWidth());
        label_2_2_2->setSizePolicy(sizePolicy3);

        hboxLayout3->addWidget(label_2_2_2);

        sheetCountSpinBox = new QSpinBox(groupBox_2);
        sheetCountSpinBox->setObjectName(QString::fromUtf8("sheetCountSpinBox"));
        QSizePolicy sizePolicy4(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy4.setHorizontalStretch(1);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(sheetCountSpinBox->sizePolicy().hasHeightForWidth());
        sheetCountSpinBox->setSizePolicy(sizePolicy4);
        sheetCountSpinBox->setMinimumSize(QSize(40, 0));
        sheetCountSpinBox->setMinimum(1);
        sheetCountSpinBox->setValue(1);

        hboxLayout3->addWidget(sheetCountSpinBox);


        vboxLayout2->addLayout(hboxLayout3);

        hboxLayout4 = new QHBoxLayout();
        hboxLayout4->setSpacing(6);
        hboxLayout4->setObjectName(QString::fromUtf8("hboxLayout4"));
        hboxLayout4->setContentsMargins(0, 0, 0, 0);
        label_2 = new QLabel(groupBox_2);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        sizePolicy3.setHeightForWidth(label_2->sizePolicy().hasHeightForWidth());
        label_2->setSizePolicy(sizePolicy3);

        hboxLayout4->addWidget(label_2);

        trackCountSpinBox = new QSpinBox(groupBox_2);
        trackCountSpinBox->setObjectName(QString::fromUtf8("trackCountSpinBox"));
        sizePolicy4.setHeightForWidth(trackCountSpinBox->sizePolicy().hasHeightForWidth());
        trackCountSpinBox->setSizePolicy(sizePolicy4);

        hboxLayout4->addWidget(trackCountSpinBox);


        vboxLayout2->addLayout(hboxLayout4);

        hboxLayout5 = new QHBoxLayout();
        hboxLayout5->setSpacing(6);
        hboxLayout5->setObjectName(QString::fromUtf8("hboxLayout5"));
        hboxLayout5->setContentsMargins(0, 0, 0, 0);
        useTemplateCheckBox = new QCheckBox(groupBox_2);
        useTemplateCheckBox->setObjectName(QString::fromUtf8("useTemplateCheckBox"));

        hboxLayout5->addWidget(useTemplateCheckBox);

        templateComboBox = new QComboBox(groupBox_2);
        templateComboBox->setObjectName(QString::fromUtf8("templateComboBox"));
        QSizePolicy sizePolicy5(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy5.setHorizontalStretch(2);
        sizePolicy5.setVerticalStretch(0);
        sizePolicy5.setHeightForWidth(templateComboBox->sizePolicy().hasHeightForWidth());
        templateComboBox->setSizePolicy(sizePolicy5);

        hboxLayout5->addWidget(templateComboBox);


        vboxLayout2->addLayout(hboxLayout5);

        spacerItem1 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        vboxLayout2->addItem(spacerItem1);


        gridLayout2->addWidget(groupBox_2, 0, 0, 1, 1);

        stackedWidget->addWidget(page_2);

        vboxLayout->addWidget(stackedWidget);

        buttonBox = new QDialogButtonBox(NewProjectDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::NoButton|QDialogButtonBox::Ok);

        vboxLayout->addWidget(buttonBox);


        retranslateUi(NewProjectDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), NewProjectDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), NewProjectDialog, SLOT(reject()));

        stackedWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(NewProjectDialog);
    } // setupUi

    void retranslateUi(QDialog *NewProjectDialog)
    {
        NewProjectDialog->setWindowTitle(QApplication::translate("NewProjectDialog", "New Project", 0, QApplication::UnicodeUTF8));
        label_2_2->setText(QApplication::translate("NewProjectDialog", "Name", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("NewProjectDialog", "Description", 0, QApplication::UnicodeUTF8));
        label_3_2->setText(QApplication::translate("NewProjectDialog", "Engineer", 0, QApplication::UnicodeUTF8));
        radioButtonImport->setText(QApplication::translate("NewProjectDialog", "Import Audio Files", 0, QApplication::UnicodeUTF8));
        radioButtonEmpty->setText(QApplication::translate("NewProjectDialog", "Empty Project", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("NewProjectDialog", "Import Audio Files", 0, QApplication::UnicodeUTF8));
        buttonAdd->setText(QApplication::translate("NewProjectDialog", "...", 0, QApplication::UnicodeUTF8));
        buttonRemove->setText(QApplication::translate("NewProjectDialog", "...", 0, QApplication::UnicodeUTF8));
        buttonUp->setText(QApplication::translate("NewProjectDialog", "...", 0, QApplication::UnicodeUTF8));
        buttonDown->setText(QApplication::translate("NewProjectDialog", "...", 0, QApplication::UnicodeUTF8));
        QTreeWidgetItem *___qtreewidgetitem = treeWidgetFiles->headerItem();
        ___qtreewidgetitem->setText(1, QApplication::translate("NewProjectDialog", "File", 0, QApplication::UnicodeUTF8));
        ___qtreewidgetitem->setText(0, QApplication::translate("NewProjectDialog", "Track Name", 0, QApplication::UnicodeUTF8));
        checkBoxCopy->setText(QApplication::translate("NewProjectDialog", "Copy files to project directory", 0, QApplication::UnicodeUTF8));
        groupBox_2->setTitle(QApplication::translate("NewProjectDialog", "Empty Project", 0, QApplication::UnicodeUTF8));
        label_2_2_2->setText(QApplication::translate("NewProjectDialog", "Number of Sheets", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("NewProjectDialog", "Tracks per Sheet", 0, QApplication::UnicodeUTF8));
        useTemplateCheckBox->setText(QApplication::translate("NewProjectDialog", "Use Template", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class NewProjectDialog: public Ui_NewProjectDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_NEWPROJECTDIALOG_H
