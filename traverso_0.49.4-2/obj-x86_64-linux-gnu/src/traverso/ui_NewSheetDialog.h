/********************************************************************************
** Form generated from reading UI file 'NewSheetDialog.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_NEWSHEETDIALOG_H
#define UI_NEWSHEETDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QComboBox>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QSpacerItem>
#include <QtGui/QSpinBox>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_NewSheetDialog
{
public:
    QVBoxLayout *vboxLayout;
    QHBoxLayout *hboxLayout;
    QVBoxLayout *vboxLayout1;
    QLabel *label;
    QLabel *label_3;
    QLabel *label_4;
    QCheckBox *useTemplateCheckBox;
    QVBoxLayout *vboxLayout2;
    QLineEdit *titleLineEdit;
    QHBoxLayout *hboxLayout1;
    QSpacerItem *spacerItem;
    QSpinBox *countSpinBox;
    QHBoxLayout *hboxLayout2;
    QSpacerItem *spacerItem1;
    QSpinBox *trackCountSpinBox;
    QComboBox *templateComboBox;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *NewSheetDialog)
    {
        if (NewSheetDialog->objectName().isEmpty())
            NewSheetDialog->setObjectName(QString::fromUtf8("NewSheetDialog"));
        NewSheetDialog->setWindowModality(Qt::NonModal);
        NewSheetDialog->resize(286, 174);
        vboxLayout = new QVBoxLayout(NewSheetDialog);
#ifndef Q_OS_MAC
        vboxLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        hboxLayout = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout->setContentsMargins(0, 0, 0, 0);
#endif
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        vboxLayout1 = new QVBoxLayout();
#ifndef Q_OS_MAC
        vboxLayout1->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout1->setContentsMargins(0, 0, 0, 0);
#endif
        vboxLayout1->setObjectName(QString::fromUtf8("vboxLayout1"));
        label = new QLabel(NewSheetDialog);
        label->setObjectName(QString::fromUtf8("label"));

        vboxLayout1->addWidget(label);

        label_3 = new QLabel(NewSheetDialog);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        vboxLayout1->addWidget(label_3);

        label_4 = new QLabel(NewSheetDialog);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        vboxLayout1->addWidget(label_4);

        useTemplateCheckBox = new QCheckBox(NewSheetDialog);
        useTemplateCheckBox->setObjectName(QString::fromUtf8("useTemplateCheckBox"));

        vboxLayout1->addWidget(useTemplateCheckBox);


        hboxLayout->addLayout(vboxLayout1);

        vboxLayout2 = new QVBoxLayout();
#ifndef Q_OS_MAC
        vboxLayout2->setSpacing(6);
#endif
        vboxLayout2->setContentsMargins(0, 0, 0, 0);
        vboxLayout2->setObjectName(QString::fromUtf8("vboxLayout2"));
        titleLineEdit = new QLineEdit(NewSheetDialog);
        titleLineEdit->setObjectName(QString::fromUtf8("titleLineEdit"));

        vboxLayout2->addWidget(titleLineEdit);

        hboxLayout1 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout1->setSpacing(6);
#endif
        hboxLayout1->setContentsMargins(0, 0, 0, 0);
        hboxLayout1->setObjectName(QString::fromUtf8("hboxLayout1"));
        spacerItem = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout1->addItem(spacerItem);

        countSpinBox = new QSpinBox(NewSheetDialog);
        countSpinBox->setObjectName(QString::fromUtf8("countSpinBox"));
        QSizePolicy sizePolicy(static_cast<QSizePolicy::Policy>(1), static_cast<QSizePolicy::Policy>(0));
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(countSpinBox->sizePolicy().hasHeightForWidth());
        countSpinBox->setSizePolicy(sizePolicy);
        countSpinBox->setMinimum(1);
        countSpinBox->setValue(1);

        hboxLayout1->addWidget(countSpinBox);


        vboxLayout2->addLayout(hboxLayout1);

        hboxLayout2 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout2->setSpacing(6);
#endif
        hboxLayout2->setContentsMargins(0, 0, 0, 0);
        hboxLayout2->setObjectName(QString::fromUtf8("hboxLayout2"));
        spacerItem1 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout2->addItem(spacerItem1);

        trackCountSpinBox = new QSpinBox(NewSheetDialog);
        trackCountSpinBox->setObjectName(QString::fromUtf8("trackCountSpinBox"));
        trackCountSpinBox->setWindowModality(Qt::NonModal);
        sizePolicy.setHeightForWidth(trackCountSpinBox->sizePolicy().hasHeightForWidth());
        trackCountSpinBox->setSizePolicy(sizePolicy);

        hboxLayout2->addWidget(trackCountSpinBox);


        vboxLayout2->addLayout(hboxLayout2);

        templateComboBox = new QComboBox(NewSheetDialog);
        templateComboBox->setObjectName(QString::fromUtf8("templateComboBox"));

        vboxLayout2->addWidget(templateComboBox);


        hboxLayout->addLayout(vboxLayout2);


        vboxLayout->addLayout(hboxLayout);

        buttonBox = new QDialogButtonBox(NewSheetDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::NoButton|QDialogButtonBox::Ok);

        vboxLayout->addWidget(buttonBox);


        retranslateUi(NewSheetDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), NewSheetDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), NewSheetDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(NewSheetDialog);
    } // setupUi

    void retranslateUi(QDialog *NewSheetDialog)
    {
        NewSheetDialog->setWindowTitle(QApplication::translate("NewSheetDialog", "New Sheet(s)", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("NewSheetDialog", "New Sheet name", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("NewSheetDialog", "Sheet count", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("NewSheetDialog", "Track count", 0, QApplication::UnicodeUTF8));
        useTemplateCheckBox->setText(QApplication::translate("NewSheetDialog", "Use Template", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class NewSheetDialog: public Ui_NewSheetDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_NEWSHEETDIALOG_H
