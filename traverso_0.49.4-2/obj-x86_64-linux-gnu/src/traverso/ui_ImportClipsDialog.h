/********************************************************************************
** Form generated from reading UI file 'ImportClipsDialog.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_IMPORTCLIPSDIALOG_H
#define UI_IMPORTCLIPSDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QComboBox>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_ImportClipsDialog
{
public:
    QVBoxLayout *vboxLayout;
    QHBoxLayout *hboxLayout;
    QLabel *label;
    QComboBox *comboBoxTrack;
    QCheckBox *checkBoxMarkers;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *ImportClipsDialog)
    {
        if (ImportClipsDialog->objectName().isEmpty())
            ImportClipsDialog->setObjectName(QString::fromUtf8("ImportClipsDialog"));
        ImportClipsDialog->resize(344, 103);
        vboxLayout = new QVBoxLayout(ImportClipsDialog);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        hboxLayout = new QHBoxLayout();
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        label = new QLabel(ImportClipsDialog);
        label->setObjectName(QString::fromUtf8("label"));

        hboxLayout->addWidget(label);

        comboBoxTrack = new QComboBox(ImportClipsDialog);
        comboBoxTrack->setObjectName(QString::fromUtf8("comboBoxTrack"));

        hboxLayout->addWidget(comboBoxTrack);


        vboxLayout->addLayout(hboxLayout);

        checkBoxMarkers = new QCheckBox(ImportClipsDialog);
        checkBoxMarkers->setObjectName(QString::fromUtf8("checkBoxMarkers"));

        vboxLayout->addWidget(checkBoxMarkers);

        buttonBox = new QDialogButtonBox(ImportClipsDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::NoButton|QDialogButtonBox::Ok);

        vboxLayout->addWidget(buttonBox);


        retranslateUi(ImportClipsDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), ImportClipsDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), ImportClipsDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(ImportClipsDialog);
    } // setupUi

    void retranslateUi(QDialog *ImportClipsDialog)
    {
        ImportClipsDialog->setWindowTitle(QApplication::translate("ImportClipsDialog", "Import Audio Clips", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("ImportClipsDialog", "Import to Track:", 0, QApplication::UnicodeUTF8));
        checkBoxMarkers->setText(QApplication::translate("ImportClipsDialog", "Add Markers", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class ImportClipsDialog: public Ui_ImportClipsDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_IMPORTCLIPSDIALOG_H
