/****************************************************************************
** Meta object code from reading C++ file 'Interface.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/traverso/Interface.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Interface.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Interface[] = {

 // content:
       6,       // revision
       0,       // classname
       8,   14, // classinfo
      51,   30, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // classinfo: key, value
      29,   10,
      66,   48,
      99,   84,
     145,  114,
     185,  173,
     211,  197,
     230,  225,
     253,  246,

 // slots: signature, parameters, type, tag, flags
     297,  289,  288,  288, 0x0a,
     325,  319,  288,  288, 0x0a,
     344,  288,  288,  288, 0x0a,
     367,  288,  288,  288, 0x0a,
     408,  288,  288,  288, 0x0a,
     435,  428,  288,  288, 0x0a,
     473,  428,  288,  288, 0x0a,
     501,  428,  288,  288, 0x0a,
     530,  288,  288,  288, 0x0a,
     547,  288,  288,  288, 0x0a,
     562,  288,  288,  288, 0x0a,
     599,  288,  288,  288, 0x0a,
     632,  288,  288,  288, 0x0a,
     667,  288,  288,  288, 0x0a,
     704,  288,  288,  288, 0x0a,
     738,  288,  288,  288, 0x0a,
     772,  288,  288,  288, 0x0a,
     808,  288,  288,  288, 0x0a,
     851,  288,  842,  288, 0x0a,
     865,  288,  842,  288, 0x0a,
     882,  288,  842,  288, 0x0a,
     896,  288,  842,  288, 0x0a,
     912,  288,  842,  288, 0x0a,
     933,  288,  842,  288, 0x0a,
     954,  288,  842,  288, 0x0a,
     979,  288,  842,  288, 0x0a,
     999,  288,  842,  288, 0x0a,
    1026,  288,  842,  288, 0x0a,
    1067, 1056,  842,  288, 0x0a,
    1111,  288,  842,  288, 0x0a,
    1139,  288,  842,  288, 0x0a,
    1160,  288,  842,  288, 0x0a,
    1183,  288,  842,  288, 0x0a,
    1206,  288,  842,  288, 0x0a,
    1231,  288,  842,  288, 0x0a,
    1249,  288,  842,  288, 0x0a,
    1286,  288,  288,  288, 0x08,
    1313,  288,  288,  288, 0x08,
    1358, 1343,  288,  288, 0x08,
    1415, 1395,  288,  288, 0x08,
    1460, 1454,  288,  288, 0x08,
    1485,  288,  288,  288, 0x08,
    1505, 1454,  288,  288, 0x08,
    1532,  288,  288,  288, 0x08,
    1555, 1454,  288,  288, 0x08,
    1582,  288,  288,  288, 0x08,
    1604, 1454,  288,  288, 0x08,
    1635,  288,  288,  288, 0x08,
    1666,  288,  288,  288, 0x08,
    1683,  288,  288,  288, 0x08,
    1718,  288,  288,  288, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_Interface[] = {
    "Interface\0Show Export Dialog\0"
    "show_export_widget\0Show Context Menu\0"
    "show_context_menu\0About Traverso\0"
    "about_traverso\0Show Project Management Dialog\0"
    "show_project_manager_dialog\0Full Screen\0"
    "full_screen\0Export keymap\0export_keymap\0"
    "Play\0start_transport\0Record\0"
    "set_recordable_and_start_transport\0\0"
    "project\0set_project(Project*)\0sheet\0"
    "show_sheet(Sheet*)\0show_settings_dialog()\0"
    "show_settings_dialog_sound_system_page()\0"
    "open_help_browser()\0action\0"
    "process_context_menu_action(QAction*)\0"
    "set_fade_in_shape(QAction*)\0"
    "set_fade_out_shape(QAction*)\0"
    "config_changed()\0import_audio()\0"
    "show_restore_project_backup_dialog()\0"
    "change_recording_format_to_wav()\0"
    "change_recording_format_to_wav64()\0"
    "change_recording_format_to_wavpack()\0"
    "change_resample_quality_to_best()\0"
    "change_resample_quality_to_high()\0"
    "change_resample_quality_to_medium()\0"
    "change_resample_quality_to_fast()\0"
    "Command*\0full_screen()\0about_traverso()\0"
    "quick_start()\0export_keymap()\0"
    "get_keymap(QString&)\0show_export_widget()\0"
    "show_cd_writing_dialog()\0show_context_menu()\0"
    "show_open_project_dialog()\0"
    "show_project_manager_dialog()\0projectdir\0"
    "show_restore_project_backup_dialog(QString)\0"
    "show_insertsilence_dialog()\0"
    "show_marker_dialog()\0show_newsheet_dialog()\0"
    "show_newtrack_dialog()\0show_newproject_dialog()\0"
    "start_transport()\0"
    "set_recordable_and_start_transport()\0"
    "delete_sheetwidget(Sheet*)\0"
    "project_dir_change_detected()\0"
    "project,reason\0project_load_failed(QString,QString)\0"
    "rootdir,projectname\0"
    "project_file_mismatch(QString,QString)\0"
    "state\0snap_state_changed(bool)\0"
    "update_snap_state()\0effect_state_changed(bool)\0"
    "update_effects_state()\0"
    "follow_state_changed(bool)\0"
    "update_follow_state()\0"
    "update_temp_follow_state(bool)\0"
    "sheet_selector_update_sheets()\0"
    "sheet_selected()\0sheet_selector_sheet_added(Sheet*)\0"
    "sheet_selector_sheet_removed(Sheet*)\0"
};

void Interface::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        Interface *_t = static_cast<Interface *>(_o);
        switch (_id) {
        case 0: _t->set_project((*reinterpret_cast< Project*(*)>(_a[1]))); break;
        case 1: _t->show_sheet((*reinterpret_cast< Sheet*(*)>(_a[1]))); break;
        case 2: _t->show_settings_dialog(); break;
        case 3: _t->show_settings_dialog_sound_system_page(); break;
        case 4: _t->open_help_browser(); break;
        case 5: _t->process_context_menu_action((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        case 6: _t->set_fade_in_shape((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        case 7: _t->set_fade_out_shape((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        case 8: _t->config_changed(); break;
        case 9: _t->import_audio(); break;
        case 10: _t->show_restore_project_backup_dialog(); break;
        case 11: _t->change_recording_format_to_wav(); break;
        case 12: _t->change_recording_format_to_wav64(); break;
        case 13: _t->change_recording_format_to_wavpack(); break;
        case 14: _t->change_resample_quality_to_best(); break;
        case 15: _t->change_resample_quality_to_high(); break;
        case 16: _t->change_resample_quality_to_medium(); break;
        case 17: _t->change_resample_quality_to_fast(); break;
        case 18: { Command* _r = _t->full_screen();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 19: { Command* _r = _t->about_traverso();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 20: { Command* _r = _t->quick_start();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 21: { Command* _r = _t->export_keymap();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 22: { Command* _r = _t->get_keymap((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 23: { Command* _r = _t->show_export_widget();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 24: { Command* _r = _t->show_cd_writing_dialog();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 25: { Command* _r = _t->show_context_menu();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 26: { Command* _r = _t->show_open_project_dialog();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 27: { Command* _r = _t->show_project_manager_dialog();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 28: { Command* _r = _t->show_restore_project_backup_dialog((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 29: { Command* _r = _t->show_insertsilence_dialog();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 30: { Command* _r = _t->show_marker_dialog();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 31: { Command* _r = _t->show_newsheet_dialog();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 32: { Command* _r = _t->show_newtrack_dialog();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 33: { Command* _r = _t->show_newproject_dialog();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 34: { Command* _r = _t->start_transport();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 35: { Command* _r = _t->set_recordable_and_start_transport();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = _r; }  break;
        case 36: _t->delete_sheetwidget((*reinterpret_cast< Sheet*(*)>(_a[1]))); break;
        case 37: _t->project_dir_change_detected(); break;
        case 38: _t->project_load_failed((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 39: _t->project_file_mismatch((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 40: _t->snap_state_changed((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 41: _t->update_snap_state(); break;
        case 42: _t->effect_state_changed((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 43: _t->update_effects_state(); break;
        case 44: _t->follow_state_changed((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 45: _t->update_follow_state(); break;
        case 46: _t->update_temp_follow_state((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 47: _t->sheet_selector_update_sheets(); break;
        case 48: _t->sheet_selected(); break;
        case 49: _t->sheet_selector_sheet_added((*reinterpret_cast< Sheet*(*)>(_a[1]))); break;
        case 50: _t->sheet_selector_sheet_removed((*reinterpret_cast< Sheet*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData Interface::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject Interface::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_Interface,
      qt_meta_data_Interface, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Interface::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Interface::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Interface::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Interface))
        return static_cast<void*>(const_cast< Interface*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int Interface::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 51)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 51;
    }
    return _id;
}
static const uint qt_meta_data_DigitalClock[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      14,   13,   13,   13, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_DigitalClock[] = {
    "DigitalClock\0\0showTime()\0"
};

void DigitalClock::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        DigitalClock *_t = static_cast<DigitalClock *>(_o);
        switch (_id) {
        case 0: _t->showTime(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData DigitalClock::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject DigitalClock::staticMetaObject = {
    { &QLCDNumber::staticMetaObject, qt_meta_stringdata_DigitalClock,
      qt_meta_data_DigitalClock, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &DigitalClock::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *DigitalClock::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *DigitalClock::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_DigitalClock))
        return static_cast<void*>(const_cast< DigitalClock*>(this));
    return QLCDNumber::qt_metacast(_clname);
}

int DigitalClock::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QLCDNumber::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
