/********************************************************************************
** Form generated from reading UI file 'KeyboardConfigPage.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_KEYBOARDCONFIGPAGE_H
#define UI_KEYBOARDCONFIGPAGE_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QSpinBox>
#include <QtGui/QTextEdit>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_KeyboardConfigPage
{
public:
    QVBoxLayout *vboxLayout;
    QGroupBox *groupBox;
    QVBoxLayout *vboxLayout1;
    QHBoxLayout *hboxLayout;
    QLabel *label;
    QSpinBox *doubleFactTimeoutSpinBox;
    QHBoxLayout *hboxLayout1;
    QLabel *label_3;
    QSpinBox *holdTimeoutSpinBox;
    QGroupBox *groupBox_2;
    QVBoxLayout *vboxLayout2;
    QHBoxLayout *hboxLayout2;
    QLabel *label_2;
    QComboBox *keymapComboBox;
    QTextEdit *descriptionTextEdit;
    QHBoxLayout *hboxLayout3;
    QSpacerItem *spacerItem;
    QPushButton *exportButton;
    QPushButton *printButton;
    QSpacerItem *spacerItem1;

    void setupUi(QWidget *KeyboardConfigPage)
    {
        if (KeyboardConfigPage->objectName().isEmpty())
            KeyboardConfigPage->setObjectName(QString::fromUtf8("KeyboardConfigPage"));
        KeyboardConfigPage->resize(290, 293);
        vboxLayout = new QVBoxLayout(KeyboardConfigPage);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        groupBox = new QGroupBox(KeyboardConfigPage);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        vboxLayout1 = new QVBoxLayout(groupBox);
        vboxLayout1->setObjectName(QString::fromUtf8("vboxLayout1"));
        hboxLayout = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout->setContentsMargins(0, 0, 0, 0);
#endif
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));

        hboxLayout->addWidget(label);

        doubleFactTimeoutSpinBox = new QSpinBox(groupBox);
        doubleFactTimeoutSpinBox->setObjectName(QString::fromUtf8("doubleFactTimeoutSpinBox"));
        doubleFactTimeoutSpinBox->setMaximumSize(QSize(100, 16777215));
        doubleFactTimeoutSpinBox->setMinimum(100);
        doubleFactTimeoutSpinBox->setMaximum(300);
        doubleFactTimeoutSpinBox->setValue(200);

        hboxLayout->addWidget(doubleFactTimeoutSpinBox);


        vboxLayout1->addLayout(hboxLayout);

        hboxLayout1 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout1->setSpacing(6);
#endif
        hboxLayout1->setContentsMargins(0, 0, 0, 0);
        hboxLayout1->setObjectName(QString::fromUtf8("hboxLayout1"));
        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        hboxLayout1->addWidget(label_3);

        holdTimeoutSpinBox = new QSpinBox(groupBox);
        holdTimeoutSpinBox->setObjectName(QString::fromUtf8("holdTimeoutSpinBox"));
        holdTimeoutSpinBox->setMaximumSize(QSize(100, 16777215));
        holdTimeoutSpinBox->setMinimum(100);
        holdTimeoutSpinBox->setMaximum(300);
        holdTimeoutSpinBox->setValue(200);

        hboxLayout1->addWidget(holdTimeoutSpinBox);


        vboxLayout1->addLayout(hboxLayout1);


        vboxLayout->addWidget(groupBox);

        groupBox_2 = new QGroupBox(KeyboardConfigPage);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        vboxLayout2 = new QVBoxLayout(groupBox_2);
        vboxLayout2->setObjectName(QString::fromUtf8("vboxLayout2"));
        hboxLayout2 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout2->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout2->setContentsMargins(0, 0, 0, 0);
#endif
        hboxLayout2->setObjectName(QString::fromUtf8("hboxLayout2"));
        label_2 = new QLabel(groupBox_2);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        hboxLayout2->addWidget(label_2);

        keymapComboBox = new QComboBox(groupBox_2);
        keymapComboBox->setObjectName(QString::fromUtf8("keymapComboBox"));

        hboxLayout2->addWidget(keymapComboBox);


        vboxLayout2->addLayout(hboxLayout2);

        descriptionTextEdit = new QTextEdit(groupBox_2);
        descriptionTextEdit->setObjectName(QString::fromUtf8("descriptionTextEdit"));
        descriptionTextEdit->setEnabled(false);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(descriptionTextEdit->sizePolicy().hasHeightForWidth());
        descriptionTextEdit->setSizePolicy(sizePolicy);
        descriptionTextEdit->setAcceptDrops(false);
        descriptionTextEdit->setFrameShape(QFrame::NoFrame);
        descriptionTextEdit->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        descriptionTextEdit->setUndoRedoEnabled(false);
        descriptionTextEdit->setTextInteractionFlags(Qt::NoTextInteraction);

        vboxLayout2->addWidget(descriptionTextEdit);

        hboxLayout3 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout3->setSpacing(6);
#endif
        hboxLayout3->setContentsMargins(0, 0, 0, 0);
        hboxLayout3->setObjectName(QString::fromUtf8("hboxLayout3"));
        spacerItem = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout3->addItem(spacerItem);

        exportButton = new QPushButton(groupBox_2);
        exportButton->setObjectName(QString::fromUtf8("exportButton"));

        hboxLayout3->addWidget(exportButton);

        printButton = new QPushButton(groupBox_2);
        printButton->setObjectName(QString::fromUtf8("printButton"));

        hboxLayout3->addWidget(printButton);


        vboxLayout2->addLayout(hboxLayout3);


        vboxLayout->addWidget(groupBox_2);

        spacerItem1 = new QSpacerItem(236, 16, QSizePolicy::Minimum, QSizePolicy::Expanding);

        vboxLayout->addItem(spacerItem1);


        retranslateUi(KeyboardConfigPage);

        QMetaObject::connectSlotsByName(KeyboardConfigPage);
    } // setupUi

    void retranslateUi(QWidget *KeyboardConfigPage)
    {
        KeyboardConfigPage->setWindowTitle(QApplication::translate("KeyboardConfigPage", "Form", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("KeyboardConfigPage", "Configure Keyboard", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        label->setToolTip(QApplication::translate("KeyboardConfigPage", "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">Double fact timeout:</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">The maximum time in miliseconds between 2 key presses </p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">to determine if the 2 key presses are a double fact ( &lt;&lt; K &gt;&gt; or &lt;&lt; KK &gt;&gt;)</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"> or 2 individual key presses ( a &lt; K &gt; "
                        "and &lt; K &gt; action, </p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">or &lt; KK &gt; and &lt; KK &gt; action for example).</p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Experienced users can set this value as low as 150 ms, </p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">if you don't have much experience yet, please leave the default of 180 ms.</p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-ind"
                        "ent:0px;\">For more information, see chapter 7: Key Actions. of the User Manual</p></body></html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        label->setText(QApplication::translate("KeyboardConfigPage", "<html><head><meta name=\"qrichtext\" content=\"1\" /></head><body style=\" white-space: pre-wrap; font-family:Bitstream Vera Sans; font-weight:400; font-style:normal; text-decoration:none;\"><p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Double fact timeout (ms)</p></body></html>", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        label_3->setToolTip(QApplication::translate("KeyboardConfigPage", "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">Hold timeout:</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">The maximum time to consider a pressed key a hold key fact, </p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">like [ K ] or [ KK ].</p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-blo"
                        "ck-indent:0; text-indent:0px;\">The shorter this time, the sooner a pressed key will be </p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">detected as a hold action. </p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Experienced users can set this value as low as 110 ms, </p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">if you don't have much experience yet, please leave the default of 150 ms.</p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-r"
                        "ight:0px; -qt-block-indent:0; text-indent:0px;\">For more information, see chapter 7: \"Key Actions\" of the User Manual.</p></body></html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        label_3->setText(QApplication::translate("KeyboardConfigPage", "<html><head><meta name=\"qrichtext\" content=\"1\" /></head><body style=\" white-space: pre-wrap; font-family:Bitstream Vera Sans; font-weight:400; font-style:normal; text-decoration:none;\"><p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Hold timeout (ms)</p></body></html>", 0, QApplication::UnicodeUTF8));
        groupBox_2->setTitle(QApplication::translate("KeyboardConfigPage", "Keymap", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("KeyboardConfigPage", "Select keymap", 0, QApplication::UnicodeUTF8));
        exportButton->setText(QApplication::translate("KeyboardConfigPage", "Export Keymap", 0, QApplication::UnicodeUTF8));
        printButton->setText(QApplication::translate("KeyboardConfigPage", "Print Keymap", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class KeyboardConfigPage: public Ui_KeyboardConfigPage {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_KEYBOARDCONFIGPAGE_H
