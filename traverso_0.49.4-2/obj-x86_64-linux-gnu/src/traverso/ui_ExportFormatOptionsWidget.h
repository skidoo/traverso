/********************************************************************************
** Form generated from reading UI file 'ExportFormatOptionsWidget.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EXPORTFORMATOPTIONSWIDGET_H
#define UI_EXPORTFORMATOPTIONSWIDGET_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QComboBox>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QSlider>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ExportFormatOptionsWidget
{
public:
    QVBoxLayout *vboxLayout;
    QGroupBox *exportSpecificationsGroupBox;
    QVBoxLayout *vboxLayout1;
    QGroupBox *groupBox_2;
    QVBoxLayout *vboxLayout2;
    QHBoxLayout *hboxLayout;
    QVBoxLayout *vboxLayout3;
    QLabel *label_2;
    QLabel *label_4;
    QVBoxLayout *vboxLayout4;
    QComboBox *audioTypeComboBox;
    QComboBox *channelComboBox;
    QVBoxLayout *vboxLayout5;
    QCheckBox *normalizeCheckBox;
    QHBoxLayout *hboxLayout1;
    QLabel *label_3;
    QComboBox *bitdepthComboBox;
    QSpacerItem *spacerItem;
    QHBoxLayout *hboxLayout2;
    QGroupBox *wacpackGroupBox;
    QVBoxLayout *vboxLayout6;
    QHBoxLayout *hboxLayout3;
    QLabel *label_17;
    QComboBox *wavpackCompressionComboBox;
    QCheckBox *skipWVXCheckBox;
    QGroupBox *oggOptionsGroupBox;
    QGridLayout *gridLayout;
    QLabel *oggBitrateLabel;
    QComboBox *oggBitrateComboBox;
    QSlider *oggQualitySlider;
    QComboBox *oggMethodComboBox;
    QLabel *oggQualityLabel;
    QLabel *label_18;
    QGroupBox *mp3OptionsGroupBox;
    QGridLayout *gridLayout1;
    QLabel *label_15;
    QLabel *mp3MaxBitrateLabel;
    QLabel *mp3MinBitrateLabel;
    QLabel *label_16;
    QComboBox *mp3MaxBitrateComboBox;
    QComboBox *mp3MinBitrateComboBox;
    QComboBox *mp3MethodComboBox;
    QSlider *mp3QualitySlider;
    QGroupBox *groupBox;
    QHBoxLayout *hboxLayout4;
    QHBoxLayout *hboxLayout5;
    QLabel *label_7;
    QComboBox *sampleRateComboBox;
    QLabel *label_8;
    QComboBox *resampleQualityComboBox;
    QSpacerItem *spacerItem1;

    void setupUi(QWidget *ExportFormatOptionsWidget)
    {
        if (ExportFormatOptionsWidget->objectName().isEmpty())
            ExportFormatOptionsWidget->setObjectName(QString::fromUtf8("ExportFormatOptionsWidget"));
        ExportFormatOptionsWidget->resize(866, 414);
        vboxLayout = new QVBoxLayout(ExportFormatOptionsWidget);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        vboxLayout->setContentsMargins(0, 0, 0, 0);
        exportSpecificationsGroupBox = new QGroupBox(ExportFormatOptionsWidget);
        exportSpecificationsGroupBox->setObjectName(QString::fromUtf8("exportSpecificationsGroupBox"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(exportSpecificationsGroupBox->sizePolicy().hasHeightForWidth());
        exportSpecificationsGroupBox->setSizePolicy(sizePolicy);
        vboxLayout1 = new QVBoxLayout(exportSpecificationsGroupBox);
        vboxLayout1->setSpacing(9);
        vboxLayout1->setObjectName(QString::fromUtf8("vboxLayout1"));
        vboxLayout1->setContentsMargins(9, 9, 9, 9);
        groupBox_2 = new QGroupBox(exportSpecificationsGroupBox);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        vboxLayout2 = new QVBoxLayout(groupBox_2);
        vboxLayout2->setSpacing(12);
        vboxLayout2->setObjectName(QString::fromUtf8("vboxLayout2"));
        vboxLayout2->setContentsMargins(9, 9, 9, 9);
        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(18);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        hboxLayout->setContentsMargins(0, 0, 0, 0);
        vboxLayout3 = new QVBoxLayout();
        vboxLayout3->setSpacing(6);
        vboxLayout3->setObjectName(QString::fromUtf8("vboxLayout3"));
        vboxLayout3->setContentsMargins(0, 0, 0, 0);
        label_2 = new QLabel(groupBox_2);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        vboxLayout3->addWidget(label_2);

        label_4 = new QLabel(groupBox_2);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        vboxLayout3->addWidget(label_4);


        hboxLayout->addLayout(vboxLayout3);

        vboxLayout4 = new QVBoxLayout();
        vboxLayout4->setSpacing(6);
        vboxLayout4->setObjectName(QString::fromUtf8("vboxLayout4"));
        vboxLayout4->setContentsMargins(0, 0, 0, 0);
        audioTypeComboBox = new QComboBox(groupBox_2);
        audioTypeComboBox->setObjectName(QString::fromUtf8("audioTypeComboBox"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(4);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(audioTypeComboBox->sizePolicy().hasHeightForWidth());
        audioTypeComboBox->setSizePolicy(sizePolicy1);

        vboxLayout4->addWidget(audioTypeComboBox);

        channelComboBox = new QComboBox(groupBox_2);
        channelComboBox->setObjectName(QString::fromUtf8("channelComboBox"));

        vboxLayout4->addWidget(channelComboBox);


        hboxLayout->addLayout(vboxLayout4);

        vboxLayout5 = new QVBoxLayout();
        vboxLayout5->setSpacing(6);
        vboxLayout5->setObjectName(QString::fromUtf8("vboxLayout5"));
        vboxLayout5->setContentsMargins(0, 0, 0, 0);
        normalizeCheckBox = new QCheckBox(groupBox_2);
        normalizeCheckBox->setObjectName(QString::fromUtf8("normalizeCheckBox"));
        QSizePolicy sizePolicy2(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(normalizeCheckBox->sizePolicy().hasHeightForWidth());
        normalizeCheckBox->setSizePolicy(sizePolicy2);

        vboxLayout5->addWidget(normalizeCheckBox);

        hboxLayout1 = new QHBoxLayout();
        hboxLayout1->setSpacing(6);
        hboxLayout1->setObjectName(QString::fromUtf8("hboxLayout1"));
        hboxLayout1->setContentsMargins(0, 0, 0, 0);
        label_3 = new QLabel(groupBox_2);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        sizePolicy.setHeightForWidth(label_3->sizePolicy().hasHeightForWidth());
        label_3->setSizePolicy(sizePolicy);
        label_3->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        hboxLayout1->addWidget(label_3);

        bitdepthComboBox = new QComboBox(groupBox_2);
        bitdepthComboBox->setObjectName(QString::fromUtf8("bitdepthComboBox"));

        hboxLayout1->addWidget(bitdepthComboBox);


        vboxLayout5->addLayout(hboxLayout1);


        hboxLayout->addLayout(vboxLayout5);

        spacerItem = new QSpacerItem(0, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacerItem);


        vboxLayout2->addLayout(hboxLayout);

        hboxLayout2 = new QHBoxLayout();
        hboxLayout2->setSpacing(6);
        hboxLayout2->setObjectName(QString::fromUtf8("hboxLayout2"));
        hboxLayout2->setContentsMargins(0, 0, 0, 0);
        wacpackGroupBox = new QGroupBox(groupBox_2);
        wacpackGroupBox->setObjectName(QString::fromUtf8("wacpackGroupBox"));
        QSizePolicy sizePolicy3(QSizePolicy::Preferred, QSizePolicy::Maximum);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(wacpackGroupBox->sizePolicy().hasHeightForWidth());
        wacpackGroupBox->setSizePolicy(sizePolicy3);
        vboxLayout6 = new QVBoxLayout(wacpackGroupBox);
        vboxLayout6->setSpacing(6);
        vboxLayout6->setObjectName(QString::fromUtf8("vboxLayout6"));
        vboxLayout6->setContentsMargins(9, 9, 9, 9);
        hboxLayout3 = new QHBoxLayout();
        hboxLayout3->setSpacing(6);
        hboxLayout3->setObjectName(QString::fromUtf8("hboxLayout3"));
        hboxLayout3->setContentsMargins(0, 0, 0, 0);
        label_17 = new QLabel(wacpackGroupBox);
        label_17->setObjectName(QString::fromUtf8("label_17"));

        hboxLayout3->addWidget(label_17);

        wavpackCompressionComboBox = new QComboBox(wacpackGroupBox);
        wavpackCompressionComboBox->setObjectName(QString::fromUtf8("wavpackCompressionComboBox"));

        hboxLayout3->addWidget(wavpackCompressionComboBox);


        vboxLayout6->addLayout(hboxLayout3);

        skipWVXCheckBox = new QCheckBox(wacpackGroupBox);
        skipWVXCheckBox->setObjectName(QString::fromUtf8("skipWVXCheckBox"));

        vboxLayout6->addWidget(skipWVXCheckBox);


        hboxLayout2->addWidget(wacpackGroupBox);

        oggOptionsGroupBox = new QGroupBox(groupBox_2);
        oggOptionsGroupBox->setObjectName(QString::fromUtf8("oggOptionsGroupBox"));
        sizePolicy3.setHeightForWidth(oggOptionsGroupBox->sizePolicy().hasHeightForWidth());
        oggOptionsGroupBox->setSizePolicy(sizePolicy3);
        gridLayout = new QGridLayout(oggOptionsGroupBox);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setHorizontalSpacing(9);
        gridLayout->setVerticalSpacing(9);
        gridLayout->setContentsMargins(9, 9, 9, 9);
        oggBitrateLabel = new QLabel(oggOptionsGroupBox);
        oggBitrateLabel->setObjectName(QString::fromUtf8("oggBitrateLabel"));
        QSizePolicy sizePolicy4(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy4.setHorizontalStretch(4);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(oggBitrateLabel->sizePolicy().hasHeightForWidth());
        oggBitrateLabel->setSizePolicy(sizePolicy4);
        oggBitrateLabel->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(oggBitrateLabel, 2, 0, 1, 1);

        oggBitrateComboBox = new QComboBox(oggOptionsGroupBox);
        oggBitrateComboBox->setObjectName(QString::fromUtf8("oggBitrateComboBox"));
        QSizePolicy sizePolicy5(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy5.setHorizontalStretch(5);
        sizePolicy5.setVerticalStretch(0);
        sizePolicy5.setHeightForWidth(oggBitrateComboBox->sizePolicy().hasHeightForWidth());
        oggBitrateComboBox->setSizePolicy(sizePolicy5);

        gridLayout->addWidget(oggBitrateComboBox, 2, 1, 1, 1);

        oggQualitySlider = new QSlider(oggOptionsGroupBox);
        oggQualitySlider->setObjectName(QString::fromUtf8("oggQualitySlider"));
        oggQualitySlider->setLayoutDirection(Qt::LeftToRight);
        oggQualitySlider->setAutoFillBackground(false);
        oggQualitySlider->setMinimum(0);
        oggQualitySlider->setMaximum(10);
        oggQualitySlider->setPageStep(1);
        oggQualitySlider->setValue(7);
        oggQualitySlider->setTracking(true);
        oggQualitySlider->setOrientation(Qt::Horizontal);
        oggQualitySlider->setInvertedAppearance(false);
        oggQualitySlider->setInvertedControls(false);
        oggQualitySlider->setTickPosition(QSlider::TicksAbove);
        oggQualitySlider->setTickInterval(1);

        gridLayout->addWidget(oggQualitySlider, 1, 1, 1, 1);

        oggMethodComboBox = new QComboBox(oggOptionsGroupBox);
        oggMethodComboBox->setObjectName(QString::fromUtf8("oggMethodComboBox"));
        sizePolicy5.setHeightForWidth(oggMethodComboBox->sizePolicy().hasHeightForWidth());
        oggMethodComboBox->setSizePolicy(sizePolicy5);

        gridLayout->addWidget(oggMethodComboBox, 0, 1, 1, 1);

        oggQualityLabel = new QLabel(oggOptionsGroupBox);
        oggQualityLabel->setObjectName(QString::fromUtf8("oggQualityLabel"));
        sizePolicy4.setHeightForWidth(oggQualityLabel->sizePolicy().hasHeightForWidth());
        oggQualityLabel->setSizePolicy(sizePolicy4);
        oggQualityLabel->setAlignment(Qt::AlignBottom|Qt::AlignRight|Qt::AlignTrailing);

        gridLayout->addWidget(oggQualityLabel, 1, 0, 1, 1);

        label_18 = new QLabel(oggOptionsGroupBox);
        label_18->setObjectName(QString::fromUtf8("label_18"));
        sizePolicy4.setHeightForWidth(label_18->sizePolicy().hasHeightForWidth());
        label_18->setSizePolicy(sizePolicy4);
        label_18->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_18, 0, 0, 1, 1);


        hboxLayout2->addWidget(oggOptionsGroupBox);

        mp3OptionsGroupBox = new QGroupBox(groupBox_2);
        mp3OptionsGroupBox->setObjectName(QString::fromUtf8("mp3OptionsGroupBox"));
        sizePolicy3.setHeightForWidth(mp3OptionsGroupBox->sizePolicy().hasHeightForWidth());
        mp3OptionsGroupBox->setSizePolicy(sizePolicy3);
        gridLayout1 = new QGridLayout(mp3OptionsGroupBox);
        gridLayout1->setObjectName(QString::fromUtf8("gridLayout1"));
        gridLayout1->setHorizontalSpacing(9);
        gridLayout1->setVerticalSpacing(9);
        gridLayout1->setContentsMargins(9, 9, 9, 9);
        label_15 = new QLabel(mp3OptionsGroupBox);
        label_15->setObjectName(QString::fromUtf8("label_15"));
        sizePolicy4.setHeightForWidth(label_15->sizePolicy().hasHeightForWidth());
        label_15->setSizePolicy(sizePolicy4);
        label_15->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout1->addWidget(label_15, 0, 0, 1, 1);

        mp3MaxBitrateLabel = new QLabel(mp3OptionsGroupBox);
        mp3MaxBitrateLabel->setObjectName(QString::fromUtf8("mp3MaxBitrateLabel"));
        sizePolicy4.setHeightForWidth(mp3MaxBitrateLabel->sizePolicy().hasHeightForWidth());
        mp3MaxBitrateLabel->setSizePolicy(sizePolicy4);
        mp3MaxBitrateLabel->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout1->addWidget(mp3MaxBitrateLabel, 3, 0, 1, 1);

        mp3MinBitrateLabel = new QLabel(mp3OptionsGroupBox);
        mp3MinBitrateLabel->setObjectName(QString::fromUtf8("mp3MinBitrateLabel"));
        sizePolicy4.setHeightForWidth(mp3MinBitrateLabel->sizePolicy().hasHeightForWidth());
        mp3MinBitrateLabel->setSizePolicy(sizePolicy4);
        mp3MinBitrateLabel->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout1->addWidget(mp3MinBitrateLabel, 2, 0, 1, 1);

        label_16 = new QLabel(mp3OptionsGroupBox);
        label_16->setObjectName(QString::fromUtf8("label_16"));
        sizePolicy4.setHeightForWidth(label_16->sizePolicy().hasHeightForWidth());
        label_16->setSizePolicy(sizePolicy4);
        label_16->setAlignment(Qt::AlignBottom|Qt::AlignRight|Qt::AlignTrailing);

        gridLayout1->addWidget(label_16, 1, 0, 1, 1);

        mp3MaxBitrateComboBox = new QComboBox(mp3OptionsGroupBox);
        mp3MaxBitrateComboBox->setObjectName(QString::fromUtf8("mp3MaxBitrateComboBox"));
        sizePolicy5.setHeightForWidth(mp3MaxBitrateComboBox->sizePolicy().hasHeightForWidth());
        mp3MaxBitrateComboBox->setSizePolicy(sizePolicy5);

        gridLayout1->addWidget(mp3MaxBitrateComboBox, 3, 1, 1, 1);

        mp3MinBitrateComboBox = new QComboBox(mp3OptionsGroupBox);
        mp3MinBitrateComboBox->setObjectName(QString::fromUtf8("mp3MinBitrateComboBox"));
        sizePolicy5.setHeightForWidth(mp3MinBitrateComboBox->sizePolicy().hasHeightForWidth());
        mp3MinBitrateComboBox->setSizePolicy(sizePolicy5);

        gridLayout1->addWidget(mp3MinBitrateComboBox, 2, 1, 1, 1);

        mp3MethodComboBox = new QComboBox(mp3OptionsGroupBox);
        mp3MethodComboBox->setObjectName(QString::fromUtf8("mp3MethodComboBox"));
        sizePolicy5.setHeightForWidth(mp3MethodComboBox->sizePolicy().hasHeightForWidth());
        mp3MethodComboBox->setSizePolicy(sizePolicy5);

        gridLayout1->addWidget(mp3MethodComboBox, 0, 1, 1, 1);

        mp3QualitySlider = new QSlider(mp3OptionsGroupBox);
        mp3QualitySlider->setObjectName(QString::fromUtf8("mp3QualitySlider"));
        mp3QualitySlider->setLayoutDirection(Qt::LeftToRight);
        mp3QualitySlider->setAutoFillBackground(false);
        mp3QualitySlider->setMinimum(1);
        mp3QualitySlider->setMaximum(9);
        mp3QualitySlider->setPageStep(1);
        mp3QualitySlider->setValue(3);
        mp3QualitySlider->setTracking(true);
        mp3QualitySlider->setOrientation(Qt::Horizontal);
        mp3QualitySlider->setInvertedAppearance(true);
        mp3QualitySlider->setInvertedControls(false);
        mp3QualitySlider->setTickPosition(QSlider::TicksAbove);
        mp3QualitySlider->setTickInterval(1);

        gridLayout1->addWidget(mp3QualitySlider, 1, 1, 1, 1);


        hboxLayout2->addWidget(mp3OptionsGroupBox);


        vboxLayout2->addLayout(hboxLayout2);


        vboxLayout1->addWidget(groupBox_2);

        groupBox = new QGroupBox(exportSpecificationsGroupBox);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        sizePolicy.setHeightForWidth(groupBox->sizePolicy().hasHeightForWidth());
        groupBox->setSizePolicy(sizePolicy);
        hboxLayout4 = new QHBoxLayout(groupBox);
        hboxLayout4->setSpacing(12);
        hboxLayout4->setObjectName(QString::fromUtf8("hboxLayout4"));
        hboxLayout4->setContentsMargins(9, 9, 9, 9);
        hboxLayout5 = new QHBoxLayout();
        hboxLayout5->setSpacing(12);
        hboxLayout5->setObjectName(QString::fromUtf8("hboxLayout5"));
        hboxLayout5->setContentsMargins(0, 0, 0, 0);
        label_7 = new QLabel(groupBox);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        QSizePolicy sizePolicy6(QSizePolicy::Maximum, QSizePolicy::Preferred);
        sizePolicy6.setHorizontalStretch(0);
        sizePolicy6.setVerticalStretch(0);
        sizePolicy6.setHeightForWidth(label_7->sizePolicy().hasHeightForWidth());
        label_7->setSizePolicy(sizePolicy6);

        hboxLayout5->addWidget(label_7);

        sampleRateComboBox = new QComboBox(groupBox);
        sampleRateComboBox->setObjectName(QString::fromUtf8("sampleRateComboBox"));
        sizePolicy2.setHeightForWidth(sampleRateComboBox->sizePolicy().hasHeightForWidth());
        sampleRateComboBox->setSizePolicy(sizePolicy2);
        sampleRateComboBox->setSizeAdjustPolicy(QComboBox::AdjustToContents);

        hboxLayout5->addWidget(sampleRateComboBox);

        label_8 = new QLabel(groupBox);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        sizePolicy6.setHeightForWidth(label_8->sizePolicy().hasHeightForWidth());
        label_8->setSizePolicy(sizePolicy6);

        hboxLayout5->addWidget(label_8);

        resampleQualityComboBox = new QComboBox(groupBox);
        resampleQualityComboBox->setObjectName(QString::fromUtf8("resampleQualityComboBox"));
        sizePolicy2.setHeightForWidth(resampleQualityComboBox->sizePolicy().hasHeightForWidth());
        resampleQualityComboBox->setSizePolicy(sizePolicy2);

        hboxLayout5->addWidget(resampleQualityComboBox);

        spacerItem1 = new QSpacerItem(0, 40, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout5->addItem(spacerItem1);


        hboxLayout4->addLayout(hboxLayout5);


        vboxLayout1->addWidget(groupBox);


        vboxLayout->addWidget(exportSpecificationsGroupBox);


        retranslateUi(ExportFormatOptionsWidget);

        QMetaObject::connectSlotsByName(ExportFormatOptionsWidget);
    } // setupUi

    void retranslateUi(QWidget *ExportFormatOptionsWidget)
    {
        ExportFormatOptionsWidget->setWindowTitle(QApplication::translate("ExportFormatOptionsWidget", "Export Format Options", 0, QApplication::UnicodeUTF8));
        exportSpecificationsGroupBox->setTitle(QApplication::translate("ExportFormatOptionsWidget", "Format Options", 0, QApplication::UnicodeUTF8));
        groupBox_2->setTitle(QApplication::translate("ExportFormatOptionsWidget", "Encoding", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("ExportFormatOptionsWidget", "File Type", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("ExportFormatOptionsWidget", "Channels", 0, QApplication::UnicodeUTF8));
        normalizeCheckBox->setText(QApplication::translate("ExportFormatOptionsWidget", "Normalize Audio", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("ExportFormatOptionsWidget", "Bitdepth", 0, QApplication::UnicodeUTF8));
        wacpackGroupBox->setTitle(QApplication::translate("ExportFormatOptionsWidget", "WavPack options (lossless compression)", 0, QApplication::UnicodeUTF8));
        label_17->setText(QApplication::translate("ExportFormatOptionsWidget", "Compression type", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        skipWVXCheckBox->setToolTip(QApplication::translate("ExportFormatOptionsWidget", "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">This option reduces the storage of some floating-point data files by up to about 10% by eliminating some information that has virtually no effect on the audio data. </p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">While this does technically make the compression lossy, it retains all the advantages of floating point data (&gt;600 dB of dynamic range, no clipping, and 25 bits of resolution). </p>\n"
"<p style="
                        "\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">This also affects large integer compression by limiting the resolution to 24 bits.</p></body></html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        skipWVXCheckBox->setText(QApplication::translate("ExportFormatOptionsWidget", "Skip WVX for extra compression (semi-lossless)", 0, QApplication::UnicodeUTF8));
        oggOptionsGroupBox->setTitle(QApplication::translate("ExportFormatOptionsWidget", "Ogg Options", 0, QApplication::UnicodeUTF8));
        oggBitrateLabel->setText(QApplication::translate("ExportFormatOptionsWidget", "Bitrate", 0, QApplication::UnicodeUTF8));
        oggQualityLabel->setText(QApplication::translate("ExportFormatOptionsWidget", "Quality (Smallest <-> Best)", 0, QApplication::UnicodeUTF8));
        label_18->setText(QApplication::translate("ExportFormatOptionsWidget", "Encoding Method", 0, QApplication::UnicodeUTF8));
        mp3OptionsGroupBox->setTitle(QApplication::translate("ExportFormatOptionsWidget", "MP3 Options", 0, QApplication::UnicodeUTF8));
        label_15->setText(QApplication::translate("ExportFormatOptionsWidget", "Encoding Method", 0, QApplication::UnicodeUTF8));
        mp3MaxBitrateLabel->setText(QApplication::translate("ExportFormatOptionsWidget", "Maximum Bitrate", 0, QApplication::UnicodeUTF8));
        mp3MinBitrateLabel->setText(QApplication::translate("ExportFormatOptionsWidget", "Minimum Bitrate", 0, QApplication::UnicodeUTF8));
        label_16->setText(QApplication::translate("ExportFormatOptionsWidget", "Quality (Fastest <-> Best)", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("ExportFormatOptionsWidget", "Sample Rate", 0, QApplication::UnicodeUTF8));
        label_7->setText(QApplication::translate("ExportFormatOptionsWidget", "Sample Rate", 0, QApplication::UnicodeUTF8));
        label_8->setText(QApplication::translate("ExportFormatOptionsWidget", "Conversion quality", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class ExportFormatOptionsWidget: public Ui_ExportFormatOptionsWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EXPORTFORMATOPTIONSWIDGET_H
