/********************************************************************************
** Form generated from reading UI file 'CDWritingDialog.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CDWRITINGDIALOG_H
#define UI_CDWRITINGDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QComboBox>
#include <QtGui/QDialog>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QProgressBar>
#include <QtGui/QPushButton>
#include <QtGui/QRadioButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QSpinBox>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_CDWritingDialog
{
public:
    QVBoxLayout *vboxLayout;
    QGroupBox *optionsGroupBox;
    QVBoxLayout *vboxLayout1;
    QHBoxLayout *hboxLayout;
    QRadioButton *cdCurrentSheetButton;
    QRadioButton *cdAllSheetsButton;
    QCheckBox *cdNormalizeCheckBox;
    QCheckBox *cdDiskExportOnlyCheckBox;
    QGroupBox *burnGroupBox;
    QVBoxLayout *vboxLayout2;
    QHBoxLayout *hboxLayout1;
    QComboBox *cdDeviceComboBox;
    QPushButton *refreshButton;
    QHBoxLayout *hboxLayout2;
    QLabel *label_10;
    QSpinBox *spinBoxNumCopies;
    QHBoxLayout *hboxLayout3;
    QCheckBox *simulateCheckBox;
    QLabel *label_9;
    QComboBox *speedComboBox;
    QGroupBox *groupBox_3;
    QVBoxLayout *vboxLayout3;
    QLabel *cdExportInformationLabel;
    QProgressBar *progressBar;
    QHBoxLayout *hboxLayout4;
    QSpacerItem *spacerItem;
    QPushButton *startButton;
    QPushButton *stopButton;
    QPushButton *closeButton;

    void setupUi(QDialog *CDWritingDialog)
    {
        if (CDWritingDialog->objectName().isEmpty())
            CDWritingDialog->setObjectName(QString::fromUtf8("CDWritingDialog"));
        CDWritingDialog->resize(357, 403);
        vboxLayout = new QVBoxLayout(CDWritingDialog);
#ifndef Q_OS_MAC
        vboxLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        optionsGroupBox = new QGroupBox(CDWritingDialog);
        optionsGroupBox->setObjectName(QString::fromUtf8("optionsGroupBox"));
        vboxLayout1 = new QVBoxLayout(optionsGroupBox);
#ifndef Q_OS_MAC
        vboxLayout1->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout1->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout1->setObjectName(QString::fromUtf8("vboxLayout1"));
        hboxLayout = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout->setContentsMargins(0, 0, 0, 0);
#endif
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        cdCurrentSheetButton = new QRadioButton(optionsGroupBox);
        cdCurrentSheetButton->setObjectName(QString::fromUtf8("cdCurrentSheetButton"));
        cdCurrentSheetButton->setChecked(true);

        hboxLayout->addWidget(cdCurrentSheetButton);

        cdAllSheetsButton = new QRadioButton(optionsGroupBox);
        cdAllSheetsButton->setObjectName(QString::fromUtf8("cdAllSheetsButton"));

        hboxLayout->addWidget(cdAllSheetsButton);


        vboxLayout1->addLayout(hboxLayout);

        cdNormalizeCheckBox = new QCheckBox(optionsGroupBox);
        cdNormalizeCheckBox->setObjectName(QString::fromUtf8("cdNormalizeCheckBox"));

        vboxLayout1->addWidget(cdNormalizeCheckBox);

        cdDiskExportOnlyCheckBox = new QCheckBox(optionsGroupBox);
        cdDiskExportOnlyCheckBox->setObjectName(QString::fromUtf8("cdDiskExportOnlyCheckBox"));

        vboxLayout1->addWidget(cdDiskExportOnlyCheckBox);


        vboxLayout->addWidget(optionsGroupBox);

        burnGroupBox = new QGroupBox(CDWritingDialog);
        burnGroupBox->setObjectName(QString::fromUtf8("burnGroupBox"));
        vboxLayout2 = new QVBoxLayout(burnGroupBox);
#ifndef Q_OS_MAC
        vboxLayout2->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout2->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout2->setObjectName(QString::fromUtf8("vboxLayout2"));
        hboxLayout1 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout1->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout1->setContentsMargins(0, 0, 0, 0);
#endif
        hboxLayout1->setObjectName(QString::fromUtf8("hboxLayout1"));
        cdDeviceComboBox = new QComboBox(burnGroupBox);
        cdDeviceComboBox->setObjectName(QString::fromUtf8("cdDeviceComboBox"));
        QSizePolicy sizePolicy(static_cast<QSizePolicy::Policy>(5), static_cast<QSizePolicy::Policy>(0));
        sizePolicy.setHorizontalStretch(5);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(cdDeviceComboBox->sizePolicy().hasHeightForWidth());
        cdDeviceComboBox->setSizePolicy(sizePolicy);

        hboxLayout1->addWidget(cdDeviceComboBox);

        refreshButton = new QPushButton(burnGroupBox);
        refreshButton->setObjectName(QString::fromUtf8("refreshButton"));

        hboxLayout1->addWidget(refreshButton);


        vboxLayout2->addLayout(hboxLayout1);

        hboxLayout2 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout2->setSpacing(6);
#endif
        hboxLayout2->setContentsMargins(0, 0, 0, 0);
        hboxLayout2->setObjectName(QString::fromUtf8("hboxLayout2"));
        label_10 = new QLabel(burnGroupBox);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        hboxLayout2->addWidget(label_10);

        spinBoxNumCopies = new QSpinBox(burnGroupBox);
        spinBoxNumCopies->setObjectName(QString::fromUtf8("spinBoxNumCopies"));
        spinBoxNumCopies->setMinimum(1);
        spinBoxNumCopies->setValue(1);

        hboxLayout2->addWidget(spinBoxNumCopies);


        vboxLayout2->addLayout(hboxLayout2);

        hboxLayout3 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout3->setSpacing(6);
#endif
        hboxLayout3->setContentsMargins(0, 0, 0, 0);
        hboxLayout3->setObjectName(QString::fromUtf8("hboxLayout3"));
        simulateCheckBox = new QCheckBox(burnGroupBox);
        simulateCheckBox->setObjectName(QString::fromUtf8("simulateCheckBox"));

        hboxLayout3->addWidget(simulateCheckBox);

        label_9 = new QLabel(burnGroupBox);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        hboxLayout3->addWidget(label_9);

        speedComboBox = new QComboBox(burnGroupBox);
        speedComboBox->setObjectName(QString::fromUtf8("speedComboBox"));

        hboxLayout3->addWidget(speedComboBox);


        vboxLayout2->addLayout(hboxLayout3);


        vboxLayout->addWidget(burnGroupBox);

        groupBox_3 = new QGroupBox(CDWritingDialog);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        vboxLayout3 = new QVBoxLayout(groupBox_3);
#ifndef Q_OS_MAC
        vboxLayout3->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout3->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout3->setObjectName(QString::fromUtf8("vboxLayout3"));
        cdExportInformationLabel = new QLabel(groupBox_3);
        cdExportInformationLabel->setObjectName(QString::fromUtf8("cdExportInformationLabel"));

        vboxLayout3->addWidget(cdExportInformationLabel);

        progressBar = new QProgressBar(groupBox_3);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setValue(0);
        progressBar->setOrientation(Qt::Horizontal);

        vboxLayout3->addWidget(progressBar);


        vboxLayout->addWidget(groupBox_3);

        hboxLayout4 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout4->setSpacing(6);
#endif
        hboxLayout4->setContentsMargins(0, 0, 0, 0);
        hboxLayout4->setObjectName(QString::fromUtf8("hboxLayout4"));
        spacerItem = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout4->addItem(spacerItem);

        startButton = new QPushButton(CDWritingDialog);
        startButton->setObjectName(QString::fromUtf8("startButton"));
        QSizePolicy sizePolicy1(static_cast<QSizePolicy::Policy>(7), static_cast<QSizePolicy::Policy>(0));
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(startButton->sizePolicy().hasHeightForWidth());
        startButton->setSizePolicy(sizePolicy1);

        hboxLayout4->addWidget(startButton);

        stopButton = new QPushButton(CDWritingDialog);
        stopButton->setObjectName(QString::fromUtf8("stopButton"));

        hboxLayout4->addWidget(stopButton);

        closeButton = new QPushButton(CDWritingDialog);
        closeButton->setObjectName(QString::fromUtf8("closeButton"));

        hboxLayout4->addWidget(closeButton);


        vboxLayout->addLayout(hboxLayout4);


        retranslateUi(CDWritingDialog);

        QMetaObject::connectSlotsByName(CDWritingDialog);
    } // setupUi

    void retranslateUi(QDialog *CDWritingDialog)
    {
        CDWritingDialog->setWindowTitle(QApplication::translate("CDWritingDialog", "CD Writing", 0, QApplication::UnicodeUTF8));
        optionsGroupBox->setTitle(QApplication::translate("CDWritingDialog", "General Options", 0, QApplication::UnicodeUTF8));
        cdCurrentSheetButton->setText(QApplication::translate("CDWritingDialog", "Write current Sheet", 0, QApplication::UnicodeUTF8));
        cdAllSheetsButton->setText(QApplication::translate("CDWritingDialog", "Write all Sheets", 0, QApplication::UnicodeUTF8));
        cdNormalizeCheckBox->setText(QApplication::translate("CDWritingDialog", "Calculate and apply normalization", 0, QApplication::UnicodeUTF8));
        cdDiskExportOnlyCheckBox->setText(QApplication::translate("CDWritingDialog", "Export wav and toc files only (don't write CD)", 0, QApplication::UnicodeUTF8));
        burnGroupBox->setTitle(QApplication::translate("CDWritingDialog", "Burning Device", 0, QApplication::UnicodeUTF8));
        refreshButton->setText(QString());
        label_10->setText(QApplication::translate("CDWritingDialog", "Number of copies", 0, QApplication::UnicodeUTF8));
        simulateCheckBox->setText(QApplication::translate("CDWritingDialog", "Simulate", 0, QApplication::UnicodeUTF8));
        label_9->setText(QApplication::translate("CDWritingDialog", "Speed", 0, QApplication::UnicodeUTF8));
        speedComboBox->clear();
        speedComboBox->insertItems(0, QStringList()
         << QApplication::translate("CDWritingDialog", "auto", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CDWritingDialog", "1x", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CDWritingDialog", "2x", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CDWritingDialog", "4x", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CDWritingDialog", "8x", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CDWritingDialog", "12x", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CDWritingDialog", "16x", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CDWritingDialog", "20x", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CDWritingDialog", "24x", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CDWritingDialog", "28x", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CDWritingDialog", "32x", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CDWritingDialog", "36x", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CDWritingDialog", "40x", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CDWritingDialog", "44x", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CDWritingDialog", "48x", 0, QApplication::UnicodeUTF8)
        );
        groupBox_3->setTitle(QApplication::translate("CDWritingDialog", "Status", 0, QApplication::UnicodeUTF8));
        cdExportInformationLabel->setText(QApplication::translate("CDWritingDialog", "Information", 0, QApplication::UnicodeUTF8));
        startButton->setText(QApplication::translate("CDWritingDialog", "Start Writing", 0, QApplication::UnicodeUTF8));
        stopButton->setText(QApplication::translate("CDWritingDialog", "Abort", 0, QApplication::UnicodeUTF8));
        closeButton->setText(QApplication::translate("CDWritingDialog", "Close", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class CDWritingDialog: public Ui_CDWritingDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CDWRITINGDIALOG_H
