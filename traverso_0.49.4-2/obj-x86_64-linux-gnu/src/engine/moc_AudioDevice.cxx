/****************************************************************************
** Meta object code from reading C++ file 'AudioDevice.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/engine/AudioDevice.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'AudioDevice.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_AudioDevice[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       7,       // signalCount

 // signals: signature, parameters, type, tag, flags
      13,   12,   12,   12, 0x05,
      23,   12,   12,   12, 0x05,
      33,   12,   12,   12, 0x05,
      55,   12,   12,   12, 0x05,
      72,   12,   12,   12, 0x05,
     107,   12,   12,   12, 0x05,
     129,  127,   12,   12, 0x05,

 // slots: signature, parameters, type, tag, flags
     157,  150,   12,   12, 0x08,
     197,  150,   12,   12, 0x08,
     240,   12,   12,   12, 0x08,
     263,   12,   12,   12, 0x08,
     287,   12,   12,   12, 0x08,
     308,   12,   12,   12, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_AudioDevice[] = {
    "AudioDevice\0\0stopped()\0started()\0"
    "driverParamsChanged()\0bufferUnderRun()\0"
    "clientRemoved(TAudioDeviceClient*)\0"
    "xrunStormDetected()\0,\0message(QString,int)\0"
    "client\0private_add_client(TAudioDeviceClient*)\0"
    "private_remove_client(TAudioDeviceClient*)\0"
    "audiothread_finished()\0switch_to_null_driver()\0"
    "reset_xrun_counter()\0check_jack_shutdown()\0"
};

void AudioDevice::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        AudioDevice *_t = static_cast<AudioDevice *>(_o);
        switch (_id) {
        case 0: _t->stopped(); break;
        case 1: _t->started(); break;
        case 2: _t->driverParamsChanged(); break;
        case 3: _t->bufferUnderRun(); break;
        case 4: _t->clientRemoved((*reinterpret_cast< TAudioDeviceClient*(*)>(_a[1]))); break;
        case 5: _t->xrunStormDetected(); break;
        case 6: _t->message((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 7: _t->private_add_client((*reinterpret_cast< TAudioDeviceClient*(*)>(_a[1]))); break;
        case 8: _t->private_remove_client((*reinterpret_cast< TAudioDeviceClient*(*)>(_a[1]))); break;
        case 9: _t->audiothread_finished(); break;
        case 10: _t->switch_to_null_driver(); break;
        case 11: _t->reset_xrun_counter(); break;
        case 12: _t->check_jack_shutdown(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData AudioDevice::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject AudioDevice::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_AudioDevice,
      qt_meta_data_AudioDevice, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &AudioDevice::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *AudioDevice::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *AudioDevice::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_AudioDevice))
        return static_cast<void*>(const_cast< AudioDevice*>(this));
    return QObject::qt_metacast(_clname);
}

int AudioDevice::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    }
    return _id;
}

// SIGNAL 0
void AudioDevice::stopped()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void AudioDevice::started()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void AudioDevice::driverParamsChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}

// SIGNAL 3
void AudioDevice::bufferUnderRun()
{
    QMetaObject::activate(this, &staticMetaObject, 3, 0);
}

// SIGNAL 4
void AudioDevice::clientRemoved(TAudioDeviceClient * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void AudioDevice::xrunStormDetected()
{
    QMetaObject::activate(this, &staticMetaObject, 5, 0);
}

// SIGNAL 6
void AudioDevice::message(QString _t1, int _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}
QT_END_MOC_NAMESPACE
