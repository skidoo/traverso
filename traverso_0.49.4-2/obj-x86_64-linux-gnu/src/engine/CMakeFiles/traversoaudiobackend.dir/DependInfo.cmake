# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/tmp/traverso/src/engine/AlsaDriver.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/engine/CMakeFiles/traversoaudiobackend.dir/AlsaDriver.o"
  "/tmp/traverso/src/engine/AudioBus.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/engine/CMakeFiles/traversoaudiobackend.dir/AudioBus.o"
  "/tmp/traverso/src/engine/AudioChannel.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/engine/CMakeFiles/traversoaudiobackend.dir/AudioChannel.o"
  "/tmp/traverso/src/engine/AudioDevice.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/engine/CMakeFiles/traversoaudiobackend.dir/AudioDevice.o"
  "/tmp/traverso/src/engine/AudioDeviceThread.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/engine/CMakeFiles/traversoaudiobackend.dir/AudioDeviceThread.o"
  "/tmp/traverso/src/engine/Driver.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/engine/CMakeFiles/traversoaudiobackend.dir/Driver.o"
  "/tmp/traverso/src/engine/JackDriver.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/engine/CMakeFiles/traversoaudiobackend.dir/JackDriver.o"
  "/tmp/traverso/src/engine/TAudioDeviceClient.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/engine/CMakeFiles/traversoaudiobackend.dir/TAudioDeviceClient.o"
  "/tmp/traverso/src/engine/memops.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/engine/CMakeFiles/traversoaudiobackend.dir/memops.o"
  "/tmp/traverso/obj-x86_64-linux-gnu/src/engine/moc_AudioBus.cxx" "/tmp/traverso/obj-x86_64-linux-gnu/src/engine/CMakeFiles/traversoaudiobackend.dir/moc_AudioBus.o"
  "/tmp/traverso/obj-x86_64-linux-gnu/src/engine/moc_AudioDevice.cxx" "/tmp/traverso/obj-x86_64-linux-gnu/src/engine/CMakeFiles/traversoaudiobackend.dir/moc_AudioDevice.o"
  "/tmp/traverso/obj-x86_64-linux-gnu/src/engine/moc_JackDriver.cxx" "/tmp/traverso/obj-x86_64-linux-gnu/src/engine/CMakeFiles/traversoaudiobackend.dir/moc_JackDriver.o"
  "/tmp/traverso/obj-x86_64-linux-gnu/src/engine/moc_TAudioDeviceClient.cxx" "/tmp/traverso/obj-x86_64-linux-gnu/src/engine/CMakeFiles/traversoaudiobackend.dir/moc_TAudioDeviceClient.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ALSA_SUPPORT"
  "HAVE_SYS_STAT_H"
  "HAVE_SYS_VFS_H"
  "JACK_SUPPORT"
  "LV2_SUPPORT"
  "MP3_DECODE_SUPPORT"
  "MP3_ENCODE_SUPPORT"
  "QT_CORE_LIB"
  "QT_GUI_LIB"
  "QT_NO_DEBUG"
  "SSE_OPTIMIZATIONS"
  "STATIC_BUILD"
  "USE_MLOCK"
  "USE_X86_64_ASM"
  "USE_XMMINTRIN"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "src/engine"
  "../src/engine"
  "/usr/include/lilv-0"
  "/usr/include/sratom-0"
  "/usr/include/sord-0"
  "/usr/include/serd-0"
  "../src/common"
  "/usr/include/qt4"
  "/usr/include/qt4/QtGui"
  "/usr/include/qt4/QtCore"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
