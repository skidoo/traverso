# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/tmp/traverso/src/commands/AddRemove.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/AddRemove.o"
  "/tmp/traverso/src/commands/ArmTracks.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/ArmTracks.o"
  "/tmp/traverso/src/commands/AudioClipExternalProcessing.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/AudioClipExternalProcessing.o"
  "/tmp/traverso/src/commands/ClipSelection.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/ClipSelection.o"
  "/tmp/traverso/src/commands/CommandGroup.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/CommandGroup.o"
  "/tmp/traverso/src/commands/Crop.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/Crop.o"
  "/tmp/traverso/src/commands/ExternalProcessingDialog.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/ExternalProcessingDialog.o"
  "/tmp/traverso/src/commands/Fade.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/Fade.o"
  "/tmp/traverso/src/commands/Gain.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/Gain.o"
  "/tmp/traverso/src/commands/Import.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/Import.o"
  "/tmp/traverso/src/commands/MoveClip.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/MoveClip.o"
  "/tmp/traverso/src/commands/MoveEdge.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/MoveEdge.o"
  "/tmp/traverso/src/commands/PCommand.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/PCommand.o"
  "/tmp/traverso/src/commands/PlayHeadMove.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/PlayHeadMove.o"
  "/tmp/traverso/src/commands/RemoveClip.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/RemoveClip.o"
  "/tmp/traverso/src/commands/Scroll.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/Scroll.o"
  "/tmp/traverso/src/commands/SplitClip.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/SplitClip.o"
  "/tmp/traverso/src/commands/TrackPan.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/TrackPan.o"
  "/tmp/traverso/src/commands/WorkCursorMove.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/WorkCursorMove.o"
  "/tmp/traverso/src/commands/Zoom.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/Zoom.o"
  "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/moc_Crop.cxx" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/moc_Crop.o"
  "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/moc_ExternalProcessingDialog.cxx" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/moc_ExternalProcessingDialog.o"
  "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/moc_Gain.cxx" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/moc_Gain.o"
  "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/moc_MoveClip.cxx" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/moc_MoveClip.o"
  "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/moc_TrackPan.cxx" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/moc_TrackPan.o"
  "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/moc_Zoom.cxx" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/moc_Zoom.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ALSA_SUPPORT"
  "HAVE_SYS_STAT_H"
  "HAVE_SYS_VFS_H"
  "JACK_SUPPORT"
  "LV2_SUPPORT"
  "MP3_DECODE_SUPPORT"
  "MP3_ENCODE_SUPPORT"
  "QT_CORE_LIB"
  "QT_GUI_LIB"
  "QT_NO_DEBUG"
  "SSE_OPTIMIZATIONS"
  "STATIC_BUILD"
  "USE_MLOCK"
  "USE_X86_64_ASM"
  "USE_XMMINTRIN"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "src/commands"
  "../src/commands"
  "/usr/include/lilv-0"
  "/usr/include/sratom-0"
  "/usr/include/sord-0"
  "/usr/include/serd-0"
  "../src/common"
  "../src/core"
  "../src/traverso"
  "../src/sheetcanvas"
  "../src/plugins"
  "../src/plugins/native"
  "/usr/include/qt4/QtXml"
  "/usr/include/qt4"
  "/usr/include/qt4/QtGui"
  "/usr/include/qt4/QtCore"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/tmp/traverso/obj-x86_64-linux-gnu/src/core/CMakeFiles/traversocore.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
