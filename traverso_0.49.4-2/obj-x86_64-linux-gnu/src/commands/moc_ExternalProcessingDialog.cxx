/****************************************************************************
** Meta object code from reading C++ file 'ExternalProcessingDialog.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/commands/ExternalProcessingDialog.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'ExternalProcessingDialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ExternalProcessingDialog[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      26,   25,   25,   25, 0x08,
      49,   25,   25,   25, 0x08,
      83,   25,   25,   25, 0x08,
     121,  101,   25,   25, 0x08,
     169,  164,   25,   25, 0x08,
     202,   25,   25,   25, 0x08,
     230,  164,   25,   25, 0x08,
     275,  269,   25,   25, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_ExternalProcessingDialog[] = {
    "ExternalProcessingDialog\0\0"
    "read_standard_output()\0"
    "prepare_for_external_processing()\0"
    "process_started()\0exitcode,exitstatus\0"
    "process_finished(int,QProcess::ExitStatus)\0"
    "text\0arg_combo_index_changed(QString)\0"
    "start_external_processing()\0"
    "command_lineedit_text_changed(QString)\0"
    "error\0process_error(QProcess::ProcessError)\0"
};

void ExternalProcessingDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ExternalProcessingDialog *_t = static_cast<ExternalProcessingDialog *>(_o);
        switch (_id) {
        case 0: _t->read_standard_output(); break;
        case 1: _t->prepare_for_external_processing(); break;
        case 2: _t->process_started(); break;
        case 3: _t->process_finished((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QProcess::ExitStatus(*)>(_a[2]))); break;
        case 4: _t->arg_combo_index_changed((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 5: _t->start_external_processing(); break;
        case 6: _t->command_lineedit_text_changed((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 7: _t->process_error((*reinterpret_cast< QProcess::ProcessError(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ExternalProcessingDialog::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ExternalProcessingDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_ExternalProcessingDialog,
      qt_meta_data_ExternalProcessingDialog, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ExternalProcessingDialog::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ExternalProcessingDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ExternalProcessingDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ExternalProcessingDialog))
        return static_cast<void*>(const_cast< ExternalProcessingDialog*>(this));
    if (!strcmp(_clname, "Ui::ExternalProcessingDialog"))
        return static_cast< Ui::ExternalProcessingDialog*>(const_cast< ExternalProcessingDialog*>(this));
    return QDialog::qt_metacast(_clname);
}

int ExternalProcessingDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
