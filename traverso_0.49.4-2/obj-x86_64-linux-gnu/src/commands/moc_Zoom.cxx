/****************************************************************************
** Meta object code from reading C++ file 'Zoom.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/commands/Zoom.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Zoom.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Zoom[] = {

 // content:
       6,       // revision
       0,       // classname
       3,   14, // classinfo
       3,   20, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // classinfo: key, value
      22,    5,
      49,   31,
      88,   59,

 // slots: signature, parameters, type, tag, flags
     136,  125,  124,  124, 0x0a,
     151,  125,  124,  124, 0x0a,
     167,  125,  124,  124, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_Zoom[] = {
    "Zoom\0Zoom Vertical In\0vzoom_in\0"
    "Zoom Vertical Out\0vzoom_out\0"
    "Toggle Vertical / Horizontal\0"
    "toggle_vertical_horizontal_jog_zoom\0"
    "\0autorepeat\0vzoom_in(bool)\0vzoom_out(bool)\0"
    "toggle_vertical_horizontal_jog_zoom(bool)\0"
};

void Zoom::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        Zoom *_t = static_cast<Zoom *>(_o);
        switch (_id) {
        case 0: _t->vzoom_in((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->vzoom_out((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 2: _t->toggle_vertical_horizontal_jog_zoom((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData Zoom::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject Zoom::staticMetaObject = {
    { &Command::staticMetaObject, qt_meta_stringdata_Zoom,
      qt_meta_data_Zoom, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Zoom::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Zoom::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Zoom::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Zoom))
        return static_cast<void*>(const_cast< Zoom*>(this));
    return Command::qt_metacast(_clname);
}

int Zoom::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Command::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
