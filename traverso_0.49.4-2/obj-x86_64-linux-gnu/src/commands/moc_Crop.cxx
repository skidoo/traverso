/****************************************************************************
** Meta object code from reading C++ file 'Crop.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/commands/Crop.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Crop.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Crop[] = {

 // content:
       6,       // revision
       0,       // classname
       2,   14, // classinfo
       2,   18, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // classinfo: key, value
      17,    5,
      42,   29,

 // slots: signature, parameters, type, tag, flags
      67,   56,   55,   55, 0x0a,
      85,   56,   55,   55, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_Crop[] = {
    "Crop\0Adjust Left\0adjust_left\0Adjust Right\0"
    "adjust_right\0\0autorepeat\0adjust_left(bool)\0"
    "adjust_right(bool)\0"
};

void Crop::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        Crop *_t = static_cast<Crop *>(_o);
        switch (_id) {
        case 0: _t->adjust_left((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->adjust_right((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData Crop::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject Crop::staticMetaObject = {
    { &Command::staticMetaObject, qt_meta_stringdata_Crop,
      qt_meta_data_Crop, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Crop::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Crop::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Crop::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Crop))
        return static_cast<void*>(const_cast< Crop*>(this));
    return Command::qt_metacast(_clname);
}

int Crop::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Command::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
