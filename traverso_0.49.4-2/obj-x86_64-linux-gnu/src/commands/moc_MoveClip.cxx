/****************************************************************************
** Meta object code from reading C++ file 'MoveClip.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/commands/MoveClip.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'MoveClip.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MoveClip[] = {

 // content:
       6,       // revision
       0,       // classname
       8,   14, // classinfo
      10,   30, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // classinfo: key, value
      31,    9,
      71,   45,
      94,   85,
     113,  105,
     131,  121,
     151,  141,
     172,  161,
     204,  183,

 // slots: signature, parameters, type, tag, flags
     237,  226,  225,  225, 0x0a,
     257,  226,  225,  225, 0x0a,
     277,  226,  225,  225, 0x0a,
     297,  226,  225,  225, 0x0a,
     315,  226,  225,  225, 0x0a,
     329,  226,  225,  225, 0x0a,
     345,  226,  225,  225, 0x0a,
     361,  226,  225,  225, 0x0a,
     378,  226,  225,  225, 0x0a,
     395,  226,  225,  225, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_MoveClip[] = {
    "MoveClip\0To next snap position\0"
    "next_snap_pos\0To previous snap position\0"
    "prev_snap_pos\0Jog Zoom\0start_zoom\0"
    "Move Up\0move_up\0Move Down\0move_down\0"
    "Move Left\0move_left\0Move Right\0"
    "move_right\0Toggle Vertical Only\0"
    "toggle_vertical_only\0\0autorepeat\0"
    "next_snap_pos(bool)\0prev_snap_pos(bool)\0"
    "move_to_start(bool)\0move_to_end(bool)\0"
    "move_up(bool)\0move_down(bool)\0"
    "move_left(bool)\0move_right(bool)\0"
    "start_zoom(bool)\0toggle_vertical_only(bool)\0"
};

void MoveClip::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        MoveClip *_t = static_cast<MoveClip *>(_o);
        switch (_id) {
        case 0: _t->next_snap_pos((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->prev_snap_pos((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 2: _t->move_to_start((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->move_to_end((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 4: _t->move_up((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 5: _t->move_down((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 6: _t->move_left((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 7: _t->move_right((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 8: _t->start_zoom((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 9: _t->toggle_vertical_only((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData MoveClip::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject MoveClip::staticMetaObject = {
    { &Command::staticMetaObject, qt_meta_stringdata_MoveClip,
      qt_meta_data_MoveClip, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MoveClip::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MoveClip::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MoveClip::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MoveClip))
        return static_cast<void*>(const_cast< MoveClip*>(this));
    return Command::qt_metacast(_clname);
}

int MoveClip::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Command::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
