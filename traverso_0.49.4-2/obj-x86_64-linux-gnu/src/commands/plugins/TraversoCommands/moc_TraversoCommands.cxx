/****************************************************************************
** Meta object code from reading C++ file 'TraversoCommands.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../src/commands/plugins/TraversoCommands/TraversoCommands.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TraversoCommands.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_TraversoCommands[] = {

 // content:
       6,       // revision
       0,       // classname
      33,   14, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // classinfo: key, value
      17,   17,
      34,   22,
      53,   44,
      78,   62,
     105,   92,
     132,  117,
     156,  146,
     175,  165,
     199,  187,
     223,  210,
     255,  235,
     294,  283,
     329,  314,
     362,  352,
     381,  371,
     410,  390,
     431,  425,
     454,  441,
     474,  463,
     495,  484,
     516,  505,
     539,  526,
     563,  551,
     586,  571,
     609,  595,
     630,  617,
     655,  639,
     671,  660,
     689,  680,
     704,  698,
     725,  720,
     743,  740,
     761,  756,

       0        // eod
};

static const char qt_meta_stringdata_TraversoCommands[] = {
    "TraversoCommands\0Gain\0Gain: Reset\0"
    "ResetGain\0Panorama\0TrackPan\0Panorama: Reset\0"
    "ResetTrackPan\0Import Audio\0ImportAudio\0"
    "Insert Silence\0InsertSilence\0Copy Clip\0"
    "CopyClip\0New Track\0AddNewTrack\0"
    "Remove Clip\0RemoveClip\0Remove Track\0"
    "RemoveTrack\0External Processing\0"
    "AudioClipExternalProcessing\0(De)Select\0"
    "ClipSelectionSelect\0(De)Select All\0"
    "ClipSelectionSelectAll\0Move Clip\0"
    "MoveClip\0Drag Edge\0DragEdge\0"
    "Move Or Resize Clip\0MoveClipOrEdge\0"
    "Split\0SplitClip\0Magnetic Cut\0CropClip\0"
    "Arm Tracks\0ArmTracks\0Fold Sheet\0"
    "FoldSheet\0Fold Track\0FoldTrack\0"
    "Fold Markers\0FoldMarkers\0Vertical In\0"
    "VZoomIn\0Horizontal Out\0HZoomOut\0"
    "Horizontal In\0HZoomIn\0Vertical Out\0"
    "VZoomOut\0Omnidirectional\0Zoom\0Horizontal\0"
    "HJogZoom\0Vertical\0VJogZoom\0Right\0"
    "ScrollRightHold\0Left\0ScrollLeftHold\0"
    "Up\0ScrollUpHold\0Down\0ScrollDownHold\0"
};

void TraversoCommands::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData TraversoCommands::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject TraversoCommands::staticMetaObject = {
    { &CommandPlugin::staticMetaObject, qt_meta_stringdata_TraversoCommands,
      qt_meta_data_TraversoCommands, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &TraversoCommands::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *TraversoCommands::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *TraversoCommands::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_TraversoCommands))
        return static_cast<void*>(const_cast< TraversoCommands*>(this));
    return CommandPlugin::qt_metacast(_clname);
}

int TraversoCommands::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = CommandPlugin::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
QT_END_MOC_NAMESPACE
