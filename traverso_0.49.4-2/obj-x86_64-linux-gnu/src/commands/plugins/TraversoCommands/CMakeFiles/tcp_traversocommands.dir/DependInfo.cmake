# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/tmp/traverso/src/commands/plugins/TraversoCommands/TraversoCommands.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/plugins/TraversoCommands/CMakeFiles/tcp_traversocommands.dir/TraversoCommands.o"
  "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/plugins/TraversoCommands/moc_TraversoCommands.cxx" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/plugins/TraversoCommands/CMakeFiles/tcp_traversocommands.dir/moc_TraversoCommands.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ALSA_SUPPORT"
  "HAVE_SYS_STAT_H"
  "HAVE_SYS_VFS_H"
  "JACK_SUPPORT"
  "LV2_SUPPORT"
  "MP3_DECODE_SUPPORT"
  "MP3_ENCODE_SUPPORT"
  "QT_CORE_LIB"
  "QT_GUI_LIB"
  "QT_NO_DEBUG"
  "QT_PLUGIN"
  "QT_STATICPLUGIN"
  "SSE_OPTIMIZATIONS"
  "STATIC_BUILD"
  "USE_MLOCK"
  "USE_X86_64_ASM"
  "USE_XMMINTRIN"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "src/commands/plugins/TraversoCommands"
  "../src/commands/plugins/TraversoCommands"
  "/usr/include/lilv-0"
  "/usr/include/sratom-0"
  "/usr/include/sord-0"
  "/usr/include/serd-0"
  "../src/common"
  "../src/core"
  "../src/sheetcanvas"
  "../src/commands"
  "../src/plugins"
  "../src/plugins/native"
  "/usr/include/qt4/QtXml"
  "/usr/include/qt4"
  "/usr/include/qt4/QtGui"
  "/usr/include/qt4/QtCore"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
