/********************************************************************************
** Form generated from reading UI file 'ExternalProcessingDialog.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EXTERNALPROCESSINGDIALOG_H
#define UI_EXTERNALPROCESSINGDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QDialog>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QProgressBar>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QTextEdit>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_ExternalProcessingDialog
{
public:
    QVBoxLayout *vboxLayout;
    QHBoxLayout *hboxLayout;
    QVBoxLayout *vboxLayout1;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_4;
    QVBoxLayout *vboxLayout2;
    QLineEdit *programLineEdit;
    QHBoxLayout *hboxLayout1;
    QLineEdit *argumentsLineEdit;
    QComboBox *argsComboBox;
    QProgressBar *progressBar;
    QHBoxLayout *hboxLayout2;
    QSpacerItem *spacerItem;
    QPushButton *startButton;
    QPushButton *cancelButton;
    QLabel *label_3;
    QTextEdit *statusText;

    void setupUi(QDialog *ExternalProcessingDialog)
    {
        if (ExternalProcessingDialog->objectName().isEmpty())
            ExternalProcessingDialog->setObjectName(QString::fromUtf8("ExternalProcessingDialog"));
        ExternalProcessingDialog->resize(430, 316);
        ExternalProcessingDialog->setMinimumSize(QSize(380, 0));
        ExternalProcessingDialog->setMaximumSize(QSize(460, 400));
        vboxLayout = new QVBoxLayout(ExternalProcessingDialog);
#ifndef Q_OS_MAC
        vboxLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        hboxLayout = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout->setContentsMargins(0, 0, 0, 0);
#endif
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        vboxLayout1 = new QVBoxLayout();
#ifndef Q_OS_MAC
        vboxLayout1->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout1->setContentsMargins(0, 0, 0, 0);
#endif
        vboxLayout1->setObjectName(QString::fromUtf8("vboxLayout1"));
        label = new QLabel(ExternalProcessingDialog);
        label->setObjectName(QString::fromUtf8("label"));

        vboxLayout1->addWidget(label);

        label_2 = new QLabel(ExternalProcessingDialog);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        vboxLayout1->addWidget(label_2);

        label_4 = new QLabel(ExternalProcessingDialog);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        vboxLayout1->addWidget(label_4);


        hboxLayout->addLayout(vboxLayout1);

        vboxLayout2 = new QVBoxLayout();
#ifndef Q_OS_MAC
        vboxLayout2->setSpacing(6);
#endif
        vboxLayout2->setContentsMargins(0, 0, 0, 0);
        vboxLayout2->setObjectName(QString::fromUtf8("vboxLayout2"));
        programLineEdit = new QLineEdit(ExternalProcessingDialog);
        programLineEdit->setObjectName(QString::fromUtf8("programLineEdit"));

        vboxLayout2->addWidget(programLineEdit);

        hboxLayout1 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout1->setSpacing(6);
#endif
        hboxLayout1->setContentsMargins(0, 0, 0, 0);
        hboxLayout1->setObjectName(QString::fromUtf8("hboxLayout1"));
        argumentsLineEdit = new QLineEdit(ExternalProcessingDialog);
        argumentsLineEdit->setObjectName(QString::fromUtf8("argumentsLineEdit"));

        hboxLayout1->addWidget(argumentsLineEdit);

        argsComboBox = new QComboBox(ExternalProcessingDialog);
        argsComboBox->setObjectName(QString::fromUtf8("argsComboBox"));
        argsComboBox->setMinimumSize(QSize(100, 0));

        hboxLayout1->addWidget(argsComboBox);


        vboxLayout2->addLayout(hboxLayout1);

        progressBar = new QProgressBar(ExternalProcessingDialog);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setValue(0);
        progressBar->setTextVisible(true);
        progressBar->setOrientation(Qt::Horizontal);

        vboxLayout2->addWidget(progressBar);


        hboxLayout->addLayout(vboxLayout2);


        vboxLayout->addLayout(hboxLayout);

        hboxLayout2 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout2->setSpacing(6);
#endif
        hboxLayout2->setContentsMargins(0, 0, 0, 0);
        hboxLayout2->setObjectName(QString::fromUtf8("hboxLayout2"));
        spacerItem = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout2->addItem(spacerItem);

        startButton = new QPushButton(ExternalProcessingDialog);
        startButton->setObjectName(QString::fromUtf8("startButton"));

        hboxLayout2->addWidget(startButton);

        cancelButton = new QPushButton(ExternalProcessingDialog);
        cancelButton->setObjectName(QString::fromUtf8("cancelButton"));

        hboxLayout2->addWidget(cancelButton);


        vboxLayout->addLayout(hboxLayout2);

        label_3 = new QLabel(ExternalProcessingDialog);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        vboxLayout->addWidget(label_3);

        statusText = new QTextEdit(ExternalProcessingDialog);
        statusText->setObjectName(QString::fromUtf8("statusText"));
        statusText->setAcceptDrops(false);
        statusText->setTextInteractionFlags(Qt::TextSelectableByMouse);

        vboxLayout->addWidget(statusText);


        retranslateUi(ExternalProcessingDialog);

        QMetaObject::connectSlotsByName(ExternalProcessingDialog);
    } // setupUi

    void retranslateUi(QDialog *ExternalProcessingDialog)
    {
        ExternalProcessingDialog->setWindowTitle(QApplication::translate("ExternalProcessingDialog", "External Processing", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("ExternalProcessingDialog", "Program", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("ExternalProcessingDialog", "Arguments", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("ExternalProcessingDialog", "Progress", 0, QApplication::UnicodeUTF8));
        programLineEdit->setText(QApplication::translate("ExternalProcessingDialog", "sox", 0, QApplication::UnicodeUTF8));
        startButton->setText(QApplication::translate("ExternalProcessingDialog", "Start", 0, QApplication::UnicodeUTF8));
        cancelButton->setText(QApplication::translate("ExternalProcessingDialog", "Cancel", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("ExternalProcessingDialog", "Program output", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class ExternalProcessingDialog: public Ui_ExternalProcessingDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EXTERNALPROCESSINGDIALOG_H
