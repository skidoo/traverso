/****************************************************************************
** Meta object code from reading C++ file 'TrackPan.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/commands/TrackPan.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TrackPan.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_TrackPan[] = {

 // content:
       6,       // revision
       0,       // classname
       2,   14, // classinfo
       2,   18, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // classinfo: key, value
      17,    9,
      35,   26,

 // slots: signature, parameters, type, tag, flags
      57,   46,   45,   45, 0x0a,
      72,   46,   45,   45, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_TrackPan[] = {
    "TrackPan\0To Left\0pan_left\0To Right\0"
    "pan_right\0\0autorepeat\0pan_left(bool)\0"
    "pan_right(bool)\0"
};

void TrackPan::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        TrackPan *_t = static_cast<TrackPan *>(_o);
        switch (_id) {
        case 0: _t->pan_left((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->pan_right((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData TrackPan::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject TrackPan::staticMetaObject = {
    { &Command::staticMetaObject, qt_meta_stringdata_TrackPan,
      qt_meta_data_TrackPan, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &TrackPan::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *TrackPan::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *TrackPan::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_TrackPan))
        return static_cast<void*>(const_cast< TrackPan*>(this));
    return Command::qt_metacast(_clname);
}

int TrackPan::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Command::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
