/********************************************************************************
** Form generated from reading UI file 'CDWritingDialog.ui'
**
** Created by: Qt User Interface Compiler version 5.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CDWRITINGDIALOG_H
#define UI_CDWRITINGDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_CDWritingDialog
{
public:
    QVBoxLayout *vboxLayout;
    QGroupBox *optionsGroupBox;
    QVBoxLayout *vboxLayout1;
    QHBoxLayout *hboxLayout;
    QRadioButton *cdCurrentSheetButton;
    QRadioButton *cdAllSheetsButton;
    QCheckBox *cdNormalizeCheckBox;
    QCheckBox *cdDiskExportOnlyCheckBox;
    QGroupBox *burnGroupBox;
    QVBoxLayout *vboxLayout2;
    QHBoxLayout *hboxLayout1;
    QComboBox *cdDeviceComboBox;
    QPushButton *refreshButton;
    QHBoxLayout *hboxLayout2;
    QLabel *label_10;
    QSpinBox *spinBoxNumCopies;
    QHBoxLayout *hboxLayout3;
    QCheckBox *simulateCheckBox;
    QLabel *label_9;
    QComboBox *speedComboBox;
    QGroupBox *groupBox_3;
    QVBoxLayout *vboxLayout3;
    QLabel *cdExportInformationLabel;
    QProgressBar *progressBar;
    QHBoxLayout *hboxLayout4;
    QSpacerItem *spacerItem;
    QPushButton *startButton;
    QPushButton *stopButton;
    QPushButton *closeButton;

    void setupUi(QDialog *CDWritingDialog)
    {
        if (CDWritingDialog->objectName().isEmpty())
            CDWritingDialog->setObjectName(QStringLiteral("CDWritingDialog"));
        CDWritingDialog->resize(357, 403);
        vboxLayout = new QVBoxLayout(CDWritingDialog);
#ifndef Q_OS_MAC
        vboxLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout->setObjectName(QStringLiteral("vboxLayout"));
        optionsGroupBox = new QGroupBox(CDWritingDialog);
        optionsGroupBox->setObjectName(QStringLiteral("optionsGroupBox"));
        vboxLayout1 = new QVBoxLayout(optionsGroupBox);
#ifndef Q_OS_MAC
        vboxLayout1->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout1->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout1->setObjectName(QStringLiteral("vboxLayout1"));
        hboxLayout = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout->setContentsMargins(0, 0, 0, 0);
#endif
        hboxLayout->setObjectName(QStringLiteral("hboxLayout"));
        cdCurrentSheetButton = new QRadioButton(optionsGroupBox);
        cdCurrentSheetButton->setObjectName(QStringLiteral("cdCurrentSheetButton"));
        cdCurrentSheetButton->setChecked(true);

        hboxLayout->addWidget(cdCurrentSheetButton);

        cdAllSheetsButton = new QRadioButton(optionsGroupBox);
        cdAllSheetsButton->setObjectName(QStringLiteral("cdAllSheetsButton"));

        hboxLayout->addWidget(cdAllSheetsButton);


        vboxLayout1->addLayout(hboxLayout);

        cdNormalizeCheckBox = new QCheckBox(optionsGroupBox);
        cdNormalizeCheckBox->setObjectName(QStringLiteral("cdNormalizeCheckBox"));

        vboxLayout1->addWidget(cdNormalizeCheckBox);

        cdDiskExportOnlyCheckBox = new QCheckBox(optionsGroupBox);
        cdDiskExportOnlyCheckBox->setObjectName(QStringLiteral("cdDiskExportOnlyCheckBox"));

        vboxLayout1->addWidget(cdDiskExportOnlyCheckBox);


        vboxLayout->addWidget(optionsGroupBox);

        burnGroupBox = new QGroupBox(CDWritingDialog);
        burnGroupBox->setObjectName(QStringLiteral("burnGroupBox"));
        vboxLayout2 = new QVBoxLayout(burnGroupBox);
#ifndef Q_OS_MAC
        vboxLayout2->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout2->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout2->setObjectName(QStringLiteral("vboxLayout2"));
        hboxLayout1 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout1->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout1->setContentsMargins(0, 0, 0, 0);
#endif
        hboxLayout1->setObjectName(QStringLiteral("hboxLayout1"));
        cdDeviceComboBox = new QComboBox(burnGroupBox);
        cdDeviceComboBox->setObjectName(QStringLiteral("cdDeviceComboBox"));
        QSizePolicy sizePolicy(static_cast<QSizePolicy::Policy>(5), static_cast<QSizePolicy::Policy>(0));
        sizePolicy.setHorizontalStretch(5);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(cdDeviceComboBox->sizePolicy().hasHeightForWidth());
        cdDeviceComboBox->setSizePolicy(sizePolicy);

        hboxLayout1->addWidget(cdDeviceComboBox);

        refreshButton = new QPushButton(burnGroupBox);
        refreshButton->setObjectName(QStringLiteral("refreshButton"));

        hboxLayout1->addWidget(refreshButton);


        vboxLayout2->addLayout(hboxLayout1);

        hboxLayout2 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout2->setSpacing(6);
#endif
        hboxLayout2->setContentsMargins(0, 0, 0, 0);
        hboxLayout2->setObjectName(QStringLiteral("hboxLayout2"));
        label_10 = new QLabel(burnGroupBox);
        label_10->setObjectName(QStringLiteral("label_10"));

        hboxLayout2->addWidget(label_10);

        spinBoxNumCopies = new QSpinBox(burnGroupBox);
        spinBoxNumCopies->setObjectName(QStringLiteral("spinBoxNumCopies"));
        spinBoxNumCopies->setMinimum(1);
        spinBoxNumCopies->setValue(1);

        hboxLayout2->addWidget(spinBoxNumCopies);


        vboxLayout2->addLayout(hboxLayout2);

        hboxLayout3 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout3->setSpacing(6);
#endif
        hboxLayout3->setContentsMargins(0, 0, 0, 0);
        hboxLayout3->setObjectName(QStringLiteral("hboxLayout3"));
        simulateCheckBox = new QCheckBox(burnGroupBox);
        simulateCheckBox->setObjectName(QStringLiteral("simulateCheckBox"));

        hboxLayout3->addWidget(simulateCheckBox);

        label_9 = new QLabel(burnGroupBox);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        hboxLayout3->addWidget(label_9);

        speedComboBox = new QComboBox(burnGroupBox);
        speedComboBox->setObjectName(QStringLiteral("speedComboBox"));

        hboxLayout3->addWidget(speedComboBox);


        vboxLayout2->addLayout(hboxLayout3);


        vboxLayout->addWidget(burnGroupBox);

        groupBox_3 = new QGroupBox(CDWritingDialog);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        vboxLayout3 = new QVBoxLayout(groupBox_3);
#ifndef Q_OS_MAC
        vboxLayout3->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout3->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout3->setObjectName(QStringLiteral("vboxLayout3"));
        cdExportInformationLabel = new QLabel(groupBox_3);
        cdExportInformationLabel->setObjectName(QStringLiteral("cdExportInformationLabel"));

        vboxLayout3->addWidget(cdExportInformationLabel);

        progressBar = new QProgressBar(groupBox_3);
        progressBar->setObjectName(QStringLiteral("progressBar"));
        progressBar->setValue(0);
        progressBar->setOrientation(Qt::Horizontal);

        vboxLayout3->addWidget(progressBar);


        vboxLayout->addWidget(groupBox_3);

        hboxLayout4 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout4->setSpacing(6);
#endif
        hboxLayout4->setContentsMargins(0, 0, 0, 0);
        hboxLayout4->setObjectName(QStringLiteral("hboxLayout4"));
        spacerItem = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout4->addItem(spacerItem);

        startButton = new QPushButton(CDWritingDialog);
        startButton->setObjectName(QStringLiteral("startButton"));
        QSizePolicy sizePolicy1(static_cast<QSizePolicy::Policy>(7), static_cast<QSizePolicy::Policy>(0));
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(startButton->sizePolicy().hasHeightForWidth());
        startButton->setSizePolicy(sizePolicy1);

        hboxLayout4->addWidget(startButton);

        stopButton = new QPushButton(CDWritingDialog);
        stopButton->setObjectName(QStringLiteral("stopButton"));

        hboxLayout4->addWidget(stopButton);

        closeButton = new QPushButton(CDWritingDialog);
        closeButton->setObjectName(QStringLiteral("closeButton"));

        hboxLayout4->addWidget(closeButton);


        vboxLayout->addLayout(hboxLayout4);


        retranslateUi(CDWritingDialog);

        QMetaObject::connectSlotsByName(CDWritingDialog);
    } // setupUi

    void retranslateUi(QDialog *CDWritingDialog)
    {
        CDWritingDialog->setWindowTitle(QApplication::translate("CDWritingDialog", "CD Writing", Q_NULLPTR));
        optionsGroupBox->setTitle(QApplication::translate("CDWritingDialog", "General Options", Q_NULLPTR));
        cdCurrentSheetButton->setText(QApplication::translate("CDWritingDialog", "Write current Sheet", Q_NULLPTR));
        cdAllSheetsButton->setText(QApplication::translate("CDWritingDialog", "Write all Sheets", Q_NULLPTR));
        cdNormalizeCheckBox->setText(QApplication::translate("CDWritingDialog", "Calculate and apply normalization", Q_NULLPTR));
        cdDiskExportOnlyCheckBox->setText(QApplication::translate("CDWritingDialog", "Export wav and toc files only (don't write CD)", Q_NULLPTR));
        burnGroupBox->setTitle(QApplication::translate("CDWritingDialog", "Burning Device", Q_NULLPTR));
        refreshButton->setText(QString());
        label_10->setText(QApplication::translate("CDWritingDialog", "Number of copies", Q_NULLPTR));
        simulateCheckBox->setText(QApplication::translate("CDWritingDialog", "Simulate", Q_NULLPTR));
        label_9->setText(QApplication::translate("CDWritingDialog", "Speed", Q_NULLPTR));
        speedComboBox->clear();
        speedComboBox->insertItems(0, QStringList()
         << QApplication::translate("CDWritingDialog", "auto", Q_NULLPTR)
         << QApplication::translate("CDWritingDialog", "1x", Q_NULLPTR)
         << QApplication::translate("CDWritingDialog", "2x", Q_NULLPTR)
         << QApplication::translate("CDWritingDialog", "4x", Q_NULLPTR)
         << QApplication::translate("CDWritingDialog", "8x", Q_NULLPTR)
         << QApplication::translate("CDWritingDialog", "12x", Q_NULLPTR)
         << QApplication::translate("CDWritingDialog", "16x", Q_NULLPTR)
         << QApplication::translate("CDWritingDialog", "20x", Q_NULLPTR)
         << QApplication::translate("CDWritingDialog", "24x", Q_NULLPTR)
         << QApplication::translate("CDWritingDialog", "28x", Q_NULLPTR)
         << QApplication::translate("CDWritingDialog", "32x", Q_NULLPTR)
         << QApplication::translate("CDWritingDialog", "36x", Q_NULLPTR)
         << QApplication::translate("CDWritingDialog", "40x", Q_NULLPTR)
         << QApplication::translate("CDWritingDialog", "44x", Q_NULLPTR)
         << QApplication::translate("CDWritingDialog", "48x", Q_NULLPTR)
        );
        groupBox_3->setTitle(QApplication::translate("CDWritingDialog", "Status", Q_NULLPTR));
        cdExportInformationLabel->setText(QApplication::translate("CDWritingDialog", "Information", Q_NULLPTR));
        startButton->setText(QApplication::translate("CDWritingDialog", "Start Writing", Q_NULLPTR));
        stopButton->setText(QApplication::translate("CDWritingDialog", "Abort", Q_NULLPTR));
        closeButton->setText(QApplication::translate("CDWritingDialog", "Close", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class CDWritingDialog: public Ui_CDWritingDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CDWRITINGDIALOG_H
