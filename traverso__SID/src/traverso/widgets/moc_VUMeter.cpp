/****************************************************************************
** Meta object code from reading C++ file 'VUMeter.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "VUMeter.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'VUMeter.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_VUMeterRuler_t {
    QByteArrayData data[1];
    char stringdata0[13];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_VUMeterRuler_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_VUMeterRuler_t qt_meta_stringdata_VUMeterRuler = {
    {
QT_MOC_LITERAL(0, 0, 12) // "VUMeterRuler"

    },
    "VUMeterRuler"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_VUMeterRuler[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void VUMeterRuler::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject VUMeterRuler::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_VUMeterRuler.data,
      qt_meta_data_VUMeterRuler,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *VUMeterRuler::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *VUMeterRuler::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_VUMeterRuler.stringdata0))
        return static_cast<void*>(const_cast< VUMeterRuler*>(this));
    return QWidget::qt_metacast(_clname);
}

int VUMeterRuler::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
struct qt_meta_stringdata_VUMeter_t {
    QByteArrayData data[4];
    char stringdata0[57];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_VUMeter_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_VUMeter_t qt_meta_stringdata_VUMeter = {
    {
QT_MOC_LITERAL(0, 0, 7), // "VUMeter"
QT_MOC_LITERAL(1, 8, 23), // "peak_monitoring_stopped"
QT_MOC_LITERAL(2, 32, 0), // ""
QT_MOC_LITERAL(3, 33, 23) // "peak_monitoring_started"

    },
    "VUMeter\0peak_monitoring_stopped\0\0"
    "peak_monitoring_started"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_VUMeter[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   24,    2, 0x08 /* Private */,
       3,    0,   25,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void VUMeter::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        VUMeter *_t = static_cast<VUMeter *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->peak_monitoring_stopped(); break;
        case 1: _t->peak_monitoring_started(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject VUMeter::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_VUMeter.data,
      qt_meta_data_VUMeter,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *VUMeter::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *VUMeter::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_VUMeter.stringdata0))
        return static_cast<void*>(const_cast< VUMeter*>(this));
    return QWidget::qt_metacast(_clname);
}

int VUMeter::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 2;
    }
    return _id;
}
struct qt_meta_stringdata_VUMeterOverLed_t {
    QByteArrayData data[4];
    char stringdata0[29];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_VUMeterOverLed_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_VUMeterOverLed_t qt_meta_stringdata_VUMeterOverLed = {
    {
QT_MOC_LITERAL(0, 0, 14), // "VUMeterOverLed"
QT_MOC_LITERAL(1, 15, 10), // "set_active"
QT_MOC_LITERAL(2, 26, 0), // ""
QT_MOC_LITERAL(3, 27, 1) // "b"

    },
    "VUMeterOverLed\0set_active\0\0b"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_VUMeterOverLed[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   19,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void, QMetaType::Bool,    3,

       0        // eod
};

void VUMeterOverLed::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        VUMeterOverLed *_t = static_cast<VUMeterOverLed *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->set_active((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObject VUMeterOverLed::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_VUMeterOverLed.data,
      qt_meta_data_VUMeterOverLed,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *VUMeterOverLed::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *VUMeterOverLed::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_VUMeterOverLed.stringdata0))
        return static_cast<void*>(const_cast< VUMeterOverLed*>(this));
    return QWidget::qt_metacast(_clname);
}

int VUMeterOverLed::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
    return _id;
}
struct qt_meta_stringdata_VUMeterLevel_t {
    QByteArrayData data[7];
    char stringdata0[77];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_VUMeterLevel_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_VUMeterLevel_t qt_meta_stringdata_VUMeterLevel = {
    {
QT_MOC_LITERAL(0, 0, 12), // "VUMeterLevel"
QT_MOC_LITERAL(1, 13, 17), // "activate_over_led"
QT_MOC_LITERAL(2, 31, 0), // ""
QT_MOC_LITERAL(3, 32, 4), // "stop"
QT_MOC_LITERAL(4, 37, 5), // "start"
QT_MOC_LITERAL(5, 43, 11), // "update_peak"
QT_MOC_LITERAL(6, 55, 21) // "reset_peak_hold_value"

    },
    "VUMeterLevel\0activate_over_led\0\0stop\0"
    "start\0update_peak\0reset_peak_hold_value"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_VUMeterLevel[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   39,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       3,    0,   42,    2, 0x08 /* Private */,
       4,    0,   43,    2, 0x08 /* Private */,
       5,    0,   44,    2, 0x08 /* Private */,
       6,    0,   45,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::Bool,    2,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void VUMeterLevel::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        VUMeterLevel *_t = static_cast<VUMeterLevel *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->activate_over_led((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->stop(); break;
        case 2: _t->start(); break;
        case 3: _t->update_peak(); break;
        case 4: _t->reset_peak_hold_value(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (VUMeterLevel::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&VUMeterLevel::activate_over_led)) {
                *result = 0;
                return;
            }
        }
    }
}

const QMetaObject VUMeterLevel::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_VUMeterLevel.data,
      qt_meta_data_VUMeterLevel,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *VUMeterLevel::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *VUMeterLevel::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_VUMeterLevel.stringdata0))
        return static_cast<void*>(const_cast< VUMeterLevel*>(this));
    return QWidget::qt_metacast(_clname);
}

int VUMeterLevel::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 5;
    }
    return _id;
}

// SIGNAL 0
void VUMeterLevel::activate_over_led(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
