/****************************************************************************
** Meta object code from reading C++ file 'ResourcesWidget.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "ResourcesWidget.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'ResourcesWidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_FileWidget_t {
    QByteArrayData data[8];
    char stringdata0[98];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_FileWidget_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_FileWidget_t qt_meta_stringdata_FileWidget = {
    {
QT_MOC_LITERAL(0, 0, 10), // "FileWidget"
QT_MOC_LITERAL(1, 11, 20), // "dirview_item_clicked"
QT_MOC_LITERAL(2, 32, 0), // ""
QT_MOC_LITERAL(3, 33, 5), // "index"
QT_MOC_LITERAL(4, 39, 21), // "dir_up_button_clicked"
QT_MOC_LITERAL(5, 61, 22), // "refresh_button_clicked"
QT_MOC_LITERAL(6, 84, 11), // "box_actived"
QT_MOC_LITERAL(7, 96, 1) // "i"

    },
    "FileWidget\0dirview_item_clicked\0\0index\0"
    "dir_up_button_clicked\0refresh_button_clicked\0"
    "box_actived\0i"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_FileWidget[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   34,    2, 0x08 /* Private */,
       4,    0,   37,    2, 0x08 /* Private */,
       5,    0,   38,    2, 0x08 /* Private */,
       6,    1,   39,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::QModelIndex,    3,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    7,

       0        // eod
};

void FileWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        FileWidget *_t = static_cast<FileWidget *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->dirview_item_clicked((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 1: _t->dir_up_button_clicked(); break;
        case 2: _t->refresh_button_clicked(); break;
        case 3: _t->box_actived((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObject FileWidget::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_FileWidget.data,
      qt_meta_data_FileWidget,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *FileWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *FileWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_FileWidget.stringdata0))
        return static_cast<void*>(const_cast< FileWidget*>(this));
    return QWidget::qt_metacast(_clname);
}

int FileWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 4)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 4;
    }
    return _id;
}
struct qt_meta_stringdata_ClipTreeItem_t {
    QByteArrayData data[3];
    char stringdata0[33];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ClipTreeItem_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ClipTreeItem_t qt_meta_stringdata_ClipTreeItem = {
    {
QT_MOC_LITERAL(0, 0, 12), // "ClipTreeItem"
QT_MOC_LITERAL(1, 13, 18), // "clip_state_changed"
QT_MOC_LITERAL(2, 32, 0) // ""

    },
    "ClipTreeItem\0clip_state_changed\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ClipTreeItem[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   19,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,

       0        // eod
};

void ClipTreeItem::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        ClipTreeItem *_t = static_cast<ClipTreeItem *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->clip_state_changed(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject ClipTreeItem::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_ClipTreeItem.data,
      qt_meta_data_ClipTreeItem,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *ClipTreeItem::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ClipTreeItem::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_ClipTreeItem.stringdata0))
        return static_cast<void*>(const_cast< ClipTreeItem*>(this));
    if (!strcmp(_clname, "QTreeWidgetItem"))
        return static_cast< QTreeWidgetItem*>(const_cast< ClipTreeItem*>(this));
    return QObject::qt_metacast(_clname);
}

int ClipTreeItem::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
    return _id;
}
struct qt_meta_stringdata_SourceTreeItem_t {
    QByteArrayData data[3];
    char stringdata0[37];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SourceTreeItem_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SourceTreeItem_t qt_meta_stringdata_SourceTreeItem = {
    {
QT_MOC_LITERAL(0, 0, 14), // "SourceTreeItem"
QT_MOC_LITERAL(1, 15, 20), // "source_state_changed"
QT_MOC_LITERAL(2, 36, 0) // ""

    },
    "SourceTreeItem\0source_state_changed\0"
    ""
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SourceTreeItem[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   19,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,

       0        // eod
};

void SourceTreeItem::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        SourceTreeItem *_t = static_cast<SourceTreeItem *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->source_state_changed(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject SourceTreeItem::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_SourceTreeItem.data,
      qt_meta_data_SourceTreeItem,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *SourceTreeItem::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SourceTreeItem::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_SourceTreeItem.stringdata0))
        return static_cast<void*>(const_cast< SourceTreeItem*>(this));
    if (!strcmp(_clname, "QTreeWidgetItem"))
        return static_cast< QTreeWidgetItem*>(const_cast< SourceTreeItem*>(this));
    return QObject::qt_metacast(_clname);
}

int SourceTreeItem::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
    return _id;
}
struct qt_meta_stringdata_ResourcesWidget_t {
    QByteArrayData data[22];
    char stringdata0[271];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ResourcesWidget_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ResourcesWidget_t qt_meta_stringdata_ResourcesWidget = {
    {
QT_MOC_LITERAL(0, 0, 15), // "ResourcesWidget"
QT_MOC_LITERAL(1, 16, 11), // "set_project"
QT_MOC_LITERAL(2, 28, 0), // ""
QT_MOC_LITERAL(3, 29, 8), // "Project*"
QT_MOC_LITERAL(4, 38, 7), // "project"
QT_MOC_LITERAL(5, 46, 21), // "project_load_finished"
QT_MOC_LITERAL(6, 68, 28), // "view_combo_box_index_changed"
QT_MOC_LITERAL(7, 97, 5), // "index"
QT_MOC_LITERAL(8, 103, 29), // "sheet_combo_box_index_changed"
QT_MOC_LITERAL(9, 133, 11), // "sheet_added"
QT_MOC_LITERAL(10, 145, 6), // "Sheet*"
QT_MOC_LITERAL(11, 152, 5), // "sheet"
QT_MOC_LITERAL(12, 158, 13), // "sheet_removed"
QT_MOC_LITERAL(13, 172, 17), // "set_current_sheet"
QT_MOC_LITERAL(14, 190, 8), // "add_clip"
QT_MOC_LITERAL(15, 199, 10), // "AudioClip*"
QT_MOC_LITERAL(16, 210, 4), // "clip"
QT_MOC_LITERAL(17, 215, 11), // "remove_clip"
QT_MOC_LITERAL(18, 227, 10), // "add_source"
QT_MOC_LITERAL(19, 238, 11), // "ReadSource*"
QT_MOC_LITERAL(20, 250, 6), // "source"
QT_MOC_LITERAL(21, 257, 13) // "remove_source"

    },
    "ResourcesWidget\0set_project\0\0Project*\0"
    "project\0project_load_finished\0"
    "view_combo_box_index_changed\0index\0"
    "sheet_combo_box_index_changed\0sheet_added\0"
    "Sheet*\0sheet\0sheet_removed\0set_current_sheet\0"
    "add_clip\0AudioClip*\0clip\0remove_clip\0"
    "add_source\0ReadSource*\0source\0"
    "remove_source"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ResourcesWidget[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   69,    2, 0x08 /* Private */,
       5,    0,   72,    2, 0x08 /* Private */,
       6,    1,   73,    2, 0x08 /* Private */,
       8,    1,   76,    2, 0x08 /* Private */,
       9,    1,   79,    2, 0x08 /* Private */,
      12,    1,   82,    2, 0x08 /* Private */,
      13,    1,   85,    2, 0x08 /* Private */,
      14,    1,   88,    2, 0x08 /* Private */,
      17,    1,   91,    2, 0x08 /* Private */,
      18,    1,   94,    2, 0x08 /* Private */,
      21,    1,   97,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    7,
    QMetaType::Void, QMetaType::Int,    7,
    QMetaType::Void, 0x80000000 | 10,   11,
    QMetaType::Void, 0x80000000 | 10,   11,
    QMetaType::Void, 0x80000000 | 10,   11,
    QMetaType::Void, 0x80000000 | 15,   16,
    QMetaType::Void, 0x80000000 | 15,   16,
    QMetaType::Void, 0x80000000 | 19,   20,
    QMetaType::Void, 0x80000000 | 19,   20,

       0        // eod
};

void ResourcesWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        ResourcesWidget *_t = static_cast<ResourcesWidget *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->set_project((*reinterpret_cast< Project*(*)>(_a[1]))); break;
        case 1: _t->project_load_finished(); break;
        case 2: _t->view_combo_box_index_changed((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->sheet_combo_box_index_changed((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->sheet_added((*reinterpret_cast< Sheet*(*)>(_a[1]))); break;
        case 5: _t->sheet_removed((*reinterpret_cast< Sheet*(*)>(_a[1]))); break;
        case 6: _t->set_current_sheet((*reinterpret_cast< Sheet*(*)>(_a[1]))); break;
        case 7: _t->add_clip((*reinterpret_cast< AudioClip*(*)>(_a[1]))); break;
        case 8: _t->remove_clip((*reinterpret_cast< AudioClip*(*)>(_a[1]))); break;
        case 9: _t->add_source((*reinterpret_cast< ReadSource*(*)>(_a[1]))); break;
        case 10: _t->remove_source((*reinterpret_cast< ReadSource*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObject ResourcesWidget::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_ResourcesWidget.data,
      qt_meta_data_ResourcesWidget,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *ResourcesWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ResourcesWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_ResourcesWidget.stringdata0))
        return static_cast<void*>(const_cast< ResourcesWidget*>(this));
    if (!strcmp(_clname, "Ui::ResourcesWidget"))
        return static_cast< Ui::ResourcesWidget*>(const_cast< ResourcesWidget*>(this));
    return QWidget::qt_metacast(_clname);
}

int ResourcesWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 11)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 11;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
