/****************************************************************************
** Meta object code from reading C++ file 'ProjectConverterDialog.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "ProjectConverterDialog.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'ProjectConverterDialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_ProjectConverterDialog_t {
    QByteArrayData data[8];
    char stringdata0[163];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ProjectConverterDialog_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ProjectConverterDialog_t qt_meta_stringdata_ProjectConverterDialog = {
    {
QT_MOC_LITERAL(0, 0, 22), // "ProjectConverterDialog"
QT_MOC_LITERAL(1, 23, 18), // "file_merge_started"
QT_MOC_LITERAL(2, 42, 0), // ""
QT_MOC_LITERAL(3, 43, 19), // "file_merge_finished"
QT_MOC_LITERAL(4, 63, 18), // "converter_messages"
QT_MOC_LITERAL(5, 82, 19), // "conversion_finished"
QT_MOC_LITERAL(6, 102, 28), // "on_loadProjectButton_clicked"
QT_MOC_LITERAL(7, 131, 31) // "on_stopConversionButton_clicked"

    },
    "ProjectConverterDialog\0file_merge_started\0"
    "\0file_merge_finished\0converter_messages\0"
    "conversion_finished\0on_loadProjectButton_clicked\0"
    "on_stopConversionButton_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ProjectConverterDialog[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   44,    2, 0x08 /* Private */,
       3,    1,   47,    2, 0x08 /* Private */,
       4,    1,   50,    2, 0x08 /* Private */,
       5,    0,   53,    2, 0x08 /* Private */,
       6,    0,   54,    2, 0x08 /* Private */,
       7,    0,   55,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void ProjectConverterDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        ProjectConverterDialog *_t = static_cast<ProjectConverterDialog *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->file_merge_started((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->file_merge_finished((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->converter_messages((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 3: _t->conversion_finished(); break;
        case 4: _t->on_loadProjectButton_clicked(); break;
        case 5: _t->on_stopConversionButton_clicked(); break;
        default: ;
        }
    }
}

const QMetaObject ProjectConverterDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_ProjectConverterDialog.data,
      qt_meta_data_ProjectConverterDialog,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *ProjectConverterDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ProjectConverterDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_ProjectConverterDialog.stringdata0))
        return static_cast<void*>(const_cast< ProjectConverterDialog*>(this));
    if (!strcmp(_clname, "Ui::ProjectConverterDialog"))
        return static_cast< Ui::ProjectConverterDialog*>(const_cast< ProjectConverterDialog*>(this));
    return QDialog::qt_metacast(_clname);
}

int ProjectConverterDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 6;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
