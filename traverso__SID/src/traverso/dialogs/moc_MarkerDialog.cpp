/****************************************************************************
** Meta object code from reading C++ file 'MarkerDialog.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "MarkerDialog.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'MarkerDialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_MarkerDialog_t {
    QByteArrayData data[27];
    char stringdata0[354];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MarkerDialog_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MarkerDialog_t qt_meta_stringdata_MarkerDialog = {
    {
QT_MOC_LITERAL(0, 0, 12), // "MarkerDialog"
QT_MOC_LITERAL(1, 13, 22), // "update_marker_treeview"
QT_MOC_LITERAL(2, 36, 0), // ""
QT_MOC_LITERAL(3, 37, 12), // "item_changed"
QT_MOC_LITERAL(4, 50, 16), // "QTreeWidgetItem*"
QT_MOC_LITERAL(5, 67, 19), // "description_changed"
QT_MOC_LITERAL(6, 87, 16), // "position_changed"
QT_MOC_LITERAL(7, 104, 13), // "remove_marker"
QT_MOC_LITERAL(8, 118, 10), // "export_toc"
QT_MOC_LITERAL(9, 129, 5), // "apply"
QT_MOC_LITERAL(10, 135, 6), // "cancel"
QT_MOC_LITERAL(11, 142, 11), // "title_enter"
QT_MOC_LITERAL(12, 154, 14), // "position_enter"
QT_MOC_LITERAL(13, 169, 15), // "performer_enter"
QT_MOC_LITERAL(14, 185, 14), // "composer_enter"
QT_MOC_LITERAL(15, 200, 14), // "arranger_enter"
QT_MOC_LITERAL(16, 215, 17), // "sheetwriter_enter"
QT_MOC_LITERAL(17, 233, 13), // "message_enter"
QT_MOC_LITERAL(18, 247, 10), // "isrc_enter"
QT_MOC_LITERAL(19, 258, 9), // "title_all"
QT_MOC_LITERAL(20, 268, 13), // "performer_all"
QT_MOC_LITERAL(21, 282, 12), // "composer_all"
QT_MOC_LITERAL(22, 295, 12), // "arranger_all"
QT_MOC_LITERAL(23, 308, 14), // "songwriter_all"
QT_MOC_LITERAL(24, 323, 11), // "message_all"
QT_MOC_LITERAL(25, 335, 8), // "copy_all"
QT_MOC_LITERAL(26, 344, 9) // "pemph_all"

    },
    "MarkerDialog\0update_marker_treeview\0"
    "\0item_changed\0QTreeWidgetItem*\0"
    "description_changed\0position_changed\0"
    "remove_marker\0export_toc\0apply\0cancel\0"
    "title_enter\0position_enter\0performer_enter\0"
    "composer_enter\0arranger_enter\0"
    "sheetwriter_enter\0message_enter\0"
    "isrc_enter\0title_all\0performer_all\0"
    "composer_all\0arranger_all\0songwriter_all\0"
    "message_all\0copy_all\0pemph_all"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MarkerDialog[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      24,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  134,    2, 0x08 /* Private */,
       3,    2,  135,    2, 0x08 /* Private */,
       5,    1,  140,    2, 0x08 /* Private */,
       6,    1,  143,    2, 0x08 /* Private */,
       7,    0,  146,    2, 0x08 /* Private */,
       8,    0,  147,    2, 0x08 /* Private */,
       9,    0,  148,    2, 0x08 /* Private */,
      10,    0,  149,    2, 0x08 /* Private */,
      11,    0,  150,    2, 0x08 /* Private */,
      12,    0,  151,    2, 0x08 /* Private */,
      13,    0,  152,    2, 0x08 /* Private */,
      14,    0,  153,    2, 0x08 /* Private */,
      15,    0,  154,    2, 0x08 /* Private */,
      16,    0,  155,    2, 0x08 /* Private */,
      17,    0,  156,    2, 0x08 /* Private */,
      18,    0,  157,    2, 0x08 /* Private */,
      19,    0,  158,    2, 0x08 /* Private */,
      20,    0,  159,    2, 0x08 /* Private */,
      21,    0,  160,    2, 0x08 /* Private */,
      22,    0,  161,    2, 0x08 /* Private */,
      23,    0,  162,    2, 0x08 /* Private */,
      24,    0,  163,    2, 0x08 /* Private */,
      25,    0,  164,    2, 0x08 /* Private */,
      26,    0,  165,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4, 0x80000000 | 4,    2,    2,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MarkerDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MarkerDialog *_t = static_cast<MarkerDialog *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->update_marker_treeview(); break;
        case 1: _t->item_changed((*reinterpret_cast< QTreeWidgetItem*(*)>(_a[1])),(*reinterpret_cast< QTreeWidgetItem*(*)>(_a[2]))); break;
        case 2: _t->description_changed((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 3: _t->position_changed((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 4: _t->remove_marker(); break;
        case 5: _t->export_toc(); break;
        case 6: _t->apply(); break;
        case 7: _t->cancel(); break;
        case 8: _t->title_enter(); break;
        case 9: _t->position_enter(); break;
        case 10: _t->performer_enter(); break;
        case 11: _t->composer_enter(); break;
        case 12: _t->arranger_enter(); break;
        case 13: _t->sheetwriter_enter(); break;
        case 14: _t->message_enter(); break;
        case 15: _t->isrc_enter(); break;
        case 16: _t->title_all(); break;
        case 17: _t->performer_all(); break;
        case 18: _t->composer_all(); break;
        case 19: _t->arranger_all(); break;
        case 20: _t->songwriter_all(); break;
        case 21: _t->message_all(); break;
        case 22: _t->copy_all(); break;
        case 23: _t->pemph_all(); break;
        default: ;
        }
    }
}

const QMetaObject MarkerDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_MarkerDialog.data,
      qt_meta_data_MarkerDialog,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *MarkerDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MarkerDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_MarkerDialog.stringdata0))
        return static_cast<void*>(const_cast< MarkerDialog*>(this));
    if (!strcmp(_clname, "Ui::MarkerDialog"))
        return static_cast< Ui::MarkerDialog*>(const_cast< MarkerDialog*>(this));
    return QDialog::qt_metacast(_clname);
}

int MarkerDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 24)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 24;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 24)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 24;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
