/********************************************************************************
** Form generated from reading UI file 'ResourcesWidget.ui'
**
** Created by: Qt User Interface Compiler version 5.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_RESOURCESWIDGET_H
#define UI_RESOURCESWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ResourcesWidget
{
public:
    QWidget *layoutWidget;
    QVBoxLayout *vboxLayout;
    QVBoxLayout *vboxLayout1;
    QHBoxLayout *hboxLayout;
    QComboBox *viewComboBox;
    QComboBox *sheetComboBox;
    QSpacerItem *spacerItem;
    QVBoxLayout *vboxLayout2;
    QTreeWidget *sourcesTreeWidget;

    void setupUi(QWidget *ResourcesWidget)
    {
        if (ResourcesWidget->objectName().isEmpty())
            ResourcesWidget->setObjectName(QStringLiteral("ResourcesWidget"));
        ResourcesWidget->resize(540, 530);
        layoutWidget = new QWidget(ResourcesWidget);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(0, 0, 2, 2));
        vboxLayout = new QVBoxLayout(layoutWidget);
#ifndef Q_OS_MAC
        vboxLayout->setSpacing(6);
#endif
        vboxLayout->setContentsMargins(0, 0, 0, 0);
        vboxLayout->setObjectName(QStringLiteral("vboxLayout"));
        vboxLayout->setContentsMargins(0, 0, 0, 0);
        vboxLayout1 = new QVBoxLayout(ResourcesWidget);
#ifndef Q_OS_MAC
        vboxLayout1->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout1->setContentsMargins(0, 0, 0, 0);
#endif
        vboxLayout1->setObjectName(QStringLiteral("vboxLayout1"));
        hboxLayout = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout->setContentsMargins(0, 0, 0, 0);
#endif
        hboxLayout->setObjectName(QStringLiteral("hboxLayout"));
        viewComboBox = new QComboBox(ResourcesWidget);
        viewComboBox->setObjectName(QStringLiteral("viewComboBox"));
        QSizePolicy sizePolicy(static_cast<QSizePolicy::Policy>(5), static_cast<QSizePolicy::Policy>(0));
        sizePolicy.setHorizontalStretch(2);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(viewComboBox->sizePolicy().hasHeightForWidth());
        viewComboBox->setSizePolicy(sizePolicy);

        hboxLayout->addWidget(viewComboBox);

        sheetComboBox = new QComboBox(ResourcesWidget);
        sheetComboBox->setObjectName(QStringLiteral("sheetComboBox"));

        hboxLayout->addWidget(sheetComboBox);

        spacerItem = new QSpacerItem(2, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacerItem);


        vboxLayout1->addLayout(hboxLayout);

        vboxLayout2 = new QVBoxLayout();
#ifndef Q_OS_MAC
        vboxLayout2->setSpacing(6);
#endif
        vboxLayout2->setContentsMargins(0, 0, 0, 0);
        vboxLayout2->setObjectName(QStringLiteral("vboxLayout2"));
        sourcesTreeWidget = new QTreeWidget(ResourcesWidget);
        sourcesTreeWidget->setObjectName(QStringLiteral("sourcesTreeWidget"));
        sourcesTreeWidget->setMouseTracking(false);
        sourcesTreeWidget->setEditTriggers(QAbstractItemView::DoubleClicked|QAbstractItemView::EditKeyPressed|QAbstractItemView::NoEditTriggers);
        sourcesTreeWidget->setAlternatingRowColors(true);
        sourcesTreeWidget->setIconSize(QSize(16, 16));
        sourcesTreeWidget->setTextElideMode(Qt::ElideMiddle);
        sourcesTreeWidget->setSortingEnabled(false);
        sourcesTreeWidget->setAnimated(false);

        vboxLayout2->addWidget(sourcesTreeWidget);


        vboxLayout1->addLayout(vboxLayout2);


        retranslateUi(ResourcesWidget);

        QMetaObject::connectSlotsByName(ResourcesWidget);
    } // setupUi

    void retranslateUi(QWidget *ResourcesWidget)
    {
        ResourcesWidget->setWindowTitle(QApplication::translate("ResourcesWidget", "Form", Q_NULLPTR));
        viewComboBox->clear();
        viewComboBox->insertItems(0, QStringList()
         << QApplication::translate("ResourcesWidget", "Sources", Q_NULLPTR)
         << QApplication::translate("ResourcesWidget", "Files", Q_NULLPTR)
        );
        QTreeWidgetItem *___qtreewidgetitem = sourcesTreeWidget->headerItem();
        ___qtreewidgetitem->setText(3, QApplication::translate("ResourcesWidget", "End", Q_NULLPTR));
        ___qtreewidgetitem->setText(2, QApplication::translate("ResourcesWidget", "Start", Q_NULLPTR));
        ___qtreewidgetitem->setText(1, QApplication::translate("ResourcesWidget", "Length", Q_NULLPTR));
        ___qtreewidgetitem->setText(0, QApplication::translate("ResourcesWidget", "Name", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class ResourcesWidget: public Ui_ResourcesWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_RESOURCESWIDGET_H
