/********************************************************************************
** Form generated from reading UI file 'BusSelectorDialog.ui'
**
** Created by: Qt User Interface Compiler version 5.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_BUSSELECTORDIALOG_H
#define UI_BUSSELECTORDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_BusSelectorDialog
{
public:
    QVBoxLayout *vboxLayout;
    QHBoxLayout *hboxLayout;
    QLabel *label_3;
    QSpacerItem *spacerItem;
    QComboBox *trackComboBox;
    QGroupBox *groupBox_2;
    QHBoxLayout *hboxLayout1;
    QListWidget *busesListWidget;
    QVBoxLayout *vboxLayout1;
    QLabel *label_2;
    QRadioButton *radioBoth;
    QRadioButton *radioLeftOnly;
    QRadioButton *radioRightOnly;
    QSpacerItem *spacerItem1;
    QGroupBox *playbackBusesGroupBox;
    QHBoxLayout *hboxLayout2;
    QListWidget *busesListWidgetPlayback;
    QVBoxLayout *vboxLayout2;
    QLabel *label_5;
    QRadioButton *radioBothPlayback;
    QRadioButton *radioLeftOnlyPlayback;
    QRadioButton *radioRightOnlyPlayback;
    QSpacerItem *spacerItem2;
    QDialogButtonBox *buttonBox;

    void setupUi(QWidget *BusSelectorDialog)
    {
        if (BusSelectorDialog->objectName().isEmpty())
            BusSelectorDialog->setObjectName(QStringLiteral("BusSelectorDialog"));
        BusSelectorDialog->resize(321, 389);
        vboxLayout = new QVBoxLayout(BusSelectorDialog);
#ifndef Q_OS_MAC
        vboxLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout->setObjectName(QStringLiteral("vboxLayout"));
        hboxLayout = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout->setContentsMargins(0, 0, 0, 0);
#endif
        hboxLayout->setObjectName(QStringLiteral("hboxLayout"));
        label_3 = new QLabel(BusSelectorDialog);
        label_3->setObjectName(QStringLiteral("label_3"));
        QSizePolicy sizePolicy(static_cast<QSizePolicy::Policy>(0), static_cast<QSizePolicy::Policy>(0));
        sizePolicy.setHorizontalStretch(2);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(label_3->sizePolicy().hasHeightForWidth());
        label_3->setSizePolicy(sizePolicy);

        hboxLayout->addWidget(label_3);

        spacerItem = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacerItem);

        trackComboBox = new QComboBox(BusSelectorDialog);
        trackComboBox->setObjectName(QStringLiteral("trackComboBox"));
        QSizePolicy sizePolicy1(static_cast<QSizePolicy::Policy>(5), static_cast<QSizePolicy::Policy>(0));
        sizePolicy1.setHorizontalStretch(4);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(trackComboBox->sizePolicy().hasHeightForWidth());
        trackComboBox->setSizePolicy(sizePolicy1);

        hboxLayout->addWidget(trackComboBox);


        vboxLayout->addLayout(hboxLayout);

        groupBox_2 = new QGroupBox(BusSelectorDialog);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        hboxLayout1 = new QHBoxLayout(groupBox_2);
#ifndef Q_OS_MAC
        hboxLayout1->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout1->setContentsMargins(9, 9, 9, 9);
#endif
        hboxLayout1->setObjectName(QStringLiteral("hboxLayout1"));
        busesListWidget = new QListWidget(groupBox_2);
        busesListWidget->setObjectName(QStringLiteral("busesListWidget"));

        hboxLayout1->addWidget(busesListWidget);

        vboxLayout1 = new QVBoxLayout();
#ifndef Q_OS_MAC
        vboxLayout1->setSpacing(6);
#endif
        vboxLayout1->setContentsMargins(0, 0, 0, 0);
        vboxLayout1->setObjectName(QStringLiteral("vboxLayout1"));
        label_2 = new QLabel(groupBox_2);
        label_2->setObjectName(QStringLiteral("label_2"));

        vboxLayout1->addWidget(label_2);

        radioBoth = new QRadioButton(groupBox_2);
        radioBoth->setObjectName(QStringLiteral("radioBoth"));
        radioBoth->setChecked(true);

        vboxLayout1->addWidget(radioBoth);

        radioLeftOnly = new QRadioButton(groupBox_2);
        radioLeftOnly->setObjectName(QStringLiteral("radioLeftOnly"));

        vboxLayout1->addWidget(radioLeftOnly);

        radioRightOnly = new QRadioButton(groupBox_2);
        radioRightOnly->setObjectName(QStringLiteral("radioRightOnly"));

        vboxLayout1->addWidget(radioRightOnly);

        spacerItem1 = new QSpacerItem(20, 1, QSizePolicy::Minimum, QSizePolicy::Expanding);

        vboxLayout1->addItem(spacerItem1);


        hboxLayout1->addLayout(vboxLayout1);


        vboxLayout->addWidget(groupBox_2);

        playbackBusesGroupBox = new QGroupBox(BusSelectorDialog);
        playbackBusesGroupBox->setObjectName(QStringLiteral("playbackBusesGroupBox"));
        hboxLayout2 = new QHBoxLayout(playbackBusesGroupBox);
#ifndef Q_OS_MAC
        hboxLayout2->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout2->setContentsMargins(9, 9, 9, 9);
#endif
        hboxLayout2->setObjectName(QStringLiteral("hboxLayout2"));
        busesListWidgetPlayback = new QListWidget(playbackBusesGroupBox);
        busesListWidgetPlayback->setObjectName(QStringLiteral("busesListWidgetPlayback"));

        hboxLayout2->addWidget(busesListWidgetPlayback);

        vboxLayout2 = new QVBoxLayout();
#ifndef Q_OS_MAC
        vboxLayout2->setSpacing(6);
#endif
        vboxLayout2->setContentsMargins(0, 0, 0, 0);
        vboxLayout2->setObjectName(QStringLiteral("vboxLayout2"));
        label_5 = new QLabel(playbackBusesGroupBox);
        label_5->setObjectName(QStringLiteral("label_5"));

        vboxLayout2->addWidget(label_5);

        radioBothPlayback = new QRadioButton(playbackBusesGroupBox);
        radioBothPlayback->setObjectName(QStringLiteral("radioBothPlayback"));
        radioBothPlayback->setChecked(true);

        vboxLayout2->addWidget(radioBothPlayback);

        radioLeftOnlyPlayback = new QRadioButton(playbackBusesGroupBox);
        radioLeftOnlyPlayback->setObjectName(QStringLiteral("radioLeftOnlyPlayback"));

        vboxLayout2->addWidget(radioLeftOnlyPlayback);

        radioRightOnlyPlayback = new QRadioButton(playbackBusesGroupBox);
        radioRightOnlyPlayback->setObjectName(QStringLiteral("radioRightOnlyPlayback"));

        vboxLayout2->addWidget(radioRightOnlyPlayback);

        spacerItem2 = new QSpacerItem(20, 1, QSizePolicy::Minimum, QSizePolicy::Expanding);

        vboxLayout2->addItem(spacerItem2);


        hboxLayout2->addLayout(vboxLayout2);


        vboxLayout->addWidget(playbackBusesGroupBox);

        buttonBox = new QDialogButtonBox(BusSelectorDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Close|QDialogButtonBox::NoButton|QDialogButtonBox::Ok);

        vboxLayout->addWidget(buttonBox);


        retranslateUi(BusSelectorDialog);

        QMetaObject::connectSlotsByName(BusSelectorDialog);
    } // setupUi

    void retranslateUi(QWidget *BusSelectorDialog)
    {
        BusSelectorDialog->setWindowTitle(QApplication::translate("BusSelectorDialog", "Bus Selector", Q_NULLPTR));
        label_3->setText(QApplication::translate("BusSelectorDialog", "Track", Q_NULLPTR));
        groupBox_2->setTitle(QApplication::translate("BusSelectorDialog", "Capture Buses", Q_NULLPTR));
        label_2->setText(QApplication::translate("BusSelectorDialog", "Channels", Q_NULLPTR));
        radioBoth->setText(QApplication::translate("BusSelectorDialog", "Both", Q_NULLPTR));
        radioLeftOnly->setText(QApplication::translate("BusSelectorDialog", "Left", Q_NULLPTR));
        radioRightOnly->setText(QApplication::translate("BusSelectorDialog", "Right", Q_NULLPTR));
        playbackBusesGroupBox->setTitle(QApplication::translate("BusSelectorDialog", "Playback Buses", Q_NULLPTR));
        label_5->setText(QApplication::translate("BusSelectorDialog", "Channels", Q_NULLPTR));
        radioBothPlayback->setText(QApplication::translate("BusSelectorDialog", "Both", Q_NULLPTR));
        radioLeftOnlyPlayback->setText(QApplication::translate("BusSelectorDialog", "Left", Q_NULLPTR));
        radioRightOnlyPlayback->setText(QApplication::translate("BusSelectorDialog", "Right", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class BusSelectorDialog: public Ui_BusSelectorDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_BUSSELECTORDIALOG_H
