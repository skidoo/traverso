/********************************************************************************
** Form generated from reading UI file 'NewTrackDialog.ui'
**
** Created by: Qt User Interface Compiler version 5.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_NEWTRACKDIALOG_H
#define UI_NEWTRACKDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_NewTrackDialog
{
public:
    QVBoxLayout *vboxLayout;
    QHBoxLayout *hboxLayout;
    QLabel *label;
    QLineEdit *titleLineEdit;
    QHBoxLayout *hboxLayout1;
    QLabel *label_2;
    QSpinBox *countSpinBox;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *NewTrackDialog)
    {
        if (NewTrackDialog->objectName().isEmpty())
            NewTrackDialog->setObjectName(QStringLiteral("NewTrackDialog"));
        NewTrackDialog->resize(239, 111);
        vboxLayout = new QVBoxLayout(NewTrackDialog);
#ifndef Q_OS_MAC
        vboxLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout->setObjectName(QStringLiteral("vboxLayout"));
        hboxLayout = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout->setContentsMargins(0, 0, 0, 0);
#endif
        hboxLayout->setObjectName(QStringLiteral("hboxLayout"));
        label = new QLabel(NewTrackDialog);
        label->setObjectName(QStringLiteral("label"));

        hboxLayout->addWidget(label);

        titleLineEdit = new QLineEdit(NewTrackDialog);
        titleLineEdit->setObjectName(QStringLiteral("titleLineEdit"));

        hboxLayout->addWidget(titleLineEdit);


        vboxLayout->addLayout(hboxLayout);

        hboxLayout1 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout1->setSpacing(6);
#endif
        hboxLayout1->setContentsMargins(0, 0, 0, 0);
        hboxLayout1->setObjectName(QStringLiteral("hboxLayout1"));
        label_2 = new QLabel(NewTrackDialog);
        label_2->setObjectName(QStringLiteral("label_2"));

        hboxLayout1->addWidget(label_2);

        countSpinBox = new QSpinBox(NewTrackDialog);
        countSpinBox->setObjectName(QStringLiteral("countSpinBox"));
        countSpinBox->setMinimum(1);
        countSpinBox->setValue(1);

        hboxLayout1->addWidget(countSpinBox);


        vboxLayout->addLayout(hboxLayout1);

        buttonBox = new QDialogButtonBox(NewTrackDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::NoButton|QDialogButtonBox::Ok);

        vboxLayout->addWidget(buttonBox);


        retranslateUi(NewTrackDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), NewTrackDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), NewTrackDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(NewTrackDialog);
    } // setupUi

    void retranslateUi(QDialog *NewTrackDialog)
    {
        NewTrackDialog->setWindowTitle(QApplication::translate("NewTrackDialog", "New Track(s)", Q_NULLPTR));
        label->setText(QApplication::translate("NewTrackDialog", "Track name", Q_NULLPTR));
        label_2->setText(QApplication::translate("NewTrackDialog", "Track count", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class NewTrackDialog: public Ui_NewTrackDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_NEWTRACKDIALOG_H
