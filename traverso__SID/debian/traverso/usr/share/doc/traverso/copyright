Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Traverso
Upstream-Contact: Remon Sijrier <remon@traverso-daw.org>
Source: http://rijschoolremon.nl/Traverso/traverso-0.49.5.tar.gz

Files: *
Copyright:
 2005-2009 Remon Sijrier
 2005-2008 Nicola Doebelin
 2007 Ben Levitt
 2000-2002 Richard W.E. Furse
 2000-2003 Paul Barton-Davis
 2000-2002 Stefan Westerfeld
 2006-2007 Steve Harris
 2006-2007 Dave Robillard
 2006 Dave Robillard
 2006-2008 Lars Luthman <lars.luthman@gmail.com>
 2005, Sampo Savolainen
 2003, Rohan Drape
License: GPL-2+

Files:
 src/common/RingBuffer.*
Copyright:
 2000, Paul Davis
 2003, Rohan Drape
 2006, Remon Sijrier
License: LGPL-2.1+

Files: debian/*
Copyright:
 2007, Gürkan Myczko <gurkan@phys.ethz.ch>
 2010, Alessio Treglia <alessio@debian.org>
 2015-2017 Jaromír Mikeš <mira.mikes@seznam.cz>
 2019 Olivier Humbert <trebmuh@tuxfamily.org>
License: GPL-2+

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-2'.

License: LGPL-2.1+
 This header is free software; you can redistribute it and/or modify it
 under the terms of the GNU Lesser General Public License as published
 by the Free Software Foundation; either version 2.1 of the License,
 or (at your option) any later version.
 .
 This header is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 .
 On Debian systems, the complete text of the Lesser General Public License
 can be found in `/usr/share/common-licenses/LGPL-2.1'.