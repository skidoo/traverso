# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/tmp/traverso/src/engine/AlsaDriver.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/engine/CMakeFiles/traversoaudiobackend.dir/AlsaDriver.cpp.o"
  "/tmp/traverso/src/engine/AudioBus.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/engine/CMakeFiles/traversoaudiobackend.dir/AudioBus.cpp.o"
  "/tmp/traverso/src/engine/AudioChannel.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/engine/CMakeFiles/traversoaudiobackend.dir/AudioChannel.cpp.o"
  "/tmp/traverso/src/engine/AudioDevice.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/engine/CMakeFiles/traversoaudiobackend.dir/AudioDevice.cpp.o"
  "/tmp/traverso/src/engine/AudioDeviceThread.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/engine/CMakeFiles/traversoaudiobackend.dir/AudioDeviceThread.cpp.o"
  "/tmp/traverso/src/engine/Driver.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/engine/CMakeFiles/traversoaudiobackend.dir/Driver.cpp.o"
  "/tmp/traverso/src/engine/JackDriver.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/engine/CMakeFiles/traversoaudiobackend.dir/JackDriver.cpp.o"
  "/tmp/traverso/src/engine/TAudioDeviceClient.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/engine/CMakeFiles/traversoaudiobackend.dir/TAudioDeviceClient.cpp.o"
  "/tmp/traverso/src/engine/memops.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/engine/CMakeFiles/traversoaudiobackend.dir/memops.cpp.o"
  "/tmp/traverso/obj-x86_64-linux-gnu/src/engine/moc_AudioBus.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/engine/CMakeFiles/traversoaudiobackend.dir/moc_AudioBus.cpp.o"
  "/tmp/traverso/obj-x86_64-linux-gnu/src/engine/moc_AudioDevice.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/engine/CMakeFiles/traversoaudiobackend.dir/moc_AudioDevice.cpp.o"
  "/tmp/traverso/obj-x86_64-linux-gnu/src/engine/moc_JackDriver.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/engine/CMakeFiles/traversoaudiobackend.dir/moc_JackDriver.cpp.o"
  "/tmp/traverso/obj-x86_64-linux-gnu/src/engine/moc_TAudioDeviceClient.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/engine/CMakeFiles/traversoaudiobackend.dir/moc_TAudioDeviceClient.cpp.o"
  "/tmp/traverso/obj-x86_64-linux-gnu/src/engine/traversoaudiobackend_autogen/mocs_compilation.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/engine/CMakeFiles/traversoaudiobackend.dir/traversoaudiobackend_autogen/mocs_compilation.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ALSA_SUPPORT"
  "HAVE_SYS_STAT_H"
  "HAVE_SYS_VFS_H"
  "JACK_SUPPORT"
  "LV2_SUPPORT"
  "MP3_DECODE_SUPPORT"
  "MP3_ENCODE_SUPPORT"
  "QT_NO_DEBUG"
  "STATIC_BUILD"
  "USE_MLOCK"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "src/engine"
  "../src/engine"
  "src/engine/traversoaudiobackend_autogen/include"
  "/usr/include/lilv-0"
  "/usr/include/sratom-0"
  "/usr/include/sord-0"
  "/usr/include/serd-0"
  "../src/common"
  "/usr/include/x86_64-linux-gnu/qt5"
  "/usr/include/x86_64-linux-gnu/qt5/QtWidgets"
  "/usr/include/x86_64-linux-gnu/qt5/QtGui"
  "/usr/include/x86_64-linux-gnu/qt5/QtCore"
  "/usr/lib/x86_64-linux-gnu/qt5/mkspecs/linux-g++"
  "/usr/include/x86_64-linux-gnu/qt5/QtXml"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
