/****************************************************************************
** Meta object code from reading C++ file 'AudioDevice.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/engine/AudioDevice.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'AudioDevice.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_AudioDevice_t {
    QByteArrayData data[17];
    char stringdata0[254];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_AudioDevice_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_AudioDevice_t qt_meta_stringdata_AudioDevice = {
    {
QT_MOC_LITERAL(0, 0, 11), // "AudioDevice"
QT_MOC_LITERAL(1, 12, 7), // "stopped"
QT_MOC_LITERAL(2, 20, 0), // ""
QT_MOC_LITERAL(3, 21, 7), // "started"
QT_MOC_LITERAL(4, 29, 19), // "driverParamsChanged"
QT_MOC_LITERAL(5, 49, 14), // "bufferUnderRun"
QT_MOC_LITERAL(6, 64, 13), // "clientRemoved"
QT_MOC_LITERAL(7, 78, 19), // "TAudioDeviceClient*"
QT_MOC_LITERAL(8, 98, 17), // "xrunStormDetected"
QT_MOC_LITERAL(9, 116, 7), // "message"
QT_MOC_LITERAL(10, 124, 18), // "private_add_client"
QT_MOC_LITERAL(11, 143, 6), // "client"
QT_MOC_LITERAL(12, 150, 21), // "private_remove_client"
QT_MOC_LITERAL(13, 172, 20), // "audiothread_finished"
QT_MOC_LITERAL(14, 193, 21), // "switch_to_null_driver"
QT_MOC_LITERAL(15, 215, 18), // "reset_xrun_counter"
QT_MOC_LITERAL(16, 234, 19) // "check_jack_shutdown"

    },
    "AudioDevice\0stopped\0\0started\0"
    "driverParamsChanged\0bufferUnderRun\0"
    "clientRemoved\0TAudioDeviceClient*\0"
    "xrunStormDetected\0message\0private_add_client\0"
    "client\0private_remove_client\0"
    "audiothread_finished\0switch_to_null_driver\0"
    "reset_xrun_counter\0check_jack_shutdown"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_AudioDevice[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       7,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   79,    2, 0x06 /* Public */,
       3,    0,   80,    2, 0x06 /* Public */,
       4,    0,   81,    2, 0x06 /* Public */,
       5,    0,   82,    2, 0x06 /* Public */,
       6,    1,   83,    2, 0x06 /* Public */,
       8,    0,   86,    2, 0x06 /* Public */,
       9,    2,   87,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      10,    1,   92,    2, 0x08 /* Private */,
      12,    1,   95,    2, 0x08 /* Private */,
      13,    0,   98,    2, 0x08 /* Private */,
      14,    0,   99,    2, 0x08 /* Private */,
      15,    0,  100,    2, 0x08 /* Private */,
      16,    0,  101,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 7,    2,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::Int,    2,    2,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 7,   11,
    QMetaType::Void, 0x80000000 | 7,   11,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void AudioDevice::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        AudioDevice *_t = static_cast<AudioDevice *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->stopped(); break;
        case 1: _t->started(); break;
        case 2: _t->driverParamsChanged(); break;
        case 3: _t->bufferUnderRun(); break;
        case 4: _t->clientRemoved((*reinterpret_cast< TAudioDeviceClient*(*)>(_a[1]))); break;
        case 5: _t->xrunStormDetected(); break;
        case 6: _t->message((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 7: _t->private_add_client((*reinterpret_cast< TAudioDeviceClient*(*)>(_a[1]))); break;
        case 8: _t->private_remove_client((*reinterpret_cast< TAudioDeviceClient*(*)>(_a[1]))); break;
        case 9: _t->audiothread_finished(); break;
        case 10: _t->switch_to_null_driver(); break;
        case 11: _t->reset_xrun_counter(); break;
        case 12: _t->check_jack_shutdown(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (AudioDevice::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&AudioDevice::stopped)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (AudioDevice::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&AudioDevice::started)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (AudioDevice::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&AudioDevice::driverParamsChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (AudioDevice::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&AudioDevice::bufferUnderRun)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (AudioDevice::*)(TAudioDeviceClient * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&AudioDevice::clientRemoved)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (AudioDevice::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&AudioDevice::xrunStormDetected)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (AudioDevice::*)(QString , int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&AudioDevice::message)) {
                *result = 6;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject AudioDevice::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_AudioDevice.data,
      qt_meta_data_AudioDevice,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *AudioDevice::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *AudioDevice::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_AudioDevice.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int AudioDevice::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 13)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 13;
    }
    return _id;
}

// SIGNAL 0
void AudioDevice::stopped()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void AudioDevice::started()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void AudioDevice::driverParamsChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void AudioDevice::bufferUnderRun()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void AudioDevice::clientRemoved(TAudioDeviceClient * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void AudioDevice::xrunStormDetected()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void AudioDevice::message(QString _t1, int _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
