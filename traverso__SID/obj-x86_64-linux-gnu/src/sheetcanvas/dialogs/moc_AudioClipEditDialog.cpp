/****************************************************************************
** Meta object code from reading C++ file 'AudioClipEditDialog.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../src/sheetcanvas/dialogs/AudioClipEditDialog.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'AudioClipEditDialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_AudioClipEditDialog_t {
    QByteArrayData data[35];
    char stringdata0[686];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_AudioClipEditDialog_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_AudioClipEditDialog_t qt_meta_stringdata_AudioClipEditDialog = {
    {
QT_MOC_LITERAL(0, 0, 19), // "AudioClipEditDialog"
QT_MOC_LITERAL(1, 20, 19), // "external_processing"
QT_MOC_LITERAL(2, 40, 0), // ""
QT_MOC_LITERAL(3, 41, 18), // "clip_state_changed"
QT_MOC_LITERAL(4, 60, 12), // "save_changes"
QT_MOC_LITERAL(5, 73, 14), // "cancel_changes"
QT_MOC_LITERAL(6, 88, 21), // "clip_position_changed"
QT_MOC_LITERAL(7, 110, 26), // "gain_spinbox_value_changed"
QT_MOC_LITERAL(8, 137, 5), // "value"
QT_MOC_LITERAL(9, 143, 21), // "fadein_length_changed"
QT_MOC_LITERAL(10, 165, 19), // "fadein_edit_changed"
QT_MOC_LITERAL(11, 185, 4), // "time"
QT_MOC_LITERAL(12, 190, 19), // "fadein_mode_changed"
QT_MOC_LITERAL(13, 210, 24), // "fadein_mode_edit_changed"
QT_MOC_LITERAL(14, 235, 5), // "index"
QT_MOC_LITERAL(15, 241, 22), // "fadein_bending_changed"
QT_MOC_LITERAL(16, 264, 27), // "fadein_bending_edit_changed"
QT_MOC_LITERAL(17, 292, 23), // "fadein_strength_changed"
QT_MOC_LITERAL(18, 316, 28), // "fadein_strength_edit_changed"
QT_MOC_LITERAL(19, 345, 13), // "fadein_linear"
QT_MOC_LITERAL(20, 359, 14), // "fadein_default"
QT_MOC_LITERAL(21, 374, 20), // "fadeout_edit_changed"
QT_MOC_LITERAL(22, 395, 22), // "fadeout_length_changed"
QT_MOC_LITERAL(23, 418, 20), // "fadeout_mode_changed"
QT_MOC_LITERAL(24, 439, 25), // "fadeout_mode_edit_changed"
QT_MOC_LITERAL(25, 465, 23), // "fadeout_bending_changed"
QT_MOC_LITERAL(26, 489, 28), // "fadeout_bending_edit_changed"
QT_MOC_LITERAL(27, 518, 24), // "fadeout_strength_changed"
QT_MOC_LITERAL(28, 543, 29), // "fadeout_strength_edit_changed"
QT_MOC_LITERAL(29, 573, 14), // "fadeout_linear"
QT_MOC_LITERAL(30, 588, 15), // "fadeout_default"
QT_MOC_LITERAL(31, 604, 23), // "clip_start_edit_changed"
QT_MOC_LITERAL(32, 628, 24), // "clip_length_edit_changed"
QT_MOC_LITERAL(33, 653, 15), // "update_clip_end"
QT_MOC_LITERAL(34, 669, 16) // "fade_curve_added"

    },
    "AudioClipEditDialog\0external_processing\0"
    "\0clip_state_changed\0save_changes\0"
    "cancel_changes\0clip_position_changed\0"
    "gain_spinbox_value_changed\0value\0"
    "fadein_length_changed\0fadein_edit_changed\0"
    "time\0fadein_mode_changed\0"
    "fadein_mode_edit_changed\0index\0"
    "fadein_bending_changed\0"
    "fadein_bending_edit_changed\0"
    "fadein_strength_changed\0"
    "fadein_strength_edit_changed\0fadein_linear\0"
    "fadein_default\0fadeout_edit_changed\0"
    "fadeout_length_changed\0fadeout_mode_changed\0"
    "fadeout_mode_edit_changed\0"
    "fadeout_bending_changed\0"
    "fadeout_bending_edit_changed\0"
    "fadeout_strength_changed\0"
    "fadeout_strength_edit_changed\0"
    "fadeout_linear\0fadeout_default\0"
    "clip_start_edit_changed\0"
    "clip_length_edit_changed\0update_clip_end\0"
    "fade_curve_added"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_AudioClipEditDialog[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      30,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  164,    2, 0x08 /* Private */,
       3,    0,  165,    2, 0x08 /* Private */,
       4,    0,  166,    2, 0x08 /* Private */,
       5,    0,  167,    2, 0x08 /* Private */,
       6,    0,  168,    2, 0x08 /* Private */,
       7,    1,  169,    2, 0x08 /* Private */,
       9,    0,  172,    2, 0x08 /* Private */,
      10,    1,  173,    2, 0x08 /* Private */,
      12,    0,  176,    2, 0x08 /* Private */,
      13,    1,  177,    2, 0x08 /* Private */,
      15,    0,  180,    2, 0x08 /* Private */,
      16,    1,  181,    2, 0x08 /* Private */,
      17,    0,  184,    2, 0x08 /* Private */,
      18,    1,  185,    2, 0x08 /* Private */,
      19,    0,  188,    2, 0x08 /* Private */,
      20,    0,  189,    2, 0x08 /* Private */,
      21,    1,  190,    2, 0x08 /* Private */,
      22,    0,  193,    2, 0x08 /* Private */,
      23,    0,  194,    2, 0x08 /* Private */,
      24,    1,  195,    2, 0x08 /* Private */,
      25,    0,  198,    2, 0x08 /* Private */,
      26,    1,  199,    2, 0x08 /* Private */,
      27,    0,  202,    2, 0x08 /* Private */,
      28,    1,  203,    2, 0x08 /* Private */,
      29,    0,  206,    2, 0x08 /* Private */,
      30,    0,  207,    2, 0x08 /* Private */,
      31,    1,  208,    2, 0x08 /* Private */,
      32,    1,  211,    2, 0x08 /* Private */,
      33,    0,  214,    2, 0x08 /* Private */,
      34,    0,  215,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Double,    8,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QTime,   11,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   14,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Double,    8,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Double,    8,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QTime,   11,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   14,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Double,    8,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Double,    8,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QTime,   11,
    QMetaType::Void, QMetaType::QTime,   11,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void AudioClipEditDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        AudioClipEditDialog *_t = static_cast<AudioClipEditDialog *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->external_processing(); break;
        case 1: _t->clip_state_changed(); break;
        case 2: _t->save_changes(); break;
        case 3: _t->cancel_changes(); break;
        case 4: _t->clip_position_changed(); break;
        case 5: _t->gain_spinbox_value_changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 6: _t->fadein_length_changed(); break;
        case 7: _t->fadein_edit_changed((*reinterpret_cast< const QTime(*)>(_a[1]))); break;
        case 8: _t->fadein_mode_changed(); break;
        case 9: _t->fadein_mode_edit_changed((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 10: _t->fadein_bending_changed(); break;
        case 11: _t->fadein_bending_edit_changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 12: _t->fadein_strength_changed(); break;
        case 13: _t->fadein_strength_edit_changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 14: _t->fadein_linear(); break;
        case 15: _t->fadein_default(); break;
        case 16: _t->fadeout_edit_changed((*reinterpret_cast< const QTime(*)>(_a[1]))); break;
        case 17: _t->fadeout_length_changed(); break;
        case 18: _t->fadeout_mode_changed(); break;
        case 19: _t->fadeout_mode_edit_changed((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 20: _t->fadeout_bending_changed(); break;
        case 21: _t->fadeout_bending_edit_changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 22: _t->fadeout_strength_changed(); break;
        case 23: _t->fadeout_strength_edit_changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 24: _t->fadeout_linear(); break;
        case 25: _t->fadeout_default(); break;
        case 26: _t->clip_start_edit_changed((*reinterpret_cast< const QTime(*)>(_a[1]))); break;
        case 27: _t->clip_length_edit_changed((*reinterpret_cast< const QTime(*)>(_a[1]))); break;
        case 28: _t->update_clip_end(); break;
        case 29: _t->fade_curve_added(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject AudioClipEditDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_AudioClipEditDialog.data,
      qt_meta_data_AudioClipEditDialog,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *AudioClipEditDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *AudioClipEditDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_AudioClipEditDialog.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "Ui::AudioClipEditDialog"))
        return static_cast< Ui::AudioClipEditDialog*>(this);
    return QDialog::qt_metacast(_clname);
}

int AudioClipEditDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 30)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 30;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 30)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 30;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
