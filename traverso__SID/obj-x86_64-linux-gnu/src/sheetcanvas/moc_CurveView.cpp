/****************************************************************************
** Meta object code from reading C++ file 'CurveView.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/sheetcanvas/CurveView.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'CurveView.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_DragNode_t {
    QByteArrayData data[7];
    char stringdata0[57];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_DragNode_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_DragNode_t qt_meta_stringdata_DragNode = {
    {
QT_MOC_LITERAL(0, 0, 8), // "DragNode"
QT_MOC_LITERAL(1, 9, 7), // "move_up"
QT_MOC_LITERAL(2, 17, 7), // "Move Up"
QT_MOC_LITERAL(3, 25, 9), // "move_down"
QT_MOC_LITERAL(4, 35, 9), // "Move Down"
QT_MOC_LITERAL(5, 45, 0), // ""
QT_MOC_LITERAL(6, 46, 10) // "autorepeat"

    },
    "DragNode\0move_up\0Move Up\0move_down\0"
    "Move Down\0\0autorepeat"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_DragNode[] = {

 // content:
       7,       // revision
       0,       // classname
       2,   14, // classinfo
       2,   18, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // classinfo: key, value
       1,    2,
       3,    4,

 // slots: name, argc, parameters, tag, flags
       1,    1,   28,    5, 0x0a /* Public */,
       3,    1,   31,    5, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void, QMetaType::Bool,    6,
    QMetaType::Void, QMetaType::Bool,    6,

       0        // eod
};

void DragNode::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        DragNode *_t = static_cast<DragNode *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->move_up((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->move_down((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject DragNode::staticMetaObject = {
    { &Command::staticMetaObject, qt_meta_stringdata_DragNode.data,
      qt_meta_data_DragNode,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *DragNode::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *DragNode::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_DragNode.stringdata0))
        return static_cast<void*>(this);
    return Command::qt_metacast(_clname);
}

int DragNode::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Command::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 2;
    }
    return _id;
}
struct qt_meta_stringdata_CurveView_t {
    QByteArrayData data[21];
    char stringdata0[281];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_CurveView_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_CurveView_t qt_meta_stringdata_CurveView = {
    {
QT_MOC_LITERAL(0, 0, 9), // "CurveView"
QT_MOC_LITERAL(1, 10, 8), // "add_node"
QT_MOC_LITERAL(2, 19, 8), // "New node"
QT_MOC_LITERAL(3, 28, 11), // "remove_node"
QT_MOC_LITERAL(4, 40, 11), // "Remove node"
QT_MOC_LITERAL(5, 52, 16), // "remove_all_nodes"
QT_MOC_LITERAL(6, 69, 16), // "Remove all Nodes"
QT_MOC_LITERAL(7, 86, 9), // "drag_node"
QT_MOC_LITERAL(8, 96, 9), // "Move node"
QT_MOC_LITERAL(9, 106, 23), // "drag_node_vertical_only"
QT_MOC_LITERAL(10, 130, 25), // "Move node (vertical only)"
QT_MOC_LITERAL(11, 156, 13), // "curveModified"
QT_MOC_LITERAL(12, 170, 0), // ""
QT_MOC_LITERAL(13, 171, 8), // "Command*"
QT_MOC_LITERAL(14, 180, 18), // "add_curvenode_view"
QT_MOC_LITERAL(15, 199, 10), // "CurveNode*"
QT_MOC_LITERAL(16, 210, 4), // "node"
QT_MOC_LITERAL(17, 215, 21), // "remove_curvenode_view"
QT_MOC_LITERAL(18, 237, 10), // "node_moved"
QT_MOC_LITERAL(19, 248, 13), // "set_view_mode"
QT_MOC_LITERAL(20, 262, 18) // "update_blink_color"

    },
    "CurveView\0add_node\0New node\0remove_node\0"
    "Remove node\0remove_all_nodes\0"
    "Remove all Nodes\0drag_node\0Move node\0"
    "drag_node_vertical_only\0"
    "Move node (vertical only)\0curveModified\0"
    "\0Command*\0add_curvenode_view\0CurveNode*\0"
    "node\0remove_curvenode_view\0node_moved\0"
    "set_view_mode\0update_blink_color"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_CurveView[] = {

 // content:
       7,       // revision
       0,       // classname
       5,   14, // classinfo
      11,   24, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // classinfo: key, value
       1,    2,
       3,    4,
       5,    6,
       7,    8,
       9,   10,

 // signals: name, argc, parameters, tag, flags
      11,    0,   79,   12, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       1,    0,   80,   12, 0x0a /* Public */,
       3,    0,   81,   12, 0x0a /* Public */,
       5,    0,   82,   12, 0x0a /* Public */,
       7,    0,   83,   12, 0x0a /* Public */,
       9,    0,   84,   12, 0x0a /* Public */,
      14,    1,   85,   12, 0x08 /* Private */,
      17,    1,   88,   12, 0x08 /* Private */,
      18,    0,   91,   12, 0x08 /* Private */,
      19,    0,   92,   12, 0x08 /* Private */,
      20,    0,   93,   12, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,

 // slots: parameters
    0x80000000 | 13,
    0x80000000 | 13,
    0x80000000 | 13,
    0x80000000 | 13,
    0x80000000 | 13,
    QMetaType::Void, 0x80000000 | 15,   16,
    QMetaType::Void, 0x80000000 | 15,   16,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void CurveView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        CurveView *_t = static_cast<CurveView *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->curveModified(); break;
        case 1: { Command* _r = _t->add_node();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 2: { Command* _r = _t->remove_node();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 3: { Command* _r = _t->remove_all_nodes();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 4: { Command* _r = _t->drag_node();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 5: { Command* _r = _t->drag_node_vertical_only();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 6: _t->add_curvenode_view((*reinterpret_cast< CurveNode*(*)>(_a[1]))); break;
        case 7: _t->remove_curvenode_view((*reinterpret_cast< CurveNode*(*)>(_a[1]))); break;
        case 8: _t->node_moved(); break;
        case 9: _t->set_view_mode(); break;
        case 10: _t->update_blink_color(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (CurveView::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&CurveView::curveModified)) {
                *result = 0;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject CurveView::staticMetaObject = {
    { &ViewItem::staticMetaObject, qt_meta_stringdata_CurveView.data,
      qt_meta_data_CurveView,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *CurveView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *CurveView::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_CurveView.stringdata0))
        return static_cast<void*>(this);
    return ViewItem::qt_metacast(_clname);
}

int CurveView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ViewItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 11)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 11;
    }
    return _id;
}

// SIGNAL 0
void CurveView::curveModified()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
