/****************************************************************************
** Meta object code from reading C++ file 'TimeLineView.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/sheetcanvas/TimeLineView.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TimeLineView.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_DragMarker_t {
    QByteArrayData data[7];
    char stringdata0[65];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_DragMarker_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_DragMarker_t qt_meta_stringdata_DragMarker = {
    {
QT_MOC_LITERAL(0, 0, 10), // "DragMarker"
QT_MOC_LITERAL(1, 11, 9), // "move_left"
QT_MOC_LITERAL(2, 21, 9), // "Move Left"
QT_MOC_LITERAL(3, 31, 10), // "move_right"
QT_MOC_LITERAL(4, 42, 10), // "Move right"
QT_MOC_LITERAL(5, 53, 0), // ""
QT_MOC_LITERAL(6, 54, 10) // "autorepeat"

    },
    "DragMarker\0move_left\0Move Left\0"
    "move_right\0Move right\0\0autorepeat"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_DragMarker[] = {

 // content:
       7,       // revision
       0,       // classname
       2,   14, // classinfo
       2,   18, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // classinfo: key, value
       1,    2,
       3,    4,

 // slots: name, argc, parameters, tag, flags
       1,    1,   28,    5, 0x0a /* Public */,
       3,    1,   31,    5, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void, QMetaType::Bool,    6,
    QMetaType::Void, QMetaType::Bool,    6,

       0        // eod
};

void DragMarker::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        DragMarker *_t = static_cast<DragMarker *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->move_left((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->move_right((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject DragMarker::staticMetaObject = {
    { &Command::staticMetaObject, qt_meta_stringdata_DragMarker.data,
      qt_meta_data_DragMarker,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *DragMarker::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *DragMarker::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_DragMarker.stringdata0))
        return static_cast<void*>(this);
    return Command::qt_metacast(_clname);
}

int DragMarker::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Command::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 2;
    }
    return _id;
}
struct qt_meta_stringdata_TimeLineView_t {
    QByteArrayData data[20];
    char stringdata0[281];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_TimeLineView_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_TimeLineView_t qt_meta_stringdata_TimeLineView = {
    {
QT_MOC_LITERAL(0, 0, 12), // "TimeLineView"
QT_MOC_LITERAL(1, 13, 10), // "add_marker"
QT_MOC_LITERAL(2, 24, 10), // "Add Marker"
QT_MOC_LITERAL(3, 35, 22), // "add_marker_at_playhead"
QT_MOC_LITERAL(4, 58, 22), // "Add Marker at Playhead"
QT_MOC_LITERAL(5, 81, 13), // "remove_marker"
QT_MOC_LITERAL(6, 95, 13), // "Remove Marker"
QT_MOC_LITERAL(7, 109, 11), // "drag_marker"
QT_MOC_LITERAL(8, 121, 11), // "Drag Marker"
QT_MOC_LITERAL(9, 133, 13), // "clear_markers"
QT_MOC_LITERAL(10, 147, 17), // "Clear all Markers"
QT_MOC_LITERAL(11, 165, 18), // "playhead_to_marker"
QT_MOC_LITERAL(12, 184, 18), // "Playhead to Marker"
QT_MOC_LITERAL(13, 203, 13), // "hzoom_changed"
QT_MOC_LITERAL(14, 217, 0), // ""
QT_MOC_LITERAL(15, 218, 8), // "Command*"
QT_MOC_LITERAL(16, 227, 19), // "add_new_marker_view"
QT_MOC_LITERAL(17, 247, 7), // "Marker*"
QT_MOC_LITERAL(18, 255, 6), // "marker"
QT_MOC_LITERAL(19, 262, 18) // "remove_marker_view"

    },
    "TimeLineView\0add_marker\0Add Marker\0"
    "add_marker_at_playhead\0Add Marker at Playhead\0"
    "remove_marker\0Remove Marker\0drag_marker\0"
    "Drag Marker\0clear_markers\0Clear all Markers\0"
    "playhead_to_marker\0Playhead to Marker\0"
    "hzoom_changed\0\0Command*\0add_new_marker_view\0"
    "Marker*\0marker\0remove_marker_view"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_TimeLineView[] = {

 // content:
       7,       // revision
       0,       // classname
       6,   14, // classinfo
       9,   26, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // classinfo: key, value
       1,    2,
       3,    4,
       5,    6,
       7,    8,
       9,   10,
      11,   12,

 // slots: name, argc, parameters, tag, flags
      13,    0,   71,   14, 0x0a /* Public */,
       1,    0,   72,   14, 0x0a /* Public */,
       3,    0,   73,   14, 0x0a /* Public */,
       5,    0,   74,   14, 0x0a /* Public */,
       7,    0,   75,   14, 0x0a /* Public */,
       9,    0,   76,   14, 0x0a /* Public */,
      11,    0,   77,   14, 0x0a /* Public */,
      16,    1,   78,   14, 0x08 /* Private */,
      19,    1,   81,   14, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    0x80000000 | 15,
    0x80000000 | 15,
    0x80000000 | 15,
    0x80000000 | 15,
    0x80000000 | 15,
    0x80000000 | 15,
    QMetaType::Void, 0x80000000 | 17,   18,
    QMetaType::Void, 0x80000000 | 17,   18,

       0        // eod
};

void TimeLineView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        TimeLineView *_t = static_cast<TimeLineView *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->hzoom_changed(); break;
        case 1: { Command* _r = _t->add_marker();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 2: { Command* _r = _t->add_marker_at_playhead();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 3: { Command* _r = _t->remove_marker();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 4: { Command* _r = _t->drag_marker();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 5: { Command* _r = _t->clear_markers();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 6: { Command* _r = _t->playhead_to_marker();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 7: _t->add_new_marker_view((*reinterpret_cast< Marker*(*)>(_a[1]))); break;
        case 8: _t->remove_marker_view((*reinterpret_cast< Marker*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject TimeLineView::staticMetaObject = {
    { &ViewItem::staticMetaObject, qt_meta_stringdata_TimeLineView.data,
      qt_meta_data_TimeLineView,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *TimeLineView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *TimeLineView::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_TimeLineView.stringdata0))
        return static_cast<void*>(this);
    return ViewItem::qt_metacast(_clname);
}

int TimeLineView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ViewItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 9;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
