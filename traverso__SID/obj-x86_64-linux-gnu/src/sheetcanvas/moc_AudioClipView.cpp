/****************************************************************************
** Meta object code from reading C++ file 'AudioClipView.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/sheetcanvas/AudioClipView.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'AudioClipView.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_AudioClipView_t {
    QByteArrayData data[33];
    char stringdata0[506];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_AudioClipView_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_AudioClipView_t qt_meta_stringdata_AudioClipView = {
    {
QT_MOC_LITERAL(0, 0, 13), // "AudioClipView"
QT_MOC_LITERAL(1, 14, 10), // "fade_range"
QT_MOC_LITERAL(2, 25, 22), // "Closest: Adjust Length"
QT_MOC_LITERAL(3, 48, 12), // "clip_fade_in"
QT_MOC_LITERAL(4, 61, 17), // "In: Adjust Length"
QT_MOC_LITERAL(5, 79, 13), // "clip_fade_out"
QT_MOC_LITERAL(6, 93, 18), // "Out: Adjust Length"
QT_MOC_LITERAL(7, 112, 20), // "select_fade_in_shape"
QT_MOC_LITERAL(8, 133, 17), // "In: Select Preset"
QT_MOC_LITERAL(9, 151, 21), // "select_fade_out_shape"
QT_MOC_LITERAL(10, 173, 18), // "Out: Select Preset"
QT_MOC_LITERAL(11, 192, 10), // "reset_fade"
QT_MOC_LITERAL(12, 203, 15), // "Closest: Delete"
QT_MOC_LITERAL(13, 219, 14), // "set_audio_file"
QT_MOC_LITERAL(14, 234, 16), // "Reset Audio File"
QT_MOC_LITERAL(15, 251, 15), // "edit_properties"
QT_MOC_LITERAL(16, 267, 15), // "Edit Properties"
QT_MOC_LITERAL(17, 283, 16), // "add_new_fadeview"
QT_MOC_LITERAL(18, 300, 0), // ""
QT_MOC_LITERAL(19, 301, 10), // "FadeCurve*"
QT_MOC_LITERAL(20, 312, 4), // "fade"
QT_MOC_LITERAL(21, 317, 15), // "remove_fadeview"
QT_MOC_LITERAL(22, 333, 7), // "repaint"
QT_MOC_LITERAL(23, 341, 16), // "update_start_pos"
QT_MOC_LITERAL(24, 358, 16), // "position_changed"
QT_MOC_LITERAL(25, 375, 8), // "Command*"
QT_MOC_LITERAL(26, 384, 20), // "update_progress_info"
QT_MOC_LITERAL(27, 405, 8), // "progress"
QT_MOC_LITERAL(28, 414, 22), // "peak_creation_finished"
QT_MOC_LITERAL(29, 437, 15), // "start_recording"
QT_MOC_LITERAL(30, 453, 16), // "finish_recording"
QT_MOC_LITERAL(31, 470, 16), // "update_recording"
QT_MOC_LITERAL(32, 487, 18) // "clip_state_changed"

    },
    "AudioClipView\0fade_range\0"
    "Closest: Adjust Length\0clip_fade_in\0"
    "In: Adjust Length\0clip_fade_out\0"
    "Out: Adjust Length\0select_fade_in_shape\0"
    "In: Select Preset\0select_fade_out_shape\0"
    "Out: Select Preset\0reset_fade\0"
    "Closest: Delete\0set_audio_file\0"
    "Reset Audio File\0edit_properties\0"
    "Edit Properties\0add_new_fadeview\0\0"
    "FadeCurve*\0fade\0remove_fadeview\0repaint\0"
    "update_start_pos\0position_changed\0"
    "Command*\0update_progress_info\0progress\0"
    "peak_creation_finished\0start_recording\0"
    "finish_recording\0update_recording\0"
    "clip_state_changed"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_AudioClipView[] = {

 // content:
       7,       // revision
       0,       // classname
       8,   14, // classinfo
      19,   30, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // classinfo: key, value
       1,    2,
       3,    4,
       5,    6,
       7,    8,
       9,   10,
      11,   12,
      13,   14,
      15,   16,

 // slots: name, argc, parameters, tag, flags
      17,    1,  125,   18, 0x0a /* Public */,
      21,    1,  128,   18, 0x0a /* Public */,
      22,    0,  131,   18, 0x0a /* Public */,
      23,    0,  132,   18, 0x0a /* Public */,
      24,    0,  133,   18, 0x0a /* Public */,
       1,    0,  134,   18, 0x0a /* Public */,
       3,    0,  135,   18, 0x0a /* Public */,
       5,    0,  136,   18, 0x0a /* Public */,
       7,    0,  137,   18, 0x0a /* Public */,
       9,    0,  138,   18, 0x0a /* Public */,
      11,    0,  139,   18, 0x0a /* Public */,
      13,    0,  140,   18, 0x0a /* Public */,
      15,    0,  141,   18, 0x0a /* Public */,
      26,    1,  142,   18, 0x08 /* Private */,
      28,    0,  145,   18, 0x08 /* Private */,
      29,    0,  146,   18, 0x08 /* Private */,
      30,    0,  147,   18, 0x08 /* Private */,
      31,    0,  148,   18, 0x08 /* Private */,
      32,    0,  149,   18, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 19,   20,
    QMetaType::Void, 0x80000000 | 19,   20,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    0x80000000 | 25,
    0x80000000 | 25,
    0x80000000 | 25,
    0x80000000 | 25,
    0x80000000 | 25,
    0x80000000 | 25,
    0x80000000 | 25,
    0x80000000 | 25,
    QMetaType::Void, QMetaType::Int,   27,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void AudioClipView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        AudioClipView *_t = static_cast<AudioClipView *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->add_new_fadeview((*reinterpret_cast< FadeCurve*(*)>(_a[1]))); break;
        case 1: _t->remove_fadeview((*reinterpret_cast< FadeCurve*(*)>(_a[1]))); break;
        case 2: _t->repaint(); break;
        case 3: _t->update_start_pos(); break;
        case 4: _t->position_changed(); break;
        case 5: { Command* _r = _t->fade_range();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 6: { Command* _r = _t->clip_fade_in();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 7: { Command* _r = _t->clip_fade_out();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 8: { Command* _r = _t->select_fade_in_shape();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 9: { Command* _r = _t->select_fade_out_shape();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 10: { Command* _r = _t->reset_fade();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 11: { Command* _r = _t->set_audio_file();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 12: { Command* _r = _t->edit_properties();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 13: _t->update_progress_info((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 14: _t->peak_creation_finished(); break;
        case 15: _t->start_recording(); break;
        case 16: _t->finish_recording(); break;
        case 17: _t->update_recording(); break;
        case 18: _t->clip_state_changed(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject AudioClipView::staticMetaObject = {
    { &ViewItem::staticMetaObject, qt_meta_stringdata_AudioClipView.data,
      qt_meta_data_AudioClipView,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *AudioClipView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *AudioClipView::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_AudioClipView.stringdata0))
        return static_cast<void*>(this);
    return ViewItem::qt_metacast(_clname);
}

int AudioClipView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ViewItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 19)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 19;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 19)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 19;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
