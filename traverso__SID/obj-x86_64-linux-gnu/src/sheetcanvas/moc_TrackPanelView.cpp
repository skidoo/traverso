/****************************************************************************
** Meta object code from reading C++ file 'TrackPanelView.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/sheetcanvas/TrackPanelView.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TrackPanelView.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_TrackPanelGain_t {
    QByteArrayData data[5];
    char stringdata0[55];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_TrackPanelGain_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_TrackPanelGain_t qt_meta_stringdata_TrackPanelGain = {
    {
QT_MOC_LITERAL(0, 0, 14), // "TrackPanelGain"
QT_MOC_LITERAL(1, 15, 14), // "gain_increment"
QT_MOC_LITERAL(2, 30, 8), // "Command*"
QT_MOC_LITERAL(3, 39, 0), // ""
QT_MOC_LITERAL(4, 40, 14) // "gain_decrement"

    },
    "TrackPanelGain\0gain_increment\0Command*\0"
    "\0gain_decrement"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_TrackPanelGain[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   24,    3, 0x0a /* Public */,
       4,    0,   25,    3, 0x0a /* Public */,

 // slots: parameters
    0x80000000 | 2,
    0x80000000 | 2,

       0        // eod
};

void TrackPanelGain::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        TrackPanelGain *_t = static_cast<TrackPanelGain *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: { Command* _r = _t->gain_increment();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 1: { Command* _r = _t->gain_decrement();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject TrackPanelGain::staticMetaObject = {
    { &ViewItem::staticMetaObject, qt_meta_stringdata_TrackPanelGain.data,
      qt_meta_data_TrackPanelGain,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *TrackPanelGain::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *TrackPanelGain::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_TrackPanelGain.stringdata0))
        return static_cast<void*>(this);
    return ViewItem::qt_metacast(_clname);
}

int TrackPanelGain::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ViewItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 2;
    }
    return _id;
}
struct qt_meta_stringdata_TrackPanelPan_t {
    QByteArrayData data[5];
    char stringdata0[43];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_TrackPanelPan_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_TrackPanelPan_t qt_meta_stringdata_TrackPanelPan = {
    {
QT_MOC_LITERAL(0, 0, 13), // "TrackPanelPan"
QT_MOC_LITERAL(1, 14, 8), // "pan_left"
QT_MOC_LITERAL(2, 23, 8), // "Command*"
QT_MOC_LITERAL(3, 32, 0), // ""
QT_MOC_LITERAL(4, 33, 9) // "pan_right"

    },
    "TrackPanelPan\0pan_left\0Command*\0\0"
    "pan_right"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_TrackPanelPan[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   24,    3, 0x0a /* Public */,
       4,    0,   25,    3, 0x0a /* Public */,

 // slots: parameters
    0x80000000 | 2,
    0x80000000 | 2,

       0        // eod
};

void TrackPanelPan::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        TrackPanelPan *_t = static_cast<TrackPanelPan *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: { Command* _r = _t->pan_left();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 1: { Command* _r = _t->pan_right();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject TrackPanelPan::staticMetaObject = {
    { &ViewItem::staticMetaObject, qt_meta_stringdata_TrackPanelPan.data,
      qt_meta_data_TrackPanelPan,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *TrackPanelPan::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *TrackPanelPan::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_TrackPanelPan.stringdata0))
        return static_cast<void*>(this);
    return ViewItem::qt_metacast(_clname);
}

int TrackPanelPan::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ViewItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 2;
    }
    return _id;
}
struct qt_meta_stringdata_TrackPanelLed_t {
    QByteArrayData data[6];
    char stringdata0[49];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_TrackPanelLed_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_TrackPanelLed_t qt_meta_stringdata_TrackPanelLed = {
    {
QT_MOC_LITERAL(0, 0, 13), // "TrackPanelLed"
QT_MOC_LITERAL(1, 14, 12), // "ison_changed"
QT_MOC_LITERAL(2, 27, 0), // ""
QT_MOC_LITERAL(3, 28, 4), // "isOn"
QT_MOC_LITERAL(4, 33, 6), // "toggle"
QT_MOC_LITERAL(5, 40, 8) // "Command*"

    },
    "TrackPanelLed\0ison_changed\0\0isOn\0"
    "toggle\0Command*"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_TrackPanelLed[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   24,    2, 0x0a /* Public */,
       4,    0,   27,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void, QMetaType::Bool,    3,
    0x80000000 | 5,

       0        // eod
};

void TrackPanelLed::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        TrackPanelLed *_t = static_cast<TrackPanelLed *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->ison_changed((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: { Command* _r = _t->toggle();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject TrackPanelLed::staticMetaObject = {
    { &ViewItem::staticMetaObject, qt_meta_stringdata_TrackPanelLed.data,
      qt_meta_data_TrackPanelLed,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *TrackPanelLed::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *TrackPanelLed::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_TrackPanelLed.stringdata0))
        return static_cast<void*>(this);
    return ViewItem::qt_metacast(_clname);
}

int TrackPanelLed::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ViewItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 2;
    }
    return _id;
}
struct qt_meta_stringdata_TrackPanelBus_t {
    QByteArrayData data[3];
    char stringdata0[27];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_TrackPanelBus_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_TrackPanelBus_t qt_meta_stringdata_TrackPanelBus = {
    {
QT_MOC_LITERAL(0, 0, 13), // "TrackPanelBus"
QT_MOC_LITERAL(1, 14, 11), // "bus_changed"
QT_MOC_LITERAL(2, 26, 0) // ""

    },
    "TrackPanelBus\0bus_changed\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_TrackPanelBus[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   19,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,

       0        // eod
};

void TrackPanelBus::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        TrackPanelBus *_t = static_cast<TrackPanelBus *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->bus_changed(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject TrackPanelBus::staticMetaObject = {
    { &ViewItem::staticMetaObject, qt_meta_stringdata_TrackPanelBus.data,
      qt_meta_data_TrackPanelBus,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *TrackPanelBus::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *TrackPanelBus::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_TrackPanelBus.stringdata0))
        return static_cast<void*>(this);
    return ViewItem::qt_metacast(_clname);
}

int TrackPanelBus::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ViewItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
    return _id;
}
struct qt_meta_stringdata_TrackPanelView_t {
    QByteArrayData data[5];
    char stringdata0[57];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_TrackPanelView_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_TrackPanelView_t qt_meta_stringdata_TrackPanelView = {
    {
QT_MOC_LITERAL(0, 0, 14), // "TrackPanelView"
QT_MOC_LITERAL(1, 15, 11), // "update_gain"
QT_MOC_LITERAL(2, 27, 0), // ""
QT_MOC_LITERAL(3, 28, 10), // "update_pan"
QT_MOC_LITERAL(4, 39, 17) // "update_track_name"

    },
    "TrackPanelView\0update_gain\0\0update_pan\0"
    "update_track_name"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_TrackPanelView[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   29,    2, 0x08 /* Private */,
       3,    0,   30,    2, 0x08 /* Private */,
       4,    0,   31,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void TrackPanelView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        TrackPanelView *_t = static_cast<TrackPanelView *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->update_gain(); break;
        case 1: _t->update_pan(); break;
        case 2: _t->update_track_name(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject TrackPanelView::staticMetaObject = {
    { &ViewItem::staticMetaObject, qt_meta_stringdata_TrackPanelView.data,
      qt_meta_data_TrackPanelView,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *TrackPanelView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *TrackPanelView::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_TrackPanelView.stringdata0))
        return static_cast<void*>(this);
    return ViewItem::qt_metacast(_clname);
}

int TrackPanelView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ViewItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 3;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
