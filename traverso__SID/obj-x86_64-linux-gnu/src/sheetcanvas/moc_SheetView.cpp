/****************************************************************************
** Meta object code from reading C++ file 'SheetView.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/sheetcanvas/SheetView.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'SheetView.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_SheetView_t {
    QByteArrayData data[50];
    char stringdata0[633];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SheetView_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SheetView_t qt_meta_stringdata_SheetView = {
    {
QT_MOC_LITERAL(0, 0, 9), // "SheetView"
QT_MOC_LITERAL(1, 10, 5), // "touch"
QT_MOC_LITERAL(2, 16, 3), // "Set"
QT_MOC_LITERAL(3, 20, 17), // "touch_play_cursor"
QT_MOC_LITERAL(4, 38, 6), // "center"
QT_MOC_LITERAL(5, 45, 11), // "Center View"
QT_MOC_LITERAL(6, 57, 12), // "scroll_right"
QT_MOC_LITERAL(7, 70, 5), // "Right"
QT_MOC_LITERAL(8, 76, 11), // "scroll_left"
QT_MOC_LITERAL(9, 88, 4), // "Left"
QT_MOC_LITERAL(10, 93, 9), // "scroll_up"
QT_MOC_LITERAL(11, 103, 2), // "Up"
QT_MOC_LITERAL(12, 106, 11), // "scroll_down"
QT_MOC_LITERAL(13, 118, 4), // "Down"
QT_MOC_LITERAL(14, 123, 7), // "shuttle"
QT_MOC_LITERAL(15, 131, 7), // "Shuttle"
QT_MOC_LITERAL(16, 139, 10), // "goto_begin"
QT_MOC_LITERAL(17, 150, 8), // "To start"
QT_MOC_LITERAL(18, 159, 8), // "goto_end"
QT_MOC_LITERAL(19, 168, 6), // "To end"
QT_MOC_LITERAL(20, 175, 13), // "play_to_begin"
QT_MOC_LITERAL(21, 189, 8), // "To Start"
QT_MOC_LITERAL(22, 198, 16), // "play_cursor_move"
QT_MOC_LITERAL(23, 215, 4), // "Move"
QT_MOC_LITERAL(24, 220, 16), // "work_cursor_move"
QT_MOC_LITERAL(25, 237, 10), // "add_marker"
QT_MOC_LITERAL(26, 248, 10), // "Add Marker"
QT_MOC_LITERAL(27, 259, 22), // "add_marker_at_playhead"
QT_MOC_LITERAL(28, 282, 22), // "Add Marker at Playhead"
QT_MOC_LITERAL(29, 305, 22), // "playhead_to_workcursor"
QT_MOC_LITERAL(30, 328, 13), // "To workcursor"
QT_MOC_LITERAL(31, 342, 15), // "center_playhead"
QT_MOC_LITERAL(32, 358, 6), // "Center"
QT_MOC_LITERAL(33, 365, 14), // "set_snap_range"
QT_MOC_LITERAL(34, 380, 0), // ""
QT_MOC_LITERAL(35, 381, 17), // "update_scrollbars"
QT_MOC_LITERAL(36, 399, 21), // "stop_follow_play_head"
QT_MOC_LITERAL(37, 421, 16), // "follow_play_head"
QT_MOC_LITERAL(38, 438, 16), // "set_follow_state"
QT_MOC_LITERAL(39, 455, 5), // "state"
QT_MOC_LITERAL(40, 461, 22), // "transport_position_set"
QT_MOC_LITERAL(41, 484, 8), // "Command*"
QT_MOC_LITERAL(42, 493, 20), // "scale_factor_changed"
QT_MOC_LITERAL(43, 514, 17), // "add_new_trackview"
QT_MOC_LITERAL(44, 532, 6), // "Track*"
QT_MOC_LITERAL(45, 539, 16), // "remove_trackview"
QT_MOC_LITERAL(46, 556, 14), // "update_shuttle"
QT_MOC_LITERAL(47, 571, 18), // "sheet_mode_changed"
QT_MOC_LITERAL(48, 590, 24), // "hscrollbar_value_changed"
QT_MOC_LITERAL(49, 615, 17) // "hscrollbar_action"

    },
    "SheetView\0touch\0Set\0touch_play_cursor\0"
    "center\0Center View\0scroll_right\0Right\0"
    "scroll_left\0Left\0scroll_up\0Up\0scroll_down\0"
    "Down\0shuttle\0Shuttle\0goto_begin\0"
    "To start\0goto_end\0To end\0play_to_begin\0"
    "To Start\0play_cursor_move\0Move\0"
    "work_cursor_move\0add_marker\0Add Marker\0"
    "add_marker_at_playhead\0Add Marker at Playhead\0"
    "playhead_to_workcursor\0To workcursor\0"
    "center_playhead\0Center\0set_snap_range\0"
    "\0update_scrollbars\0stop_follow_play_head\0"
    "follow_play_head\0set_follow_state\0"
    "state\0transport_position_set\0Command*\0"
    "scale_factor_changed\0add_new_trackview\0"
    "Track*\0remove_trackview\0update_shuttle\0"
    "sheet_mode_changed\0hscrollbar_value_changed\0"
    "hscrollbar_action"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SheetView[] = {

 // content:
       7,       // revision
       0,       // classname
      17,   14, // classinfo
      30,   48, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // classinfo: key, value
       1,    2,
       3,    2,
       4,    5,
       6,    7,
       8,    9,
      10,   11,
      12,   13,
      14,   15,
      16,   17,
      18,   19,
      20,   21,
      22,   23,
      24,   23,
      25,   26,
      27,   28,
      29,   30,
      31,   32,

 // slots: name, argc, parameters, tag, flags
      33,    1,  198,   34, 0x0a /* Public */,
      35,    0,  201,   34, 0x0a /* Public */,
      36,    0,  202,   34, 0x0a /* Public */,
      37,    0,  203,   34, 0x0a /* Public */,
      38,    1,  204,   34, 0x0a /* Public */,
      40,    0,  207,   34, 0x0a /* Public */,
       1,    0,  208,   34, 0x0a /* Public */,
       3,    0,  209,   34, 0x0a /* Public */,
       4,    0,  210,   34, 0x0a /* Public */,
       6,    0,  211,   34, 0x0a /* Public */,
       8,    0,  212,   34, 0x0a /* Public */,
      10,    0,  213,   34, 0x0a /* Public */,
      12,    0,  214,   34, 0x0a /* Public */,
      14,    0,  215,   34, 0x0a /* Public */,
      16,    0,  216,   34, 0x0a /* Public */,
      18,    0,  217,   34, 0x0a /* Public */,
      20,    0,  218,   34, 0x0a /* Public */,
      22,    0,  219,   34, 0x0a /* Public */,
      24,    0,  220,   34, 0x0a /* Public */,
      25,    0,  221,   34, 0x0a /* Public */,
      27,    0,  222,   34, 0x0a /* Public */,
      29,    0,  223,   34, 0x0a /* Public */,
      31,    0,  224,   34, 0x0a /* Public */,
      42,    0,  225,   34, 0x08 /* Private */,
      43,    1,  226,   34, 0x08 /* Private */,
      45,    1,  229,   34, 0x08 /* Private */,
      46,    0,  232,   34, 0x08 /* Private */,
      47,    0,  233,   34, 0x08 /* Private */,
      48,    1,  234,   34, 0x08 /* Private */,
      49,    1,  237,   34, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,   34,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   39,
    QMetaType::Void,
    0x80000000 | 41,
    0x80000000 | 41,
    0x80000000 | 41,
    0x80000000 | 41,
    0x80000000 | 41,
    0x80000000 | 41,
    0x80000000 | 41,
    0x80000000 | 41,
    0x80000000 | 41,
    0x80000000 | 41,
    0x80000000 | 41,
    0x80000000 | 41,
    0x80000000 | 41,
    0x80000000 | 41,
    0x80000000 | 41,
    0x80000000 | 41,
    0x80000000 | 41,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 44,   34,
    QMetaType::Void, 0x80000000 | 44,   34,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   34,
    QMetaType::Void, QMetaType::Int,   34,

       0        // eod
};

void SheetView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        SheetView *_t = static_cast<SheetView *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->set_snap_range((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->update_scrollbars(); break;
        case 2: _t->stop_follow_play_head(); break;
        case 3: _t->follow_play_head(); break;
        case 4: _t->set_follow_state((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 5: _t->transport_position_set(); break;
        case 6: { Command* _r = _t->touch();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 7: { Command* _r = _t->touch_play_cursor();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 8: { Command* _r = _t->center();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 9: { Command* _r = _t->scroll_right();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 10: { Command* _r = _t->scroll_left();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 11: { Command* _r = _t->scroll_up();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 12: { Command* _r = _t->scroll_down();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 13: { Command* _r = _t->shuttle();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 14: { Command* _r = _t->goto_begin();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 15: { Command* _r = _t->goto_end();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 16: { Command* _r = _t->play_to_begin();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 17: { Command* _r = _t->play_cursor_move();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 18: { Command* _r = _t->work_cursor_move();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 19: { Command* _r = _t->add_marker();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 20: { Command* _r = _t->add_marker_at_playhead();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 21: { Command* _r = _t->playhead_to_workcursor();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 22: { Command* _r = _t->center_playhead();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 23: _t->scale_factor_changed(); break;
        case 24: _t->add_new_trackview((*reinterpret_cast< Track*(*)>(_a[1]))); break;
        case 25: _t->remove_trackview((*reinterpret_cast< Track*(*)>(_a[1]))); break;
        case 26: _t->update_shuttle(); break;
        case 27: _t->sheet_mode_changed(); break;
        case 28: _t->hscrollbar_value_changed((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 29: _t->hscrollbar_action((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject SheetView::staticMetaObject = {
    { &ViewItem::staticMetaObject, qt_meta_stringdata_SheetView.data,
      qt_meta_data_SheetView,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *SheetView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SheetView::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_SheetView.stringdata0))
        return static_cast<void*>(this);
    return ViewItem::qt_metacast(_clname);
}

int SheetView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ViewItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 30)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 30;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 30)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 30;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
