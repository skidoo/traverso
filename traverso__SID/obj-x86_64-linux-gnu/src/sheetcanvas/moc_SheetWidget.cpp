/****************************************************************************
** Meta object code from reading C++ file 'SheetWidget.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/sheetcanvas/SheetWidget.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'SheetWidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_SheetPanelGain_t {
    QByteArrayData data[6];
    char stringdata0[67];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SheetPanelGain_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SheetPanelGain_t qt_meta_stringdata_SheetPanelGain = {
    {
QT_MOC_LITERAL(0, 0, 14), // "SheetPanelGain"
QT_MOC_LITERAL(1, 15, 14), // "gain_increment"
QT_MOC_LITERAL(2, 30, 8), // "Command*"
QT_MOC_LITERAL(3, 39, 0), // ""
QT_MOC_LITERAL(4, 40, 14), // "gain_decrement"
QT_MOC_LITERAL(5, 55, 11) // "update_gain"

    },
    "SheetPanelGain\0gain_increment\0Command*\0"
    "\0gain_decrement\0update_gain"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SheetPanelGain[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   29,    3, 0x0a /* Public */,
       4,    0,   30,    3, 0x0a /* Public */,
       5,    0,   31,    3, 0x08 /* Private */,

 // slots: parameters
    0x80000000 | 2,
    0x80000000 | 2,
    QMetaType::Void,

       0        // eod
};

void SheetPanelGain::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        SheetPanelGain *_t = static_cast<SheetPanelGain *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: { Command* _r = _t->gain_increment();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 1: { Command* _r = _t->gain_decrement();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 2: _t->update_gain(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject SheetPanelGain::staticMetaObject = {
    { &ViewItem::staticMetaObject, qt_meta_stringdata_SheetPanelGain.data,
      qt_meta_data_SheetPanelGain,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *SheetPanelGain::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SheetPanelGain::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_SheetPanelGain.stringdata0))
        return static_cast<void*>(this);
    return ViewItem::qt_metacast(_clname);
}

int SheetPanelGain::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ViewItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 3;
    }
    return _id;
}
struct qt_meta_stringdata_SheetPanelView_t {
    QByteArrayData data[1];
    char stringdata0[15];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SheetPanelView_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SheetPanelView_t qt_meta_stringdata_SheetPanelView = {
    {
QT_MOC_LITERAL(0, 0, 14) // "SheetPanelView"

    },
    "SheetPanelView"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SheetPanelView[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void SheetPanelView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject SheetPanelView::staticMetaObject = {
    { &ViewItem::staticMetaObject, qt_meta_stringdata_SheetPanelView.data,
      qt_meta_data_SheetPanelView,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *SheetPanelView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SheetPanelView::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_SheetPanelView.stringdata0))
        return static_cast<void*>(this);
    return ViewItem::qt_metacast(_clname);
}

int SheetPanelView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ViewItem::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_SheetWidget_t {
    QByteArrayData data[3];
    char stringdata0[29];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SheetWidget_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SheetWidget_t qt_meta_stringdata_SheetWidget = {
    {
QT_MOC_LITERAL(0, 0, 11), // "SheetWidget"
QT_MOC_LITERAL(1, 12, 15), // "load_theme_data"
QT_MOC_LITERAL(2, 28, 0) // ""

    },
    "SheetWidget\0load_theme_data\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SheetWidget[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   19,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,

       0        // eod
};

void SheetWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        SheetWidget *_t = static_cast<SheetWidget *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->load_theme_data(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject SheetWidget::staticMetaObject = {
    { &QFrame::staticMetaObject, qt_meta_stringdata_SheetWidget.data,
      qt_meta_data_SheetWidget,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *SheetWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SheetWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_SheetWidget.stringdata0))
        return static_cast<void*>(this);
    return QFrame::qt_metacast(_clname);
}

int SheetWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QFrame::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
