/****************************************************************************
** Meta object code from reading C++ file 'Cursors.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/sheetcanvas/Cursors.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Cursors.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_PlayHead_t {
    QByteArrayData data[10];
    char stringdata0[128];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PlayHead_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PlayHead_t qt_meta_stringdata_PlayHead = {
    {
QT_MOC_LITERAL(0, 0, 8), // "PlayHead"
QT_MOC_LITERAL(1, 9, 12), // "check_config"
QT_MOC_LITERAL(2, 22, 0), // ""
QT_MOC_LITERAL(3, 23, 10), // "play_start"
QT_MOC_LITERAL(4, 34, 9), // "play_stop"
QT_MOC_LITERAL(5, 44, 19), // "set_animation_value"
QT_MOC_LITERAL(6, 64, 18), // "animation_finished"
QT_MOC_LITERAL(7, 83, 15), // "update_position"
QT_MOC_LITERAL(8, 99, 13), // "enable_follow"
QT_MOC_LITERAL(9, 113, 14) // "disable_follow"

    },
    "PlayHead\0check_config\0\0play_start\0"
    "play_stop\0set_animation_value\0"
    "animation_finished\0update_position\0"
    "enable_follow\0disable_follow"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PlayHead[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   54,    2, 0x08 /* Private */,
       3,    0,   55,    2, 0x08 /* Private */,
       4,    0,   56,    2, 0x08 /* Private */,
       5,    1,   57,    2, 0x08 /* Private */,
       6,    0,   60,    2, 0x08 /* Private */,
       7,    0,   61,    2, 0x0a /* Public */,
       8,    0,   62,    2, 0x0a /* Public */,
       9,    0,   63,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void PlayHead::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        PlayHead *_t = static_cast<PlayHead *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->check_config(); break;
        case 1: _t->play_start(); break;
        case 2: _t->play_stop(); break;
        case 3: _t->set_animation_value((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->animation_finished(); break;
        case 5: _t->update_position(); break;
        case 6: _t->enable_follow(); break;
        case 7: _t->disable_follow(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject PlayHead::staticMetaObject = {
    { &ViewItem::staticMetaObject, qt_meta_stringdata_PlayHead.data,
      qt_meta_data_PlayHead,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *PlayHead::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PlayHead::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PlayHead.stringdata0))
        return static_cast<void*>(this);
    return ViewItem::qt_metacast(_clname);
}

int PlayHead::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ViewItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 8;
    }
    return _id;
}
struct qt_meta_stringdata_WorkCursor_t {
    QByteArrayData data[3];
    char stringdata0[28];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_WorkCursor_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_WorkCursor_t qt_meta_stringdata_WorkCursor = {
    {
QT_MOC_LITERAL(0, 0, 10), // "WorkCursor"
QT_MOC_LITERAL(1, 11, 15), // "update_position"
QT_MOC_LITERAL(2, 27, 0) // ""

    },
    "WorkCursor\0update_position\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_WorkCursor[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   19,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,

       0        // eod
};

void WorkCursor::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        WorkCursor *_t = static_cast<WorkCursor *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->update_position(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject WorkCursor::staticMetaObject = {
    { &ViewItem::staticMetaObject, qt_meta_stringdata_WorkCursor.data,
      qt_meta_data_WorkCursor,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *WorkCursor::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *WorkCursor::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_WorkCursor.stringdata0))
        return static_cast<void*>(this);
    return ViewItem::qt_metacast(_clname);
}

int WorkCursor::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ViewItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
