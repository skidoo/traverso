/****************************************************************************
** Meta object code from reading C++ file 'TrackView.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/sheetcanvas/TrackView.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TrackView.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_TrackView_t {
    QByteArrayData data[15];
    char stringdata0[193];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_TrackView_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_TrackView_t qt_meta_stringdata_TrackView = {
    {
QT_MOC_LITERAL(0, 0, 9), // "TrackView"
QT_MOC_LITERAL(1, 10, 15), // "edit_properties"
QT_MOC_LITERAL(2, 26, 15), // "Edit properties"
QT_MOC_LITERAL(3, 42, 14), // "add_new_plugin"
QT_MOC_LITERAL(4, 57, 14), // "Add new Plugin"
QT_MOC_LITERAL(5, 72, 10), // "select_bus"
QT_MOC_LITERAL(6, 83, 10), // "Select Bus"
QT_MOC_LITERAL(7, 94, 14), // "insert_silence"
QT_MOC_LITERAL(8, 109, 14), // "Insert Silence"
QT_MOC_LITERAL(9, 124, 8), // "Command*"
QT_MOC_LITERAL(10, 133, 0), // ""
QT_MOC_LITERAL(11, 134, 21), // "add_new_audioclipview"
QT_MOC_LITERAL(12, 156, 10), // "AudioClip*"
QT_MOC_LITERAL(13, 167, 4), // "clip"
QT_MOC_LITERAL(14, 172, 20) // "remove_audioclipview"

    },
    "TrackView\0edit_properties\0Edit properties\0"
    "add_new_plugin\0Add new Plugin\0select_bus\0"
    "Select Bus\0insert_silence\0Insert Silence\0"
    "Command*\0\0add_new_audioclipview\0"
    "AudioClip*\0clip\0remove_audioclipview"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_TrackView[] = {

 // content:
       7,       // revision
       0,       // classname
       4,   14, // classinfo
       6,   22, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // classinfo: key, value
       1,    2,
       3,    4,
       5,    6,
       7,    8,

 // slots: name, argc, parameters, tag, flags
       1,    0,   52,   10, 0x0a /* Public */,
       3,    0,   53,   10, 0x0a /* Public */,
       5,    0,   54,   10, 0x0a /* Public */,
       7,    0,   55,   10, 0x0a /* Public */,
      11,    1,   56,   10, 0x08 /* Private */,
      14,    1,   59,   10, 0x08 /* Private */,

 // slots: parameters
    0x80000000 | 9,
    0x80000000 | 9,
    0x80000000 | 9,
    0x80000000 | 9,
    QMetaType::Void, 0x80000000 | 12,   13,
    QMetaType::Void, 0x80000000 | 12,   13,

       0        // eod
};

void TrackView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        TrackView *_t = static_cast<TrackView *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: { Command* _r = _t->edit_properties();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 1: { Command* _r = _t->add_new_plugin();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 2: { Command* _r = _t->select_bus();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 3: { Command* _r = _t->insert_silence();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 4: _t->add_new_audioclipview((*reinterpret_cast< AudioClip*(*)>(_a[1]))); break;
        case 5: _t->remove_audioclipview((*reinterpret_cast< AudioClip*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject TrackView::staticMetaObject = {
    { &ViewItem::staticMetaObject, qt_meta_stringdata_TrackView.data,
      qt_meta_data_TrackView,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *TrackView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *TrackView::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_TrackView.stringdata0))
        return static_cast<void*>(this);
    return ViewItem::qt_metacast(_clname);
}

int TrackView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ViewItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 6;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
