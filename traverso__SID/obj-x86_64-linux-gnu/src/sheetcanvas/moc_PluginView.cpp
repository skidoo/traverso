/****************************************************************************
** Meta object code from reading C++ file 'PluginView.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/sheetcanvas/PluginView.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'PluginView.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_PluginView_t {
    QByteArrayData data[8];
    char stringdata0[74];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PluginView_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PluginView_t qt_meta_stringdata_PluginView = {
    {
QT_MOC_LITERAL(0, 0, 10), // "PluginView"
QT_MOC_LITERAL(1, 11, 15), // "edit_properties"
QT_MOC_LITERAL(2, 27, 7), // "Edit..."
QT_MOC_LITERAL(3, 35, 13), // "remove_plugin"
QT_MOC_LITERAL(4, 49, 6), // "Remove"
QT_MOC_LITERAL(5, 56, 8), // "Command*"
QT_MOC_LITERAL(6, 65, 0), // ""
QT_MOC_LITERAL(7, 66, 7) // "repaint"

    },
    "PluginView\0edit_properties\0Edit...\0"
    "remove_plugin\0Remove\0Command*\0\0repaint"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PluginView[] = {

 // content:
       7,       // revision
       0,       // classname
       2,   14, // classinfo
       3,   18, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // classinfo: key, value
       1,    2,
       3,    4,

 // slots: name, argc, parameters, tag, flags
       1,    0,   33,    6, 0x0a /* Public */,
       3,    0,   34,    6, 0x0a /* Public */,
       7,    0,   35,    6, 0x08 /* Private */,

 // slots: parameters
    0x80000000 | 5,
    0x80000000 | 5,
    QMetaType::Void,

       0        // eod
};

void PluginView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        PluginView *_t = static_cast<PluginView *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: { Command* _r = _t->edit_properties();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 1: { Command* _r = _t->remove_plugin();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 2: _t->repaint(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject PluginView::staticMetaObject = {
    { &ViewItem::staticMetaObject, qt_meta_stringdata_PluginView.data,
      qt_meta_data_PluginView,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *PluginView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PluginView::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PluginView.stringdata0))
        return static_cast<void*>(this);
    return ViewItem::qt_metacast(_clname);
}

int PluginView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ViewItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 3;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
