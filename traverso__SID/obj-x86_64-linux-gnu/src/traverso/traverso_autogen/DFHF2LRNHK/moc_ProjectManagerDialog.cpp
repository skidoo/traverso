/****************************************************************************
** Meta object code from reading C++ file 'ProjectManagerDialog.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../src/traverso/dialogs/project/ProjectManagerDialog.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'ProjectManagerDialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_ProjectManagerDialog_t {
    QByteArrayData data[21];
    char stringdata0[357];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ProjectManagerDialog_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ProjectManagerDialog_t qt_meta_stringdata_ProjectManagerDialog = {
    {
QT_MOC_LITERAL(0, 0, 20), // "ProjectManagerDialog"
QT_MOC_LITERAL(1, 21, 17), // "update_sheet_list"
QT_MOC_LITERAL(2, 39, 0), // ""
QT_MOC_LITERAL(3, 40, 11), // "set_project"
QT_MOC_LITERAL(4, 52, 8), // "Project*"
QT_MOC_LITERAL(5, 61, 7), // "project"
QT_MOC_LITERAL(6, 69, 17), // "sheetitem_clicked"
QT_MOC_LITERAL(7, 87, 16), // "QTreeWidgetItem*"
QT_MOC_LITERAL(8, 104, 4), // "item"
QT_MOC_LITERAL(9, 109, 28), // "on_renameSheetButton_clicked"
QT_MOC_LITERAL(10, 138, 28), // "on_deleteSheetButton_clicked"
QT_MOC_LITERAL(11, 167, 28), // "on_createSheetButton_clicked"
QT_MOC_LITERAL(12, 196, 17), // "redo_text_changed"
QT_MOC_LITERAL(13, 214, 4), // "text"
QT_MOC_LITERAL(14, 219, 17), // "undo_text_changed"
QT_MOC_LITERAL(15, 237, 21), // "on_undoButton_clicked"
QT_MOC_LITERAL(16, 259, 21), // "on_redoButton_clicked"
QT_MOC_LITERAL(17, 281, 29), // "on_sheetsExportButton_clicked"
QT_MOC_LITERAL(18, 311, 31), // "on_exportTemplateButton_clicked"
QT_MOC_LITERAL(19, 343, 6), // "accept"
QT_MOC_LITERAL(20, 350, 6) // "reject"

    },
    "ProjectManagerDialog\0update_sheet_list\0"
    "\0set_project\0Project*\0project\0"
    "sheetitem_clicked\0QTreeWidgetItem*\0"
    "item\0on_renameSheetButton_clicked\0"
    "on_deleteSheetButton_clicked\0"
    "on_createSheetButton_clicked\0"
    "redo_text_changed\0text\0undo_text_changed\0"
    "on_undoButton_clicked\0on_redoButton_clicked\0"
    "on_sheetsExportButton_clicked\0"
    "on_exportTemplateButton_clicked\0accept\0"
    "reject"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ProjectManagerDialog[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      14,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   84,    2, 0x08 /* Private */,
       3,    1,   85,    2, 0x08 /* Private */,
       6,    2,   88,    2, 0x08 /* Private */,
       9,    0,   93,    2, 0x08 /* Private */,
      10,    0,   94,    2, 0x08 /* Private */,
      11,    0,   95,    2, 0x08 /* Private */,
      12,    1,   96,    2, 0x08 /* Private */,
      14,    1,   99,    2, 0x08 /* Private */,
      15,    0,  102,    2, 0x08 /* Private */,
      16,    0,  103,    2, 0x08 /* Private */,
      17,    0,  104,    2, 0x08 /* Private */,
      18,    0,  105,    2, 0x08 /* Private */,
      19,    0,  106,    2, 0x08 /* Private */,
      20,    0,  107,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4,    5,
    QMetaType::Void, 0x80000000 | 7, QMetaType::Int,    8,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   13,
    QMetaType::Void, QMetaType::QString,   13,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void ProjectManagerDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        ProjectManagerDialog *_t = static_cast<ProjectManagerDialog *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->update_sheet_list(); break;
        case 1: _t->set_project((*reinterpret_cast< Project*(*)>(_a[1]))); break;
        case 2: _t->sheetitem_clicked((*reinterpret_cast< QTreeWidgetItem*(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 3: _t->on_renameSheetButton_clicked(); break;
        case 4: _t->on_deleteSheetButton_clicked(); break;
        case 5: _t->on_createSheetButton_clicked(); break;
        case 6: _t->redo_text_changed((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 7: _t->undo_text_changed((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 8: _t->on_undoButton_clicked(); break;
        case 9: _t->on_redoButton_clicked(); break;
        case 10: _t->on_sheetsExportButton_clicked(); break;
        case 11: _t->on_exportTemplateButton_clicked(); break;
        case 12: _t->accept(); break;
        case 13: _t->reject(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject ProjectManagerDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_ProjectManagerDialog.data,
      qt_meta_data_ProjectManagerDialog,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *ProjectManagerDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ProjectManagerDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_ProjectManagerDialog.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "Ui::ProjectManagerDialog"))
        return static_cast< Ui::ProjectManagerDialog*>(this);
    return QDialog::qt_metacast(_clname);
}

int ProjectManagerDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 14)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 14)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 14;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
