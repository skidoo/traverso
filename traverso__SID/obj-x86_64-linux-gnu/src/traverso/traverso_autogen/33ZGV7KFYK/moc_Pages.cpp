/****************************************************************************
** Meta object code from reading C++ file 'Pages.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../src/traverso/dialogs/settings/Pages.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Pages.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_ConfigPage_t {
    QByteArrayData data[1];
    char stringdata0[11];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ConfigPage_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ConfigPage_t qt_meta_stringdata_ConfigPage = {
    {
QT_MOC_LITERAL(0, 0, 10) // "ConfigPage"

    },
    "ConfigPage"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ConfigPage[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void ConfigPage::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject ConfigPage::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_ConfigPage.data,
      qt_meta_data_ConfigPage,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *ConfigPage::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ConfigPage::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_ConfigPage.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int ConfigPage::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_AudioDriverConfigPage_t {
    QByteArrayData data[6];
    char stringdata0[135];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_AudioDriverConfigPage_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_AudioDriverConfigPage_t qt_meta_stringdata_AudioDriverConfigPage = {
    {
QT_MOC_LITERAL(0, 0, 21), // "AudioDriverConfigPage"
QT_MOC_LITERAL(1, 22, 23), // "update_latency_combobox"
QT_MOC_LITERAL(2, 46, 0), // ""
QT_MOC_LITERAL(3, 47, 27), // "rate_combobox_index_changed"
QT_MOC_LITERAL(4, 75, 29), // "driver_combobox_index_changed"
QT_MOC_LITERAL(5, 105, 29) // "restart_driver_button_clicked"

    },
    "AudioDriverConfigPage\0update_latency_combobox\0"
    "\0rate_combobox_index_changed\0"
    "driver_combobox_index_changed\0"
    "restart_driver_button_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_AudioDriverConfigPage[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   34,    2, 0x08 /* Private */,
       3,    1,   35,    2, 0x08 /* Private */,
       4,    1,   38,    2, 0x08 /* Private */,
       5,    0,   41,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void,

       0        // eod
};

void AudioDriverConfigPage::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        AudioDriverConfigPage *_t = static_cast<AudioDriverConfigPage *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->update_latency_combobox(); break;
        case 1: _t->rate_combobox_index_changed((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->driver_combobox_index_changed((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 3: _t->restart_driver_button_clicked(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject AudioDriverConfigPage::staticMetaObject = {
    { &ConfigPage::staticMetaObject, qt_meta_stringdata_AudioDriverConfigPage.data,
      qt_meta_data_AudioDriverConfigPage,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *AudioDriverConfigPage::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *AudioDriverConfigPage::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_AudioDriverConfigPage.stringdata0))
        return static_cast<void*>(this);
    return ConfigPage::qt_metacast(_clname);
}

int AudioDriverConfigPage::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ConfigPage::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 4)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 4;
    }
    return _id;
}
struct qt_meta_stringdata_AppearenceConfigPage_t {
    QByteArrayData data[10];
    char stringdata0[187];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_AppearenceConfigPage_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_AppearenceConfigPage_t qt_meta_stringdata_AppearenceConfigPage = {
    {
QT_MOC_LITERAL(0, 0, 20), // "AppearenceConfigPage"
QT_MOC_LITERAL(1, 21, 24), // "dirselect_button_clicked"
QT_MOC_LITERAL(2, 46, 0), // ""
QT_MOC_LITERAL(3, 47, 19), // "style_index_changed"
QT_MOC_LITERAL(4, 67, 4), // "text"
QT_MOC_LITERAL(5, 72, 19), // "theme_index_changed"
QT_MOC_LITERAL(6, 92, 5), // "theme"
QT_MOC_LITERAL(7, 98, 43), // "use_selected_styles_pallet_ch..."
QT_MOC_LITERAL(8, 142, 23), // "color_adjustbox_changed"
QT_MOC_LITERAL(9, 166, 20) // "theme_option_changed"

    },
    "AppearenceConfigPage\0dirselect_button_clicked\0"
    "\0style_index_changed\0text\0theme_index_changed\0"
    "theme\0use_selected_styles_pallet_checkbox_toggled\0"
    "color_adjustbox_changed\0theme_option_changed"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_AppearenceConfigPage[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   44,    2, 0x08 /* Private */,
       3,    1,   45,    2, 0x08 /* Private */,
       5,    1,   48,    2, 0x08 /* Private */,
       7,    1,   51,    2, 0x08 /* Private */,
       8,    1,   54,    2, 0x08 /* Private */,
       9,    0,   57,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    4,
    QMetaType::Void, QMetaType::QString,    6,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void,

       0        // eod
};

void AppearenceConfigPage::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        AppearenceConfigPage *_t = static_cast<AppearenceConfigPage *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->dirselect_button_clicked(); break;
        case 1: _t->style_index_changed((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 2: _t->theme_index_changed((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 3: _t->use_selected_styles_pallet_checkbox_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 4: _t->color_adjustbox_changed((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->theme_option_changed(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject AppearenceConfigPage::staticMetaObject = {
    { &ConfigPage::staticMetaObject, qt_meta_stringdata_AppearenceConfigPage.data,
      qt_meta_data_AppearenceConfigPage,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *AppearenceConfigPage::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *AppearenceConfigPage::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_AppearenceConfigPage.stringdata0))
        return static_cast<void*>(this);
    return ConfigPage::qt_metacast(_clname);
}

int AppearenceConfigPage::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ConfigPage::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 6;
    }
    return _id;
}
struct qt_meta_stringdata_BehaviorConfigPage_t {
    QByteArrayData data[3];
    char stringdata0[34];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_BehaviorConfigPage_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_BehaviorConfigPage_t qt_meta_stringdata_BehaviorConfigPage = {
    {
QT_MOC_LITERAL(0, 0, 18), // "BehaviorConfigPage"
QT_MOC_LITERAL(1, 19, 13), // "update_follow"
QT_MOC_LITERAL(2, 33, 0) // ""

    },
    "BehaviorConfigPage\0update_follow\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_BehaviorConfigPage[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   19,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,

       0        // eod
};

void BehaviorConfigPage::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        BehaviorConfigPage *_t = static_cast<BehaviorConfigPage *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->update_follow(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject BehaviorConfigPage::staticMetaObject = {
    { &ConfigPage::staticMetaObject, qt_meta_stringdata_BehaviorConfigPage.data,
      qt_meta_data_BehaviorConfigPage,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *BehaviorConfigPage::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *BehaviorConfigPage::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_BehaviorConfigPage.stringdata0))
        return static_cast<void*>(this);
    return ConfigPage::qt_metacast(_clname);
}

int BehaviorConfigPage::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ConfigPage::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
    return _id;
}
struct qt_meta_stringdata_KeyboardConfigPage_t {
    QByteArrayData data[7];
    char stringdata0[115];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_KeyboardConfigPage_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_KeyboardConfigPage_t qt_meta_stringdata_KeyboardConfigPage = {
    {
QT_MOC_LITERAL(0, 0, 18), // "KeyboardConfigPage"
QT_MOC_LITERAL(1, 19, 20), // "keymap_index_changed"
QT_MOC_LITERAL(2, 40, 0), // ""
QT_MOC_LITERAL(3, 41, 6), // "keymap"
QT_MOC_LITERAL(4, 48, 19), // "update_keymap_combo"
QT_MOC_LITERAL(5, 68, 23), // "on_exportButton_clicked"
QT_MOC_LITERAL(6, 92, 22) // "on_printButton_clicked"

    },
    "KeyboardConfigPage\0keymap_index_changed\0"
    "\0keymap\0update_keymap_combo\0"
    "on_exportButton_clicked\0on_printButton_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_KeyboardConfigPage[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   34,    2, 0x08 /* Private */,
       4,    0,   37,    2, 0x08 /* Private */,
       5,    0,   38,    2, 0x08 /* Private */,
       6,    0,   39,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void KeyboardConfigPage::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        KeyboardConfigPage *_t = static_cast<KeyboardConfigPage *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->keymap_index_changed((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: _t->update_keymap_combo(); break;
        case 2: _t->on_exportButton_clicked(); break;
        case 3: _t->on_printButton_clicked(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject KeyboardConfigPage::staticMetaObject = {
    { &ConfigPage::staticMetaObject, qt_meta_stringdata_KeyboardConfigPage.data,
      qt_meta_data_KeyboardConfigPage,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *KeyboardConfigPage::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *KeyboardConfigPage::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_KeyboardConfigPage.stringdata0))
        return static_cast<void*>(this);
    return ConfigPage::qt_metacast(_clname);
}

int KeyboardConfigPage::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ConfigPage::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 4)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 4;
    }
    return _id;
}
struct qt_meta_stringdata_RecordingConfigPage_t {
    QByteArrayData data[6];
    char stringdata0[97];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_RecordingConfigPage_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_RecordingConfigPage_t qt_meta_stringdata_RecordingConfigPage = {
    {
QT_MOC_LITERAL(0, 0, 19), // "RecordingConfigPage"
QT_MOC_LITERAL(1, 20, 22), // "encoding_index_changed"
QT_MOC_LITERAL(2, 43, 0), // ""
QT_MOC_LITERAL(3, 44, 5), // "index"
QT_MOC_LITERAL(4, 50, 40), // "use_onthefly_resampling_check..."
QT_MOC_LITERAL(5, 91, 5) // "state"

    },
    "RecordingConfigPage\0encoding_index_changed\0"
    "\0index\0use_onthefly_resampling_checkbox_changed\0"
    "state"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_RecordingConfigPage[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   24,    2, 0x08 /* Private */,
       4,    1,   27,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    5,

       0        // eod
};

void RecordingConfigPage::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        RecordingConfigPage *_t = static_cast<RecordingConfigPage *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->encoding_index_changed((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->use_onthefly_resampling_checkbox_changed((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject RecordingConfigPage::staticMetaObject = {
    { &ConfigPage::staticMetaObject, qt_meta_stringdata_RecordingConfigPage.data,
      qt_meta_data_RecordingConfigPage,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *RecordingConfigPage::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *RecordingConfigPage::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_RecordingConfigPage.stringdata0))
        return static_cast<void*>(this);
    return ConfigPage::qt_metacast(_clname);
}

int RecordingConfigPage::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ConfigPage::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 2;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
