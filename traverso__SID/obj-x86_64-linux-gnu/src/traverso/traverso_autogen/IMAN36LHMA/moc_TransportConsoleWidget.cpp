/****************************************************************************
** Meta object code from reading C++ file 'TransportConsoleWidget.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../src/traverso/widgets/TransportConsoleWidget.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TransportConsoleWidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_TransportConsoleWidget_t {
    QByteArrayData data[17];
    char stringdata0[206];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_TransportConsoleWidget_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_TransportConsoleWidget_t qt_meta_stringdata_TransportConsoleWidget = {
    {
QT_MOC_LITERAL(0, 0, 22), // "TransportConsoleWidget"
QT_MOC_LITERAL(1, 23, 11), // "set_project"
QT_MOC_LITERAL(2, 35, 0), // ""
QT_MOC_LITERAL(3, 36, 8), // "Project*"
QT_MOC_LITERAL(4, 45, 9), // "set_sheet"
QT_MOC_LITERAL(5, 55, 6), // "Sheet*"
QT_MOC_LITERAL(6, 62, 8), // "to_start"
QT_MOC_LITERAL(7, 71, 7), // "to_left"
QT_MOC_LITERAL(8, 79, 11), // "rec_toggled"
QT_MOC_LITERAL(9, 91, 12), // "play_toggled"
QT_MOC_LITERAL(10, 104, 6), // "to_end"
QT_MOC_LITERAL(11, 111, 8), // "to_right"
QT_MOC_LITERAL(12, 120, 17), // "transport_started"
QT_MOC_LITERAL(13, 138, 17), // "transport_stopped"
QT_MOC_LITERAL(14, 156, 22), // "update_recording_state"
QT_MOC_LITERAL(15, 179, 12), // "update_label"
QT_MOC_LITERAL(16, 192, 13) // "update_layout"

    },
    "TransportConsoleWidget\0set_project\0\0"
    "Project*\0set_sheet\0Sheet*\0to_start\0"
    "to_left\0rec_toggled\0play_toggled\0"
    "to_end\0to_right\0transport_started\0"
    "transport_stopped\0update_recording_state\0"
    "update_label\0update_layout"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_TransportConsoleWidget[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   79,    2, 0x09 /* Protected */,
       4,    1,   82,    2, 0x09 /* Protected */,
       6,    0,   85,    2, 0x08 /* Private */,
       7,    0,   86,    2, 0x08 /* Private */,
       8,    0,   87,    2, 0x08 /* Private */,
       9,    0,   88,    2, 0x08 /* Private */,
      10,    0,   89,    2, 0x08 /* Private */,
      11,    0,   90,    2, 0x08 /* Private */,
      12,    0,   91,    2, 0x08 /* Private */,
      13,    0,   92,    2, 0x08 /* Private */,
      14,    0,   93,    2, 0x08 /* Private */,
      15,    0,   94,    2, 0x08 /* Private */,
      16,    0,   95,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    2,
    QMetaType::Void, 0x80000000 | 5,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void TransportConsoleWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        TransportConsoleWidget *_t = static_cast<TransportConsoleWidget *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->set_project((*reinterpret_cast< Project*(*)>(_a[1]))); break;
        case 1: _t->set_sheet((*reinterpret_cast< Sheet*(*)>(_a[1]))); break;
        case 2: _t->to_start(); break;
        case 3: _t->to_left(); break;
        case 4: _t->rec_toggled(); break;
        case 5: _t->play_toggled(); break;
        case 6: _t->to_end(); break;
        case 7: _t->to_right(); break;
        case 8: _t->transport_started(); break;
        case 9: _t->transport_stopped(); break;
        case 10: _t->update_recording_state(); break;
        case 11: _t->update_label(); break;
        case 12: _t->update_layout(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject TransportConsoleWidget::staticMetaObject = {
    { &QToolBar::staticMetaObject, qt_meta_stringdata_TransportConsoleWidget.data,
      qt_meta_data_TransportConsoleWidget,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *TransportConsoleWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *TransportConsoleWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_TransportConsoleWidget.stringdata0))
        return static_cast<void*>(this);
    return QToolBar::qt_metacast(_clname);
}

int TransportConsoleWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QToolBar::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 13)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 13;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
