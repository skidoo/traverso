/****************************************************************************
** Meta object code from reading C++ file 'SpectralMeterWidget.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../src/traverso/widgets/SpectralMeterWidget.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'SpectralMeterWidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_SpectralMeterConfigWidget_t {
    QByteArrayData data[6];
    char stringdata0[110];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SpectralMeterConfigWidget_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SpectralMeterConfigWidget_t qt_meta_stringdata_SpectralMeterConfigWidget = {
    {
QT_MOC_LITERAL(0, 0, 25), // "SpectralMeterConfigWidget"
QT_MOC_LITERAL(1, 26, 13), // "configChanged"
QT_MOC_LITERAL(2, 40, 0), // ""
QT_MOC_LITERAL(3, 41, 22), // "on_buttonClose_clicked"
QT_MOC_LITERAL(4, 64, 22), // "on_buttonApply_clicked"
QT_MOC_LITERAL(5, 87, 22) // "advancedButton_toggled"

    },
    "SpectralMeterConfigWidget\0configChanged\0"
    "\0on_buttonClose_clicked\0on_buttonApply_clicked\0"
    "advancedButton_toggled"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SpectralMeterConfigWidget[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   34,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       3,    0,   35,    2, 0x08 /* Private */,
       4,    0,   36,    2, 0x08 /* Private */,
       5,    1,   37,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    2,

       0        // eod
};

void SpectralMeterConfigWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        SpectralMeterConfigWidget *_t = static_cast<SpectralMeterConfigWidget *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->configChanged(); break;
        case 1: _t->on_buttonClose_clicked(); break;
        case 2: _t->on_buttonApply_clicked(); break;
        case 3: _t->advancedButton_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (SpectralMeterConfigWidget::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&SpectralMeterConfigWidget::configChanged)) {
                *result = 0;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject SpectralMeterConfigWidget::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_SpectralMeterConfigWidget.data,
      qt_meta_data_SpectralMeterConfigWidget,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *SpectralMeterConfigWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SpectralMeterConfigWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_SpectralMeterConfigWidget.stringdata0))
        return static_cast<void*>(this);
    return QDialog::qt_metacast(_clname);
}

int SpectralMeterConfigWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 4)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 4;
    }
    return _id;
}

// SIGNAL 0
void SpectralMeterConfigWidget::configChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}
struct qt_meta_stringdata_SpectralMeterView_t {
    QByteArrayData data[18];
    char stringdata0[238];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SpectralMeterView_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SpectralMeterView_t qt_meta_stringdata_SpectralMeterView = {
    {
QT_MOC_LITERAL(0, 0, 17), // "SpectralMeterView"
QT_MOC_LITERAL(1, 18, 15), // "edit_properties"
QT_MOC_LITERAL(2, 34, 11), // "Settings..."
QT_MOC_LITERAL(3, 46, 8), // "set_mode"
QT_MOC_LITERAL(4, 55, 20), // "Toggle avarage curve"
QT_MOC_LITERAL(5, 76, 5), // "reset"
QT_MOC_LITERAL(6, 82, 19), // "Reset average curve"
QT_MOC_LITERAL(7, 102, 20), // "export_avarage_curve"
QT_MOC_LITERAL(8, 123, 20), // "Export avarage curve"
QT_MOC_LITERAL(9, 144, 14), // "screen_capture"
QT_MOC_LITERAL(10, 159, 14), // "Capture Screen"
QT_MOC_LITERAL(11, 174, 11), // "update_data"
QT_MOC_LITERAL(12, 186, 0), // ""
QT_MOC_LITERAL(13, 187, 18), // "load_configuration"
QT_MOC_LITERAL(14, 206, 9), // "set_sheet"
QT_MOC_LITERAL(15, 216, 6), // "Sheet*"
QT_MOC_LITERAL(16, 223, 5), // "sheet"
QT_MOC_LITERAL(17, 229, 8) // "Command*"

    },
    "SpectralMeterView\0edit_properties\0"
    "Settings...\0set_mode\0Toggle avarage curve\0"
    "reset\0Reset average curve\0"
    "export_avarage_curve\0Export avarage curve\0"
    "screen_capture\0Capture Screen\0update_data\0"
    "\0load_configuration\0set_sheet\0Sheet*\0"
    "sheet\0Command*"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SpectralMeterView[] = {

 // content:
       7,       // revision
       0,       // classname
       5,   14, // classinfo
       8,   24, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // classinfo: key, value
       1,    2,
       3,    4,
       5,    6,
       7,    8,
       9,   10,

 // slots: name, argc, parameters, tag, flags
      11,    0,   64,   12, 0x08 /* Private */,
      13,    0,   65,   12, 0x0a /* Public */,
      14,    1,   66,   12, 0x0a /* Public */,
       1,    0,   69,   12, 0x0a /* Public */,
       3,    0,   70,   12, 0x0a /* Public */,
       5,    0,   71,   12, 0x0a /* Public */,
       7,    0,   72,   12, 0x0a /* Public */,
       9,    0,   73,   12, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 15,   16,
    0x80000000 | 17,
    0x80000000 | 17,
    0x80000000 | 17,
    0x80000000 | 17,
    0x80000000 | 17,

       0        // eod
};

void SpectralMeterView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        SpectralMeterView *_t = static_cast<SpectralMeterView *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->update_data(); break;
        case 1: _t->load_configuration(); break;
        case 2: _t->set_sheet((*reinterpret_cast< Sheet*(*)>(_a[1]))); break;
        case 3: { Command* _r = _t->edit_properties();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 4: { Command* _r = _t->set_mode();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 5: { Command* _r = _t->reset();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 6: { Command* _r = _t->export_avarage_curve();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 7: { Command* _r = _t->screen_capture();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject SpectralMeterView::staticMetaObject = {
    { &MeterView::staticMetaObject, qt_meta_stringdata_SpectralMeterView.data,
      qt_meta_data_SpectralMeterView,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *SpectralMeterView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SpectralMeterView::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_SpectralMeterView.stringdata0))
        return static_cast<void*>(this);
    return MeterView::qt_metacast(_clname);
}

int SpectralMeterView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = MeterView::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 8;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
