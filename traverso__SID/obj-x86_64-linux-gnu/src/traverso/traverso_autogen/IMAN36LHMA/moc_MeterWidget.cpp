/****************************************************************************
** Meta object code from reading C++ file 'MeterWidget.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../src/traverso/widgets/MeterWidget.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'MeterWidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MeterView_t {
    QByteArrayData data[8];
    char stringdata0[94];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MeterView_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MeterView_t qt_meta_stringdata_MeterView = {
    {
QT_MOC_LITERAL(0, 0, 9), // "MeterView"
QT_MOC_LITERAL(1, 10, 11), // "set_project"
QT_MOC_LITERAL(2, 22, 0), // ""
QT_MOC_LITERAL(3, 23, 8), // "Project*"
QT_MOC_LITERAL(4, 32, 11), // "update_data"
QT_MOC_LITERAL(5, 44, 17), // "transport_started"
QT_MOC_LITERAL(6, 62, 17), // "transport_stopped"
QT_MOC_LITERAL(7, 80, 13) // "delay_timeout"

    },
    "MeterView\0set_project\0\0Project*\0"
    "update_data\0transport_started\0"
    "transport_stopped\0delay_timeout"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MeterView[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   39,    2, 0x08 /* Private */,
       4,    0,   42,    2, 0x08 /* Private */,
       5,    0,   43,    2, 0x08 /* Private */,
       6,    0,   44,    2, 0x08 /* Private */,
       7,    0,   45,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MeterView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MeterView *_t = static_cast<MeterView *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->set_project((*reinterpret_cast< Project*(*)>(_a[1]))); break;
        case 1: _t->update_data(); break;
        case 2: _t->transport_started(); break;
        case 3: _t->transport_stopped(); break;
        case 4: _t->delay_timeout(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MeterView::staticMetaObject = {
    { &ViewItem::staticMetaObject, qt_meta_stringdata_MeterView.data,
      qt_meta_data_MeterView,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *MeterView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MeterView::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MeterView.stringdata0))
        return static_cast<void*>(this);
    return ViewItem::qt_metacast(_clname);
}

int MeterView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ViewItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 5;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
