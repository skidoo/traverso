/****************************************************************************
** Meta object code from reading C++ file 'InfoWidgets.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../src/traverso/widgets/InfoWidgets.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'InfoWidgets.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_InfoWidget_t {
    QByteArrayData data[6];
    char stringdata0[50];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_InfoWidget_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_InfoWidget_t qt_meta_stringdata_InfoWidget = {
    {
QT_MOC_LITERAL(0, 0, 10), // "InfoWidget"
QT_MOC_LITERAL(1, 11, 9), // "set_sheet"
QT_MOC_LITERAL(2, 21, 0), // ""
QT_MOC_LITERAL(3, 22, 6), // "Sheet*"
QT_MOC_LITERAL(4, 29, 11), // "set_project"
QT_MOC_LITERAL(5, 41, 8) // "Project*"

    },
    "InfoWidget\0set_sheet\0\0Sheet*\0set_project\0"
    "Project*"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_InfoWidget[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   24,    2, 0x09 /* Protected */,
       4,    1,   27,    2, 0x09 /* Protected */,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    2,
    QMetaType::Void, 0x80000000 | 5,    2,

       0        // eod
};

void InfoWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        InfoWidget *_t = static_cast<InfoWidget *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->set_sheet((*reinterpret_cast< Sheet*(*)>(_a[1]))); break;
        case 1: _t->set_project((*reinterpret_cast< Project*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject InfoWidget::staticMetaObject = {
    { &QFrame::staticMetaObject, qt_meta_stringdata_InfoWidget.data,
      qt_meta_data_InfoWidget,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *InfoWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *InfoWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_InfoWidget.stringdata0))
        return static_cast<void*>(this);
    return QFrame::qt_metacast(_clname);
}

int InfoWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QFrame::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 2;
    }
    return _id;
}
struct qt_meta_stringdata_SystemResources_t {
    QByteArrayData data[3];
    char stringdata0[31];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SystemResources_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SystemResources_t qt_meta_stringdata_SystemResources = {
    {
QT_MOC_LITERAL(0, 0, 15), // "SystemResources"
QT_MOC_LITERAL(1, 16, 13), // "update_status"
QT_MOC_LITERAL(2, 30, 0) // ""

    },
    "SystemResources\0update_status\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SystemResources[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   19,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,

       0        // eod
};

void SystemResources::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        SystemResources *_t = static_cast<SystemResources *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->update_status(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject SystemResources::staticMetaObject = {
    { &InfoWidget::staticMetaObject, qt_meta_stringdata_SystemResources.data,
      qt_meta_data_SystemResources,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *SystemResources::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SystemResources::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_SystemResources.stringdata0))
        return static_cast<void*>(this);
    return InfoWidget::qt_metacast(_clname);
}

int SystemResources::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = InfoWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
    return _id;
}
struct qt_meta_stringdata_DriverInfo_t {
    QByteArrayData data[5];
    char stringdata0[74];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_DriverInfo_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_DriverInfo_t qt_meta_stringdata_DriverInfo = {
    {
QT_MOC_LITERAL(0, 0, 10), // "DriverInfo"
QT_MOC_LITERAL(1, 11, 18), // "update_driver_info"
QT_MOC_LITERAL(2, 30, 0), // ""
QT_MOC_LITERAL(3, 31, 16), // "update_xrun_info"
QT_MOC_LITERAL(4, 48, 25) // "show_driver_config_dialog"

    },
    "DriverInfo\0update_driver_info\0\0"
    "update_xrun_info\0show_driver_config_dialog"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_DriverInfo[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   29,    2, 0x08 /* Private */,
       3,    0,   30,    2, 0x08 /* Private */,
       4,    0,   31,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void DriverInfo::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        DriverInfo *_t = static_cast<DriverInfo *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->update_driver_info(); break;
        case 1: _t->update_xrun_info(); break;
        case 2: _t->show_driver_config_dialog(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject DriverInfo::staticMetaObject = {
    { &InfoWidget::staticMetaObject, qt_meta_stringdata_DriverInfo.data,
      qt_meta_data_DriverInfo,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *DriverInfo::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *DriverInfo::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_DriverInfo.stringdata0))
        return static_cast<void*>(this);
    return InfoWidget::qt_metacast(_clname);
}

int DriverInfo::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = InfoWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 3;
    }
    return _id;
}
struct qt_meta_stringdata_HDDSpaceInfo_t {
    QByteArrayData data[7];
    char stringdata0[73];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_HDDSpaceInfo_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_HDDSpaceInfo_t qt_meta_stringdata_HDDSpaceInfo = {
    {
QT_MOC_LITERAL(0, 0, 12), // "HDDSpaceInfo"
QT_MOC_LITERAL(1, 13, 9), // "set_sheet"
QT_MOC_LITERAL(2, 23, 0), // ""
QT_MOC_LITERAL(3, 24, 6), // "Sheet*"
QT_MOC_LITERAL(4, 31, 13), // "update_status"
QT_MOC_LITERAL(5, 45, 13), // "sheet_started"
QT_MOC_LITERAL(6, 59, 13) // "sheet_stopped"

    },
    "HDDSpaceInfo\0set_sheet\0\0Sheet*\0"
    "update_status\0sheet_started\0sheet_stopped"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_HDDSpaceInfo[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   34,    2, 0x08 /* Private */,
       4,    0,   37,    2, 0x08 /* Private */,
       5,    0,   38,    2, 0x08 /* Private */,
       6,    0,   39,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void HDDSpaceInfo::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        HDDSpaceInfo *_t = static_cast<HDDSpaceInfo *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->set_sheet((*reinterpret_cast< Sheet*(*)>(_a[1]))); break;
        case 1: _t->update_status(); break;
        case 2: _t->sheet_started(); break;
        case 3: _t->sheet_stopped(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject HDDSpaceInfo::staticMetaObject = {
    { &InfoWidget::staticMetaObject, qt_meta_stringdata_HDDSpaceInfo.data,
      qt_meta_data_HDDSpaceInfo,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *HDDSpaceInfo::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *HDDSpaceInfo::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_HDDSpaceInfo.stringdata0))
        return static_cast<void*>(this);
    return InfoWidget::qt_metacast(_clname);
}

int HDDSpaceInfo::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = InfoWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 4)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 4;
    }
    return _id;
}
struct qt_meta_stringdata_SystemValueBar_t {
    QByteArrayData data[1];
    char stringdata0[15];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SystemValueBar_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SystemValueBar_t qt_meta_stringdata_SystemValueBar = {
    {
QT_MOC_LITERAL(0, 0, 14) // "SystemValueBar"

    },
    "SystemValueBar"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SystemValueBar[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void SystemValueBar::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject SystemValueBar::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_SystemValueBar.data,
      qt_meta_data_SystemValueBar,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *SystemValueBar::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SystemValueBar::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_SystemValueBar.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int SystemValueBar::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_ProgressToolBar_t {
    QByteArrayData data[5];
    char stringdata0[54];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ProgressToolBar_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ProgressToolBar_t qt_meta_stringdata_ProgressToolBar = {
    {
QT_MOC_LITERAL(0, 0, 15), // "ProgressToolBar"
QT_MOC_LITERAL(1, 16, 12), // "set_progress"
QT_MOC_LITERAL(2, 29, 0), // ""
QT_MOC_LITERAL(3, 30, 9), // "set_label"
QT_MOC_LITERAL(4, 40, 13) // "set_num_files"

    },
    "ProgressToolBar\0set_progress\0\0set_label\0"
    "set_num_files"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ProgressToolBar[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   29,    2, 0x0a /* Public */,
       3,    1,   32,    2, 0x0a /* Public */,
       4,    1,   35,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::Int,    2,

       0        // eod
};

void ProgressToolBar::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        ProgressToolBar *_t = static_cast<ProgressToolBar *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->set_progress((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->set_label((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->set_num_files((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject ProgressToolBar::staticMetaObject = {
    { &QToolBar::staticMetaObject, qt_meta_stringdata_ProgressToolBar.data,
      qt_meta_data_ProgressToolBar,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *ProgressToolBar::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ProgressToolBar::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_ProgressToolBar.stringdata0))
        return static_cast<void*>(this);
    return QToolBar::qt_metacast(_clname);
}

int ProgressToolBar::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QToolBar::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 3;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
