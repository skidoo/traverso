/****************************************************************************
** Meta object code from reading C++ file 'CorrelationMeterWidget.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../src/traverso/widgets/CorrelationMeterWidget.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'CorrelationMeterWidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_CorrelationMeterView_t {
    QByteArrayData data[9];
    char stringdata0[96];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_CorrelationMeterView_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_CorrelationMeterView_t qt_meta_stringdata_CorrelationMeterView = {
    {
QT_MOC_LITERAL(0, 0, 20), // "CorrelationMeterView"
QT_MOC_LITERAL(1, 21, 8), // "set_mode"
QT_MOC_LITERAL(2, 30, 20), // "Toggle display range"
QT_MOC_LITERAL(3, 51, 11), // "update_data"
QT_MOC_LITERAL(4, 63, 0), // ""
QT_MOC_LITERAL(5, 64, 9), // "set_sheet"
QT_MOC_LITERAL(6, 74, 6), // "Sheet*"
QT_MOC_LITERAL(7, 81, 5), // "sheet"
QT_MOC_LITERAL(8, 87, 8) // "Command*"

    },
    "CorrelationMeterView\0set_mode\0"
    "Toggle display range\0update_data\0\0"
    "set_sheet\0Sheet*\0sheet\0Command*"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_CorrelationMeterView[] = {

 // content:
       7,       // revision
       0,       // classname
       1,   14, // classinfo
       3,   16, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // classinfo: key, value
       1,    2,

 // slots: name, argc, parameters, tag, flags
       3,    0,   31,    4, 0x08 /* Private */,
       5,    1,   32,    4, 0x08 /* Private */,
       1,    0,   35,    4, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 6,    7,
    0x80000000 | 8,

       0        // eod
};

void CorrelationMeterView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        CorrelationMeterView *_t = static_cast<CorrelationMeterView *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->update_data(); break;
        case 1: _t->set_sheet((*reinterpret_cast< Sheet*(*)>(_a[1]))); break;
        case 2: { Command* _r = _t->set_mode();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject CorrelationMeterView::staticMetaObject = {
    { &MeterView::staticMetaObject, qt_meta_stringdata_CorrelationMeterView.data,
      qt_meta_data_CorrelationMeterView,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *CorrelationMeterView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *CorrelationMeterView::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_CorrelationMeterView.stringdata0))
        return static_cast<void*>(this);
    return MeterView::qt_metacast(_clname);
}

int CorrelationMeterView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = MeterView::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 3;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
