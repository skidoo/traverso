/****************************************************************************
** Meta object code from reading C++ file 'Interface.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../src/traverso/Interface.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Interface.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Interface_t {
    QByteArrayData data[73];
    char stringdata0[1370];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Interface_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Interface_t qt_meta_stringdata_Interface = {
    {
QT_MOC_LITERAL(0, 0, 9), // "Interface"
QT_MOC_LITERAL(1, 10, 18), // "show_export_widget"
QT_MOC_LITERAL(2, 29, 18), // "Show Export Dialog"
QT_MOC_LITERAL(3, 48, 17), // "show_context_menu"
QT_MOC_LITERAL(4, 66, 17), // "Show Context Menu"
QT_MOC_LITERAL(5, 84, 14), // "about_traverso"
QT_MOC_LITERAL(6, 99, 14), // "About Traverso"
QT_MOC_LITERAL(7, 114, 27), // "show_project_manager_dialog"
QT_MOC_LITERAL(8, 142, 30), // "Show Project Management Dialog"
QT_MOC_LITERAL(9, 173, 11), // "full_screen"
QT_MOC_LITERAL(10, 185, 11), // "Full Screen"
QT_MOC_LITERAL(11, 197, 13), // "export_keymap"
QT_MOC_LITERAL(12, 211, 13), // "Export keymap"
QT_MOC_LITERAL(13, 225, 15), // "start_transport"
QT_MOC_LITERAL(14, 241, 4), // "Play"
QT_MOC_LITERAL(15, 246, 34), // "set_recordable_and_start_tran..."
QT_MOC_LITERAL(16, 281, 6), // "Record"
QT_MOC_LITERAL(17, 288, 11), // "set_project"
QT_MOC_LITERAL(18, 300, 0), // ""
QT_MOC_LITERAL(19, 301, 8), // "Project*"
QT_MOC_LITERAL(20, 310, 7), // "project"
QT_MOC_LITERAL(21, 318, 10), // "show_sheet"
QT_MOC_LITERAL(22, 329, 6), // "Sheet*"
QT_MOC_LITERAL(23, 336, 5), // "sheet"
QT_MOC_LITERAL(24, 342, 20), // "show_settings_dialog"
QT_MOC_LITERAL(25, 363, 38), // "show_settings_dialog_sound_sy..."
QT_MOC_LITERAL(26, 402, 17), // "open_help_browser"
QT_MOC_LITERAL(27, 420, 27), // "process_context_menu_action"
QT_MOC_LITERAL(28, 448, 8), // "QAction*"
QT_MOC_LITERAL(29, 457, 6), // "action"
QT_MOC_LITERAL(30, 464, 17), // "set_fade_in_shape"
QT_MOC_LITERAL(31, 482, 18), // "set_fade_out_shape"
QT_MOC_LITERAL(32, 501, 14), // "config_changed"
QT_MOC_LITERAL(33, 516, 12), // "import_audio"
QT_MOC_LITERAL(34, 529, 34), // "show_restore_project_backup_d..."
QT_MOC_LITERAL(35, 564, 30), // "change_recording_format_to_wav"
QT_MOC_LITERAL(36, 595, 32), // "change_recording_format_to_wav64"
QT_MOC_LITERAL(37, 628, 34), // "change_recording_format_to_wa..."
QT_MOC_LITERAL(38, 663, 31), // "change_resample_quality_to_best"
QT_MOC_LITERAL(39, 695, 31), // "change_resample_quality_to_high"
QT_MOC_LITERAL(40, 727, 33), // "change_resample_quality_to_me..."
QT_MOC_LITERAL(41, 761, 31), // "change_resample_quality_to_fast"
QT_MOC_LITERAL(42, 793, 8), // "Command*"
QT_MOC_LITERAL(43, 802, 11), // "quick_start"
QT_MOC_LITERAL(44, 814, 10), // "get_keymap"
QT_MOC_LITERAL(45, 825, 8), // "QString&"
QT_MOC_LITERAL(46, 834, 22), // "show_cd_writing_dialog"
QT_MOC_LITERAL(47, 857, 24), // "show_open_project_dialog"
QT_MOC_LITERAL(48, 882, 10), // "projectdir"
QT_MOC_LITERAL(49, 893, 25), // "show_insertsilence_dialog"
QT_MOC_LITERAL(50, 919, 18), // "show_marker_dialog"
QT_MOC_LITERAL(51, 938, 20), // "show_newsheet_dialog"
QT_MOC_LITERAL(52, 959, 20), // "show_newtrack_dialog"
QT_MOC_LITERAL(53, 980, 22), // "show_newproject_dialog"
QT_MOC_LITERAL(54, 1003, 18), // "delete_sheetwidget"
QT_MOC_LITERAL(55, 1022, 27), // "project_dir_change_detected"
QT_MOC_LITERAL(56, 1050, 19), // "project_load_failed"
QT_MOC_LITERAL(57, 1070, 6), // "reason"
QT_MOC_LITERAL(58, 1077, 21), // "project_file_mismatch"
QT_MOC_LITERAL(59, 1099, 7), // "rootdir"
QT_MOC_LITERAL(60, 1107, 11), // "projectname"
QT_MOC_LITERAL(61, 1119, 18), // "snap_state_changed"
QT_MOC_LITERAL(62, 1138, 5), // "state"
QT_MOC_LITERAL(63, 1144, 17), // "update_snap_state"
QT_MOC_LITERAL(64, 1162, 20), // "effect_state_changed"
QT_MOC_LITERAL(65, 1183, 20), // "update_effects_state"
QT_MOC_LITERAL(66, 1204, 20), // "follow_state_changed"
QT_MOC_LITERAL(67, 1225, 19), // "update_follow_state"
QT_MOC_LITERAL(68, 1245, 24), // "update_temp_follow_state"
QT_MOC_LITERAL(69, 1270, 28), // "sheet_selector_update_sheets"
QT_MOC_LITERAL(70, 1299, 14), // "sheet_selected"
QT_MOC_LITERAL(71, 1314, 26), // "sheet_selector_sheet_added"
QT_MOC_LITERAL(72, 1341, 28) // "sheet_selector_sheet_removed"

    },
    "Interface\0show_export_widget\0"
    "Show Export Dialog\0show_context_menu\0"
    "Show Context Menu\0about_traverso\0"
    "About Traverso\0show_project_manager_dialog\0"
    "Show Project Management Dialog\0"
    "full_screen\0Full Screen\0export_keymap\0"
    "Export keymap\0start_transport\0Play\0"
    "set_recordable_and_start_transport\0"
    "Record\0set_project\0\0Project*\0project\0"
    "show_sheet\0Sheet*\0sheet\0show_settings_dialog\0"
    "show_settings_dialog_sound_system_page\0"
    "open_help_browser\0process_context_menu_action\0"
    "QAction*\0action\0set_fade_in_shape\0"
    "set_fade_out_shape\0config_changed\0"
    "import_audio\0show_restore_project_backup_dialog\0"
    "change_recording_format_to_wav\0"
    "change_recording_format_to_wav64\0"
    "change_recording_format_to_wavpack\0"
    "change_resample_quality_to_best\0"
    "change_resample_quality_to_high\0"
    "change_resample_quality_to_medium\0"
    "change_resample_quality_to_fast\0"
    "Command*\0quick_start\0get_keymap\0"
    "QString&\0show_cd_writing_dialog\0"
    "show_open_project_dialog\0projectdir\0"
    "show_insertsilence_dialog\0show_marker_dialog\0"
    "show_newsheet_dialog\0show_newtrack_dialog\0"
    "show_newproject_dialog\0delete_sheetwidget\0"
    "project_dir_change_detected\0"
    "project_load_failed\0reason\0"
    "project_file_mismatch\0rootdir\0projectname\0"
    "snap_state_changed\0state\0update_snap_state\0"
    "effect_state_changed\0update_effects_state\0"
    "follow_state_changed\0update_follow_state\0"
    "update_temp_follow_state\0"
    "sheet_selector_update_sheets\0"
    "sheet_selected\0sheet_selector_sheet_added\0"
    "sheet_selector_sheet_removed"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Interface[] = {

 // content:
       7,       // revision
       0,       // classname
       8,   14, // classinfo
      51,   30, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // classinfo: key, value
       1,    2,
       3,    4,
       5,    6,
       7,    8,
       9,   10,
      11,   12,
      13,   14,
      15,   16,

 // slots: name, argc, parameters, tag, flags
      17,    1,  285,   18, 0x0a /* Public */,
      21,    1,  288,   18, 0x0a /* Public */,
      24,    0,  291,   18, 0x0a /* Public */,
      25,    0,  292,   18, 0x0a /* Public */,
      26,    0,  293,   18, 0x0a /* Public */,
      27,    1,  294,   18, 0x0a /* Public */,
      30,    1,  297,   18, 0x0a /* Public */,
      31,    1,  300,   18, 0x0a /* Public */,
      32,    0,  303,   18, 0x0a /* Public */,
      33,    0,  304,   18, 0x0a /* Public */,
      34,    0,  305,   18, 0x0a /* Public */,
      35,    0,  306,   18, 0x0a /* Public */,
      36,    0,  307,   18, 0x0a /* Public */,
      37,    0,  308,   18, 0x0a /* Public */,
      38,    0,  309,   18, 0x0a /* Public */,
      39,    0,  310,   18, 0x0a /* Public */,
      40,    0,  311,   18, 0x0a /* Public */,
      41,    0,  312,   18, 0x0a /* Public */,
       9,    0,  313,   18, 0x0a /* Public */,
       5,    0,  314,   18, 0x0a /* Public */,
      43,    0,  315,   18, 0x0a /* Public */,
      11,    0,  316,   18, 0x0a /* Public */,
      44,    1,  317,   18, 0x0a /* Public */,
       1,    0,  320,   18, 0x0a /* Public */,
      46,    0,  321,   18, 0x0a /* Public */,
       3,    0,  322,   18, 0x0a /* Public */,
      47,    0,  323,   18, 0x0a /* Public */,
       7,    0,  324,   18, 0x0a /* Public */,
      34,    1,  325,   18, 0x0a /* Public */,
      49,    0,  328,   18, 0x0a /* Public */,
      50,    0,  329,   18, 0x0a /* Public */,
      51,    0,  330,   18, 0x0a /* Public */,
      52,    0,  331,   18, 0x0a /* Public */,
      53,    0,  332,   18, 0x0a /* Public */,
      13,    0,  333,   18, 0x0a /* Public */,
      15,    0,  334,   18, 0x0a /* Public */,
      54,    1,  335,   18, 0x08 /* Private */,
      55,    0,  338,   18, 0x08 /* Private */,
      56,    2,  339,   18, 0x08 /* Private */,
      58,    2,  344,   18, 0x08 /* Private */,
      61,    1,  349,   18, 0x08 /* Private */,
      63,    0,  352,   18, 0x08 /* Private */,
      64,    1,  353,   18, 0x08 /* Private */,
      65,    0,  356,   18, 0x08 /* Private */,
      66,    1,  357,   18, 0x08 /* Private */,
      67,    0,  360,   18, 0x08 /* Private */,
      68,    1,  361,   18, 0x08 /* Private */,
      69,    0,  364,   18, 0x08 /* Private */,
      70,    0,  365,   18, 0x08 /* Private */,
      71,    1,  366,   18, 0x08 /* Private */,
      72,    1,  369,   18, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 19,   20,
    QMetaType::Void, 0x80000000 | 22,   23,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 28,   29,
    QMetaType::Void, 0x80000000 | 28,   29,
    QMetaType::Void, 0x80000000 | 28,   29,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    0x80000000 | 42,
    0x80000000 | 42,
    0x80000000 | 42,
    0x80000000 | 42,
    0x80000000 | 42, 0x80000000 | 45,   18,
    0x80000000 | 42,
    0x80000000 | 42,
    0x80000000 | 42,
    0x80000000 | 42,
    0x80000000 | 42,
    0x80000000 | 42, QMetaType::QString,   48,
    0x80000000 | 42,
    0x80000000 | 42,
    0x80000000 | 42,
    0x80000000 | 42,
    0x80000000 | 42,
    0x80000000 | 42,
    0x80000000 | 42,
    QMetaType::Void, 0x80000000 | 22,   18,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,   20,   57,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,   59,   60,
    QMetaType::Void, QMetaType::Bool,   62,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   62,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   62,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   62,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 22,   18,
    QMetaType::Void, 0x80000000 | 22,   18,

       0        // eod
};

void Interface::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Interface *_t = static_cast<Interface *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->set_project((*reinterpret_cast< Project*(*)>(_a[1]))); break;
        case 1: _t->show_sheet((*reinterpret_cast< Sheet*(*)>(_a[1]))); break;
        case 2: _t->show_settings_dialog(); break;
        case 3: _t->show_settings_dialog_sound_system_page(); break;
        case 4: _t->open_help_browser(); break;
        case 5: _t->process_context_menu_action((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        case 6: _t->set_fade_in_shape((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        case 7: _t->set_fade_out_shape((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        case 8: _t->config_changed(); break;
        case 9: _t->import_audio(); break;
        case 10: _t->show_restore_project_backup_dialog(); break;
        case 11: _t->change_recording_format_to_wav(); break;
        case 12: _t->change_recording_format_to_wav64(); break;
        case 13: _t->change_recording_format_to_wavpack(); break;
        case 14: _t->change_resample_quality_to_best(); break;
        case 15: _t->change_resample_quality_to_high(); break;
        case 16: _t->change_resample_quality_to_medium(); break;
        case 17: _t->change_resample_quality_to_fast(); break;
        case 18: { Command* _r = _t->full_screen();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 19: { Command* _r = _t->about_traverso();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 20: { Command* _r = _t->quick_start();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 21: { Command* _r = _t->export_keymap();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 22: { Command* _r = _t->get_keymap((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 23: { Command* _r = _t->show_export_widget();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 24: { Command* _r = _t->show_cd_writing_dialog();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 25: { Command* _r = _t->show_context_menu();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 26: { Command* _r = _t->show_open_project_dialog();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 27: { Command* _r = _t->show_project_manager_dialog();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 28: { Command* _r = _t->show_restore_project_backup_dialog((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 29: { Command* _r = _t->show_insertsilence_dialog();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 30: { Command* _r = _t->show_marker_dialog();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 31: { Command* _r = _t->show_newsheet_dialog();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 32: { Command* _r = _t->show_newtrack_dialog();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 33: { Command* _r = _t->show_newproject_dialog();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 34: { Command* _r = _t->start_transport();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 35: { Command* _r = _t->set_recordable_and_start_transport();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 36: _t->delete_sheetwidget((*reinterpret_cast< Sheet*(*)>(_a[1]))); break;
        case 37: _t->project_dir_change_detected(); break;
        case 38: _t->project_load_failed((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 39: _t->project_file_mismatch((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 40: _t->snap_state_changed((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 41: _t->update_snap_state(); break;
        case 42: _t->effect_state_changed((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 43: _t->update_effects_state(); break;
        case 44: _t->follow_state_changed((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 45: _t->update_follow_state(); break;
        case 46: _t->update_temp_follow_state((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 47: _t->sheet_selector_update_sheets(); break;
        case 48: _t->sheet_selected(); break;
        case 49: _t->sheet_selector_sheet_added((*reinterpret_cast< Sheet*(*)>(_a[1]))); break;
        case 50: _t->sheet_selector_sheet_removed((*reinterpret_cast< Sheet*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject Interface::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_Interface.data,
      qt_meta_data_Interface,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *Interface::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Interface::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Interface.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int Interface::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 51)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 51;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 51)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 51;
    }
    return _id;
}
struct qt_meta_stringdata_DigitalClock_t {
    QByteArrayData data[3];
    char stringdata0[23];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_DigitalClock_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_DigitalClock_t qt_meta_stringdata_DigitalClock = {
    {
QT_MOC_LITERAL(0, 0, 12), // "DigitalClock"
QT_MOC_LITERAL(1, 13, 8), // "showTime"
QT_MOC_LITERAL(2, 22, 0) // ""

    },
    "DigitalClock\0showTime\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_DigitalClock[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   19,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,

       0        // eod
};

void DigitalClock::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        DigitalClock *_t = static_cast<DigitalClock *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->showTime(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject DigitalClock::staticMetaObject = {
    { &QLCDNumber::staticMetaObject, qt_meta_stringdata_DigitalClock.data,
      qt_meta_data_DigitalClock,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *DigitalClock::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *DigitalClock::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_DigitalClock.stringdata0))
        return static_cast<void*>(this);
    return QLCDNumber::qt_metacast(_clname);
}

int DigitalClock::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QLCDNumber::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
