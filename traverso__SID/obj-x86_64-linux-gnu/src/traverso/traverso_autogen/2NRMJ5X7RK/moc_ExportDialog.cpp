/****************************************************************************
** Meta object code from reading C++ file 'ExportDialog.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../src/traverso/dialogs/ExportDialog.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'ExportDialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_ExportDialog_t {
    QByteArrayData data[17];
    char stringdata0[251];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ExportDialog_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ExportDialog_t qt_meta_stringdata_ExportDialog = {
    {
QT_MOC_LITERAL(0, 0, 12), // "ExportDialog"
QT_MOC_LITERAL(1, 13, 11), // "set_project"
QT_MOC_LITERAL(2, 25, 0), // ""
QT_MOC_LITERAL(3, 26, 8), // "Project*"
QT_MOC_LITERAL(4, 35, 7), // "project"
QT_MOC_LITERAL(5, 43, 21), // "update_sheet_progress"
QT_MOC_LITERAL(6, 65, 8), // "progress"
QT_MOC_LITERAL(7, 74, 23), // "update_overall_progress"
QT_MOC_LITERAL(8, 98, 15), // "render_finished"
QT_MOC_LITERAL(9, 114, 19), // "set_exporting_sheet"
QT_MOC_LITERAL(10, 134, 6), // "Sheet*"
QT_MOC_LITERAL(11, 141, 5), // "sheet"
QT_MOC_LITERAL(12, 147, 27), // "on_fileSelectButton_clicked"
QT_MOC_LITERAL(13, 175, 22), // "on_startButton_clicked"
QT_MOC_LITERAL(14, 198, 22), // "on_abortButton_clicked"
QT_MOC_LITERAL(15, 221, 22), // "on_closeButton_clicked"
QT_MOC_LITERAL(16, 244, 6) // "reject"

    },
    "ExportDialog\0set_project\0\0Project*\0"
    "project\0update_sheet_progress\0progress\0"
    "update_overall_progress\0render_finished\0"
    "set_exporting_sheet\0Sheet*\0sheet\0"
    "on_fileSelectButton_clicked\0"
    "on_startButton_clicked\0on_abortButton_clicked\0"
    "on_closeButton_clicked\0reject"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ExportDialog[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   64,    2, 0x08 /* Private */,
       5,    1,   67,    2, 0x08 /* Private */,
       7,    1,   70,    2, 0x08 /* Private */,
       8,    0,   73,    2, 0x08 /* Private */,
       9,    1,   74,    2, 0x08 /* Private */,
      12,    0,   77,    2, 0x08 /* Private */,
      13,    0,   78,    2, 0x08 /* Private */,
      14,    0,   79,    2, 0x08 /* Private */,
      15,    0,   80,    2, 0x08 /* Private */,
      16,    0,   81,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, QMetaType::Int,    6,
    QMetaType::Void, QMetaType::Int,    6,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 10,   11,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void ExportDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        ExportDialog *_t = static_cast<ExportDialog *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->set_project((*reinterpret_cast< Project*(*)>(_a[1]))); break;
        case 1: _t->update_sheet_progress((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->update_overall_progress((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->render_finished(); break;
        case 4: _t->set_exporting_sheet((*reinterpret_cast< Sheet*(*)>(_a[1]))); break;
        case 5: _t->on_fileSelectButton_clicked(); break;
        case 6: _t->on_startButton_clicked(); break;
        case 7: _t->on_abortButton_clicked(); break;
        case 8: _t->on_closeButton_clicked(); break;
        case 9: _t->reject(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject ExportDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_ExportDialog.data,
      qt_meta_data_ExportDialog,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *ExportDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ExportDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_ExportDialog.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "Ui::ExportDialog"))
        return static_cast< Ui::ExportDialog*>(this);
    return QDialog::qt_metacast(_clname);
}

int ExportDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 10)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 10;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
