/****************************************************************************
** Meta object code from reading C++ file 'CDWritingDialog.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../src/traverso/dialogs/CDWritingDialog.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'CDWritingDialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_CDWritingDialog_t {
    QByteArrayData data[21];
    char stringdata0[308];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_CDWritingDialog_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_CDWritingDialog_t qt_meta_stringdata_CDWritingDialog = {
    {
QT_MOC_LITERAL(0, 0, 15), // "CDWritingDialog"
QT_MOC_LITERAL(1, 16, 11), // "set_project"
QT_MOC_LITERAL(2, 28, 0), // ""
QT_MOC_LITERAL(3, 29, 8), // "Project*"
QT_MOC_LITERAL(4, 38, 7), // "project"
QT_MOC_LITERAL(5, 46, 21), // "on_stopButton_clicked"
QT_MOC_LITERAL(6, 68, 19), // "export_only_changed"
QT_MOC_LITERAL(7, 88, 5), // "state"
QT_MOC_LITERAL(8, 94, 18), // "start_burn_process"
QT_MOC_LITERAL(9, 113, 17), // "stop_burn_process"
QT_MOC_LITERAL(10, 131, 20), // "read_standard_output"
QT_MOC_LITERAL(11, 152, 22), // "cdrdao_process_started"
QT_MOC_LITERAL(12, 175, 23), // "cdrdao_process_finished"
QT_MOC_LITERAL(13, 199, 8), // "exitcode"
QT_MOC_LITERAL(14, 208, 20), // "QProcess::ExitStatus"
QT_MOC_LITERAL(15, 229, 10), // "exitstatus"
QT_MOC_LITERAL(16, 240, 18), // "cd_export_finished"
QT_MOC_LITERAL(17, 259, 18), // "cd_export_progress"
QT_MOC_LITERAL(18, 278, 8), // "progress"
QT_MOC_LITERAL(19, 287, 13), // "query_devices"
QT_MOC_LITERAL(20, 301, 6) // "reject"

    },
    "CDWritingDialog\0set_project\0\0Project*\0"
    "project\0on_stopButton_clicked\0"
    "export_only_changed\0state\0start_burn_process\0"
    "stop_burn_process\0read_standard_output\0"
    "cdrdao_process_started\0cdrdao_process_finished\0"
    "exitcode\0QProcess::ExitStatus\0exitstatus\0"
    "cd_export_finished\0cd_export_progress\0"
    "progress\0query_devices\0reject"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_CDWritingDialog[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   74,    2, 0x08 /* Private */,
       5,    0,   77,    2, 0x08 /* Private */,
       6,    1,   78,    2, 0x08 /* Private */,
       8,    0,   81,    2, 0x08 /* Private */,
       9,    0,   82,    2, 0x08 /* Private */,
      10,    0,   83,    2, 0x08 /* Private */,
      11,    0,   84,    2, 0x08 /* Private */,
      12,    2,   85,    2, 0x08 /* Private */,
      16,    0,   90,    2, 0x08 /* Private */,
      17,    1,   91,    2, 0x08 /* Private */,
      19,    0,   94,    2, 0x08 /* Private */,
      20,    0,   95,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    7,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, 0x80000000 | 14,   13,   15,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   18,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void CDWritingDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        CDWritingDialog *_t = static_cast<CDWritingDialog *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->set_project((*reinterpret_cast< Project*(*)>(_a[1]))); break;
        case 1: _t->on_stopButton_clicked(); break;
        case 2: _t->export_only_changed((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->start_burn_process(); break;
        case 4: _t->stop_burn_process(); break;
        case 5: _t->read_standard_output(); break;
        case 6: _t->cdrdao_process_started(); break;
        case 7: _t->cdrdao_process_finished((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QProcess::ExitStatus(*)>(_a[2]))); break;
        case 8: _t->cd_export_finished(); break;
        case 9: _t->cd_export_progress((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 10: _t->query_devices(); break;
        case 11: _t->reject(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject CDWritingDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_CDWritingDialog.data,
      qt_meta_data_CDWritingDialog,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *CDWritingDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *CDWritingDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_CDWritingDialog.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "Ui::CDWritingDialog"))
        return static_cast< Ui::CDWritingDialog*>(this);
    return QDialog::qt_metacast(_clname);
}

int CDWritingDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 12)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 12;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 12)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 12;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
