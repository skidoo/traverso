/********************************************************************************
** Form generated from reading UI file 'MarkerDialog.ui'
**
** Created by: Qt User Interface Compiler version 5.11.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MARKERDIALOG_H
#define UI_MARKERDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MarkerDialog
{
public:
    QVBoxLayout *vboxLayout;
    QTreeWidget *markersTreeWidget;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QLineEdit *lineEditIsrc;
    QLabel *label_6;
    QToolButton *toolButtonPEmphAll;
    QToolButton *toolButtonCopyAll;
    QCheckBox *checkBoxPreEmph;
    QCheckBox *checkBoxCopy;
    QLabel *label_9;
    QLineEdit *lineEditPosition;
    QTabWidget *tabWidget;
    QWidget *tab;
    QGridLayout *gridLayout1;
    QLabel *label;
    QLineEdit *lineEditTitle;
    QToolButton *toolButtonTitleAll;
    QLabel *label_2;
    QLineEdit *lineEditPerformer;
    QToolButton *toolButtonPerformerAll;
    QLabel *label_3;
    QLineEdit *lineEditComposer;
    QToolButton *toolButtonComposerAll;
    QWidget *tab_2;
    QGridLayout *gridLayout2;
    QLabel *label_4;
    QLineEdit *lineEditSongwriter;
    QLabel *label_5;
    QLineEdit *lineEditArranger;
    QLabel *label_7;
    QLineEdit *lineEditMessage;
    QToolButton *toolButtonMessageAll;
    QToolButton *toolButtonArrangerAll;
    QToolButton *toolButtonSongWriterAll;
    QHBoxLayout *hboxLayout;
    QPushButton *pushButtonRemove;
    QPushButton *pushButtonExport;
    QSpacerItem *spacerItem;
    QPushButton *pushButtonOk;
    QPushButton *pushButtonCancel;

    void setupUi(QDialog *MarkerDialog)
    {
        if (MarkerDialog->objectName().isEmpty())
            MarkerDialog->setObjectName(QStringLiteral("MarkerDialog"));
        MarkerDialog->resize(400, 558);
        vboxLayout = new QVBoxLayout(MarkerDialog);
#ifndef Q_OS_MAC
        vboxLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout->setObjectName(QStringLiteral("vboxLayout"));
        markersTreeWidget = new QTreeWidget(MarkerDialog);
        markersTreeWidget->setObjectName(QStringLiteral("markersTreeWidget"));
        markersTreeWidget->setEditTriggers(QAbstractItemView::AnyKeyPressed|QAbstractItemView::DoubleClicked|QAbstractItemView::EditKeyPressed);
        markersTreeWidget->setRootIsDecorated(false);

        vboxLayout->addWidget(markersTreeWidget);

        groupBox = new QGroupBox(MarkerDialog);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        gridLayout = new QGridLayout(groupBox);
#ifndef Q_OS_MAC
        gridLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        gridLayout->setContentsMargins(9, 9, 9, 9);
#endif
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        lineEditIsrc = new QLineEdit(groupBox);
        lineEditIsrc->setObjectName(QStringLiteral("lineEditIsrc"));

        gridLayout->addWidget(lineEditIsrc, 1, 1, 1, 2);

        label_6 = new QLabel(groupBox);
        label_6->setObjectName(QStringLiteral("label_6"));

        gridLayout->addWidget(label_6, 1, 0, 1, 1);

        toolButtonPEmphAll = new QToolButton(groupBox);
        toolButtonPEmphAll->setObjectName(QStringLiteral("toolButtonPEmphAll"));
        QIcon icon;
        icon.addFile(QStringLiteral("../../../../../../../usr/share/icons/crystalsvg/16x16/actions/tab_duplicate.png"), QSize(), QIcon::Normal, QIcon::Off);
        toolButtonPEmphAll->setIcon(icon);

        gridLayout->addWidget(toolButtonPEmphAll, 3, 2, 1, 1);

        toolButtonCopyAll = new QToolButton(groupBox);
        toolButtonCopyAll->setObjectName(QStringLiteral("toolButtonCopyAll"));
        toolButtonCopyAll->setIcon(icon);

        gridLayout->addWidget(toolButtonCopyAll, 2, 2, 1, 1);

        checkBoxPreEmph = new QCheckBox(groupBox);
        checkBoxPreEmph->setObjectName(QStringLiteral("checkBoxPreEmph"));

        gridLayout->addWidget(checkBoxPreEmph, 3, 0, 1, 2);

        checkBoxCopy = new QCheckBox(groupBox);
        checkBoxCopy->setObjectName(QStringLiteral("checkBoxCopy"));

        gridLayout->addWidget(checkBoxCopy, 2, 0, 1, 2);

        label_9 = new QLabel(groupBox);
        label_9->setObjectName(QStringLiteral("label_9"));

        gridLayout->addWidget(label_9, 0, 0, 1, 1);

        lineEditPosition = new QLineEdit(groupBox);
        lineEditPosition->setObjectName(QStringLiteral("lineEditPosition"));

        gridLayout->addWidget(lineEditPosition, 0, 1, 1, 2);


        vboxLayout->addWidget(groupBox);

        tabWidget = new QTabWidget(MarkerDialog);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        gridLayout1 = new QGridLayout(tab);
#ifndef Q_OS_MAC
        gridLayout1->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        gridLayout1->setContentsMargins(9, 9, 9, 9);
#endif
        gridLayout1->setObjectName(QStringLiteral("gridLayout1"));
        label = new QLabel(tab);
        label->setObjectName(QStringLiteral("label"));

        gridLayout1->addWidget(label, 0, 0, 1, 1);

        lineEditTitle = new QLineEdit(tab);
        lineEditTitle->setObjectName(QStringLiteral("lineEditTitle"));

        gridLayout1->addWidget(lineEditTitle, 0, 1, 1, 1);

        toolButtonTitleAll = new QToolButton(tab);
        toolButtonTitleAll->setObjectName(QStringLiteral("toolButtonTitleAll"));
        toolButtonTitleAll->setIcon(icon);

        gridLayout1->addWidget(toolButtonTitleAll, 0, 2, 1, 1);

        label_2 = new QLabel(tab);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout1->addWidget(label_2, 1, 0, 1, 1);

        lineEditPerformer = new QLineEdit(tab);
        lineEditPerformer->setObjectName(QStringLiteral("lineEditPerformer"));

        gridLayout1->addWidget(lineEditPerformer, 1, 1, 1, 1);

        toolButtonPerformerAll = new QToolButton(tab);
        toolButtonPerformerAll->setObjectName(QStringLiteral("toolButtonPerformerAll"));
        toolButtonPerformerAll->setIcon(icon);

        gridLayout1->addWidget(toolButtonPerformerAll, 1, 2, 1, 1);

        label_3 = new QLabel(tab);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout1->addWidget(label_3, 2, 0, 1, 1);

        lineEditComposer = new QLineEdit(tab);
        lineEditComposer->setObjectName(QStringLiteral("lineEditComposer"));

        gridLayout1->addWidget(lineEditComposer, 2, 1, 1, 1);

        toolButtonComposerAll = new QToolButton(tab);
        toolButtonComposerAll->setObjectName(QStringLiteral("toolButtonComposerAll"));
        toolButtonComposerAll->setIcon(icon);

        gridLayout1->addWidget(toolButtonComposerAll, 2, 2, 1, 1);

        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QStringLiteral("tab_2"));
        gridLayout2 = new QGridLayout(tab_2);
#ifndef Q_OS_MAC
        gridLayout2->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        gridLayout2->setContentsMargins(9, 9, 9, 9);
#endif
        gridLayout2->setObjectName(QStringLiteral("gridLayout2"));
        label_4 = new QLabel(tab_2);
        label_4->setObjectName(QStringLiteral("label_4"));

        gridLayout2->addWidget(label_4, 0, 0, 1, 1);

        lineEditSongwriter = new QLineEdit(tab_2);
        lineEditSongwriter->setObjectName(QStringLiteral("lineEditSongwriter"));

        gridLayout2->addWidget(lineEditSongwriter, 0, 1, 1, 1);

        label_5 = new QLabel(tab_2);
        label_5->setObjectName(QStringLiteral("label_5"));

        gridLayout2->addWidget(label_5, 1, 0, 1, 1);

        lineEditArranger = new QLineEdit(tab_2);
        lineEditArranger->setObjectName(QStringLiteral("lineEditArranger"));

        gridLayout2->addWidget(lineEditArranger, 1, 1, 1, 1);

        label_7 = new QLabel(tab_2);
        label_7->setObjectName(QStringLiteral("label_7"));

        gridLayout2->addWidget(label_7, 2, 0, 1, 1);

        lineEditMessage = new QLineEdit(tab_2);
        lineEditMessage->setObjectName(QStringLiteral("lineEditMessage"));

        gridLayout2->addWidget(lineEditMessage, 2, 1, 1, 1);

        toolButtonMessageAll = new QToolButton(tab_2);
        toolButtonMessageAll->setObjectName(QStringLiteral("toolButtonMessageAll"));
        toolButtonMessageAll->setIcon(icon);

        gridLayout2->addWidget(toolButtonMessageAll, 2, 2, 1, 1);

        toolButtonArrangerAll = new QToolButton(tab_2);
        toolButtonArrangerAll->setObjectName(QStringLiteral("toolButtonArrangerAll"));
        toolButtonArrangerAll->setIcon(icon);

        gridLayout2->addWidget(toolButtonArrangerAll, 1, 2, 1, 1);

        toolButtonSongWriterAll = new QToolButton(tab_2);
        toolButtonSongWriterAll->setObjectName(QStringLiteral("toolButtonSongWriterAll"));
        toolButtonSongWriterAll->setIcon(icon);

        gridLayout2->addWidget(toolButtonSongWriterAll, 0, 2, 1, 1);

        tabWidget->addTab(tab_2, QString());

        vboxLayout->addWidget(tabWidget);

        hboxLayout = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout->setSpacing(6);
#endif
        hboxLayout->setContentsMargins(0, 0, 0, 0);
        hboxLayout->setObjectName(QStringLiteral("hboxLayout"));
        pushButtonRemove = new QPushButton(MarkerDialog);
        pushButtonRemove->setObjectName(QStringLiteral("pushButtonRemove"));

        hboxLayout->addWidget(pushButtonRemove);

        pushButtonExport = new QPushButton(MarkerDialog);
        pushButtonExport->setObjectName(QStringLiteral("pushButtonExport"));

        hboxLayout->addWidget(pushButtonExport);

        spacerItem = new QSpacerItem(281, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacerItem);

        pushButtonOk = new QPushButton(MarkerDialog);
        pushButtonOk->setObjectName(QStringLiteral("pushButtonOk"));
        pushButtonOk->setAutoDefault(false);

        hboxLayout->addWidget(pushButtonOk);

        pushButtonCancel = new QPushButton(MarkerDialog);
        pushButtonCancel->setObjectName(QStringLiteral("pushButtonCancel"));

        hboxLayout->addWidget(pushButtonCancel);


        vboxLayout->addLayout(hboxLayout);

        QWidget::setTabOrder(markersTreeWidget, checkBoxCopy);
        QWidget::setTabOrder(checkBoxCopy, toolButtonCopyAll);
        QWidget::setTabOrder(toolButtonCopyAll, checkBoxPreEmph);
        QWidget::setTabOrder(checkBoxPreEmph, toolButtonPEmphAll);
        QWidget::setTabOrder(toolButtonPEmphAll, lineEditIsrc);
        QWidget::setTabOrder(lineEditIsrc, tabWidget);
        QWidget::setTabOrder(tabWidget, lineEditTitle);
        QWidget::setTabOrder(lineEditTitle, toolButtonTitleAll);
        QWidget::setTabOrder(toolButtonTitleAll, lineEditPerformer);
        QWidget::setTabOrder(lineEditPerformer, toolButtonPerformerAll);
        QWidget::setTabOrder(toolButtonPerformerAll, lineEditComposer);
        QWidget::setTabOrder(lineEditComposer, toolButtonComposerAll);
        QWidget::setTabOrder(toolButtonComposerAll, lineEditSongwriter);
        QWidget::setTabOrder(lineEditSongwriter, lineEditArranger);
        QWidget::setTabOrder(lineEditArranger, toolButtonArrangerAll);
        QWidget::setTabOrder(toolButtonArrangerAll, lineEditMessage);
        QWidget::setTabOrder(lineEditMessage, toolButtonMessageAll);
        QWidget::setTabOrder(toolButtonMessageAll, pushButtonOk);

        retranslateUi(MarkerDialog);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MarkerDialog);
    } // setupUi

    void retranslateUi(QDialog *MarkerDialog)
    {
        MarkerDialog->setWindowTitle(QApplication::translate("MarkerDialog", "Markers", nullptr));
        QTreeWidgetItem *___qtreewidgetitem = markersTreeWidget->headerItem();
        ___qtreewidgetitem->setText(1, QApplication::translate("MarkerDialog", "Title", nullptr));
        ___qtreewidgetitem->setText(0, QApplication::translate("MarkerDialog", "Position", nullptr));
        groupBox->setTitle(QApplication::translate("MarkerDialog", "Options", nullptr));
        label_6->setText(QApplication::translate("MarkerDialog", "ISRC:", nullptr));
#ifndef QT_NO_TOOLTIP
        toolButtonPEmphAll->setToolTip(QApplication::translate("MarkerDialog", "Apply to all", nullptr));
#endif // QT_NO_TOOLTIP
        toolButtonPEmphAll->setText(QApplication::translate("MarkerDialog", "...", nullptr));
#ifndef QT_NO_TOOLTIP
        toolButtonCopyAll->setToolTip(QApplication::translate("MarkerDialog", "Apply to all", nullptr));
#endif // QT_NO_TOOLTIP
        toolButtonCopyAll->setText(QApplication::translate("MarkerDialog", "...", nullptr));
        checkBoxPreEmph->setText(QApplication::translate("MarkerDialog", "Pre-Emphasis", nullptr));
        checkBoxCopy->setText(QApplication::translate("MarkerDialog", "Copy protection", nullptr));
        label_9->setText(QApplication::translate("MarkerDialog", "Position: (MM:SS:75ths)", nullptr));
#ifndef QT_NO_TOOLTIP
        tabWidget->setToolTip(QString());
#endif // QT_NO_TOOLTIP
        label->setText(QApplication::translate("MarkerDialog", "Title:", nullptr));
#ifndef QT_NO_TOOLTIP
        toolButtonTitleAll->setToolTip(QApplication::translate("MarkerDialog", "Apply to all", nullptr));
#endif // QT_NO_TOOLTIP
        toolButtonTitleAll->setText(QApplication::translate("MarkerDialog", "...", nullptr));
        label_2->setText(QApplication::translate("MarkerDialog", "Performer:", nullptr));
#ifndef QT_NO_TOOLTIP
        toolButtonPerformerAll->setToolTip(QApplication::translate("MarkerDialog", "Apply to all", nullptr));
#endif // QT_NO_TOOLTIP
        toolButtonPerformerAll->setText(QApplication::translate("MarkerDialog", "...", nullptr));
        label_3->setText(QApplication::translate("MarkerDialog", "Composer:", nullptr));
#ifndef QT_NO_TOOLTIP
        toolButtonComposerAll->setToolTip(QApplication::translate("MarkerDialog", "Apply to all", nullptr));
#endif // QT_NO_TOOLTIP
        toolButtonComposerAll->setText(QApplication::translate("MarkerDialog", "...", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("MarkerDialog", "CD-Text", nullptr));
        label_4->setText(QApplication::translate("MarkerDialog", "Songwriter", nullptr));
        label_5->setText(QApplication::translate("MarkerDialog", "Arranger:", nullptr));
        label_7->setText(QApplication::translate("MarkerDialog", "Message:", nullptr));
#ifndef QT_NO_TOOLTIP
        toolButtonMessageAll->setToolTip(QApplication::translate("MarkerDialog", "Apply to all", nullptr));
#endif // QT_NO_TOOLTIP
        toolButtonMessageAll->setText(QApplication::translate("MarkerDialog", "...", nullptr));
#ifndef QT_NO_TOOLTIP
        toolButtonArrangerAll->setToolTip(QApplication::translate("MarkerDialog", "Apply to all", nullptr));
#endif // QT_NO_TOOLTIP
        toolButtonArrangerAll->setText(QApplication::translate("MarkerDialog", "...", nullptr));
#ifndef QT_NO_TOOLTIP
        toolButtonSongWriterAll->setToolTip(QApplication::translate("MarkerDialog", "Apply to all", nullptr));
#endif // QT_NO_TOOLTIP
        toolButtonSongWriterAll->setText(QApplication::translate("MarkerDialog", "...", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("MarkerDialog", "CD-Text optional", nullptr));
        pushButtonRemove->setText(QApplication::translate("MarkerDialog", "&Remove", nullptr));
        pushButtonExport->setText(QApplication::translate("MarkerDialog", "&Export", nullptr));
        pushButtonOk->setText(QApplication::translate("MarkerDialog", "&Ok", nullptr));
        pushButtonCancel->setText(QApplication::translate("MarkerDialog", "&Cancel", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MarkerDialog: public Ui_MarkerDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MARKERDIALOG_H
