/********************************************************************************
** Form generated from reading UI file 'RecordingConfigPage.ui'
**
** Created by: Qt User Interface Compiler version 5.11.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_RECORDINGCONFIGPAGE_H
#define UI_RECORDINGCONFIGPAGE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_RecordingConfigPage
{
public:
    QVBoxLayout *verticalLayout;
    QGroupBox *groupBox;
    QVBoxLayout *vboxLayout;
    QHBoxLayout *hboxLayout;
    QLabel *label;
    QSpacerItem *spacerItem;
    QComboBox *encodingComboBox;
    QGroupBox *wacpackGroupBox;
    QVBoxLayout *vboxLayout1;
    QHBoxLayout *hboxLayout1;
    QLabel *label_4;
    QComboBox *wavpackCompressionComboBox;
    QCheckBox *wavpackUseAlmostLosslessCheckBox;
    QGroupBox *groupBox_2;
    QVBoxLayout *vboxLayout2;
    QCheckBox *useResamplingCheckBox;
    QHBoxLayout *hboxLayout2;
    QVBoxLayout *vboxLayout3;
    QLabel *label_3;
    QLabel *label_2;
    QVBoxLayout *vboxLayout4;
    QComboBox *ontheflyResampleComboBox;
    QComboBox *exportDefaultResampleQualityComboBox;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *RecordingConfigPage)
    {
        if (RecordingConfigPage->objectName().isEmpty())
            RecordingConfigPage->setObjectName(QStringLiteral("RecordingConfigPage"));
        RecordingConfigPage->resize(354, 248);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(1);
        sizePolicy.setHeightForWidth(RecordingConfigPage->sizePolicy().hasHeightForWidth());
        RecordingConfigPage->setSizePolicy(sizePolicy);
        verticalLayout = new QVBoxLayout(RecordingConfigPage);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        groupBox = new QGroupBox(RecordingConfigPage);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        vboxLayout = new QVBoxLayout(groupBox);
#ifndef Q_OS_MAC
        vboxLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout->setObjectName(QStringLiteral("vboxLayout"));
        hboxLayout = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout->setContentsMargins(0, 0, 0, 0);
#endif
        hboxLayout->setObjectName(QStringLiteral("hboxLayout"));
        label = new QLabel(groupBox);
        label->setObjectName(QStringLiteral("label"));

        hboxLayout->addWidget(label);

        spacerItem = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacerItem);

        encodingComboBox = new QComboBox(groupBox);
        encodingComboBox->setObjectName(QStringLiteral("encodingComboBox"));

        hboxLayout->addWidget(encodingComboBox);


        vboxLayout->addLayout(hboxLayout);

        wacpackGroupBox = new QGroupBox(groupBox);
        wacpackGroupBox->setObjectName(QStringLiteral("wacpackGroupBox"));
        vboxLayout1 = new QVBoxLayout(wacpackGroupBox);
#ifndef Q_OS_MAC
        vboxLayout1->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout1->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout1->setObjectName(QStringLiteral("vboxLayout1"));
        hboxLayout1 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout1->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout1->setContentsMargins(0, 0, 0, 0);
#endif
        hboxLayout1->setObjectName(QStringLiteral("hboxLayout1"));
        label_4 = new QLabel(wacpackGroupBox);
        label_4->setObjectName(QStringLiteral("label_4"));

        hboxLayout1->addWidget(label_4);

        wavpackCompressionComboBox = new QComboBox(wacpackGroupBox);
        wavpackCompressionComboBox->setObjectName(QStringLiteral("wavpackCompressionComboBox"));

        hboxLayout1->addWidget(wavpackCompressionComboBox);


        vboxLayout1->addLayout(hboxLayout1);

        wavpackUseAlmostLosslessCheckBox = new QCheckBox(wacpackGroupBox);
        wavpackUseAlmostLosslessCheckBox->setObjectName(QStringLiteral("wavpackUseAlmostLosslessCheckBox"));

        vboxLayout1->addWidget(wavpackUseAlmostLosslessCheckBox);


        vboxLayout->addWidget(wacpackGroupBox);


        verticalLayout->addWidget(groupBox);

        groupBox_2 = new QGroupBox(RecordingConfigPage);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        vboxLayout2 = new QVBoxLayout(groupBox_2);
#ifndef Q_OS_MAC
        vboxLayout2->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout2->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout2->setObjectName(QStringLiteral("vboxLayout2"));
        useResamplingCheckBox = new QCheckBox(groupBox_2);
        useResamplingCheckBox->setObjectName(QStringLiteral("useResamplingCheckBox"));

        vboxLayout2->addWidget(useResamplingCheckBox);

        hboxLayout2 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout2->setSpacing(6);
#endif
        hboxLayout2->setContentsMargins(0, 0, 0, 0);
        hboxLayout2->setObjectName(QStringLiteral("hboxLayout2"));
        vboxLayout3 = new QVBoxLayout();
#ifndef Q_OS_MAC
        vboxLayout3->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout3->setContentsMargins(0, 0, 0, 0);
#endif
        vboxLayout3->setObjectName(QStringLiteral("vboxLayout3"));
        label_3 = new QLabel(groupBox_2);
        label_3->setObjectName(QStringLiteral("label_3"));

        vboxLayout3->addWidget(label_3);

        label_2 = new QLabel(groupBox_2);
        label_2->setObjectName(QStringLiteral("label_2"));

        vboxLayout3->addWidget(label_2);


        hboxLayout2->addLayout(vboxLayout3);

        vboxLayout4 = new QVBoxLayout();
#ifndef Q_OS_MAC
        vboxLayout4->setSpacing(6);
#endif
        vboxLayout4->setContentsMargins(0, 0, 0, 0);
        vboxLayout4->setObjectName(QStringLiteral("vboxLayout4"));
        ontheflyResampleComboBox = new QComboBox(groupBox_2);
        ontheflyResampleComboBox->addItem(QString());
        ontheflyResampleComboBox->addItem(QString());
        ontheflyResampleComboBox->addItem(QString());
        ontheflyResampleComboBox->addItem(QString());
        ontheflyResampleComboBox->setObjectName(QStringLiteral("ontheflyResampleComboBox"));

        vboxLayout4->addWidget(ontheflyResampleComboBox);

        exportDefaultResampleQualityComboBox = new QComboBox(groupBox_2);
        exportDefaultResampleQualityComboBox->addItem(QString());
        exportDefaultResampleQualityComboBox->addItem(QString());
        exportDefaultResampleQualityComboBox->addItem(QString());
        exportDefaultResampleQualityComboBox->addItem(QString());
        exportDefaultResampleQualityComboBox->setObjectName(QStringLiteral("exportDefaultResampleQualityComboBox"));

        vboxLayout4->addWidget(exportDefaultResampleQualityComboBox);


        hboxLayout2->addLayout(vboxLayout4);


        vboxLayout2->addLayout(hboxLayout2);


        verticalLayout->addWidget(groupBox_2);

        verticalSpacer = new QSpacerItem(20, 3, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);


        retranslateUi(RecordingConfigPage);

        QMetaObject::connectSlotsByName(RecordingConfigPage);
    } // setupUi

    void retranslateUi(QWidget *RecordingConfigPage)
    {
        RecordingConfigPage->setWindowTitle(QApplication::translate("RecordingConfigPage", "Form", nullptr));
        groupBox->setTitle(QApplication::translate("RecordingConfigPage", "Recording", nullptr));
#ifndef QT_NO_TOOLTIP
        label->setToolTip(QApplication::translate("RecordingConfigPage", "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">WAV</span> : A format without compression. Uses more hard disk space then compressed encoding formats, but needs very little cpu.</p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Limits : ~ 1.7 hours of recording time @ Stereo - 44.1 KHz</p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-i"
                        "ndent:0px;\"></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">WavPack</span> : A format with compression, up to 2 times smaller compared to WAV, and no quality loss. Uses significantly more cpu then WAV, but with modern (year 2005 and above) cpu's this shouldn't be a problem.</p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Using this format stresses the hard disk much less, increasing hard disk live, and less chance of saturating the hard disk bandwidth.</p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px;"
                        " margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Limits : ~ 5 hours recording time @ Stereo - 44.1 KHz</p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">WAV64</span> : WAV format with 64 bit header, support by other programs is currently limited.</p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"></p>\n"
"<p style=\" margi"
                        "n-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Limits : None, thousands of hours of recording time, supposed that your hard disk can hold it.</p></body></html>", nullptr));
#endif // QT_NO_TOOLTIP
        label->setText(QApplication::translate("RecordingConfigPage", "Encoding format", nullptr));
        wacpackGroupBox->setTitle(QApplication::translate("RecordingConfigPage", "WavPack options", nullptr));
        label_4->setText(QApplication::translate("RecordingConfigPage", "Compression type", nullptr));
#ifndef QT_NO_TOOLTIP
        wavpackUseAlmostLosslessCheckBox->setToolTip(QApplication::translate("RecordingConfigPage", "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">This option reduces the storage of some floating-point data files by up to about 10% by eliminating some information that has virtually no effect on the audio data. </p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">While this does technically make the compression lossy, it retains all the advantages of floating point data (&gt;600 dB of dynamic range, no clipping, and 25 bits of resolution). </p>\n"
"<p style="
                        "\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">This also affects large integer compression by limiting the resolution to 24 bits.</p></body></html>", nullptr));
#endif // QT_NO_TOOLTIP
        wavpackUseAlmostLosslessCheckBox->setText(QApplication::translate("RecordingConfigPage", "Skip WVX for extra compression (semi-lossless)", nullptr));
        groupBox_2->setTitle(QApplication::translate("RecordingConfigPage", "Resampling", nullptr));
        useResamplingCheckBox->setText(QApplication::translate("RecordingConfigPage", "Enable on the fly resampling (Playback only)", nullptr));
        label_3->setText(QApplication::translate("RecordingConfigPage", "On the fly resample quality", nullptr));
        label_2->setText(QApplication::translate("RecordingConfigPage", "Default export resample quality", nullptr));
        ontheflyResampleComboBox->setItemText(0, QApplication::translate("RecordingConfigPage", "Best", nullptr));
        ontheflyResampleComboBox->setItemText(1, QApplication::translate("RecordingConfigPage", "High", nullptr));
        ontheflyResampleComboBox->setItemText(2, QApplication::translate("RecordingConfigPage", "Medium", nullptr));
        ontheflyResampleComboBox->setItemText(3, QApplication::translate("RecordingConfigPage", "Fast", nullptr));

        exportDefaultResampleQualityComboBox->setItemText(0, QApplication::translate("RecordingConfigPage", "Best", nullptr));
        exportDefaultResampleQualityComboBox->setItemText(1, QApplication::translate("RecordingConfigPage", "High", nullptr));
        exportDefaultResampleQualityComboBox->setItemText(2, QApplication::translate("RecordingConfigPage", "Medium", nullptr));
        exportDefaultResampleQualityComboBox->setItemText(3, QApplication::translate("RecordingConfigPage", "Fast", nullptr));

    } // retranslateUi

};

namespace Ui {
    class RecordingConfigPage: public Ui_RecordingConfigPage {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_RECORDINGCONFIGPAGE_H
