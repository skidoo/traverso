/********************************************************************************
** Form generated from reading UI file 'AudioDriverConfigPage.ui'
**
** Created by: Qt User Interface Compiler version 5.11.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_AUDIODRIVERCONFIGPAGE_H
#define UI_AUDIODRIVERCONFIGPAGE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_AudioDriverConfigPage
{
public:
    QVBoxLayout *vboxLayout;
    QGroupBox *groupBox;
    QHBoxLayout *hboxLayout;
    QLabel *label;
    QComboBox *driverCombo;
    QGroupBox *driverConfigGroupBox;
    QVBoxLayout *vboxLayout1;
    QHBoxLayout *hboxLayout1;
    QLabel *label_2;
    QComboBox *duplexComboBox;
    QHBoxLayout *hboxLayout2;
    QLabel *label_2_2;
    QComboBox *rateComboBox;
    QHBoxLayout *hboxLayout3;
    QLabel *label_7;
    QComboBox *latencyComboBox;
    QHBoxLayout *hboxLayout4;
    QSpacerItem *spacerItem;
    QPushButton *restartDriverButton;
    QGroupBox *jackGroupBox;
    QVBoxLayout *vboxLayout2;
    QCheckBox *jackTransportCheckBox;
    QSpacerItem *spacerItem1;

    void setupUi(QWidget *AudioDriverConfigPage)
    {
        if (AudioDriverConfigPage->objectName().isEmpty())
            AudioDriverConfigPage->setObjectName(QStringLiteral("AudioDriverConfigPage"));
        AudioDriverConfigPage->resize(250, 344);
        vboxLayout = new QVBoxLayout(AudioDriverConfigPage);
        vboxLayout->setObjectName(QStringLiteral("vboxLayout"));
        groupBox = new QGroupBox(AudioDriverConfigPage);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        hboxLayout = new QHBoxLayout(groupBox);
        hboxLayout->setObjectName(QStringLiteral("hboxLayout"));
        label = new QLabel(groupBox);
        label->setObjectName(QStringLiteral("label"));

        hboxLayout->addWidget(label);

        driverCombo = new QComboBox(groupBox);
        driverCombo->setObjectName(QStringLiteral("driverCombo"));

        hboxLayout->addWidget(driverCombo);


        vboxLayout->addWidget(groupBox);

        driverConfigGroupBox = new QGroupBox(AudioDriverConfigPage);
        driverConfigGroupBox->setObjectName(QStringLiteral("driverConfigGroupBox"));
        vboxLayout1 = new QVBoxLayout(driverConfigGroupBox);
        vboxLayout1->setObjectName(QStringLiteral("vboxLayout1"));
        hboxLayout1 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout1->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout1->setContentsMargins(0, 0, 0, 0);
#endif
        hboxLayout1->setObjectName(QStringLiteral("hboxLayout1"));
        label_2 = new QLabel(driverConfigGroupBox);
        label_2->setObjectName(QStringLiteral("label_2"));

        hboxLayout1->addWidget(label_2);

        duplexComboBox = new QComboBox(driverConfigGroupBox);
        duplexComboBox->addItem(QString());
        duplexComboBox->addItem(QString());
        duplexComboBox->addItem(QString());
        duplexComboBox->setObjectName(QStringLiteral("duplexComboBox"));

        hboxLayout1->addWidget(duplexComboBox);


        vboxLayout1->addLayout(hboxLayout1);

        hboxLayout2 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout2->setSpacing(6);
#endif
        hboxLayout2->setContentsMargins(0, 0, 0, 0);
        hboxLayout2->setObjectName(QStringLiteral("hboxLayout2"));
        label_2_2 = new QLabel(driverConfigGroupBox);
        label_2_2->setObjectName(QStringLiteral("label_2_2"));

        hboxLayout2->addWidget(label_2_2);

        rateComboBox = new QComboBox(driverConfigGroupBox);
        rateComboBox->addItem(QString());
        rateComboBox->addItem(QString());
        rateComboBox->addItem(QString());
        rateComboBox->addItem(QString());
        rateComboBox->addItem(QString());
        rateComboBox->addItem(QString());
        rateComboBox->setObjectName(QStringLiteral("rateComboBox"));

        hboxLayout2->addWidget(rateComboBox);


        vboxLayout1->addLayout(hboxLayout2);

        hboxLayout3 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout3->setSpacing(6);
#endif
        hboxLayout3->setContentsMargins(0, 0, 0, 0);
        hboxLayout3->setObjectName(QStringLiteral("hboxLayout3"));
        label_7 = new QLabel(driverConfigGroupBox);
        label_7->setObjectName(QStringLiteral("label_7"));

        hboxLayout3->addWidget(label_7);

        latencyComboBox = new QComboBox(driverConfigGroupBox);
        latencyComboBox->setObjectName(QStringLiteral("latencyComboBox"));

        hboxLayout3->addWidget(latencyComboBox);


        vboxLayout1->addLayout(hboxLayout3);

        hboxLayout4 = new QHBoxLayout();
        hboxLayout4->setObjectName(QStringLiteral("hboxLayout4"));
        spacerItem = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout4->addItem(spacerItem);

        restartDriverButton = new QPushButton(driverConfigGroupBox);
        restartDriverButton->setObjectName(QStringLiteral("restartDriverButton"));

        hboxLayout4->addWidget(restartDriverButton);


        vboxLayout1->addLayout(hboxLayout4);


        vboxLayout->addWidget(driverConfigGroupBox);

        jackGroupBox = new QGroupBox(AudioDriverConfigPage);
        jackGroupBox->setObjectName(QStringLiteral("jackGroupBox"));
        vboxLayout2 = new QVBoxLayout(jackGroupBox);
#ifndef Q_OS_MAC
        vboxLayout2->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout2->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout2->setObjectName(QStringLiteral("vboxLayout2"));
        jackTransportCheckBox = new QCheckBox(jackGroupBox);
        jackTransportCheckBox->setObjectName(QStringLiteral("jackTransportCheckBox"));

        vboxLayout2->addWidget(jackTransportCheckBox);


        vboxLayout->addWidget(jackGroupBox);

        spacerItem1 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        vboxLayout->addItem(spacerItem1);


        retranslateUi(AudioDriverConfigPage);

        QMetaObject::connectSlotsByName(AudioDriverConfigPage);
    } // setupUi

    void retranslateUi(QWidget *AudioDriverConfigPage)
    {
        AudioDriverConfigPage->setWindowTitle(QApplication::translate("AudioDriverConfigPage", "Form", nullptr));
        groupBox->setTitle(QApplication::translate("AudioDriverConfigPage", "Driver Selection", nullptr));
        label->setText(QApplication::translate("AudioDriverConfigPage", "Driver:", nullptr));
        driverConfigGroupBox->setTitle(QApplication::translate("AudioDriverConfigPage", "Configure driver", nullptr));
#ifndef QT_NO_TOOLTIP
        label_2->setToolTip(QApplication::translate("AudioDriverConfigPage", "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">Duplex mode:</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Defines if both the Playback and Capture buses </p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">of your soundcard are to be used, </p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">or only the Playback or Capture bus(es).</p></body></html>", nullptr));
#endif // QT_NO_TOOLTIP
        label_2->setText(QApplication::translate("AudioDriverConfigPage", "Duplex mode", nullptr));
        duplexComboBox->setItemText(0, QApplication::translate("AudioDriverConfigPage", "Full", nullptr));
        duplexComboBox->setItemText(1, QApplication::translate("AudioDriverConfigPage", "Playback", nullptr));
        duplexComboBox->setItemText(2, QApplication::translate("AudioDriverConfigPage", "Capture", nullptr));

#ifndef QT_NO_TOOLTIP
        label_2_2->setToolTip(QApplication::translate("AudioDriverConfigPage", "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">Sample rate:</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">The sample rate used by the audio card.</p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">New projects will use this samplerate as </p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block"
                        "-indent:0; text-indent:0px;\">the project's sample rate on creation.</p></body></html>", nullptr));
#endif // QT_NO_TOOLTIP
        label_2_2->setText(QApplication::translate("AudioDriverConfigPage", "Sample rate", nullptr));
        rateComboBox->setItemText(0, QApplication::translate("AudioDriverConfigPage", "22050", nullptr));
        rateComboBox->setItemText(1, QApplication::translate("AudioDriverConfigPage", "32000", nullptr));
        rateComboBox->setItemText(2, QApplication::translate("AudioDriverConfigPage", "44100", nullptr));
        rateComboBox->setItemText(3, QApplication::translate("AudioDriverConfigPage", "48000", nullptr));
        rateComboBox->setItemText(4, QApplication::translate("AudioDriverConfigPage", "88200", nullptr));
        rateComboBox->setItemText(5, QApplication::translate("AudioDriverConfigPage", "96000", nullptr));

#ifndef QT_NO_TOOLTIP
        label_7->setToolTip(QApplication::translate("AudioDriverConfigPage", "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">Buffer latency:</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">The latency introduced by the size of the audio buffers.</p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Some people need low latencies, if you don't need it, </p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left"
                        ":0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">or don't know what it means, please leave the default setting!</p></body></html>", nullptr));
#endif // QT_NO_TOOLTIP
        label_7->setText(QApplication::translate("AudioDriverConfigPage", "Buffer latency (ms)", nullptr));
        restartDriverButton->setText(QApplication::translate("AudioDriverConfigPage", "Restart Driver", nullptr));
        jackGroupBox->setTitle(QApplication::translate("AudioDriverConfigPage", "Jack", nullptr));
        jackTransportCheckBox->setText(QApplication::translate("AudioDriverConfigPage", "Enable Jack transport control", nullptr));
    } // retranslateUi

};

namespace Ui {
    class AudioDriverConfigPage: public Ui_AudioDriverConfigPage {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_AUDIODRIVERCONFIGPAGE_H
