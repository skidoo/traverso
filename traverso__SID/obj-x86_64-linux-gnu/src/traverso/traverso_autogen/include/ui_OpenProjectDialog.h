/********************************************************************************
** Form generated from reading UI file 'OpenProjectDialog.ui'
**
** Created by: Qt User Interface Compiler version 5.11.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_OPENPROJECTDIALOG_H
#define UI_OPENPROJECTDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_OpenProjectDialog
{
public:
    QVBoxLayout *vboxLayout;
    QTreeWidget *projectListView;
    QGroupBox *groupBox_2;
    QVBoxLayout *vboxLayout1;
    QHBoxLayout *hboxLayout;
    QLineEdit *selectedProjectName;
    QPushButton *loadProjectButton;
    QHBoxLayout *hboxLayout1;
    QPushButton *deleteProjectbutton;
    QSpacerItem *spacerItem;
    QHBoxLayout *hboxLayout2;
    QPushButton *projectDirSelectButton;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *OpenProjectDialog)
    {
        if (OpenProjectDialog->objectName().isEmpty())
            OpenProjectDialog->setObjectName(QStringLiteral("OpenProjectDialog"));
        OpenProjectDialog->resize(269, 356);
        vboxLayout = new QVBoxLayout(OpenProjectDialog);
#ifndef Q_OS_MAC
        vboxLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout->setObjectName(QStringLiteral("vboxLayout"));
        projectListView = new QTreeWidget(OpenProjectDialog);
        projectListView->setObjectName(QStringLiteral("projectListView"));

        vboxLayout->addWidget(projectListView);

        groupBox_2 = new QGroupBox(OpenProjectDialog);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        groupBox_2->setMinimumSize(QSize(16, 90));
        vboxLayout1 = new QVBoxLayout(groupBox_2);
#ifndef Q_OS_MAC
        vboxLayout1->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout1->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout1->setObjectName(QStringLiteral("vboxLayout1"));
        hboxLayout = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout->setContentsMargins(0, 0, 0, 0);
#endif
        hboxLayout->setObjectName(QStringLiteral("hboxLayout"));
        selectedProjectName = new QLineEdit(groupBox_2);
        selectedProjectName->setObjectName(QStringLiteral("selectedProjectName"));
        selectedProjectName->setEnabled(false);
        QSizePolicy sizePolicy(static_cast<QSizePolicy::Policy>(7), static_cast<QSizePolicy::Policy>(0));
        sizePolicy.setHorizontalStretch(1);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(selectedProjectName->sizePolicy().hasHeightForWidth());
        selectedProjectName->setSizePolicy(sizePolicy);
        selectedProjectName->setMinimumSize(QSize(50, 0));
        selectedProjectName->setMouseTracking(true);
        selectedProjectName->setReadOnly(false);

        hboxLayout->addWidget(selectedProjectName);

        loadProjectButton = new QPushButton(groupBox_2);
        loadProjectButton->setObjectName(QStringLiteral("loadProjectButton"));

        hboxLayout->addWidget(loadProjectButton);


        vboxLayout1->addLayout(hboxLayout);

        hboxLayout1 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout1->setSpacing(6);
#endif
        hboxLayout1->setContentsMargins(0, 0, 0, 0);
        hboxLayout1->setObjectName(QStringLiteral("hboxLayout1"));
        deleteProjectbutton = new QPushButton(groupBox_2);
        deleteProjectbutton->setObjectName(QStringLiteral("deleteProjectbutton"));

        hboxLayout1->addWidget(deleteProjectbutton);

        spacerItem = new QSpacerItem(61, 27, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout1->addItem(spacerItem);


        vboxLayout1->addLayout(hboxLayout1);


        vboxLayout->addWidget(groupBox_2);

        hboxLayout2 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout2->setSpacing(6);
#endif
        hboxLayout2->setContentsMargins(0, 0, 0, 0);
        hboxLayout2->setObjectName(QStringLiteral("hboxLayout2"));
        projectDirSelectButton = new QPushButton(OpenProjectDialog);
        projectDirSelectButton->setObjectName(QStringLiteral("projectDirSelectButton"));

        hboxLayout2->addWidget(projectDirSelectButton);

        buttonBox = new QDialogButtonBox(OpenProjectDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Close);

        hboxLayout2->addWidget(buttonBox);


        vboxLayout->addLayout(hboxLayout2);


        retranslateUi(OpenProjectDialog);
        QObject::connect(buttonBox, SIGNAL(clicked(QAbstractButton*)), OpenProjectDialog, SLOT(accept()));
        QObject::connect(projectListView, SIGNAL(activated(QModelIndex)), loadProjectButton, SLOT(click()));

        QMetaObject::connectSlotsByName(OpenProjectDialog);
    } // setupUi

    void retranslateUi(QDialog *OpenProjectDialog)
    {
        OpenProjectDialog->setWindowTitle(QApplication::translate("OpenProjectDialog", "Open Project", nullptr));
        groupBox_2->setTitle(QApplication::translate("OpenProjectDialog", "Selected Project", nullptr));
        loadProjectButton->setText(QApplication::translate("OpenProjectDialog", "Load", nullptr));
        deleteProjectbutton->setText(QApplication::translate("OpenProjectDialog", "Delete", nullptr));
        projectDirSelectButton->setText(QApplication::translate("OpenProjectDialog", "Select Project Dir", nullptr));
    } // retranslateUi

};

namespace Ui {
    class OpenProjectDialog: public Ui_OpenProjectDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_OPENPROJECTDIALOG_H
