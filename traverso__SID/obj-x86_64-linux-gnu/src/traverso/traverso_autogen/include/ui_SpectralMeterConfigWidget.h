/********************************************************************************
** Form generated from reading UI file 'SpectralMeterConfigWidget.ui'
**
** Created by: Qt User Interface Compiler version 5.11.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SPECTRALMETERCONFIGWIDGET_H
#define UI_SPECTRALMETERCONFIGWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SpectralMeterConfigWidget
{
public:
    QVBoxLayout *vboxLayout;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QCheckBox *checkBoxAverage;
    QSpinBox *spinBoxNumBands;
    QLabel *label_2;
    QSpinBox *spinBoxLowerDb;
    QLabel *label_6;
    QSpinBox *spinBoxUpperDb;
    QLabel *label_7;
    QSpinBox *spinBoxLowerFreq;
    QLabel *label;
    QSpinBox *spinBoxUpperFreq;
    QLabel *label_3;
    QGroupBox *groupBoxAdvanced;
    QGridLayout *gridLayout1;
    QLabel *label_4;
    QComboBox *comboBoxFftSize;
    QComboBox *comboBoxWindowing;
    QLabel *label_5;
    QHBoxLayout *hboxLayout;
    QPushButton *buttonAdvanced;
    QSpacerItem *spacerItem;
    QPushButton *buttonApply;
    QPushButton *buttonClose;

    void setupUi(QWidget *SpectralMeterConfigWidget)
    {
        if (SpectralMeterConfigWidget->objectName().isEmpty())
            SpectralMeterConfigWidget->setObjectName(QStringLiteral("SpectralMeterConfigWidget"));
        SpectralMeterConfigWidget->resize(377, 364);
        vboxLayout = new QVBoxLayout(SpectralMeterConfigWidget);
#ifndef Q_OS_MAC
        vboxLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout->setObjectName(QStringLiteral("vboxLayout"));
        groupBox = new QGroupBox(SpectralMeterConfigWidget);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        gridLayout = new QGridLayout(groupBox);
#ifndef Q_OS_MAC
        gridLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        gridLayout->setContentsMargins(9, 9, 9, 9);
#endif
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        checkBoxAverage = new QCheckBox(groupBox);
        checkBoxAverage->setObjectName(QStringLiteral("checkBoxAverage"));

        gridLayout->addWidget(checkBoxAverage, 5, 0, 1, 2);

        spinBoxNumBands = new QSpinBox(groupBox);
        spinBoxNumBands->setObjectName(QStringLiteral("spinBoxNumBands"));
        spinBoxNumBands->setMaximum(2048);
        spinBoxNumBands->setMinimum(4);
        spinBoxNumBands->setValue(16);

        gridLayout->addWidget(spinBoxNumBands, 4, 1, 1, 1);

        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout->addWidget(label_2, 4, 0, 1, 1);

        spinBoxLowerDb = new QSpinBox(groupBox);
        spinBoxLowerDb->setObjectName(QStringLiteral("spinBoxLowerDb"));
        spinBoxLowerDb->setMaximum(0);
        spinBoxLowerDb->setMinimum(-140);
        spinBoxLowerDb->setValue(-140);

        gridLayout->addWidget(spinBoxLowerDb, 3, 1, 1, 1);

        label_6 = new QLabel(groupBox);
        label_6->setObjectName(QStringLiteral("label_6"));

        gridLayout->addWidget(label_6, 3, 0, 1, 1);

        spinBoxUpperDb = new QSpinBox(groupBox);
        spinBoxUpperDb->setObjectName(QStringLiteral("spinBoxUpperDb"));
        spinBoxUpperDb->setMaximum(12);
        spinBoxUpperDb->setMinimum(-24);

        gridLayout->addWidget(spinBoxUpperDb, 2, 1, 1, 1);

        label_7 = new QLabel(groupBox);
        label_7->setObjectName(QStringLiteral("label_7"));

        gridLayout->addWidget(label_7, 2, 0, 1, 1);

        spinBoxLowerFreq = new QSpinBox(groupBox);
        spinBoxLowerFreq->setObjectName(QStringLiteral("spinBoxLowerFreq"));
        spinBoxLowerFreq->setMaximum(20000);
        spinBoxLowerFreq->setMinimum(1);
        spinBoxLowerFreq->setValue(20);

        gridLayout->addWidget(spinBoxLowerFreq, 1, 1, 1, 1);

        label = new QLabel(groupBox);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 1, 0, 1, 1);

        spinBoxUpperFreq = new QSpinBox(groupBox);
        spinBoxUpperFreq->setObjectName(QStringLiteral("spinBoxUpperFreq"));
        spinBoxUpperFreq->setMaximum(96000);
        spinBoxUpperFreq->setMinimum(100);
        spinBoxUpperFreq->setValue(20000);

        gridLayout->addWidget(spinBoxUpperFreq, 0, 1, 1, 1);

        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout->addWidget(label_3, 0, 0, 1, 1);


        vboxLayout->addWidget(groupBox);

        groupBoxAdvanced = new QGroupBox(SpectralMeterConfigWidget);
        groupBoxAdvanced->setObjectName(QStringLiteral("groupBoxAdvanced"));
        gridLayout1 = new QGridLayout(groupBoxAdvanced);
#ifndef Q_OS_MAC
        gridLayout1->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        gridLayout1->setContentsMargins(9, 9, 9, 9);
#endif
        gridLayout1->setObjectName(QStringLiteral("gridLayout1"));
        label_4 = new QLabel(groupBoxAdvanced);
        label_4->setObjectName(QStringLiteral("label_4"));

        gridLayout1->addWidget(label_4, 0, 0, 1, 1);

        comboBoxFftSize = new QComboBox(groupBoxAdvanced);
        comboBoxFftSize->addItem(QString());
        comboBoxFftSize->addItem(QString());
        comboBoxFftSize->addItem(QString());
        comboBoxFftSize->addItem(QString());
        comboBoxFftSize->addItem(QString());
        comboBoxFftSize->addItem(QString());
        comboBoxFftSize->setObjectName(QStringLiteral("comboBoxFftSize"));

        gridLayout1->addWidget(comboBoxFftSize, 0, 1, 1, 1);

        comboBoxWindowing = new QComboBox(groupBoxAdvanced);
        comboBoxWindowing->addItem(QString());
        comboBoxWindowing->addItem(QString());
        comboBoxWindowing->addItem(QString());
        comboBoxWindowing->addItem(QString());
        comboBoxWindowing->setObjectName(QStringLiteral("comboBoxWindowing"));

        gridLayout1->addWidget(comboBoxWindowing, 1, 1, 1, 1);

        label_5 = new QLabel(groupBoxAdvanced);
        label_5->setObjectName(QStringLiteral("label_5"));

        gridLayout1->addWidget(label_5, 1, 0, 1, 1);


        vboxLayout->addWidget(groupBoxAdvanced);

        hboxLayout = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout->setSpacing(6);
#endif
        hboxLayout->setContentsMargins(0, 0, 0, 0);
        hboxLayout->setObjectName(QStringLiteral("hboxLayout"));
        buttonAdvanced = new QPushButton(SpectralMeterConfigWidget);
        buttonAdvanced->setObjectName(QStringLiteral("buttonAdvanced"));
        buttonAdvanced->setCheckable(true);

        hboxLayout->addWidget(buttonAdvanced);

        spacerItem = new QSpacerItem(271, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacerItem);

        buttonApply = new QPushButton(SpectralMeterConfigWidget);
        buttonApply->setObjectName(QStringLiteral("buttonApply"));
        buttonApply->setAutoDefault(true);

        hboxLayout->addWidget(buttonApply);

        buttonClose = new QPushButton(SpectralMeterConfigWidget);
        buttonClose->setObjectName(QStringLiteral("buttonClose"));

        hboxLayout->addWidget(buttonClose);


        vboxLayout->addLayout(hboxLayout);

        QWidget::setTabOrder(spinBoxUpperFreq, spinBoxLowerFreq);
        QWidget::setTabOrder(spinBoxLowerFreq, spinBoxUpperDb);
        QWidget::setTabOrder(spinBoxUpperDb, spinBoxLowerDb);
        QWidget::setTabOrder(spinBoxLowerDb, spinBoxNumBands);
        QWidget::setTabOrder(spinBoxNumBands, checkBoxAverage);
        QWidget::setTabOrder(checkBoxAverage, comboBoxFftSize);
        QWidget::setTabOrder(comboBoxFftSize, comboBoxWindowing);
        QWidget::setTabOrder(comboBoxWindowing, buttonApply);
        QWidget::setTabOrder(buttonApply, buttonClose);
        QWidget::setTabOrder(buttonClose, buttonAdvanced);

        retranslateUi(SpectralMeterConfigWidget);
        QObject::connect(checkBoxAverage, SIGNAL(clicked()), buttonApply, SLOT(animateClick()));

        comboBoxFftSize->setCurrentIndex(3);


        QMetaObject::connectSlotsByName(SpectralMeterConfigWidget);
    } // setupUi

    void retranslateUi(QWidget *SpectralMeterConfigWidget)
    {
        SpectralMeterConfigWidget->setWindowTitle(QApplication::translate("SpectralMeterConfigWidget", "FFT-Spectrum Configuration", nullptr));
        groupBox->setTitle(QApplication::translate("SpectralMeterConfigWidget", "Frequency Range", nullptr));
        checkBoxAverage->setText(QApplication::translate("SpectralMeterConfigWidget", "Show average spectrum", nullptr));
        label_2->setText(QApplication::translate("SpectralMeterConfigWidget", "Number of bands:", nullptr));
        label_6->setText(QApplication::translate("SpectralMeterConfigWidget", "Lower dB value:", nullptr));
        label_7->setText(QApplication::translate("SpectralMeterConfigWidget", "Upper dB value:", nullptr));
        spinBoxLowerFreq->setSuffix(QApplication::translate("SpectralMeterConfigWidget", " Hz", nullptr));
        label->setText(QApplication::translate("SpectralMeterConfigWidget", "Lower Limit:", nullptr));
        spinBoxUpperFreq->setSuffix(QApplication::translate("SpectralMeterConfigWidget", " Hz", nullptr));
        label_3->setText(QApplication::translate("SpectralMeterConfigWidget", "Upper Limit:", nullptr));
        groupBoxAdvanced->setTitle(QApplication::translate("SpectralMeterConfigWidget", "Advanced FFT Options", nullptr));
        label_4->setText(QApplication::translate("SpectralMeterConfigWidget", "FFT Size:", nullptr));
        comboBoxFftSize->setItemText(0, QApplication::translate("SpectralMeterConfigWidget", "256", nullptr));
        comboBoxFftSize->setItemText(1, QApplication::translate("SpectralMeterConfigWidget", "512", nullptr));
        comboBoxFftSize->setItemText(2, QApplication::translate("SpectralMeterConfigWidget", "1024", nullptr));
        comboBoxFftSize->setItemText(3, QApplication::translate("SpectralMeterConfigWidget", "2048", nullptr));
        comboBoxFftSize->setItemText(4, QApplication::translate("SpectralMeterConfigWidget", "4096", nullptr));
        comboBoxFftSize->setItemText(5, QApplication::translate("SpectralMeterConfigWidget", "8192", nullptr));

        comboBoxWindowing->setItemText(0, QApplication::translate("SpectralMeterConfigWidget", "Rectangle", nullptr));
        comboBoxWindowing->setItemText(1, QApplication::translate("SpectralMeterConfigWidget", "Hanning", nullptr));
        comboBoxWindowing->setItemText(2, QApplication::translate("SpectralMeterConfigWidget", "Hamming", nullptr));
        comboBoxWindowing->setItemText(3, QApplication::translate("SpectralMeterConfigWidget", "Blackman", nullptr));

        label_5->setText(QApplication::translate("SpectralMeterConfigWidget", "Windowing function:", nullptr));
        buttonAdvanced->setText(QApplication::translate("SpectralMeterConfigWidget", "Advanced", nullptr));
        buttonApply->setText(QApplication::translate("SpectralMeterConfigWidget", "Apply", nullptr));
        buttonClose->setText(QApplication::translate("SpectralMeterConfigWidget", "&Close", nullptr));
    } // retranslateUi

};

namespace Ui {
    class SpectralMeterConfigWidget: public Ui_SpectralMeterConfigWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SPECTRALMETERCONFIGWIDGET_H
