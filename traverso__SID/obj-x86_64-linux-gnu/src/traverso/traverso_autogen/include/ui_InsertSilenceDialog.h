/********************************************************************************
** Form generated from reading UI file 'InsertSilenceDialog.ui'
**
** Created by: Qt User Interface Compiler version 5.11.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_INSERTSILENCEDIALOG_H
#define UI_INSERTSILENCEDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_InsertSilenceDialog
{
public:
    QVBoxLayout *vboxLayout;
    QHBoxLayout *hboxLayout;
    QLabel *label;
    QDoubleSpinBox *lengthSpinBox;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *InsertSilenceDialog)
    {
        if (InsertSilenceDialog->objectName().isEmpty())
            InsertSilenceDialog->setObjectName(QStringLiteral("InsertSilenceDialog"));
        InsertSilenceDialog->resize(261, 87);
        vboxLayout = new QVBoxLayout(InsertSilenceDialog);
#ifndef Q_OS_MAC
        vboxLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout->setObjectName(QStringLiteral("vboxLayout"));
        hboxLayout = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout->setContentsMargins(0, 0, 0, 0);
#endif
        hboxLayout->setObjectName(QStringLiteral("hboxLayout"));
        label = new QLabel(InsertSilenceDialog);
        label->setObjectName(QStringLiteral("label"));

        hboxLayout->addWidget(label);

        lengthSpinBox = new QDoubleSpinBox(InsertSilenceDialog);
        lengthSpinBox->setObjectName(QStringLiteral("lengthSpinBox"));
        lengthSpinBox->setDecimals(3);
        lengthSpinBox->setMaximum(3600);
        lengthSpinBox->setMinimum(0.001);
        lengthSpinBox->setValue(10);

        hboxLayout->addWidget(lengthSpinBox);


        vboxLayout->addLayout(hboxLayout);

        buttonBox = new QDialogButtonBox(InsertSilenceDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::NoButton|QDialogButtonBox::Ok);

        vboxLayout->addWidget(buttonBox);


        retranslateUi(InsertSilenceDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), InsertSilenceDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), InsertSilenceDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(InsertSilenceDialog);
    } // setupUi

    void retranslateUi(QDialog *InsertSilenceDialog)
    {
        InsertSilenceDialog->setWindowTitle(QApplication::translate("InsertSilenceDialog", "Insert Silence", nullptr));
        label->setText(QApplication::translate("InsertSilenceDialog", "Insert Silence (seconds):", nullptr));
    } // retranslateUi

};

namespace Ui {
    class InsertSilenceDialog: public Ui_InsertSilenceDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_INSERTSILENCEDIALOG_H
