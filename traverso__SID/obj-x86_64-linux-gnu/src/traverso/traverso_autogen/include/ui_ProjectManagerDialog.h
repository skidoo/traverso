/********************************************************************************
** Form generated from reading UI file 'ProjectManagerDialog.ui'
**
** Created by: Qt User Interface Compiler version 5.11.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PROJECTMANAGERDIALOG_H
#define UI_PROJECTMANAGERDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ProjectManagerDialog
{
public:
    QVBoxLayout *vboxLayout;
    QTabWidget *tabWidget;
    QWidget *tab_4;
    QGridLayout *gridLayout;
    QGroupBox *groupBox_3;
    QVBoxLayout *vboxLayout1;
    QHBoxLayout *hboxLayout;
    QLabel *label_3;
    QLineEdit *lineEditTitle;
    QHBoxLayout *hboxLayout1;
    QLabel *label;
    QTextEdit *descriptionTextEdit;
    QHBoxLayout *hboxLayout2;
    QLabel *label_2;
    QLineEdit *lineEdit;
    QGroupBox *groupBox;
    QVBoxLayout *vboxLayout2;
    QPushButton *sheetsExportButton;
    QPushButton *exportTemplateButton;
    QSpacerItem *spacerItem;
    QSpacerItem *spacerItem1;
    QWidget *tab;
    QHBoxLayout *hboxLayout3;
    QTreeWidget *treeSheetWidget;
    QVBoxLayout *vboxLayout3;
    QGroupBox *groupBox_4;
    QVBoxLayout *vboxLayout4;
    QLineEdit *selectedSheetName;
    QHBoxLayout *hboxLayout4;
    QPushButton *deleteSheetButton;
    QSpacerItem *spacerItem2;
    QPushButton *renameSheetButton;
    QGroupBox *groupBox_2;
    QHBoxLayout *hboxLayout5;
    QHBoxLayout *hboxLayout6;
    QPushButton *createSheetButton;
    QSpacerItem *spacerItem3;
    QWidget *cdtext;
    QGridLayout *gridLayout1;
    QLabel *label_7;
    QLineEdit *lineEditPerformer;
    QLabel *label_4;
    QLineEdit *lineEditId;
    QLabel *label_6;
    QLineEdit *lineEditUPC;
    QLabel *label_5;
    QComboBox *comboBoxGenre;
    QSpacerItem *spacerItem4;
    QLabel *label_8;
    QLineEdit *lineEditArranger;
    QLabel *label_9;
    QLineEdit *lineEditSongwriter;
    QLabel *label_10;
    QLineEdit *lineEditMessage;
    QHBoxLayout *hboxLayout7;
    QHBoxLayout *hboxLayout8;
    QPushButton *undoButton;
    QPushButton *redoButton;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *ProjectManagerDialog)
    {
        if (ProjectManagerDialog->objectName().isEmpty())
            ProjectManagerDialog->setObjectName(QStringLiteral("ProjectManagerDialog"));
        ProjectManagerDialog->resize(581, 308);
        ProjectManagerDialog->setMaximumSize(QSize(600, 500));
        vboxLayout = new QVBoxLayout(ProjectManagerDialog);
#ifndef Q_OS_MAC
        vboxLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout->setObjectName(QStringLiteral("vboxLayout"));
        tabWidget = new QTabWidget(ProjectManagerDialog);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tab_4 = new QWidget();
        tab_4->setObjectName(QStringLiteral("tab_4"));
        gridLayout = new QGridLayout(tab_4);
#ifndef Q_OS_MAC
        gridLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        gridLayout->setContentsMargins(9, 9, 9, 9);
#endif
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        groupBox_3 = new QGroupBox(tab_4);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        vboxLayout1 = new QVBoxLayout(groupBox_3);
#ifndef Q_OS_MAC
        vboxLayout1->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout1->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout1->setObjectName(QStringLiteral("vboxLayout1"));
        hboxLayout = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout->setContentsMargins(0, 0, 0, 0);
#endif
        hboxLayout->setObjectName(QStringLiteral("hboxLayout"));
        label_3 = new QLabel(groupBox_3);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setMinimumSize(QSize(100, 0));

        hboxLayout->addWidget(label_3);

        lineEditTitle = new QLineEdit(groupBox_3);
        lineEditTitle->setObjectName(QStringLiteral("lineEditTitle"));
        lineEditTitle->setEnabled(true);
        lineEditTitle->setAcceptDrops(true);

        hboxLayout->addWidget(lineEditTitle);


        vboxLayout1->addLayout(hboxLayout);

        hboxLayout1 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout1->setSpacing(6);
#endif
        hboxLayout1->setContentsMargins(0, 0, 0, 0);
        hboxLayout1->setObjectName(QStringLiteral("hboxLayout1"));
        label = new QLabel(groupBox_3);
        label->setObjectName(QStringLiteral("label"));
        label->setMinimumSize(QSize(100, 0));

        hboxLayout1->addWidget(label);

        descriptionTextEdit = new QTextEdit(groupBox_3);
        descriptionTextEdit->setObjectName(QStringLiteral("descriptionTextEdit"));
        descriptionTextEdit->setMaximumSize(QSize(16777215, 50));

        hboxLayout1->addWidget(descriptionTextEdit);


        vboxLayout1->addLayout(hboxLayout1);

        hboxLayout2 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout2->setSpacing(6);
#endif
        hboxLayout2->setContentsMargins(0, 0, 0, 0);
        hboxLayout2->setObjectName(QStringLiteral("hboxLayout2"));
        label_2 = new QLabel(groupBox_3);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setMinimumSize(QSize(100, 0));

        hboxLayout2->addWidget(label_2);

        lineEdit = new QLineEdit(groupBox_3);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));

        hboxLayout2->addWidget(lineEdit);


        vboxLayout1->addLayout(hboxLayout2);


        gridLayout->addWidget(groupBox_3, 0, 0, 2, 1);

        groupBox = new QGroupBox(tab_4);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setMinimumSize(QSize(150, 0));
        vboxLayout2 = new QVBoxLayout(groupBox);
        vboxLayout2->setSpacing(9);
#ifndef Q_OS_MAC
        vboxLayout2->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout2->setObjectName(QStringLiteral("vboxLayout2"));
        sheetsExportButton = new QPushButton(groupBox);
        sheetsExportButton->setObjectName(QStringLiteral("sheetsExportButton"));

        vboxLayout2->addWidget(sheetsExportButton);

        exportTemplateButton = new QPushButton(groupBox);
        exportTemplateButton->setObjectName(QStringLiteral("exportTemplateButton"));

        vboxLayout2->addWidget(exportTemplateButton);


        gridLayout->addWidget(groupBox, 0, 1, 1, 1);

        spacerItem = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(spacerItem, 1, 1, 2, 1);

        spacerItem1 = new QSpacerItem(385, 20, QSizePolicy::Minimum, QSizePolicy::Maximum);

        gridLayout->addItem(spacerItem1, 2, 0, 1, 1);

        tabWidget->addTab(tab_4, QString());
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        hboxLayout3 = new QHBoxLayout(tab);
#ifndef Q_OS_MAC
        hboxLayout3->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout3->setContentsMargins(9, 9, 9, 9);
#endif
        hboxLayout3->setObjectName(QStringLiteral("hboxLayout3"));
        treeSheetWidget = new QTreeWidget(tab);
        treeSheetWidget->setObjectName(QStringLiteral("treeSheetWidget"));
        QSizePolicy sizePolicy(static_cast<QSizePolicy::Policy>(0), static_cast<QSizePolicy::Policy>(0));
        sizePolicy.setHorizontalStretch(10);
        sizePolicy.setVerticalStretch(5);
        sizePolicy.setHeightForWidth(treeSheetWidget->sizePolicy().hasHeightForWidth());
        treeSheetWidget->setSizePolicy(sizePolicy);
        treeSheetWidget->setMinimumSize(QSize(330, 120));

        hboxLayout3->addWidget(treeSheetWidget);

        vboxLayout3 = new QVBoxLayout();
#ifndef Q_OS_MAC
        vboxLayout3->setSpacing(6);
#endif
        vboxLayout3->setContentsMargins(0, 0, 0, 0);
        vboxLayout3->setObjectName(QStringLiteral("vboxLayout3"));
        groupBox_4 = new QGroupBox(tab);
        groupBox_4->setObjectName(QStringLiteral("groupBox_4"));
        vboxLayout4 = new QVBoxLayout(groupBox_4);
#ifndef Q_OS_MAC
        vboxLayout4->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout4->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout4->setObjectName(QStringLiteral("vboxLayout4"));
        selectedSheetName = new QLineEdit(groupBox_4);
        selectedSheetName->setObjectName(QStringLiteral("selectedSheetName"));
        selectedSheetName->setMinimumSize(QSize(0, 0));

        vboxLayout4->addWidget(selectedSheetName);

        hboxLayout4 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout4->setSpacing(6);
#endif
        hboxLayout4->setContentsMargins(0, 0, 0, 0);
        hboxLayout4->setObjectName(QStringLiteral("hboxLayout4"));
        deleteSheetButton = new QPushButton(groupBox_4);
        deleteSheetButton->setObjectName(QStringLiteral("deleteSheetButton"));
        deleteSheetButton->setAutoDefault(false);

        hboxLayout4->addWidget(deleteSheetButton);

        spacerItem2 = new QSpacerItem(1, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        hboxLayout4->addItem(spacerItem2);

        renameSheetButton = new QPushButton(groupBox_4);
        renameSheetButton->setObjectName(QStringLiteral("renameSheetButton"));

        hboxLayout4->addWidget(renameSheetButton);


        vboxLayout4->addLayout(hboxLayout4);


        vboxLayout3->addWidget(groupBox_4);

        groupBox_2 = new QGroupBox(tab);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        hboxLayout5 = new QHBoxLayout(groupBox_2);
#ifndef Q_OS_MAC
        hboxLayout5->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout5->setContentsMargins(9, 9, 9, 9);
#endif
        hboxLayout5->setObjectName(QStringLiteral("hboxLayout5"));
        hboxLayout6 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout6->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout6->setContentsMargins(0, 0, 0, 0);
#endif
        hboxLayout6->setObjectName(QStringLiteral("hboxLayout6"));
        createSheetButton = new QPushButton(groupBox_2);
        createSheetButton->setObjectName(QStringLiteral("createSheetButton"));

        hboxLayout6->addWidget(createSheetButton);


        hboxLayout5->addLayout(hboxLayout6);


        vboxLayout3->addWidget(groupBox_2);

        spacerItem3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        vboxLayout3->addItem(spacerItem3);


        hboxLayout3->addLayout(vboxLayout3);

        tabWidget->addTab(tab, QString());
        cdtext = new QWidget();
        cdtext->setObjectName(QStringLiteral("cdtext"));
        gridLayout1 = new QGridLayout(cdtext);
#ifndef Q_OS_MAC
        gridLayout1->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        gridLayout1->setContentsMargins(9, 9, 9, 9);
#endif
        gridLayout1->setObjectName(QStringLiteral("gridLayout1"));
        label_7 = new QLabel(cdtext);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setMinimumSize(QSize(100, 0));

        gridLayout1->addWidget(label_7, 0, 0, 1, 1);

        lineEditPerformer = new QLineEdit(cdtext);
        lineEditPerformer->setObjectName(QStringLiteral("lineEditPerformer"));

        gridLayout1->addWidget(lineEditPerformer, 0, 1, 1, 2);

        label_4 = new QLabel(cdtext);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setMinimumSize(QSize(100, 0));

        gridLayout1->addWidget(label_4, 1, 0, 1, 1);

        lineEditId = new QLineEdit(cdtext);
        lineEditId->setObjectName(QStringLiteral("lineEditId"));

        gridLayout1->addWidget(lineEditId, 1, 1, 1, 2);

        label_6 = new QLabel(cdtext);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setMinimumSize(QSize(100, 0));

        gridLayout1->addWidget(label_6, 2, 0, 1, 1);

        lineEditUPC = new QLineEdit(cdtext);
        lineEditUPC->setObjectName(QStringLiteral("lineEditUPC"));

        gridLayout1->addWidget(lineEditUPC, 2, 1, 1, 2);

        label_5 = new QLabel(cdtext);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setMinimumSize(QSize(100, 0));

        gridLayout1->addWidget(label_5, 3, 0, 1, 1);

        comboBoxGenre = new QComboBox(cdtext);
        comboBoxGenre->addItem(QString());
        comboBoxGenre->addItem(QString());
        comboBoxGenre->addItem(QString());
        comboBoxGenre->addItem(QString());
        comboBoxGenre->addItem(QString());
        comboBoxGenre->addItem(QString());
        comboBoxGenre->addItem(QString());
        comboBoxGenre->addItem(QString());
        comboBoxGenre->addItem(QString());
        comboBoxGenre->addItem(QString());
        comboBoxGenre->addItem(QString());
        comboBoxGenre->addItem(QString());
        comboBoxGenre->addItem(QString());
        comboBoxGenre->addItem(QString());
        comboBoxGenre->addItem(QString());
        comboBoxGenre->addItem(QString());
        comboBoxGenre->addItem(QString());
        comboBoxGenre->addItem(QString());
        comboBoxGenre->addItem(QString());
        comboBoxGenre->addItem(QString());
        comboBoxGenre->addItem(QString());
        comboBoxGenre->addItem(QString());
        comboBoxGenre->addItem(QString());
        comboBoxGenre->addItem(QString());
        comboBoxGenre->addItem(QString());
        comboBoxGenre->addItem(QString());
        comboBoxGenre->addItem(QString());
        comboBoxGenre->addItem(QString());
        comboBoxGenre->setObjectName(QStringLiteral("comboBoxGenre"));

        gridLayout1->addWidget(comboBoxGenre, 3, 1, 1, 1);

        spacerItem4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout1->addItem(spacerItem4, 3, 2, 1, 1);

        label_8 = new QLabel(cdtext);
        label_8->setObjectName(QStringLiteral("label_8"));

        gridLayout1->addWidget(label_8, 4, 0, 1, 1);

        lineEditArranger = new QLineEdit(cdtext);
        lineEditArranger->setObjectName(QStringLiteral("lineEditArranger"));

        gridLayout1->addWidget(lineEditArranger, 4, 1, 1, 2);

        label_9 = new QLabel(cdtext);
        label_9->setObjectName(QStringLiteral("label_9"));

        gridLayout1->addWidget(label_9, 5, 0, 1, 1);

        lineEditSongwriter = new QLineEdit(cdtext);
        lineEditSongwriter->setObjectName(QStringLiteral("lineEditSongwriter"));

        gridLayout1->addWidget(lineEditSongwriter, 5, 1, 1, 2);

        label_10 = new QLabel(cdtext);
        label_10->setObjectName(QStringLiteral("label_10"));

        gridLayout1->addWidget(label_10, 6, 0, 1, 1);

        lineEditMessage = new QLineEdit(cdtext);
        lineEditMessage->setObjectName(QStringLiteral("lineEditMessage"));

        gridLayout1->addWidget(lineEditMessage, 6, 1, 1, 2);

        tabWidget->addTab(cdtext, QString());

        vboxLayout->addWidget(tabWidget);

        hboxLayout7 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout7->setSpacing(6);
#endif
        hboxLayout7->setContentsMargins(0, 0, 0, 0);
        hboxLayout7->setObjectName(QStringLiteral("hboxLayout7"));
        hboxLayout8 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout8->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout8->setContentsMargins(0, 0, 0, 0);
#endif
        hboxLayout8->setObjectName(QStringLiteral("hboxLayout8"));
        undoButton = new QPushButton(ProjectManagerDialog);
        undoButton->setObjectName(QStringLiteral("undoButton"));
        undoButton->setMaximumSize(QSize(16777215, 22));

        hboxLayout8->addWidget(undoButton);

        redoButton = new QPushButton(ProjectManagerDialog);
        redoButton->setObjectName(QStringLiteral("redoButton"));
        redoButton->setMaximumSize(QSize(16777215, 22));

        hboxLayout8->addWidget(redoButton);


        hboxLayout7->addLayout(hboxLayout8);

        buttonBox = new QDialogButtonBox(ProjectManagerDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::NoButton|QDialogButtonBox::Ok);

        hboxLayout7->addWidget(buttonBox);


        vboxLayout->addLayout(hboxLayout7);


        retranslateUi(ProjectManagerDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), ProjectManagerDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), ProjectManagerDialog, SLOT(reject()));

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(ProjectManagerDialog);
    } // setupUi

    void retranslateUi(QDialog *ProjectManagerDialog)
    {
        ProjectManagerDialog->setWindowTitle(QApplication::translate("ProjectManagerDialog", "Dialog", nullptr));
        groupBox_3->setTitle(QApplication::translate("ProjectManagerDialog", "Informational", nullptr));
        label_3->setText(QApplication::translate("ProjectManagerDialog", "Title", nullptr));
        label->setText(QApplication::translate("ProjectManagerDialog", "Description", nullptr));
        label_2->setText(QApplication::translate("ProjectManagerDialog", "Engineer", nullptr));
        groupBox->setTitle(QApplication::translate("ProjectManagerDialog", "Export", nullptr));
        sheetsExportButton->setText(QApplication::translate("ProjectManagerDialog", "Sheet(s)", nullptr));
        exportTemplateButton->setText(QApplication::translate("ProjectManagerDialog", "Template", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_4), QApplication::translate("ProjectManagerDialog", "Project", nullptr));
        QTreeWidgetItem *___qtreewidgetitem = treeSheetWidget->headerItem();
        ___qtreewidgetitem->setText(2, QApplication::translate("ProjectManagerDialog", "Length", nullptr));
        ___qtreewidgetitem->setText(1, QApplication::translate("ProjectManagerDialog", "Tracks", nullptr));
        ___qtreewidgetitem->setText(0, QApplication::translate("ProjectManagerDialog", "Sheet Name", nullptr));
        groupBox_4->setTitle(QApplication::translate("ProjectManagerDialog", "Selected Sheet", nullptr));
        deleteSheetButton->setText(QApplication::translate("ProjectManagerDialog", "Delete", nullptr));
        renameSheetButton->setText(QApplication::translate("ProjectManagerDialog", "Rename", nullptr));
        groupBox_2->setTitle(QApplication::translate("ProjectManagerDialog", "New Sheet", nullptr));
        createSheetButton->setText(QApplication::translate("ProjectManagerDialog", "Create new Sheet", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("ProjectManagerDialog", "Sheets", nullptr));
        label_7->setText(QApplication::translate("ProjectManagerDialog", "Performer", nullptr));
        label_4->setText(QApplication::translate("ProjectManagerDialog", "Disc ID:", nullptr));
        label_6->setText(QApplication::translate("ProjectManagerDialog", "UPC EAN:", nullptr));
        label_5->setText(QApplication::translate("ProjectManagerDialog", "Genre:", nullptr));
        comboBoxGenre->setItemText(0, QApplication::translate("ProjectManagerDialog", "Unused", nullptr));
        comboBoxGenre->setItemText(1, QApplication::translate("ProjectManagerDialog", "Undefined", nullptr));
        comboBoxGenre->setItemText(2, QApplication::translate("ProjectManagerDialog", "Adult Contemporary", nullptr));
        comboBoxGenre->setItemText(3, QApplication::translate("ProjectManagerDialog", "Alternative Rock", nullptr));
        comboBoxGenre->setItemText(4, QApplication::translate("ProjectManagerDialog", "Childrens", nullptr));
        comboBoxGenre->setItemText(5, QApplication::translate("ProjectManagerDialog", "Classical", nullptr));
        comboBoxGenre->setItemText(6, QApplication::translate("ProjectManagerDialog", "Contemporary Christian", nullptr));
        comboBoxGenre->setItemText(7, QApplication::translate("ProjectManagerDialog", "Country", nullptr));
        comboBoxGenre->setItemText(8, QApplication::translate("ProjectManagerDialog", "Dance", nullptr));
        comboBoxGenre->setItemText(9, QApplication::translate("ProjectManagerDialog", "Easy Listening", nullptr));
        comboBoxGenre->setItemText(10, QApplication::translate("ProjectManagerDialog", "Erotic", nullptr));
        comboBoxGenre->setItemText(11, QApplication::translate("ProjectManagerDialog", "Folk", nullptr));
        comboBoxGenre->setItemText(12, QApplication::translate("ProjectManagerDialog", "Gospel", nullptr));
        comboBoxGenre->setItemText(13, QApplication::translate("ProjectManagerDialog", "Hip Hop", nullptr));
        comboBoxGenre->setItemText(14, QApplication::translate("ProjectManagerDialog", "Jazz", nullptr));
        comboBoxGenre->setItemText(15, QApplication::translate("ProjectManagerDialog", "Latin", nullptr));
        comboBoxGenre->setItemText(16, QApplication::translate("ProjectManagerDialog", "Musical", nullptr));
        comboBoxGenre->setItemText(17, QApplication::translate("ProjectManagerDialog", "New Age", nullptr));
        comboBoxGenre->setItemText(18, QApplication::translate("ProjectManagerDialog", "Opera", nullptr));
        comboBoxGenre->setItemText(19, QApplication::translate("ProjectManagerDialog", "Operette", nullptr));
        comboBoxGenre->setItemText(20, QApplication::translate("ProjectManagerDialog", "Pop\302\240Music", nullptr));
        comboBoxGenre->setItemText(21, QApplication::translate("ProjectManagerDialog", "Rap", nullptr));
        comboBoxGenre->setItemText(22, QApplication::translate("ProjectManagerDialog", "Reggae", nullptr));
        comboBoxGenre->setItemText(23, QApplication::translate("ProjectManagerDialog", "Rock Music", nullptr));
        comboBoxGenre->setItemText(24, QApplication::translate("ProjectManagerDialog", "Rhythm and Blues", nullptr));
        comboBoxGenre->setItemText(25, QApplication::translate("ProjectManagerDialog", "Sound Effects", nullptr));
        comboBoxGenre->setItemText(26, QApplication::translate("ProjectManagerDialog", "Spoken Word", nullptr));
        comboBoxGenre->setItemText(27, QApplication::translate("ProjectManagerDialog", "World Music", nullptr));

        label_8->setText(QApplication::translate("ProjectManagerDialog", "Arranger", nullptr));
        label_9->setText(QApplication::translate("ProjectManagerDialog", "Songwriter", nullptr));
        label_10->setText(QApplication::translate("ProjectManagerDialog", "Message", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(cdtext), QApplication::translate("ProjectManagerDialog", "CD\302\240Text", nullptr));
#ifndef QT_NO_TOOLTIP
        undoButton->setToolTip(QApplication::translate("ProjectManagerDialog", "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Undo last change</p></body></html>", nullptr));
#endif // QT_NO_TOOLTIP
        undoButton->setText(QString());
#ifndef QT_NO_TOOLTIP
        redoButton->setToolTip(QApplication::translate("ProjectManagerDialog", "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Redo last change</p></body></html>", nullptr));
#endif // QT_NO_TOOLTIP
        redoButton->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class ProjectManagerDialog: public Ui_ProjectManagerDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PROJECTMANAGERDIALOG_H
