/********************************************************************************
** Form generated from reading UI file 'PerformanceConfigPage.ui'
**
** Created by: Qt User Interface Compiler version 5.11.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PERFORMANCECONFIGPAGE_H
#define UI_PERFORMANCECONFIGPAGE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_PerformanceConfigPage
{
public:
    QVBoxLayout *vboxLayout;
    QGroupBox *groupBox_2;
    QVBoxLayout *vboxLayout1;
    QHBoxLayout *hboxLayout;
    QLabel *label;
    QDoubleSpinBox *bufferTimeSpinBox;
    QHBoxLayout *hboxLayout1;
    QLabel *reloadWarningLabel;
    QLabel *label_2;
    QGroupBox *groupBox;
    QVBoxLayout *vboxLayout2;
    QHBoxLayout *hboxLayout2;
    QLabel *label_4;
    QSpinBox *jogUpdateIntervalSpinBox;
    QCheckBox *useOpenGLCheckBox;
    QSpacerItem *spacerItem;

    void setupUi(QWidget *PerformanceConfigPage)
    {
        if (PerformanceConfigPage->objectName().isEmpty())
            PerformanceConfigPage->setObjectName(QStringLiteral("PerformanceConfigPage"));
        PerformanceConfigPage->resize(343, 234);
        vboxLayout = new QVBoxLayout(PerformanceConfigPage);
        vboxLayout->setObjectName(QStringLiteral("vboxLayout"));
        groupBox_2 = new QGroupBox(PerformanceConfigPage);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        vboxLayout1 = new QVBoxLayout(groupBox_2);
        vboxLayout1->setSpacing(12);
        vboxLayout1->setObjectName(QStringLiteral("vboxLayout1"));
        vboxLayout1->setContentsMargins(9, 9, 9, 9);
        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        hboxLayout->setObjectName(QStringLiteral("hboxLayout"));
        hboxLayout->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(groupBox_2);
        label->setObjectName(QStringLiteral("label"));

        hboxLayout->addWidget(label);

        bufferTimeSpinBox = new QDoubleSpinBox(groupBox_2);
        bufferTimeSpinBox->setObjectName(QStringLiteral("bufferTimeSpinBox"));
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(1);
        sizePolicy.setHeightForWidth(bufferTimeSpinBox->sizePolicy().hasHeightForWidth());
        bufferTimeSpinBox->setSizePolicy(sizePolicy);
        bufferTimeSpinBox->setDecimals(1);
        bufferTimeSpinBox->setMinimum(0.4);
        bufferTimeSpinBox->setMaximum(3);
        bufferTimeSpinBox->setSingleStep(0.1);
        bufferTimeSpinBox->setValue(1);

        hboxLayout->addWidget(bufferTimeSpinBox);


        vboxLayout1->addLayout(hboxLayout);

        hboxLayout1 = new QHBoxLayout();
        hboxLayout1->setSpacing(6);
        hboxLayout1->setObjectName(QStringLiteral("hboxLayout1"));
        hboxLayout1->setContentsMargins(0, 0, 0, 0);
        reloadWarningLabel = new QLabel(groupBox_2);
        reloadWarningLabel->setObjectName(QStringLiteral("reloadWarningLabel"));
        reloadWarningLabel->setMinimumSize(QSize(40, 0));

        hboxLayout1->addWidget(reloadWarningLabel);

        label_2 = new QLabel(groupBox_2);
        label_2->setObjectName(QStringLiteral("label_2"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(1);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(label_2->sizePolicy().hasHeightForWidth());
        label_2->setSizePolicy(sizePolicy1);

        hboxLayout1->addWidget(label_2);


        vboxLayout1->addLayout(hboxLayout1);


        vboxLayout->addWidget(groupBox_2);

        groupBox = new QGroupBox(PerformanceConfigPage);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        vboxLayout2 = new QVBoxLayout(groupBox);
        vboxLayout2->setSpacing(6);
        vboxLayout2->setObjectName(QStringLiteral("vboxLayout2"));
        vboxLayout2->setContentsMargins(9, 9, 9, 9);
        hboxLayout2 = new QHBoxLayout();
        hboxLayout2->setSpacing(6);
        hboxLayout2->setObjectName(QStringLiteral("hboxLayout2"));
        hboxLayout2->setContentsMargins(0, 0, 0, 0);
        label_4 = new QLabel(groupBox);
        label_4->setObjectName(QStringLiteral("label_4"));

        hboxLayout2->addWidget(label_4);

        jogUpdateIntervalSpinBox = new QSpinBox(groupBox);
        jogUpdateIntervalSpinBox->setObjectName(QStringLiteral("jogUpdateIntervalSpinBox"));
        jogUpdateIntervalSpinBox->setMaximumSize(QSize(100, 16777215));
        jogUpdateIntervalSpinBox->setMinimum(10);
        jogUpdateIntervalSpinBox->setMaximum(50);
        jogUpdateIntervalSpinBox->setValue(25);

        hboxLayout2->addWidget(jogUpdateIntervalSpinBox);


        vboxLayout2->addLayout(hboxLayout2);

        useOpenGLCheckBox = new QCheckBox(groupBox);
        useOpenGLCheckBox->setObjectName(QStringLiteral("useOpenGLCheckBox"));

        vboxLayout2->addWidget(useOpenGLCheckBox);


        vboxLayout->addWidget(groupBox);

        spacerItem = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        vboxLayout->addItem(spacerItem);


        retranslateUi(PerformanceConfigPage);

        QMetaObject::connectSlotsByName(PerformanceConfigPage);
    } // setupUi

    void retranslateUi(QWidget *PerformanceConfigPage)
    {
        PerformanceConfigPage->setWindowTitle(QApplication::translate("PerformanceConfigPage", "Form", nullptr));
        groupBox_2->setTitle(QApplication::translate("PerformanceConfigPage", "Audio file buffering", nullptr));
#ifndef QT_NO_TOOLTIP
        label->setToolTip(QApplication::translate("PerformanceConfigPage", "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">Read buffer size:</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">The amount of audio data that can be stored in the </p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">read buffers in seconds.</p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block"
                        "-indent:0; text-indent:0px;\">The default value of 1 second should do just fine.</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">However, if you're tight on memory, you can make this value lower.</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">If you experience buffer underruns when the hard disk bandwidth is </p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">(almost) saturated, or when buffer underruns happen regularly due </p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">the hard disk can't keep up for some reason, you can try a larger </p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">value, like 1.5 or 2.0 seconds"
                        ".</p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Keep in mind that when using a larger buffer, </p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">it will take considerably more time to move (i.e. seeking) </p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">the playhead to another positions, since all the buffers </p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">(one for each audioclip * channel count) need to be refilled!</p></body></html>", nullptr));
#endif // QT_NO_TOOLTIP
        label->setText(QApplication::translate("PerformanceConfigPage", "Read buffer size (seconds)", nullptr));
        reloadWarningLabel->setText(QApplication::translate("PerformanceConfigPage", "info icon", nullptr));
        label_2->setText(QApplication::translate("PerformanceConfigPage", "Changing the buffer size only will take \n"
"into effect after (re)loading a project.", nullptr));
        groupBox->setTitle(QApplication::translate("PerformanceConfigPage", "Painting", nullptr));
#ifndef QT_NO_TOOLTIP
        label_4->setToolTip(QApplication::translate("PerformanceConfigPage", "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">The number of times per second at which the Graphical</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Items are repainted during a jog action, like moving an</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">AudioClip, or changing the Gain.</p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -q"
                        "t-block-indent:0; text-indent:0px;\">The default frames per second of 35 is a perfect compromise </p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">between smooth painting, and low cpu usage.</p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">However, if you find the painting to be not smooth enough,</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">change this value to a higher one, but keep in mind that it</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">will consume considerably more cpu!</p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0"
                        "px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">If for example moving an AudioClip still takes to much cpu,</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">you should consider to lower this value.</p></body></html>", nullptr));
#endif // QT_NO_TOOLTIP
        label_4->setText(QApplication::translate("PerformanceConfigPage", "Jog repaint speed (fps)", nullptr));
#ifndef QT_NO_TOOLTIP
        useOpenGLCheckBox->setToolTip(QApplication::translate("PerformanceConfigPage", "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Accelerates the painting of AudioClips and Tracks by using</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">the 3D engine of your graphics card.</p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Depending on your graphics card and driver support, this </p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-ri"
                        "ght:0px; -qt-block-indent:0; text-indent:0px;\">can speed up painting considerably!</p></body></html>", nullptr));
#endif // QT_NO_TOOLTIP
        useOpenGLCheckBox->setText(QApplication::translate("PerformanceConfigPage", "Use hardware acceleration", nullptr));
    } // retranslateUi

};

namespace Ui {
    class PerformanceConfigPage: public Ui_PerformanceConfigPage {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PERFORMANCECONFIGPAGE_H
