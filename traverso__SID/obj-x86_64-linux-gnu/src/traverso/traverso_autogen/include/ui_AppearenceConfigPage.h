/********************************************************************************
** Form generated from reading UI file 'AppearenceConfigPage.ui'
**
** Created by: Qt User Interface Compiler version 5.11.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_APPEARENCECONFIGPAGE_H
#define UI_APPEARENCECONFIGPAGE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_AppearenceConfigPage
{
public:
    QVBoxLayout *vboxLayout;
    QTabWidget *tabWidget;
    QWidget *tab;
    QVBoxLayout *vboxLayout1;
    QGroupBox *groupBox;
    QVBoxLayout *vboxLayout2;
    QVBoxLayout *vboxLayout3;
    QHBoxLayout *hboxLayout;
    QLabel *label_2;
    QSpacerItem *spacerItem;
    QHBoxLayout *hboxLayout1;
    QLineEdit *themePathLineEdit;
    QPushButton *pathSelectButton;
    QHBoxLayout *hboxLayout2;
    QLabel *label;
    QComboBox *themeSelecterCombo;
    QHBoxLayout *hboxLayout3;
    QLabel *label_3;
    QSpinBox *colorAdjustBox;
    QGroupBox *groupBox_2;
    QVBoxLayout *vboxLayout4;
    QCheckBox *rectifiedCheckBox;
    QCheckBox *paintAudioWithOutlineCheckBox;
    QCheckBox *mergedCheckBox;
    QCheckBox *dbGridCheckBox;
    QGroupBox *styleOptionsGroupBox;
    QVBoxLayout *vboxLayout5;
    QHBoxLayout *hboxLayout4;
    QLabel *label_4;
    QComboBox *styleCombo;
    QCheckBox *useStylePalletCheckBox;
    QSpacerItem *spacerItem1;
    QWidget *tab_2;
    QVBoxLayout *vboxLayout6;
    QGroupBox *toolbarGroupBox;
    QVBoxLayout *vboxLayout7;
    QHBoxLayout *hboxLayout5;
    QLabel *label_5;
    QComboBox *iconSizeCombo;
    QHBoxLayout *hboxLayout6;
    QLabel *label_7;
    QComboBox *toolbarStyleCombo;
    QHBoxLayout *hboxLayout7;
    QLabel *label_6;
    QComboBox *transportConsoleCombo;
    QSpacerItem *spacerItem2;
    QWidget *tab_3;
    QVBoxLayout *vboxLayout8;
    QGroupBox *groupBox_3;
    QVBoxLayout *verticalLayout;
    QComboBox *languageComboBox;
    QLabel *label_8;
    QSpacerItem *spacerItem3;

    void setupUi(QWidget *AppearenceConfigPage)
    {
        if (AppearenceConfigPage->objectName().isEmpty())
            AppearenceConfigPage->setObjectName(QStringLiteral("AppearenceConfigPage"));
        AppearenceConfigPage->resize(352, 531);
        vboxLayout = new QVBoxLayout(AppearenceConfigPage);
        vboxLayout->setObjectName(QStringLiteral("vboxLayout"));
        tabWidget = new QTabWidget(AppearenceConfigPage);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        vboxLayout1 = new QVBoxLayout(tab);
        vboxLayout1->setObjectName(QStringLiteral("vboxLayout1"));
        groupBox = new QGroupBox(tab);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(1);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(groupBox->sizePolicy().hasHeightForWidth());
        groupBox->setSizePolicy(sizePolicy);
        vboxLayout2 = new QVBoxLayout(groupBox);
        vboxLayout2->setSpacing(12);
#ifndef Q_OS_MAC
        vboxLayout2->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout2->setObjectName(QStringLiteral("vboxLayout2"));
        vboxLayout3 = new QVBoxLayout();
#ifndef Q_OS_MAC
        vboxLayout3->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout3->setContentsMargins(0, 0, 0, 0);
#endif
        vboxLayout3->setObjectName(QStringLiteral("vboxLayout3"));
        hboxLayout = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout->setContentsMargins(0, 0, 0, 0);
#endif
        hboxLayout->setObjectName(QStringLiteral("hboxLayout"));
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QStringLiteral("label_2"));

        hboxLayout->addWidget(label_2);

        spacerItem = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacerItem);


        vboxLayout3->addLayout(hboxLayout);

        hboxLayout1 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout1->setSpacing(6);
#endif
        hboxLayout1->setContentsMargins(0, 0, 0, 0);
        hboxLayout1->setObjectName(QStringLiteral("hboxLayout1"));
        themePathLineEdit = new QLineEdit(groupBox);
        themePathLineEdit->setObjectName(QStringLiteral("themePathLineEdit"));
        themePathLineEdit->setEnabled(true);
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(5);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(themePathLineEdit->sizePolicy().hasHeightForWidth());
        themePathLineEdit->setSizePolicy(sizePolicy1);
        themePathLineEdit->setMouseTracking(true);
        themePathLineEdit->setAcceptDrops(true);
        themePathLineEdit->setReadOnly(false);

        hboxLayout1->addWidget(themePathLineEdit);

        pathSelectButton = new QPushButton(groupBox);
        pathSelectButton->setObjectName(QStringLiteral("pathSelectButton"));

        hboxLayout1->addWidget(pathSelectButton);


        vboxLayout3->addLayout(hboxLayout1);


        vboxLayout2->addLayout(vboxLayout3);

        hboxLayout2 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout2->setSpacing(6);
#endif
        hboxLayout2->setContentsMargins(0, 0, 0, 0);
        hboxLayout2->setObjectName(QStringLiteral("hboxLayout2"));
        label = new QLabel(groupBox);
        label->setObjectName(QStringLiteral("label"));
        sizePolicy.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy);

        hboxLayout2->addWidget(label);

        themeSelecterCombo = new QComboBox(groupBox);
        themeSelecterCombo->setObjectName(QStringLiteral("themeSelecterCombo"));
        QSizePolicy sizePolicy2(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(1);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(themeSelecterCombo->sizePolicy().hasHeightForWidth());
        themeSelecterCombo->setSizePolicy(sizePolicy2);

        hboxLayout2->addWidget(themeSelecterCombo);


        vboxLayout2->addLayout(hboxLayout2);

        hboxLayout3 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout3->setSpacing(6);
#endif
        hboxLayout3->setContentsMargins(0, 0, 0, 0);
        hboxLayout3->setObjectName(QStringLiteral("hboxLayout3"));
        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QStringLiteral("label_3"));

        hboxLayout3->addWidget(label_3);

        colorAdjustBox = new QSpinBox(groupBox);
        colorAdjustBox->setObjectName(QStringLiteral("colorAdjustBox"));
        colorAdjustBox->setMaximum(200);
        colorAdjustBox->setValue(100);

        hboxLayout3->addWidget(colorAdjustBox);


        vboxLayout2->addLayout(hboxLayout3);


        vboxLayout1->addWidget(groupBox);

        groupBox_2 = new QGroupBox(tab);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        vboxLayout4 = new QVBoxLayout(groupBox_2);
#ifndef Q_OS_MAC
        vboxLayout4->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout4->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout4->setObjectName(QStringLiteral("vboxLayout4"));
        rectifiedCheckBox = new QCheckBox(groupBox_2);
        rectifiedCheckBox->setObjectName(QStringLiteral("rectifiedCheckBox"));

        vboxLayout4->addWidget(rectifiedCheckBox);

        paintAudioWithOutlineCheckBox = new QCheckBox(groupBox_2);
        paintAudioWithOutlineCheckBox->setObjectName(QStringLiteral("paintAudioWithOutlineCheckBox"));

        vboxLayout4->addWidget(paintAudioWithOutlineCheckBox);

        mergedCheckBox = new QCheckBox(groupBox_2);
        mergedCheckBox->setObjectName(QStringLiteral("mergedCheckBox"));

        vboxLayout4->addWidget(mergedCheckBox);

        dbGridCheckBox = new QCheckBox(groupBox_2);
        dbGridCheckBox->setObjectName(QStringLiteral("dbGridCheckBox"));

        vboxLayout4->addWidget(dbGridCheckBox);


        vboxLayout1->addWidget(groupBox_2);

        styleOptionsGroupBox = new QGroupBox(tab);
        styleOptionsGroupBox->setObjectName(QStringLiteral("styleOptionsGroupBox"));
        vboxLayout5 = new QVBoxLayout(styleOptionsGroupBox);
        vboxLayout5->setObjectName(QStringLiteral("vboxLayout5"));
        hboxLayout4 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout4->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout4->setContentsMargins(0, 0, 0, 0);
#endif
        hboxLayout4->setObjectName(QStringLiteral("hboxLayout4"));
        label_4 = new QLabel(styleOptionsGroupBox);
        label_4->setObjectName(QStringLiteral("label_4"));
        sizePolicy.setHeightForWidth(label_4->sizePolicy().hasHeightForWidth());
        label_4->setSizePolicy(sizePolicy);

        hboxLayout4->addWidget(label_4);

        styleCombo = new QComboBox(styleOptionsGroupBox);
        styleCombo->setObjectName(QStringLiteral("styleCombo"));
        sizePolicy2.setHeightForWidth(styleCombo->sizePolicy().hasHeightForWidth());
        styleCombo->setSizePolicy(sizePolicy2);

        hboxLayout4->addWidget(styleCombo);


        vboxLayout5->addLayout(hboxLayout4);

        useStylePalletCheckBox = new QCheckBox(styleOptionsGroupBox);
        useStylePalletCheckBox->setObjectName(QStringLiteral("useStylePalletCheckBox"));

        vboxLayout5->addWidget(useStylePalletCheckBox);


        vboxLayout1->addWidget(styleOptionsGroupBox);

        spacerItem1 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        vboxLayout1->addItem(spacerItem1);

        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QStringLiteral("tab_2"));
        vboxLayout6 = new QVBoxLayout(tab_2);
        vboxLayout6->setObjectName(QStringLiteral("vboxLayout6"));
        toolbarGroupBox = new QGroupBox(tab_2);
        toolbarGroupBox->setObjectName(QStringLiteral("toolbarGroupBox"));
        vboxLayout7 = new QVBoxLayout(toolbarGroupBox);
        vboxLayout7->setObjectName(QStringLiteral("vboxLayout7"));
        hboxLayout5 = new QHBoxLayout();
        hboxLayout5->setObjectName(QStringLiteral("hboxLayout5"));
        label_5 = new QLabel(toolbarGroupBox);
        label_5->setObjectName(QStringLiteral("label_5"));

        hboxLayout5->addWidget(label_5);

        iconSizeCombo = new QComboBox(toolbarGroupBox);
        iconSizeCombo->setObjectName(QStringLiteral("iconSizeCombo"));

        hboxLayout5->addWidget(iconSizeCombo);


        vboxLayout7->addLayout(hboxLayout5);

        hboxLayout6 = new QHBoxLayout();
        hboxLayout6->setObjectName(QStringLiteral("hboxLayout6"));
        label_7 = new QLabel(toolbarGroupBox);
        label_7->setObjectName(QStringLiteral("label_7"));

        hboxLayout6->addWidget(label_7);

        toolbarStyleCombo = new QComboBox(toolbarGroupBox);
        toolbarStyleCombo->setObjectName(QStringLiteral("toolbarStyleCombo"));

        hboxLayout6->addWidget(toolbarStyleCombo);


        vboxLayout7->addLayout(hboxLayout6);

        hboxLayout7 = new QHBoxLayout();
        hboxLayout7->setObjectName(QStringLiteral("hboxLayout7"));
        label_6 = new QLabel(toolbarGroupBox);
        label_6->setObjectName(QStringLiteral("label_6"));

        hboxLayout7->addWidget(label_6);

        transportConsoleCombo = new QComboBox(toolbarGroupBox);
        transportConsoleCombo->setObjectName(QStringLiteral("transportConsoleCombo"));

        hboxLayout7->addWidget(transportConsoleCombo);


        vboxLayout7->addLayout(hboxLayout7);

        spacerItem2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        vboxLayout7->addItem(spacerItem2);


        vboxLayout6->addWidget(toolbarGroupBox);

        tabWidget->addTab(tab_2, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName(QStringLiteral("tab_3"));
        vboxLayout8 = new QVBoxLayout(tab_3);
        vboxLayout8->setObjectName(QStringLiteral("vboxLayout8"));
        groupBox_3 = new QGroupBox(tab_3);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        verticalLayout = new QVBoxLayout(groupBox_3);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        languageComboBox = new QComboBox(groupBox_3);
        languageComboBox->setObjectName(QStringLiteral("languageComboBox"));
        languageComboBox->setInsertPolicy(QComboBox::InsertAlphabetically);

        verticalLayout->addWidget(languageComboBox);

        label_8 = new QLabel(groupBox_3);
        label_8->setObjectName(QStringLiteral("label_8"));

        verticalLayout->addWidget(label_8);

        spacerItem3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(spacerItem3);


        vboxLayout8->addWidget(groupBox_3);

        tabWidget->addTab(tab_3, QString());

        vboxLayout->addWidget(tabWidget);


        retranslateUi(AppearenceConfigPage);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(AppearenceConfigPage);
    } // setupUi

    void retranslateUi(QWidget *AppearenceConfigPage)
    {
        AppearenceConfigPage->setWindowTitle(QApplication::translate("AppearenceConfigPage", "Form", nullptr));
        groupBox->setTitle(QApplication::translate("AppearenceConfigPage", "Theme selector", nullptr));
        label_2->setText(QApplication::translate("AppearenceConfigPage", "Path to theme files", nullptr));
        pathSelectButton->setText(QString());
        label->setText(QApplication::translate("AppearenceConfigPage", "Available themes", nullptr));
        label_3->setText(QApplication::translate("AppearenceConfigPage", "Adjust theme color", nullptr));
        groupBox_2->setTitle(QApplication::translate("AppearenceConfigPage", "Theme Options", nullptr));
        rectifiedCheckBox->setText(QApplication::translate("AppearenceConfigPage", "Paint audio rectified", nullptr));
#ifndef QT_NO_TOOLTIP
        paintAudioWithOutlineCheckBox->setToolTip(QApplication::translate("AppearenceConfigPage", "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Painting the waveform with an outline is more detailed, but requires more cpu.</p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">If you experience slowness when painting many clips, or during animated scroll, unselect this option!</p></body></html>", nullptr));
#endif // QT_NO_TOOLTIP
        paintAudioWithOutlineCheckBox->setText(QApplication::translate("AppearenceConfigPage", "Paint audio with outline", nullptr));
        mergedCheckBox->setText(QApplication::translate("AppearenceConfigPage", "Paint stereo audio as mono audio", nullptr));
        dbGridCheckBox->setText(QApplication::translate("AppearenceConfigPage", "Draw lines at 0 and -6 dB", nullptr));
        styleOptionsGroupBox->setTitle(QApplication::translate("AppearenceConfigPage", "Style Options", nullptr));
        label_4->setText(QApplication::translate("AppearenceConfigPage", "Select style", nullptr));
        useStylePalletCheckBox->setText(QApplication::translate("AppearenceConfigPage", "Use selected style's palette", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("AppearenceConfigPage", "Theme", nullptr));
        toolbarGroupBox->setTitle(QApplication::translate("AppearenceConfigPage", "Toolbars", nullptr));
        label_5->setText(QApplication::translate("AppearenceConfigPage", "Icon size", nullptr));
        label_7->setText(QApplication::translate("AppearenceConfigPage", "Button style", nullptr));
        label_6->setText(QApplication::translate("AppearenceConfigPage", "Transport Console size", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("AppearenceConfigPage", "Toolbars", nullptr));
        groupBox_3->setTitle(QApplication::translate("AppearenceConfigPage", "Interface Language", nullptr));
        label_8->setText(QApplication::translate("AppearenceConfigPage", "Changing the language of the Interface will take\n"
"effect after restarting Traverso!", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_3), QApplication::translate("AppearenceConfigPage", "Language", nullptr));
    } // retranslateUi

};

namespace Ui {
    class AppearenceConfigPage: public Ui_AppearenceConfigPage {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_APPEARENCECONFIGPAGE_H
