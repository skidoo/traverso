/********************************************************************************
** Form generated from reading UI file 'BehaviorConfigPage.ui'
**
** Created by: Qt User Interface Compiler version 5.11.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_BEHAVIORCONFIGPAGE_H
#define UI_BEHAVIORCONFIGPAGE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_BehaviorConfigPage
{
public:
    QVBoxLayout *verticalLayout;
    QGroupBox *groupBox;
    QVBoxLayout *vboxLayout;
    QHBoxLayout *hboxLayout;
    QLabel *label_2;
    QSpacerItem *spacerItem;
    QRadioButton *saveRadioButton;
    QRadioButton *askRadioButton;
    QRadioButton *neverRadioButton;
    QCheckBox *loadLastProjectCheckBox;
    QGroupBox *groupBox_2;
    QVBoxLayout *vboxLayout1;
    QHBoxLayout *hboxLayout1;
    QLabel *label_4;
    QSpinBox *numberOfTrackSpinBox;
    QGroupBox *groupBox_4;
    QVBoxLayout *vboxLayout2;
    QCheckBox *lockClipsCheckBox;
    QGroupBox *groupBox_3;
    QVBoxLayout *vboxLayout3;
    QHBoxLayout *hboxLayout2;
    QCheckBox *keepCursorVisibleCheckBox;
    QComboBox *scrollModeComboBox;
    QCheckBox *resyncAudioCheckBox;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *BehaviorConfigPage)
    {
        if (BehaviorConfigPage->objectName().isEmpty())
            BehaviorConfigPage->setObjectName(QStringLiteral("BehaviorConfigPage"));
        BehaviorConfigPage->resize(336, 412);
        verticalLayout = new QVBoxLayout(BehaviorConfigPage);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        groupBox = new QGroupBox(BehaviorConfigPage);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        vboxLayout = new QVBoxLayout(groupBox);
#ifndef Q_OS_MAC
        vboxLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout->setObjectName(QStringLiteral("vboxLayout"));
        hboxLayout = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout->setContentsMargins(0, 0, 0, 0);
#endif
        hboxLayout->setObjectName(QStringLiteral("hboxLayout"));
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QStringLiteral("label_2"));

        hboxLayout->addWidget(label_2);

        spacerItem = new QSpacerItem(20, 23, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacerItem);

        saveRadioButton = new QRadioButton(groupBox);
        saveRadioButton->setObjectName(QStringLiteral("saveRadioButton"));
        saveRadioButton->setChecked(true);

        hboxLayout->addWidget(saveRadioButton);

        askRadioButton = new QRadioButton(groupBox);
        askRadioButton->setObjectName(QStringLiteral("askRadioButton"));

        hboxLayout->addWidget(askRadioButton);

        neverRadioButton = new QRadioButton(groupBox);
        neverRadioButton->setObjectName(QStringLiteral("neverRadioButton"));

        hboxLayout->addWidget(neverRadioButton);


        vboxLayout->addLayout(hboxLayout);

        loadLastProjectCheckBox = new QCheckBox(groupBox);
        loadLastProjectCheckBox->setObjectName(QStringLiteral("loadLastProjectCheckBox"));

        vboxLayout->addWidget(loadLastProjectCheckBox);


        verticalLayout->addWidget(groupBox);

        groupBox_2 = new QGroupBox(BehaviorConfigPage);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        vboxLayout1 = new QVBoxLayout(groupBox_2);
#ifndef Q_OS_MAC
        vboxLayout1->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout1->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout1->setObjectName(QStringLiteral("vboxLayout1"));
        hboxLayout1 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout1->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout1->setContentsMargins(0, 0, 0, 0);
#endif
        hboxLayout1->setObjectName(QStringLiteral("hboxLayout1"));
        label_4 = new QLabel(groupBox_2);
        label_4->setObjectName(QStringLiteral("label_4"));

        hboxLayout1->addWidget(label_4);

        numberOfTrackSpinBox = new QSpinBox(groupBox_2);
        numberOfTrackSpinBox->setObjectName(QStringLiteral("numberOfTrackSpinBox"));
        numberOfTrackSpinBox->setMinimum(1);
        numberOfTrackSpinBox->setMaximum(1024);
        numberOfTrackSpinBox->setValue(1);

        hboxLayout1->addWidget(numberOfTrackSpinBox);


        vboxLayout1->addLayout(hboxLayout1);


        verticalLayout->addWidget(groupBox_2);

        groupBox_4 = new QGroupBox(BehaviorConfigPage);
        groupBox_4->setObjectName(QStringLiteral("groupBox_4"));
        vboxLayout2 = new QVBoxLayout(groupBox_4);
#ifndef Q_OS_MAC
        vboxLayout2->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout2->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout2->setObjectName(QStringLiteral("vboxLayout2"));
        lockClipsCheckBox = new QCheckBox(groupBox_4);
        lockClipsCheckBox->setObjectName(QStringLiteral("lockClipsCheckBox"));

        vboxLayout2->addWidget(lockClipsCheckBox);


        verticalLayout->addWidget(groupBox_4);

        groupBox_3 = new QGroupBox(BehaviorConfigPage);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        vboxLayout3 = new QVBoxLayout(groupBox_3);
#ifndef Q_OS_MAC
        vboxLayout3->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout3->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout3->setObjectName(QStringLiteral("vboxLayout3"));
        hboxLayout2 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout2->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout2->setContentsMargins(0, 0, 0, 0);
#endif
        hboxLayout2->setObjectName(QStringLiteral("hboxLayout2"));
        keepCursorVisibleCheckBox = new QCheckBox(groupBox_3);
        keepCursorVisibleCheckBox->setObjectName(QStringLiteral("keepCursorVisibleCheckBox"));
        keepCursorVisibleCheckBox->setChecked(true);

        hboxLayout2->addWidget(keepCursorVisibleCheckBox);

        scrollModeComboBox = new QComboBox(groupBox_3);
        scrollModeComboBox->addItem(QString());
        scrollModeComboBox->addItem(QString());
        scrollModeComboBox->addItem(QString());
        scrollModeComboBox->setObjectName(QStringLiteral("scrollModeComboBox"));

        hboxLayout2->addWidget(scrollModeComboBox);


        vboxLayout3->addLayout(hboxLayout2);

        resyncAudioCheckBox = new QCheckBox(groupBox_3);
        resyncAudioCheckBox->setObjectName(QStringLiteral("resyncAudioCheckBox"));
        resyncAudioCheckBox->setChecked(false);

        vboxLayout3->addWidget(resyncAudioCheckBox);


        verticalLayout->addWidget(groupBox_3);

        verticalSpacer = new QSpacerItem(20, 51, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);


        retranslateUi(BehaviorConfigPage);
        QObject::connect(keepCursorVisibleCheckBox, SIGNAL(toggled(bool)), scrollModeComboBox, SLOT(setEnabled(bool)));

        QMetaObject::connectSlotsByName(BehaviorConfigPage);
    } // setupUi

    void retranslateUi(QWidget *BehaviorConfigPage)
    {
        BehaviorConfigPage->setWindowTitle(QApplication::translate("BehaviorConfigPage", "Form", nullptr));
        groupBox->setTitle(QApplication::translate("BehaviorConfigPage", "Project Settings", nullptr));
        label_2->setText(QApplication::translate("BehaviorConfigPage", "On close:", nullptr));
        saveRadioButton->setText(QApplication::translate("BehaviorConfigPage", "Save", nullptr));
        askRadioButton->setText(QApplication::translate("BehaviorConfigPage", "Ask", nullptr));
        neverRadioButton->setText(QApplication::translate("BehaviorConfigPage", "Don't save", nullptr));
        loadLastProjectCheckBox->setText(QApplication::translate("BehaviorConfigPage", "Load last used project at startup", nullptr));
        groupBox_2->setTitle(QApplication::translate("BehaviorConfigPage", "New Sheet Settings", nullptr));
        label_4->setText(QApplication::translate("BehaviorConfigPage", "Number of tracks", nullptr));
        groupBox_4->setTitle(QApplication::translate("BehaviorConfigPage", "Audio Clip Settings", nullptr));
        lockClipsCheckBox->setText(QApplication::translate("BehaviorConfigPage", "Lock Audio Clips by default", nullptr));
        groupBox_3->setTitle(QApplication::translate("BehaviorConfigPage", "Playback Settings", nullptr));
#ifndef QT_NO_TOOLTIP
        keepCursorVisibleCheckBox->setToolTip(QApplication::translate("BehaviorConfigPage", "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Keep the play cursor in view while playing or recording.</p></body></html>", nullptr));
#endif // QT_NO_TOOLTIP
        keepCursorVisibleCheckBox->setText(QApplication::translate("BehaviorConfigPage", "Scroll playback", nullptr));
        scrollModeComboBox->setItemText(0, QApplication::translate("BehaviorConfigPage", "Jump", nullptr));
        scrollModeComboBox->setItemText(1, QApplication::translate("BehaviorConfigPage", "Stay Centered", nullptr));
        scrollModeComboBox->setItemText(2, QApplication::translate("BehaviorConfigPage", "Animated", nullptr));

        resyncAudioCheckBox->setText(QApplication::translate("BehaviorConfigPage", "Continuously adjust audio while dragging", nullptr));
    } // retranslateUi

};

namespace Ui {
    class BehaviorConfigPage: public Ui_BehaviorConfigPage {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_BEHAVIORCONFIGPAGE_H
