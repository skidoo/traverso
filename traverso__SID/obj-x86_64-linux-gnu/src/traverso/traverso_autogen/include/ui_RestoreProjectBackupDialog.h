/********************************************************************************
** Form generated from reading UI file 'RestoreProjectBackupDialog.ui'
**
** Created by: Qt User Interface Compiler version 5.11.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_RESTOREPROJECTBACKUPDIALOG_H
#define UI_RESTOREPROJECTBACKUPDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_RestoreProjectBackupDialog
{
public:
    QVBoxLayout *vboxLayout;
    QLabel *label;
    QHBoxLayout *hboxLayout;
    QVBoxLayout *vboxLayout1;
    QLabel *label_2;
    QLabel *label_3;
    QVBoxLayout *vboxLayout2;
    QLabel *currentDateLable;
    QLabel *lastBackupLable;
    QTreeWidget *dateTreeWidget;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *RestoreProjectBackupDialog)
    {
        if (RestoreProjectBackupDialog->objectName().isEmpty())
            RestoreProjectBackupDialog->setObjectName(QStringLiteral("RestoreProjectBackupDialog"));
        RestoreProjectBackupDialog->resize(323, 358);
        vboxLayout = new QVBoxLayout(RestoreProjectBackupDialog);
        vboxLayout->setObjectName(QStringLiteral("vboxLayout"));
        label = new QLabel(RestoreProjectBackupDialog);
        label->setObjectName(QStringLiteral("label"));

        vboxLayout->addWidget(label);

        hboxLayout = new QHBoxLayout();
        hboxLayout->setObjectName(QStringLiteral("hboxLayout"));
        vboxLayout1 = new QVBoxLayout();
        vboxLayout1->setObjectName(QStringLiteral("vboxLayout1"));
        label_2 = new QLabel(RestoreProjectBackupDialog);
        label_2->setObjectName(QStringLiteral("label_2"));

        vboxLayout1->addWidget(label_2);

        label_3 = new QLabel(RestoreProjectBackupDialog);
        label_3->setObjectName(QStringLiteral("label_3"));

        vboxLayout1->addWidget(label_3);


        hboxLayout->addLayout(vboxLayout1);

        vboxLayout2 = new QVBoxLayout();
        vboxLayout2->setObjectName(QStringLiteral("vboxLayout2"));
        currentDateLable = new QLabel(RestoreProjectBackupDialog);
        currentDateLable->setObjectName(QStringLiteral("currentDateLable"));

        vboxLayout2->addWidget(currentDateLable);

        lastBackupLable = new QLabel(RestoreProjectBackupDialog);
        lastBackupLable->setObjectName(QStringLiteral("lastBackupLable"));

        vboxLayout2->addWidget(lastBackupLable);


        hboxLayout->addLayout(vboxLayout2);


        vboxLayout->addLayout(hboxLayout);

        dateTreeWidget = new QTreeWidget(RestoreProjectBackupDialog);
        dateTreeWidget->setObjectName(QStringLiteral("dateTreeWidget"));

        vboxLayout->addWidget(dateTreeWidget);

        buttonBox = new QDialogButtonBox(RestoreProjectBackupDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::NoButton|QDialogButtonBox::Ok);

        vboxLayout->addWidget(buttonBox);


        retranslateUi(RestoreProjectBackupDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), RestoreProjectBackupDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), RestoreProjectBackupDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(RestoreProjectBackupDialog);
    } // setupUi

    void retranslateUi(QDialog *RestoreProjectBackupDialog)
    {
        RestoreProjectBackupDialog->setWindowTitle(QApplication::translate("RestoreProjectBackupDialog", "Restore from backup ", nullptr));
        label->setText(QApplication::translate("RestoreProjectBackupDialog", "Set the date to restore the selected backup.", nullptr));
        label_2->setText(QApplication::translate("RestoreProjectBackupDialog", "Current date and time:", nullptr));
        label_3->setText(QApplication::translate("RestoreProjectBackupDialog", "Last backup:", nullptr));
        currentDateLable->setText(QApplication::translate("RestoreProjectBackupDialog", "-", nullptr));
        lastBackupLable->setText(QApplication::translate("RestoreProjectBackupDialog", "-", nullptr));
        QTreeWidgetItem *___qtreewidgetitem = dateTreeWidget->headerItem();
        ___qtreewidgetitem->setText(1, QApplication::translate("RestoreProjectBackupDialog", "Time", nullptr));
        ___qtreewidgetitem->setText(0, QApplication::translate("RestoreProjectBackupDialog", "Date", nullptr));
    } // retranslateUi

};

namespace Ui {
    class RestoreProjectBackupDialog: public Ui_RestoreProjectBackupDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_RESTOREPROJECTBACKUPDIALOG_H
