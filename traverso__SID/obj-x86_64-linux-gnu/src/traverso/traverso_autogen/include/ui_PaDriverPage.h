/********************************************************************************
** Form generated from reading UI file 'PaDriverPage.ui'
**
** Created by: Qt User Interface Compiler version 5.11.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PADRIVERPAGE_H
#define UI_PADRIVERPAGE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_PaDriverPage
{
public:
    QHBoxLayout *hboxLayout;
    QGroupBox *groupBox;
    QVBoxLayout *vboxLayout;
    QHBoxLayout *hboxLayout1;
    QLabel *label;
    QComboBox *driverCombo;

    void setupUi(QWidget *PaDriverPage)
    {
        if (PaDriverPage->objectName().isEmpty())
            PaDriverPage->setObjectName(QStringLiteral("PaDriverPage"));
        PaDriverPage->resize(307, 63);
        hboxLayout = new QHBoxLayout(PaDriverPage);
        hboxLayout->setSpacing(9);
        hboxLayout->setContentsMargins(0, 0, 0, 0);
        hboxLayout->setObjectName(QStringLiteral("hboxLayout"));
        groupBox = new QGroupBox(PaDriverPage);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        vboxLayout = new QVBoxLayout(groupBox);
        vboxLayout->setSpacing(9);
#ifndef Q_OS_MAC
        vboxLayout->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout->setObjectName(QStringLiteral("vboxLayout"));
        hboxLayout1 = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout1->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        hboxLayout1->setContentsMargins(0, 0, 0, 0);
#endif
        hboxLayout1->setObjectName(QStringLiteral("hboxLayout1"));
        label = new QLabel(groupBox);
        label->setObjectName(QStringLiteral("label"));

        hboxLayout1->addWidget(label);

        driverCombo = new QComboBox(groupBox);
        driverCombo->setObjectName(QStringLiteral("driverCombo"));

        hboxLayout1->addWidget(driverCombo);


        vboxLayout->addLayout(hboxLayout1);


        hboxLayout->addWidget(groupBox);


        retranslateUi(PaDriverPage);

        QMetaObject::connectSlotsByName(PaDriverPage);
    } // setupUi

    void retranslateUi(QWidget *PaDriverPage)
    {
        PaDriverPage->setWindowTitle(QApplication::translate("PaDriverPage", "Form", nullptr));
        groupBox->setTitle(QApplication::translate("PaDriverPage", "Portaudio drivers", nullptr));
#ifndef QT_NO_TOOLTIP
        label->setToolTip(QApplication::translate("PaDriverPage", "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">PortAudio Driver:</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">The driver which should drive the PortAudio backend</p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">PortAudio supports many driver backends, some of which Traverso has native support for too. </p>\n"
"<p style=\" margin-top:"
                        "0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">It is recommended to use Traverso's native drivers instead of using PortAudio's, however, if the native drivers give problems, you might try the ones supplied by PortAudio instead!</p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">PortAudio provides transparent driver support for multiple platforms, including Windows (see the wmme, direct x and asio options), and Mac OS X (see the CoreAudio and jack options)</p></body></html>", nullptr));
#endif // QT_NO_TOOLTIP
        label->setText(QApplication::translate("PaDriverPage", "Driver", nullptr));
    } // retranslateUi

};

namespace Ui {
    class PaDriverPage: public Ui_PaDriverPage {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PADRIVERPAGE_H
