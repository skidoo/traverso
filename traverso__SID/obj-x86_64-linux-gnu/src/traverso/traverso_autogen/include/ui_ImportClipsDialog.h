/********************************************************************************
** Form generated from reading UI file 'ImportClipsDialog.ui'
**
** Created by: Qt User Interface Compiler version 5.11.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_IMPORTCLIPSDIALOG_H
#define UI_IMPORTCLIPSDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_ImportClipsDialog
{
public:
    QVBoxLayout *vboxLayout;
    QHBoxLayout *hboxLayout;
    QLabel *label;
    QComboBox *comboBoxTrack;
    QCheckBox *checkBoxMarkers;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *ImportClipsDialog)
    {
        if (ImportClipsDialog->objectName().isEmpty())
            ImportClipsDialog->setObjectName(QStringLiteral("ImportClipsDialog"));
        ImportClipsDialog->resize(344, 103);
        vboxLayout = new QVBoxLayout(ImportClipsDialog);
        vboxLayout->setObjectName(QStringLiteral("vboxLayout"));
        hboxLayout = new QHBoxLayout();
        hboxLayout->setObjectName(QStringLiteral("hboxLayout"));
        label = new QLabel(ImportClipsDialog);
        label->setObjectName(QStringLiteral("label"));

        hboxLayout->addWidget(label);

        comboBoxTrack = new QComboBox(ImportClipsDialog);
        comboBoxTrack->setObjectName(QStringLiteral("comboBoxTrack"));

        hboxLayout->addWidget(comboBoxTrack);


        vboxLayout->addLayout(hboxLayout);

        checkBoxMarkers = new QCheckBox(ImportClipsDialog);
        checkBoxMarkers->setObjectName(QStringLiteral("checkBoxMarkers"));

        vboxLayout->addWidget(checkBoxMarkers);

        buttonBox = new QDialogButtonBox(ImportClipsDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::NoButton|QDialogButtonBox::Ok);

        vboxLayout->addWidget(buttonBox);


        retranslateUi(ImportClipsDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), ImportClipsDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), ImportClipsDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(ImportClipsDialog);
    } // setupUi

    void retranslateUi(QDialog *ImportClipsDialog)
    {
        ImportClipsDialog->setWindowTitle(QApplication::translate("ImportClipsDialog", "Import Audio Clips", nullptr));
        label->setText(QApplication::translate("ImportClipsDialog", "Import to Track:", nullptr));
        checkBoxMarkers->setText(QApplication::translate("ImportClipsDialog", "Add Markers", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ImportClipsDialog: public Ui_ImportClipsDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_IMPORTCLIPSDIALOG_H
