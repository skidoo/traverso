/********************************************************************************
** Form generated from reading UI file 'ProjectConverterDialog.ui'
**
** Created by: Qt User Interface Compiler version 5.11.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PROJECTCONVERTERDIALOG_H
#define UI_PROJECTCONVERTERDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_ProjectConverterDialog
{
public:
    QVBoxLayout *vboxLayout;
    QLabel *projectNameLable;
    QTextBrowser *conversionInfoText;
    QLabel *taskLable;
    QTextBrowser *taskTextBrowswer;
    QLabel *progressLable;
    QProgressBar *progressBar;
    QHBoxLayout *hboxLayout;
    QSpacerItem *spacerItem;
    QPushButton *startButton;
    QPushButton *stopConversionButton;
    QPushButton *loadProjectButton;
    QPushButton *closeButton;

    void setupUi(QDialog *ProjectConverterDialog)
    {
        if (ProjectConverterDialog->objectName().isEmpty())
            ProjectConverterDialog->setObjectName(QStringLiteral("ProjectConverterDialog"));
        ProjectConverterDialog->resize(452, 480);
        vboxLayout = new QVBoxLayout(ProjectConverterDialog);
#ifndef Q_OS_MAC
        vboxLayout->setSpacing(6);
#endif
#ifndef Q_OS_MAC
        vboxLayout->setContentsMargins(9, 9, 9, 9);
#endif
        vboxLayout->setObjectName(QStringLiteral("vboxLayout"));
        projectNameLable = new QLabel(ProjectConverterDialog);
        projectNameLable->setObjectName(QStringLiteral("projectNameLable"));

        vboxLayout->addWidget(projectNameLable);

        conversionInfoText = new QTextBrowser(ProjectConverterDialog);
        conversionInfoText->setObjectName(QStringLiteral("conversionInfoText"));
        QSizePolicy sizePolicy(static_cast<QSizePolicy::Policy>(7), static_cast<QSizePolicy::Policy>(7));
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(5);
        sizePolicy.setHeightForWidth(conversionInfoText->sizePolicy().hasHeightForWidth());
        conversionInfoText->setSizePolicy(sizePolicy);

        vboxLayout->addWidget(conversionInfoText);

        taskLable = new QLabel(ProjectConverterDialog);
        taskLable->setObjectName(QStringLiteral("taskLable"));

        vboxLayout->addWidget(taskLable);

        taskTextBrowswer = new QTextBrowser(ProjectConverterDialog);
        taskTextBrowswer->setObjectName(QStringLiteral("taskTextBrowswer"));
        QSizePolicy sizePolicy1(static_cast<QSizePolicy::Policy>(7), static_cast<QSizePolicy::Policy>(7));
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(1);
        sizePolicy1.setHeightForWidth(taskTextBrowswer->sizePolicy().hasHeightForWidth());
        taskTextBrowswer->setSizePolicy(sizePolicy1);

        vboxLayout->addWidget(taskTextBrowswer);

        progressLable = new QLabel(ProjectConverterDialog);
        progressLable->setObjectName(QStringLiteral("progressLable"));

        vboxLayout->addWidget(progressLable);

        progressBar = new QProgressBar(ProjectConverterDialog);
        progressBar->setObjectName(QStringLiteral("progressBar"));
        progressBar->setValue(0);
        progressBar->setOrientation(Qt::Horizontal);

        vboxLayout->addWidget(progressBar);

        hboxLayout = new QHBoxLayout();
#ifndef Q_OS_MAC
        hboxLayout->setSpacing(6);
#endif
        hboxLayout->setContentsMargins(0, 0, 0, 0);
        hboxLayout->setObjectName(QStringLiteral("hboxLayout"));
        spacerItem = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacerItem);

        startButton = new QPushButton(ProjectConverterDialog);
        startButton->setObjectName(QStringLiteral("startButton"));

        hboxLayout->addWidget(startButton);

        stopConversionButton = new QPushButton(ProjectConverterDialog);
        stopConversionButton->setObjectName(QStringLiteral("stopConversionButton"));

        hboxLayout->addWidget(stopConversionButton);

        loadProjectButton = new QPushButton(ProjectConverterDialog);
        loadProjectButton->setObjectName(QStringLiteral("loadProjectButton"));

        hboxLayout->addWidget(loadProjectButton);

        closeButton = new QPushButton(ProjectConverterDialog);
        closeButton->setObjectName(QStringLiteral("closeButton"));

        hboxLayout->addWidget(closeButton);


        vboxLayout->addLayout(hboxLayout);


        retranslateUi(ProjectConverterDialog);
        QObject::connect(closeButton, SIGNAL(clicked()), ProjectConverterDialog, SLOT(reject()));
        QObject::connect(startButton, SIGNAL(clicked()), ProjectConverterDialog, SLOT(accept()));

        QMetaObject::connectSlotsByName(ProjectConverterDialog);
    } // setupUi

    void retranslateUi(QDialog *ProjectConverterDialog)
    {
        ProjectConverterDialog->setWindowTitle(QApplication::translate("ProjectConverterDialog", "Project Converter", nullptr));
        projectNameLable->setText(QApplication::translate("ProjectConverterDialog", "Project XXX (no translation needed)", nullptr));
        taskLable->setText(QApplication::translate("ProjectConverterDialog", "Conversion information", nullptr));
        progressLable->setText(QApplication::translate("ProjectConverterDialog", "Conversion progress", nullptr));
        startButton->setText(QApplication::translate("ProjectConverterDialog", "Start conversion", nullptr));
        stopConversionButton->setText(QApplication::translate("ProjectConverterDialog", "Stop conversion", nullptr));
        loadProjectButton->setText(QApplication::translate("ProjectConverterDialog", "Load Project", nullptr));
        closeButton->setText(QApplication::translate("ProjectConverterDialog", "Close", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ProjectConverterDialog: public Ui_ProjectConverterDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PROJECTCONVERTERDIALOG_H
