/********************************************************************************
** Form generated from reading UI file 'AlsaDevicesPage.ui'
**
** Created by: Qt User Interface Compiler version 5.11.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ALSADEVICESPAGE_H
#define UI_ALSADEVICESPAGE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_AlsaDevicesPage
{
public:
    QVBoxLayout *vboxLayout;
    QGroupBox *groupBox;
    QVBoxLayout *vboxLayout1;
    QHBoxLayout *hboxLayout;
    QLabel *label;
    QComboBox *devicesCombo;
    QHBoxLayout *hboxLayout1;
    QLabel *label_2;
    QComboBox *periodsCombo;
    QHBoxLayout *hboxLayout2;
    QLabel *label_3;
    QComboBox *ditherShapeComboBox;

    void setupUi(QWidget *AlsaDevicesPage)
    {
        if (AlsaDevicesPage->objectName().isEmpty())
            AlsaDevicesPage->setObjectName(QStringLiteral("AlsaDevicesPage"));
        AlsaDevicesPage->resize(356, 141);
        vboxLayout = new QVBoxLayout(AlsaDevicesPage);
        vboxLayout->setObjectName(QStringLiteral("vboxLayout"));
        groupBox = new QGroupBox(AlsaDevicesPage);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        vboxLayout1 = new QVBoxLayout(groupBox);
        vboxLayout1->setSpacing(6);
        vboxLayout1->setObjectName(QStringLiteral("vboxLayout1"));
        vboxLayout1->setContentsMargins(9, 9, 9, 9);
        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        hboxLayout->setObjectName(QStringLiteral("hboxLayout"));
        hboxLayout->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(groupBox);
        label->setObjectName(QStringLiteral("label"));

        hboxLayout->addWidget(label);

        devicesCombo = new QComboBox(groupBox);
        devicesCombo->setObjectName(QStringLiteral("devicesCombo"));

        hboxLayout->addWidget(devicesCombo);


        vboxLayout1->addLayout(hboxLayout);

        hboxLayout1 = new QHBoxLayout();
        hboxLayout1->setSpacing(6);
        hboxLayout1->setObjectName(QStringLiteral("hboxLayout1"));
        hboxLayout1->setContentsMargins(0, 0, 0, 0);
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QStringLiteral("label_2"));

        hboxLayout1->addWidget(label_2);

        periodsCombo = new QComboBox(groupBox);
        periodsCombo->addItem(QString());
        periodsCombo->addItem(QString());
        periodsCombo->setObjectName(QStringLiteral("periodsCombo"));

        hboxLayout1->addWidget(periodsCombo);


        vboxLayout1->addLayout(hboxLayout1);

        hboxLayout2 = new QHBoxLayout();
        hboxLayout2->setSpacing(6);
        hboxLayout2->setObjectName(QStringLiteral("hboxLayout2"));
        hboxLayout2->setContentsMargins(0, 0, 0, 0);
        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QStringLiteral("label_3"));

        hboxLayout2->addWidget(label_3);

        ditherShapeComboBox = new QComboBox(groupBox);
        ditherShapeComboBox->addItem(QString());
        ditherShapeComboBox->addItem(QString());
        ditherShapeComboBox->addItem(QString());
        ditherShapeComboBox->addItem(QString());
        ditherShapeComboBox->setObjectName(QStringLiteral("ditherShapeComboBox"));

        hboxLayout2->addWidget(ditherShapeComboBox);


        vboxLayout1->addLayout(hboxLayout2);


        vboxLayout->addWidget(groupBox);


        retranslateUi(AlsaDevicesPage);

        QMetaObject::connectSlotsByName(AlsaDevicesPage);
    } // setupUi

    void retranslateUi(QWidget *AlsaDevicesPage)
    {
        AlsaDevicesPage->setWindowTitle(QApplication::translate("AlsaDevicesPage", "Form", nullptr));
        groupBox->setTitle(QApplication::translate("AlsaDevicesPage", "ALSA Device", nullptr));
#ifndef QT_NO_TOOLTIP
        label->setToolTip(QApplication::translate("AlsaDevicesPage", "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">Device:</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">The real or virtual ALSA device to be used.</p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">A real device is the audiocard installed in your system.</p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margi"
                        "n-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">A virtual device is one created in the .asoundrc file, </p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">often located in your home folder.</p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">If unsure, use either the default device, this will use the audiodevice </p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">configured by your distribution, or the device that names your audio card.</p>\n"
"<p style=\" margin-top:0px; margin-bo"
                        "ttom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">In the latter case, please make sure no application uses the audiocard, </p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">else the driver won't be able to initialize!</p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">For more info see chapter 3.1: \"The Driver Backend\" of the User Manual</p></body></html>", nullptr));
#endif // QT_NO_TOOLTIP
        label->setText(QApplication::translate("AlsaDevicesPage", "Device", nullptr));
#ifndef QT_NO_TOOLTIP
        label_2->setToolTip(QApplication::translate("AlsaDevicesPage", "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">Number of Periods:</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Audio is managed in small chunks called periods. </p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">This value determines how many of these chunks are </p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">to be used by the driver of the audiocard.</p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:"
                        "0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">The default should work just fine, and gives optimal latency behavior.</p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">However, some (buggy) alsa drivers don't work correctly </p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">with the default of 2, if you experience very choppy audio, </p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">please try to use 3 periods.</p></body></html>", nullptr));
#endif // QT_NO_TOOLTIP
        label_2->setText(QApplication::translate("AlsaDevicesPage", "Nr. of periods", nullptr));
        periodsCombo->setItemText(0, QApplication::translate("AlsaDevicesPage", "2", nullptr));
        periodsCombo->setItemText(1, QApplication::translate("AlsaDevicesPage", "3", nullptr));

#ifndef QT_NO_TOOLTIP
        label_3->setToolTip(QApplication::translate("AlsaDevicesPage", "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Dither is used to make the audio cleaner. </p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">The best way to describe it is to imagine a painting with many dots. If you view it up close you can see each dot and the image is not very clear. If you view it from far away the image becomes clearer because your eyes/brain dither the dots to smooth out the image. It is a murky subject and obviously a very personal ch"
                        "oice as to what dither is the best. For most people it is just plain magic. Anyone running at 16bit who cares about quality or has CPU cycles to spare should run with dither. Triangular is probably the best compromise of quality vs cpu cost (its very fast), but shaped is the best</p></body></html>", nullptr));
#endif // QT_NO_TOOLTIP
        label_3->setText(QApplication::translate("AlsaDevicesPage", "Dither", nullptr));
        ditherShapeComboBox->setItemText(0, QApplication::translate("AlsaDevicesPage", "None", nullptr));
        ditherShapeComboBox->setItemText(1, QApplication::translate("AlsaDevicesPage", "Shaped", nullptr));
        ditherShapeComboBox->setItemText(2, QApplication::translate("AlsaDevicesPage", "Rectangular", nullptr));
        ditherShapeComboBox->setItemText(3, QApplication::translate("AlsaDevicesPage", "Triangular", nullptr));

    } // retranslateUi

};

namespace Ui {
    class AlsaDevicesPage: public Ui_AlsaDevicesPage {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ALSADEVICESPAGE_H
