/****************************************************************************
** Meta object code from reading C++ file 'ExternalProcessingDialog.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/commands/ExternalProcessingDialog.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'ExternalProcessingDialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_ExternalProcessingDialog_t {
    QByteArrayData data[16];
    char stringdata0[281];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ExternalProcessingDialog_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ExternalProcessingDialog_t qt_meta_stringdata_ExternalProcessingDialog = {
    {
QT_MOC_LITERAL(0, 0, 24), // "ExternalProcessingDialog"
QT_MOC_LITERAL(1, 25, 20), // "read_standard_output"
QT_MOC_LITERAL(2, 46, 0), // ""
QT_MOC_LITERAL(3, 47, 31), // "prepare_for_external_processing"
QT_MOC_LITERAL(4, 79, 15), // "process_started"
QT_MOC_LITERAL(5, 95, 16), // "process_finished"
QT_MOC_LITERAL(6, 112, 8), // "exitcode"
QT_MOC_LITERAL(7, 121, 20), // "QProcess::ExitStatus"
QT_MOC_LITERAL(8, 142, 10), // "exitstatus"
QT_MOC_LITERAL(9, 153, 23), // "arg_combo_index_changed"
QT_MOC_LITERAL(10, 177, 4), // "text"
QT_MOC_LITERAL(11, 182, 25), // "start_external_processing"
QT_MOC_LITERAL(12, 208, 29), // "command_lineedit_text_changed"
QT_MOC_LITERAL(13, 238, 13), // "process_error"
QT_MOC_LITERAL(14, 252, 22), // "QProcess::ProcessError"
QT_MOC_LITERAL(15, 275, 5) // "error"

    },
    "ExternalProcessingDialog\0read_standard_output\0"
    "\0prepare_for_external_processing\0"
    "process_started\0process_finished\0"
    "exitcode\0QProcess::ExitStatus\0exitstatus\0"
    "arg_combo_index_changed\0text\0"
    "start_external_processing\0"
    "command_lineedit_text_changed\0"
    "process_error\0QProcess::ProcessError\0"
    "error"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ExternalProcessingDialog[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   54,    2, 0x08 /* Private */,
       3,    0,   55,    2, 0x08 /* Private */,
       4,    0,   56,    2, 0x08 /* Private */,
       5,    2,   57,    2, 0x08 /* Private */,
       9,    1,   62,    2, 0x08 /* Private */,
      11,    0,   65,    2, 0x08 /* Private */,
      12,    1,   66,    2, 0x08 /* Private */,
      13,    1,   69,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, 0x80000000 | 7,    6,    8,
    QMetaType::Void, QMetaType::QString,   10,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   10,
    QMetaType::Void, 0x80000000 | 14,   15,

       0        // eod
};

void ExternalProcessingDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        ExternalProcessingDialog *_t = static_cast<ExternalProcessingDialog *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->read_standard_output(); break;
        case 1: _t->prepare_for_external_processing(); break;
        case 2: _t->process_started(); break;
        case 3: _t->process_finished((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QProcess::ExitStatus(*)>(_a[2]))); break;
        case 4: _t->arg_combo_index_changed((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 5: _t->start_external_processing(); break;
        case 6: _t->command_lineedit_text_changed((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 7: _t->process_error((*reinterpret_cast< QProcess::ProcessError(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject ExternalProcessingDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_ExternalProcessingDialog.data,
      qt_meta_data_ExternalProcessingDialog,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *ExternalProcessingDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ExternalProcessingDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_ExternalProcessingDialog.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "Ui::ExternalProcessingDialog"))
        return static_cast< Ui::ExternalProcessingDialog*>(this);
    return QDialog::qt_metacast(_clname);
}

int ExternalProcessingDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 8;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
