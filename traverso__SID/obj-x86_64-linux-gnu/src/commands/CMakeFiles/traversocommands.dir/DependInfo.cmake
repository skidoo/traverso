# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/tmp/traverso/src/commands/AddRemove.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/AddRemove.cpp.o"
  "/tmp/traverso/src/commands/ArmTracks.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/ArmTracks.cpp.o"
  "/tmp/traverso/src/commands/AudioClipExternalProcessing.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/AudioClipExternalProcessing.cpp.o"
  "/tmp/traverso/src/commands/ClipSelection.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/ClipSelection.cpp.o"
  "/tmp/traverso/src/commands/CommandGroup.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/CommandGroup.cpp.o"
  "/tmp/traverso/src/commands/Crop.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/Crop.cpp.o"
  "/tmp/traverso/src/commands/ExternalProcessingDialog.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/ExternalProcessingDialog.cpp.o"
  "/tmp/traverso/src/commands/Fade.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/Fade.cpp.o"
  "/tmp/traverso/src/commands/Gain.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/Gain.cpp.o"
  "/tmp/traverso/src/commands/Import.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/Import.cpp.o"
  "/tmp/traverso/src/commands/MoveClip.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/MoveClip.cpp.o"
  "/tmp/traverso/src/commands/MoveEdge.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/MoveEdge.cpp.o"
  "/tmp/traverso/src/commands/PCommand.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/PCommand.cpp.o"
  "/tmp/traverso/src/commands/PlayHeadMove.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/PlayHeadMove.cpp.o"
  "/tmp/traverso/src/commands/RemoveClip.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/RemoveClip.cpp.o"
  "/tmp/traverso/src/commands/Scroll.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/Scroll.cpp.o"
  "/tmp/traverso/src/commands/SplitClip.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/SplitClip.cpp.o"
  "/tmp/traverso/src/commands/TrackPan.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/TrackPan.cpp.o"
  "/tmp/traverso/src/commands/WorkCursorMove.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/WorkCursorMove.cpp.o"
  "/tmp/traverso/src/commands/Zoom.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/Zoom.cpp.o"
  "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/moc_Crop.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/moc_Crop.cpp.o"
  "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/moc_ExternalProcessingDialog.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/moc_ExternalProcessingDialog.cpp.o"
  "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/moc_Gain.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/moc_Gain.cpp.o"
  "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/moc_MoveClip.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/moc_MoveClip.cpp.o"
  "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/moc_TrackPan.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/moc_TrackPan.cpp.o"
  "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/moc_Zoom.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/moc_Zoom.cpp.o"
  "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/traversocommands_autogen/mocs_compilation.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/CMakeFiles/traversocommands.dir/traversocommands_autogen/mocs_compilation.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ALSA_SUPPORT"
  "HAVE_SYS_STAT_H"
  "HAVE_SYS_VFS_H"
  "JACK_SUPPORT"
  "LV2_SUPPORT"
  "MP3_DECODE_SUPPORT"
  "MP3_ENCODE_SUPPORT"
  "QT_NO_DEBUG"
  "STATIC_BUILD"
  "USE_MLOCK"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "src/commands"
  "../src/commands"
  "src/commands/traversocommands_autogen/include"
  "/usr/include/lilv-0"
  "/usr/include/sratom-0"
  "/usr/include/sord-0"
  "/usr/include/serd-0"
  "../src/common"
  "/usr/include/x86_64-linux-gnu/qt5"
  "/usr/include/x86_64-linux-gnu/qt5/QtWidgets"
  "/usr/include/x86_64-linux-gnu/qt5/QtGui"
  "/usr/include/x86_64-linux-gnu/qt5/QtCore"
  "/usr/lib/x86_64-linux-gnu/qt5/mkspecs/linux-g++"
  "/usr/include/x86_64-linux-gnu/qt5/QtXml"
  "../src/core"
  "../src/traverso"
  "../src/sheetcanvas"
  "../src/plugins"
  "../src/plugins/native"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/tmp/traverso/obj-x86_64-linux-gnu/src/core/CMakeFiles/traversocore.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
