/****************************************************************************
** Meta object code from reading C++ file 'TraversoCommands.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../../../src/commands/plugins/TraversoCommands/TraversoCommands.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TraversoCommands.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_TraversoCommands_t {
    QByteArrayData data[66];
    char stringdata0[776];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_TraversoCommands_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_TraversoCommands_t qt_meta_stringdata_TraversoCommands = {
    {
QT_MOC_LITERAL(0, 0, 16), // "TraversoCommands"
QT_MOC_LITERAL(1, 17, 4), // "Gain"
QT_MOC_LITERAL(2, 22, 9), // "ResetGain"
QT_MOC_LITERAL(3, 32, 11), // "Gain: Reset"
QT_MOC_LITERAL(4, 44, 8), // "TrackPan"
QT_MOC_LITERAL(5, 53, 8), // "Panorama"
QT_MOC_LITERAL(6, 62, 13), // "ResetTrackPan"
QT_MOC_LITERAL(7, 76, 15), // "Panorama: Reset"
QT_MOC_LITERAL(8, 92, 11), // "ImportAudio"
QT_MOC_LITERAL(9, 104, 12), // "Import Audio"
QT_MOC_LITERAL(10, 117, 13), // "InsertSilence"
QT_MOC_LITERAL(11, 131, 14), // "Insert Silence"
QT_MOC_LITERAL(12, 146, 8), // "CopyClip"
QT_MOC_LITERAL(13, 155, 9), // "Copy Clip"
QT_MOC_LITERAL(14, 165, 11), // "AddNewTrack"
QT_MOC_LITERAL(15, 177, 9), // "New Track"
QT_MOC_LITERAL(16, 187, 10), // "RemoveClip"
QT_MOC_LITERAL(17, 198, 11), // "Remove Clip"
QT_MOC_LITERAL(18, 210, 11), // "RemoveTrack"
QT_MOC_LITERAL(19, 222, 12), // "Remove Track"
QT_MOC_LITERAL(20, 235, 27), // "AudioClipExternalProcessing"
QT_MOC_LITERAL(21, 263, 19), // "External Processing"
QT_MOC_LITERAL(22, 283, 19), // "ClipSelectionSelect"
QT_MOC_LITERAL(23, 303, 10), // "(De)Select"
QT_MOC_LITERAL(24, 314, 22), // "ClipSelectionSelectAll"
QT_MOC_LITERAL(25, 337, 14), // "(De)Select All"
QT_MOC_LITERAL(26, 352, 8), // "MoveClip"
QT_MOC_LITERAL(27, 361, 9), // "Move Clip"
QT_MOC_LITERAL(28, 371, 8), // "DragEdge"
QT_MOC_LITERAL(29, 380, 9), // "Drag Edge"
QT_MOC_LITERAL(30, 390, 14), // "MoveClipOrEdge"
QT_MOC_LITERAL(31, 405, 19), // "Move Or Resize Clip"
QT_MOC_LITERAL(32, 425, 9), // "SplitClip"
QT_MOC_LITERAL(33, 435, 5), // "Split"
QT_MOC_LITERAL(34, 441, 8), // "CropClip"
QT_MOC_LITERAL(35, 450, 12), // "Magnetic Cut"
QT_MOC_LITERAL(36, 463, 9), // "ArmTracks"
QT_MOC_LITERAL(37, 473, 10), // "Arm Tracks"
QT_MOC_LITERAL(38, 484, 9), // "FoldSheet"
QT_MOC_LITERAL(39, 494, 10), // "Fold Sheet"
QT_MOC_LITERAL(40, 505, 9), // "FoldTrack"
QT_MOC_LITERAL(41, 515, 10), // "Fold Track"
QT_MOC_LITERAL(42, 526, 11), // "FoldMarkers"
QT_MOC_LITERAL(43, 538, 12), // "Fold Markers"
QT_MOC_LITERAL(44, 551, 7), // "VZoomIn"
QT_MOC_LITERAL(45, 559, 11), // "Vertical In"
QT_MOC_LITERAL(46, 571, 8), // "HZoomOut"
QT_MOC_LITERAL(47, 580, 14), // "Horizontal Out"
QT_MOC_LITERAL(48, 595, 7), // "HZoomIn"
QT_MOC_LITERAL(49, 603, 13), // "Horizontal In"
QT_MOC_LITERAL(50, 617, 8), // "VZoomOut"
QT_MOC_LITERAL(51, 626, 12), // "Vertical Out"
QT_MOC_LITERAL(52, 639, 4), // "Zoom"
QT_MOC_LITERAL(53, 644, 15), // "Omnidirectional"
QT_MOC_LITERAL(54, 660, 8), // "HJogZoom"
QT_MOC_LITERAL(55, 669, 10), // "Horizontal"
QT_MOC_LITERAL(56, 680, 8), // "VJogZoom"
QT_MOC_LITERAL(57, 689, 8), // "Vertical"
QT_MOC_LITERAL(58, 698, 15), // "ScrollRightHold"
QT_MOC_LITERAL(59, 714, 5), // "Right"
QT_MOC_LITERAL(60, 720, 14), // "ScrollLeftHold"
QT_MOC_LITERAL(61, 735, 4), // "Left"
QT_MOC_LITERAL(62, 740, 12), // "ScrollUpHold"
QT_MOC_LITERAL(63, 753, 2), // "Up"
QT_MOC_LITERAL(64, 756, 14), // "ScrollDownHold"
QT_MOC_LITERAL(65, 771, 4) // "Down"

    },
    "TraversoCommands\0Gain\0ResetGain\0"
    "Gain: Reset\0TrackPan\0Panorama\0"
    "ResetTrackPan\0Panorama: Reset\0ImportAudio\0"
    "Import Audio\0InsertSilence\0Insert Silence\0"
    "CopyClip\0Copy Clip\0AddNewTrack\0New Track\0"
    "RemoveClip\0Remove Clip\0RemoveTrack\0"
    "Remove Track\0AudioClipExternalProcessing\0"
    "External Processing\0ClipSelectionSelect\0"
    "(De)Select\0ClipSelectionSelectAll\0"
    "(De)Select All\0MoveClip\0Move Clip\0"
    "DragEdge\0Drag Edge\0MoveClipOrEdge\0"
    "Move Or Resize Clip\0SplitClip\0Split\0"
    "CropClip\0Magnetic Cut\0ArmTracks\0"
    "Arm Tracks\0FoldSheet\0Fold Sheet\0"
    "FoldTrack\0Fold Track\0FoldMarkers\0"
    "Fold Markers\0VZoomIn\0Vertical In\0"
    "HZoomOut\0Horizontal Out\0HZoomIn\0"
    "Horizontal In\0VZoomOut\0Vertical Out\0"
    "Zoom\0Omnidirectional\0HJogZoom\0Horizontal\0"
    "VJogZoom\0Vertical\0ScrollRightHold\0"
    "Right\0ScrollLeftHold\0Left\0ScrollUpHold\0"
    "Up\0ScrollDownHold\0Down"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_TraversoCommands[] = {

 // content:
       7,       // revision
       0,       // classname
      33,   14, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // classinfo: key, value
       1,    1,
       2,    3,
       4,    5,
       6,    7,
       8,    9,
      10,   11,
      12,   13,
      14,   15,
      16,   17,
      18,   19,
      20,   21,
      22,   23,
      24,   25,
      26,   27,
      28,   29,
      30,   31,
      32,   33,
      34,   35,
      36,   37,
      38,   39,
      40,   41,
      42,   43,
      44,   45,
      46,   47,
      48,   49,
      50,   51,
      52,   53,
      54,   55,
      56,   57,
      58,   59,
      60,   61,
      62,   63,
      64,   65,

       0        // eod
};

void TraversoCommands::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject TraversoCommands::staticMetaObject = {
    { &CommandPlugin::staticMetaObject, qt_meta_stringdata_TraversoCommands.data,
      qt_meta_data_TraversoCommands,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *TraversoCommands::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *TraversoCommands::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_TraversoCommands.stringdata0))
        return static_cast<void*>(this);
    return CommandPlugin::qt_metacast(_clname);
}

int TraversoCommands::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = CommandPlugin::qt_metacall(_c, _id, _a);
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
