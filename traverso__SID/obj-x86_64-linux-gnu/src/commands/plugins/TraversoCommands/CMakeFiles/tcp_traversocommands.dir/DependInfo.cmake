# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/tmp/traverso/src/commands/plugins/TraversoCommands/TraversoCommands.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/plugins/TraversoCommands/CMakeFiles/tcp_traversocommands.dir/TraversoCommands.cpp.o"
  "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/plugins/TraversoCommands/tcp_traversocommands_autogen/mocs_compilation.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/commands/plugins/TraversoCommands/CMakeFiles/tcp_traversocommands.dir/tcp_traversocommands_autogen/mocs_compilation.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ALSA_SUPPORT"
  "HAVE_SYS_STAT_H"
  "HAVE_SYS_VFS_H"
  "JACK_SUPPORT"
  "LV2_SUPPORT"
  "MP3_DECODE_SUPPORT"
  "MP3_ENCODE_SUPPORT"
  "QT_NO_DEBUG"
  "STATIC_BUILD"
  "USE_MLOCK"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "src/commands/plugins/TraversoCommands"
  "../src/commands/plugins/TraversoCommands"
  "src/commands/plugins/TraversoCommands/tcp_traversocommands_autogen/include"
  "/usr/include/lilv-0"
  "/usr/include/sratom-0"
  "/usr/include/sord-0"
  "/usr/include/serd-0"
  "../src/common"
  "/usr/include/x86_64-linux-gnu/qt5"
  "/usr/include/x86_64-linux-gnu/qt5/QtWidgets"
  "/usr/include/x86_64-linux-gnu/qt5/QtGui"
  "/usr/include/x86_64-linux-gnu/qt5/QtCore"
  "/usr/lib/x86_64-linux-gnu/qt5/mkspecs/linux-g++"
  "/usr/include/x86_64-linux-gnu/qt5/QtXml"
  "../src/core"
  "../src/sheetcanvas"
  "../src/commands"
  "../src/plugins"
  "../src/plugins/native"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
