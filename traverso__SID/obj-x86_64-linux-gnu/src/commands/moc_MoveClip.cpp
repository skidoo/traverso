/****************************************************************************
** Meta object code from reading C++ file 'MoveClip.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/commands/MoveClip.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'MoveClip.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MoveClip_t {
    QByteArrayData data[21];
    char stringdata0[263];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MoveClip_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MoveClip_t qt_meta_stringdata_MoveClip = {
    {
QT_MOC_LITERAL(0, 0, 8), // "MoveClip"
QT_MOC_LITERAL(1, 9, 13), // "next_snap_pos"
QT_MOC_LITERAL(2, 23, 21), // "To next snap position"
QT_MOC_LITERAL(3, 45, 13), // "prev_snap_pos"
QT_MOC_LITERAL(4, 59, 25), // "To previous snap position"
QT_MOC_LITERAL(5, 85, 10), // "start_zoom"
QT_MOC_LITERAL(6, 96, 8), // "Jog Zoom"
QT_MOC_LITERAL(7, 105, 7), // "move_up"
QT_MOC_LITERAL(8, 113, 7), // "Move Up"
QT_MOC_LITERAL(9, 121, 9), // "move_down"
QT_MOC_LITERAL(10, 131, 9), // "Move Down"
QT_MOC_LITERAL(11, 141, 9), // "move_left"
QT_MOC_LITERAL(12, 151, 9), // "Move Left"
QT_MOC_LITERAL(13, 161, 10), // "move_right"
QT_MOC_LITERAL(14, 172, 10), // "Move Right"
QT_MOC_LITERAL(15, 183, 20), // "toggle_vertical_only"
QT_MOC_LITERAL(16, 204, 20), // "Toggle Vertical Only"
QT_MOC_LITERAL(17, 225, 0), // ""
QT_MOC_LITERAL(18, 226, 10), // "autorepeat"
QT_MOC_LITERAL(19, 237, 13), // "move_to_start"
QT_MOC_LITERAL(20, 251, 11) // "move_to_end"

    },
    "MoveClip\0next_snap_pos\0To next snap position\0"
    "prev_snap_pos\0To previous snap position\0"
    "start_zoom\0Jog Zoom\0move_up\0Move Up\0"
    "move_down\0Move Down\0move_left\0Move Left\0"
    "move_right\0Move Right\0toggle_vertical_only\0"
    "Toggle Vertical Only\0\0autorepeat\0"
    "move_to_start\0move_to_end"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MoveClip[] = {

 // content:
       7,       // revision
       0,       // classname
       8,   14, // classinfo
      10,   30, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // classinfo: key, value
       1,    2,
       3,    4,
       5,    6,
       7,    8,
       9,   10,
      11,   12,
      13,   14,
      15,   16,

 // slots: name, argc, parameters, tag, flags
       1,    1,   80,   17, 0x0a /* Public */,
       3,    1,   83,   17, 0x0a /* Public */,
      19,    1,   86,   17, 0x0a /* Public */,
      20,    1,   89,   17, 0x0a /* Public */,
       7,    1,   92,   17, 0x0a /* Public */,
       9,    1,   95,   17, 0x0a /* Public */,
      11,    1,   98,   17, 0x0a /* Public */,
      13,    1,  101,   17, 0x0a /* Public */,
       5,    1,  104,   17, 0x0a /* Public */,
      15,    1,  107,   17, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void, QMetaType::Bool,   18,
    QMetaType::Void, QMetaType::Bool,   18,
    QMetaType::Void, QMetaType::Bool,   18,
    QMetaType::Void, QMetaType::Bool,   18,
    QMetaType::Void, QMetaType::Bool,   18,
    QMetaType::Void, QMetaType::Bool,   18,
    QMetaType::Void, QMetaType::Bool,   18,
    QMetaType::Void, QMetaType::Bool,   18,
    QMetaType::Void, QMetaType::Bool,   18,
    QMetaType::Void, QMetaType::Bool,   18,

       0        // eod
};

void MoveClip::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MoveClip *_t = static_cast<MoveClip *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->next_snap_pos((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->prev_snap_pos((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 2: _t->move_to_start((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->move_to_end((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 4: _t->move_up((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 5: _t->move_down((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 6: _t->move_left((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 7: _t->move_right((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 8: _t->start_zoom((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 9: _t->toggle_vertical_only((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MoveClip::staticMetaObject = {
    { &Command::staticMetaObject, qt_meta_stringdata_MoveClip.data,
      qt_meta_data_MoveClip,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *MoveClip::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MoveClip::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MoveClip.stringdata0))
        return static_cast<void*>(this);
    return Command::qt_metacast(_clname);
}

int MoveClip::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Command::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 10)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 10;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
