# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/tmp/traverso/src/audiofileio/decode/AbstractAudioReader.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/audiofileio/CMakeFiles/traversoaudiofileio.dir/decode/AbstractAudioReader.cpp.o"
  "/tmp/traverso/src/audiofileio/decode/FlacAudioReader.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/audiofileio/CMakeFiles/traversoaudiofileio.dir/decode/FlacAudioReader.cpp.o"
  "/tmp/traverso/src/audiofileio/decode/MadAudioReader.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/audiofileio/CMakeFiles/traversoaudiofileio.dir/decode/MadAudioReader.cpp.o"
  "/tmp/traverso/src/audiofileio/decode/ResampleAudioReader.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/audiofileio/CMakeFiles/traversoaudiofileio.dir/decode/ResampleAudioReader.cpp.o"
  "/tmp/traverso/src/audiofileio/decode/SFAudioReader.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/audiofileio/CMakeFiles/traversoaudiofileio.dir/decode/SFAudioReader.cpp.o"
  "/tmp/traverso/src/audiofileio/decode/VorbisAudioReader.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/audiofileio/CMakeFiles/traversoaudiofileio.dir/decode/VorbisAudioReader.cpp.o"
  "/tmp/traverso/src/audiofileio/decode/WPAudioReader.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/audiofileio/CMakeFiles/traversoaudiofileio.dir/decode/WPAudioReader.cpp.o"
  "/tmp/traverso/src/audiofileio/encode/AbstractAudioWriter.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/audiofileio/CMakeFiles/traversoaudiofileio.dir/encode/AbstractAudioWriter.cpp.o"
  "/tmp/traverso/src/audiofileio/encode/FlacAudioWriter.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/audiofileio/CMakeFiles/traversoaudiofileio.dir/encode/FlacAudioWriter.cpp.o"
  "/tmp/traverso/src/audiofileio/encode/LameAudioWriter.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/audiofileio/CMakeFiles/traversoaudiofileio.dir/encode/LameAudioWriter.cpp.o"
  "/tmp/traverso/src/audiofileio/encode/SFAudioWriter.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/audiofileio/CMakeFiles/traversoaudiofileio.dir/encode/SFAudioWriter.cpp.o"
  "/tmp/traverso/src/audiofileio/encode/VorbisAudioWriter.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/audiofileio/CMakeFiles/traversoaudiofileio.dir/encode/VorbisAudioWriter.cpp.o"
  "/tmp/traverso/src/audiofileio/encode/WPAudioWriter.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/audiofileio/CMakeFiles/traversoaudiofileio.dir/encode/WPAudioWriter.cpp.o"
  "/tmp/traverso/obj-x86_64-linux-gnu/src/audiofileio/traversoaudiofileio_autogen/mocs_compilation.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/audiofileio/CMakeFiles/traversoaudiofileio.dir/traversoaudiofileio_autogen/mocs_compilation.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ALSA_SUPPORT"
  "HAVE_SYS_STAT_H"
  "HAVE_SYS_VFS_H"
  "JACK_SUPPORT"
  "LV2_SUPPORT"
  "MP3_DECODE_SUPPORT"
  "MP3_ENCODE_SUPPORT"
  "QT_NO_DEBUG"
  "STATIC_BUILD"
  "USE_MLOCK"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "src/audiofileio"
  "../src/audiofileio"
  "src/audiofileio/traversoaudiofileio_autogen/include"
  "/usr/include/lilv-0"
  "/usr/include/sratom-0"
  "/usr/include/sord-0"
  "/usr/include/serd-0"
  "../src/common"
  "/usr/include/x86_64-linux-gnu/qt5"
  "/usr/include/x86_64-linux-gnu/qt5/QtWidgets"
  "/usr/include/x86_64-linux-gnu/qt5/QtGui"
  "/usr/include/x86_64-linux-gnu/qt5/QtCore"
  "/usr/lib/x86_64-linux-gnu/qt5/mkspecs/linux-g++"
  "/usr/include/x86_64-linux-gnu/qt5/QtXml"
  "../src/audiofileio/decode"
  "../src/audiofileio/encode"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
