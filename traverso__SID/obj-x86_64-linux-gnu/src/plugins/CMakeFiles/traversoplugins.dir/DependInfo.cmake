# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/tmp/traverso/src/plugins/LV2/LV2Plugin.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/CMakeFiles/traversoplugins.dir/LV2/LV2Plugin.cpp.o"
  "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/LV2/moc_LV2Plugin.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/CMakeFiles/traversoplugins.dir/LV2/moc_LV2Plugin.cpp.o"
  "/tmp/traverso/src/plugins/Plugin.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/CMakeFiles/traversoplugins.dir/Plugin.cpp.o"
  "/tmp/traverso/src/plugins/PluginChain.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/CMakeFiles/traversoplugins.dir/PluginChain.cpp.o"
  "/tmp/traverso/src/plugins/PluginManager.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/CMakeFiles/traversoplugins.dir/PluginManager.cpp.o"
  "/tmp/traverso/src/plugins/PluginPropertiesDialog.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/CMakeFiles/traversoplugins.dir/PluginPropertiesDialog.cpp.o"
  "/tmp/traverso/src/plugins/PluginSlider.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/CMakeFiles/traversoplugins.dir/PluginSlider.cpp.o"
  "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/moc_Plugin.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/CMakeFiles/traversoplugins.dir/moc_Plugin.cpp.o"
  "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/moc_PluginChain.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/CMakeFiles/traversoplugins.dir/moc_PluginChain.cpp.o"
  "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/moc_PluginPropertiesDialog.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/CMakeFiles/traversoplugins.dir/moc_PluginPropertiesDialog.cpp.o"
  "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/moc_PluginSlider.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/CMakeFiles/traversoplugins.dir/moc_PluginSlider.cpp.o"
  "/tmp/traverso/src/plugins/native/CorrelationMeter.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/CMakeFiles/traversoplugins.dir/native/CorrelationMeter.cpp.o"
  "/tmp/traverso/src/plugins/native/GainEnvelope.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/CMakeFiles/traversoplugins.dir/native/GainEnvelope.cpp.o"
  "/tmp/traverso/src/plugins/native/SpectralMeter.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/CMakeFiles/traversoplugins.dir/native/SpectralMeter.cpp.o"
  "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/native/moc_CorrelationMeter.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/CMakeFiles/traversoplugins.dir/native/moc_CorrelationMeter.cpp.o"
  "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/native/moc_GainEnvelope.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/CMakeFiles/traversoplugins.dir/native/moc_GainEnvelope.cpp.o"
  "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/native/moc_SpectralMeter.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/CMakeFiles/traversoplugins.dir/native/moc_SpectralMeter.cpp.o"
  "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/traversoplugins_autogen/mocs_compilation.cpp" "/tmp/traverso/obj-x86_64-linux-gnu/src/plugins/CMakeFiles/traversoplugins.dir/traversoplugins_autogen/mocs_compilation.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ALSA_SUPPORT"
  "HAVE_SYS_STAT_H"
  "HAVE_SYS_VFS_H"
  "JACK_SUPPORT"
  "LV2_SUPPORT"
  "MP3_DECODE_SUPPORT"
  "MP3_ENCODE_SUPPORT"
  "QT_NO_DEBUG"
  "STATIC_BUILD"
  "USE_MLOCK"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "src/plugins"
  "../src/plugins"
  "src/plugins/traversoplugins_autogen/include"
  "/usr/include/lilv-0"
  "/usr/include/sratom-0"
  "/usr/include/sord-0"
  "/usr/include/serd-0"
  "../src/common"
  "/usr/include/x86_64-linux-gnu/qt5"
  "/usr/include/x86_64-linux-gnu/qt5/QtWidgets"
  "/usr/include/x86_64-linux-gnu/qt5/QtGui"
  "/usr/include/x86_64-linux-gnu/qt5/QtCore"
  "/usr/lib/x86_64-linux-gnu/qt5/mkspecs/linux-g++"
  "/usr/include/x86_64-linux-gnu/qt5/QtXml"
  "../src/core"
  "../src/engine"
  "../src/commands"
  "../src/plugins/native"
  "../src/plugins/LV2"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
