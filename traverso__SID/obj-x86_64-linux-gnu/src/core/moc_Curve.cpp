/****************************************************************************
** Meta object code from reading C++ file 'Curve.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/core/Curve.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Curve.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Curve_t {
    QByteArrayData data[11];
    char stringdata0[127];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Curve_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Curve_t qt_meta_stringdata_Curve = {
    {
QT_MOC_LITERAL(0, 0, 5), // "Curve"
QT_MOC_LITERAL(1, 6, 12), // "stateChanged"
QT_MOC_LITERAL(2, 19, 0), // ""
QT_MOC_LITERAL(3, 20, 9), // "nodeAdded"
QT_MOC_LITERAL(4, 30, 10), // "CurveNode*"
QT_MOC_LITERAL(5, 41, 11), // "nodeRemoved"
QT_MOC_LITERAL(6, 53, 19), // "nodePositionChanged"
QT_MOC_LITERAL(7, 73, 11), // "set_changed"
QT_MOC_LITERAL(8, 85, 16), // "private_add_node"
QT_MOC_LITERAL(9, 102, 4), // "node"
QT_MOC_LITERAL(10, 107, 19) // "private_remove_node"

    },
    "Curve\0stateChanged\0\0nodeAdded\0CurveNode*\0"
    "nodeRemoved\0nodePositionChanged\0"
    "set_changed\0private_add_node\0node\0"
    "private_remove_node"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Curve[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   49,    2, 0x06 /* Public */,
       3,    1,   50,    2, 0x06 /* Public */,
       5,    1,   53,    2, 0x06 /* Public */,
       6,    0,   56,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       7,    0,   57,    2, 0x09 /* Protected */,
       8,    1,   58,    2, 0x08 /* Private */,
      10,    1,   61,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4,    2,
    QMetaType::Void, 0x80000000 | 4,    2,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4,    9,
    QMetaType::Void, 0x80000000 | 4,    9,

       0        // eod
};

void Curve::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Curve *_t = static_cast<Curve *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->stateChanged(); break;
        case 1: _t->nodeAdded((*reinterpret_cast< CurveNode*(*)>(_a[1]))); break;
        case 2: _t->nodeRemoved((*reinterpret_cast< CurveNode*(*)>(_a[1]))); break;
        case 3: _t->nodePositionChanged(); break;
        case 4: _t->set_changed(); break;
        case 5: _t->private_add_node((*reinterpret_cast< CurveNode*(*)>(_a[1]))); break;
        case 6: _t->private_remove_node((*reinterpret_cast< CurveNode*(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (Curve::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Curve::stateChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (Curve::*)(CurveNode * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Curve::nodeAdded)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (Curve::*)(CurveNode * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Curve::nodeRemoved)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (Curve::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Curve::nodePositionChanged)) {
                *result = 3;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject Curve::staticMetaObject = {
    { &ContextItem::staticMetaObject, qt_meta_stringdata_Curve.data,
      qt_meta_data_Curve,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *Curve::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Curve::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Curve.stringdata0))
        return static_cast<void*>(this);
    return ContextItem::qt_metacast(_clname);
}

int Curve::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ContextItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 7;
    }
    return _id;
}

// SIGNAL 0
void Curve::stateChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void Curve::nodeAdded(CurveNode * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Curve::nodeRemoved(CurveNode * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Curve::nodePositionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
