/****************************************************************************
** Meta object code from reading C++ file 'Track.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/core/Track.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Track.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Track_t {
    QByteArrayData data[36];
    char stringdata0[406];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Track_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Track_t qt_meta_stringdata_Track = {
    {
QT_MOC_LITERAL(0, 0, 5), // "Track"
QT_MOC_LITERAL(1, 6, 4), // "mute"
QT_MOC_LITERAL(2, 11, 4), // "Mute"
QT_MOC_LITERAL(3, 16, 10), // "toggle_arm"
QT_MOC_LITERAL(4, 27, 14), // "Record: On/Off"
QT_MOC_LITERAL(5, 42, 4), // "solo"
QT_MOC_LITERAL(6, 47, 4), // "Solo"
QT_MOC_LITERAL(7, 52, 14), // "silence_others"
QT_MOC_LITERAL(8, 67, 20), // "Silence other tracks"
QT_MOC_LITERAL(9, 88, 14), // "audioClipAdded"
QT_MOC_LITERAL(10, 103, 0), // ""
QT_MOC_LITERAL(11, 104, 10), // "AudioClip*"
QT_MOC_LITERAL(12, 115, 4), // "clip"
QT_MOC_LITERAL(13, 120, 16), // "audioClipRemoved"
QT_MOC_LITERAL(14, 137, 13), // "heightChanged"
QT_MOC_LITERAL(15, 151, 11), // "muteChanged"
QT_MOC_LITERAL(16, 163, 7), // "isMuted"
QT_MOC_LITERAL(17, 171, 11), // "soloChanged"
QT_MOC_LITERAL(18, 183, 6), // "isSolo"
QT_MOC_LITERAL(19, 190, 12), // "armedChanged"
QT_MOC_LITERAL(20, 203, 7), // "isArmed"
QT_MOC_LITERAL(21, 211, 11), // "lockChanged"
QT_MOC_LITERAL(22, 223, 8), // "isLocked"
QT_MOC_LITERAL(23, 232, 11), // "gainChanged"
QT_MOC_LITERAL(24, 244, 10), // "panChanged"
QT_MOC_LITERAL(25, 255, 12), // "stateChanged"
QT_MOC_LITERAL(26, 268, 19), // "audibleStateChanged"
QT_MOC_LITERAL(27, 288, 12), // "inBusChanged"
QT_MOC_LITERAL(28, 301, 13), // "outBusChanged"
QT_MOC_LITERAL(29, 315, 8), // "set_gain"
QT_MOC_LITERAL(30, 324, 4), // "gain"
QT_MOC_LITERAL(31, 329, 21), // "clip_position_changed"
QT_MOC_LITERAL(32, 351, 8), // "get_gain"
QT_MOC_LITERAL(33, 360, 8), // "Command*"
QT_MOC_LITERAL(34, 369, 16), // "private_add_clip"
QT_MOC_LITERAL(35, 386, 19) // "private_remove_clip"

    },
    "Track\0mute\0Mute\0toggle_arm\0Record: On/Off\0"
    "solo\0Solo\0silence_others\0Silence other tracks\0"
    "audioClipAdded\0\0AudioClip*\0clip\0"
    "audioClipRemoved\0heightChanged\0"
    "muteChanged\0isMuted\0soloChanged\0isSolo\0"
    "armedChanged\0isArmed\0lockChanged\0"
    "isLocked\0gainChanged\0panChanged\0"
    "stateChanged\0audibleStateChanged\0"
    "inBusChanged\0outBusChanged\0set_gain\0"
    "gain\0clip_position_changed\0get_gain\0"
    "Command*\0private_add_clip\0private_remove_clip"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Track[] = {

 // content:
       7,       // revision
       0,       // classname
       4,   14, // classinfo
      22,   22, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      13,       // signalCount

 // classinfo: key, value
       1,    2,
       3,    4,
       5,    6,
       7,    8,

 // signals: name, argc, parameters, tag, flags
       9,    1,  132,   10, 0x06 /* Public */,
      13,    1,  135,   10, 0x06 /* Public */,
      14,    0,  138,   10, 0x06 /* Public */,
      15,    1,  139,   10, 0x06 /* Public */,
      17,    1,  142,   10, 0x06 /* Public */,
      19,    1,  145,   10, 0x06 /* Public */,
      21,    1,  148,   10, 0x06 /* Public */,
      23,    0,  151,   10, 0x06 /* Public */,
      24,    0,  152,   10, 0x06 /* Public */,
      25,    0,  153,   10, 0x06 /* Public */,
      26,    0,  154,   10, 0x06 /* Public */,
      27,    0,  155,   10, 0x06 /* Public */,
      28,    0,  156,   10, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      29,    1,  157,   10, 0x0a /* Public */,
      31,    1,  160,   10, 0x0a /* Public */,
      32,    0,  163,   10, 0x0a /* Public */,
       1,    0,  164,   10, 0x0a /* Public */,
       3,    0,  165,   10, 0x0a /* Public */,
       5,    0,  166,   10, 0x0a /* Public */,
       7,    0,  167,   10, 0x0a /* Public */,
      34,    1,  168,   10, 0x08 /* Private */,
      35,    1,  171,   10, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   16,
    QMetaType::Void, QMetaType::Bool,   18,
    QMetaType::Void, QMetaType::Bool,   20,
    QMetaType::Void, QMetaType::Bool,   22,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, QMetaType::Float,   30,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Float,
    0x80000000 | 33,
    0x80000000 | 33,
    0x80000000 | 33,
    0x80000000 | 33,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void, 0x80000000 | 11,   12,

       0        // eod
};

void Track::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Track *_t = static_cast<Track *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->audioClipAdded((*reinterpret_cast< AudioClip*(*)>(_a[1]))); break;
        case 1: _t->audioClipRemoved((*reinterpret_cast< AudioClip*(*)>(_a[1]))); break;
        case 2: _t->heightChanged(); break;
        case 3: _t->muteChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 4: _t->soloChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 5: _t->armedChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 6: _t->lockChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 7: _t->gainChanged(); break;
        case 8: _t->panChanged(); break;
        case 9: _t->stateChanged(); break;
        case 10: _t->audibleStateChanged(); break;
        case 11: _t->inBusChanged(); break;
        case 12: _t->outBusChanged(); break;
        case 13: _t->set_gain((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 14: _t->clip_position_changed((*reinterpret_cast< AudioClip*(*)>(_a[1]))); break;
        case 15: { float _r = _t->get_gain();
            if (_a[0]) *reinterpret_cast< float*>(_a[0]) = std::move(_r); }  break;
        case 16: { Command* _r = _t->mute();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 17: { Command* _r = _t->toggle_arm();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 18: { Command* _r = _t->solo();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 19: { Command* _r = _t->silence_others();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 20: _t->private_add_clip((*reinterpret_cast< AudioClip*(*)>(_a[1]))); break;
        case 21: _t->private_remove_clip((*reinterpret_cast< AudioClip*(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (Track::*)(AudioClip * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Track::audioClipAdded)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (Track::*)(AudioClip * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Track::audioClipRemoved)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (Track::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Track::heightChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (Track::*)(bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Track::muteChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (Track::*)(bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Track::soloChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (Track::*)(bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Track::armedChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (Track::*)(bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Track::lockChanged)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (Track::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Track::gainChanged)) {
                *result = 7;
                return;
            }
        }
        {
            using _t = void (Track::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Track::panChanged)) {
                *result = 8;
                return;
            }
        }
        {
            using _t = void (Track::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Track::stateChanged)) {
                *result = 9;
                return;
            }
        }
        {
            using _t = void (Track::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Track::audibleStateChanged)) {
                *result = 10;
                return;
            }
        }
        {
            using _t = void (Track::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Track::inBusChanged)) {
                *result = 11;
                return;
            }
        }
        {
            using _t = void (Track::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Track::outBusChanged)) {
                *result = 12;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject Track::staticMetaObject = {
    { &ContextItem::staticMetaObject, qt_meta_stringdata_Track.data,
      qt_meta_data_Track,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *Track::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Track::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Track.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "AudioProcessingItem"))
        return static_cast< AudioProcessingItem*>(this);
    return ContextItem::qt_metacast(_clname);
}

int Track::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ContextItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 22)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 22;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 22)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 22;
    }
    return _id;
}

// SIGNAL 0
void Track::audioClipAdded(AudioClip * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Track::audioClipRemoved(AudioClip * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Track::heightChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void Track::muteChanged(bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void Track::soloChanged(bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void Track::armedChanged(bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void Track::lockChanged(bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void Track::gainChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, nullptr);
}

// SIGNAL 8
void Track::panChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 8, nullptr);
}

// SIGNAL 9
void Track::stateChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 9, nullptr);
}

// SIGNAL 10
void Track::audibleStateChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 10, nullptr);
}

// SIGNAL 11
void Track::inBusChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 11, nullptr);
}

// SIGNAL 12
void Track::outBusChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 12, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
