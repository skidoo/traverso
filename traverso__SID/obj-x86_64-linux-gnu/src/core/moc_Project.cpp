/****************************************************************************
** Meta object code from reading C++ file 'Project.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/core/Project.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Project.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Project_t {
    QByteArrayData data[16];
    char stringdata0[234];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Project_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Project_t qt_meta_stringdata_Project = {
    {
QT_MOC_LITERAL(0, 0, 7), // "Project"
QT_MOC_LITERAL(1, 8, 19), // "currentSheetChanged"
QT_MOC_LITERAL(2, 28, 0), // ""
QT_MOC_LITERAL(3, 29, 6), // "Sheet*"
QT_MOC_LITERAL(4, 36, 10), // "sheetAdded"
QT_MOC_LITERAL(5, 47, 12), // "sheetRemoved"
QT_MOC_LITERAL(6, 60, 26), // "sheetExportProgressChanged"
QT_MOC_LITERAL(7, 87, 28), // "overallExportProgressChanged"
QT_MOC_LITERAL(8, 116, 14), // "exportFinished"
QT_MOC_LITERAL(9, 131, 21), // "exportStartedForSheet"
QT_MOC_LITERAL(10, 153, 19), // "projectLoadFinished"
QT_MOC_LITERAL(11, 173, 6), // "select"
QT_MOC_LITERAL(12, 180, 8), // "Command*"
QT_MOC_LITERAL(13, 189, 17), // "private_add_sheet"
QT_MOC_LITERAL(14, 207, 5), // "sheet"
QT_MOC_LITERAL(15, 213, 20) // "private_remove_sheet"

    },
    "Project\0currentSheetChanged\0\0Sheet*\0"
    "sheetAdded\0sheetRemoved\0"
    "sheetExportProgressChanged\0"
    "overallExportProgressChanged\0"
    "exportFinished\0exportStartedForSheet\0"
    "projectLoadFinished\0select\0Command*\0"
    "private_add_sheet\0sheet\0private_remove_sheet"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Project[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       8,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   69,    2, 0x06 /* Public */,
       4,    1,   72,    2, 0x06 /* Public */,
       5,    1,   75,    2, 0x06 /* Public */,
       6,    1,   78,    2, 0x06 /* Public */,
       7,    1,   81,    2, 0x06 /* Public */,
       8,    0,   84,    2, 0x06 /* Public */,
       9,    1,   85,    2, 0x06 /* Public */,
      10,    0,   88,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      11,    0,   89,    2, 0x0a /* Public */,
      13,    1,   90,    2, 0x08 /* Private */,
      15,    1,   93,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    2,
    QMetaType::Void, 0x80000000 | 3,    2,
    QMetaType::Void, 0x80000000 | 3,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 3,    2,
    QMetaType::Void,

 // slots: parameters
    0x80000000 | 12,
    QMetaType::Void, 0x80000000 | 3,   14,
    QMetaType::Void, 0x80000000 | 3,   14,

       0        // eod
};

void Project::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Project *_t = static_cast<Project *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->currentSheetChanged((*reinterpret_cast< Sheet*(*)>(_a[1]))); break;
        case 1: _t->sheetAdded((*reinterpret_cast< Sheet*(*)>(_a[1]))); break;
        case 2: _t->sheetRemoved((*reinterpret_cast< Sheet*(*)>(_a[1]))); break;
        case 3: _t->sheetExportProgressChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->overallExportProgressChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->exportFinished(); break;
        case 6: _t->exportStartedForSheet((*reinterpret_cast< Sheet*(*)>(_a[1]))); break;
        case 7: _t->projectLoadFinished(); break;
        case 8: { Command* _r = _t->select();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 9: _t->private_add_sheet((*reinterpret_cast< Sheet*(*)>(_a[1]))); break;
        case 10: _t->private_remove_sheet((*reinterpret_cast< Sheet*(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (Project::*)(Sheet * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Project::currentSheetChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (Project::*)(Sheet * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Project::sheetAdded)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (Project::*)(Sheet * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Project::sheetRemoved)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (Project::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Project::sheetExportProgressChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (Project::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Project::overallExportProgressChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (Project::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Project::exportFinished)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (Project::*)(Sheet * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Project::exportStartedForSheet)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (Project::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Project::projectLoadFinished)) {
                *result = 7;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject Project::staticMetaObject = {
    { &ContextItem::staticMetaObject, qt_meta_stringdata_Project.data,
      qt_meta_data_Project,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *Project::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Project::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Project.stringdata0))
        return static_cast<void*>(this);
    return ContextItem::qt_metacast(_clname);
}

int Project::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ContextItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 11)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 11;
    }
    return _id;
}

// SIGNAL 0
void Project::currentSheetChanged(Sheet * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Project::sheetAdded(Sheet * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Project::sheetRemoved(Sheet * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Project::sheetExportProgressChanged(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void Project::overallExportProgressChanged(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void Project::exportFinished()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void Project::exportStartedForSheet(Sheet * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void Project::projectLoadFinished()
{
    QMetaObject::activate(this, &staticMetaObject, 7, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
