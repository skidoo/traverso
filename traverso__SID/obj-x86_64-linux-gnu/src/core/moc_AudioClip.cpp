/****************************************************************************
** Meta object code from reading C++ file 'AudioClip.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/core/AudioClip.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'AudioClip.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_AudioClip_t {
    QByteArrayData data[41];
    char stringdata0[493];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_AudioClip_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_AudioClip_t qt_meta_stringdata_AudioClip = {
    {
QT_MOC_LITERAL(0, 0, 9), // "AudioClip"
QT_MOC_LITERAL(1, 10, 4), // "mute"
QT_MOC_LITERAL(2, 15, 4), // "Mute"
QT_MOC_LITERAL(3, 20, 13), // "reset_fade_in"
QT_MOC_LITERAL(4, 34, 10), // "In: Remove"
QT_MOC_LITERAL(5, 45, 14), // "reset_fade_out"
QT_MOC_LITERAL(6, 60, 11), // "Out: Remove"
QT_MOC_LITERAL(7, 72, 15), // "reset_fade_both"
QT_MOC_LITERAL(8, 88, 12), // "Both: Remove"
QT_MOC_LITERAL(9, 101, 9), // "normalize"
QT_MOC_LITERAL(10, 111, 9), // "Normalize"
QT_MOC_LITERAL(11, 121, 4), // "lock"
QT_MOC_LITERAL(12, 126, 4), // "Lock"
QT_MOC_LITERAL(13, 131, 12), // "stateChanged"
QT_MOC_LITERAL(14, 144, 0), // ""
QT_MOC_LITERAL(15, 145, 11), // "muteChanged"
QT_MOC_LITERAL(16, 157, 11), // "lockChanged"
QT_MOC_LITERAL(17, 169, 15), // "positionChanged"
QT_MOC_LITERAL(18, 185, 9), // "fadeAdded"
QT_MOC_LITERAL(19, 195, 10), // "FadeCurve*"
QT_MOC_LITERAL(20, 206, 11), // "fadeRemoved"
QT_MOC_LITERAL(21, 218, 17), // "recordingFinished"
QT_MOC_LITERAL(22, 236, 10), // "AudioClip*"
QT_MOC_LITERAL(23, 247, 16), // "finish_recording"
QT_MOC_LITERAL(24, 264, 19), // "finish_write_source"
QT_MOC_LITERAL(25, 284, 13), // "set_left_edge"
QT_MOC_LITERAL(26, 298, 7), // "TimeRef"
QT_MOC_LITERAL(27, 306, 15), // "newLeftLocation"
QT_MOC_LITERAL(28, 322, 14), // "set_right_edge"
QT_MOC_LITERAL(29, 337, 16), // "newRightLocation"
QT_MOC_LITERAL(30, 354, 27), // "track_audible_state_changed"
QT_MOC_LITERAL(31, 382, 11), // "toggle_mute"
QT_MOC_LITERAL(32, 394, 11), // "toggle_lock"
QT_MOC_LITERAL(33, 406, 8), // "set_gain"
QT_MOC_LITERAL(34, 415, 1), // "g"
QT_MOC_LITERAL(35, 417, 8), // "get_gain"
QT_MOC_LITERAL(36, 426, 8), // "Command*"
QT_MOC_LITERAL(37, 435, 16), // "private_add_fade"
QT_MOC_LITERAL(38, 452, 4), // "fade"
QT_MOC_LITERAL(39, 457, 19), // "private_remove_fade"
QT_MOC_LITERAL(40, 477, 15) // "get_capture_bus"

    },
    "AudioClip\0mute\0Mute\0reset_fade_in\0"
    "In: Remove\0reset_fade_out\0Out: Remove\0"
    "reset_fade_both\0Both: Remove\0normalize\0"
    "Normalize\0lock\0Lock\0stateChanged\0\0"
    "muteChanged\0lockChanged\0positionChanged\0"
    "fadeAdded\0FadeCurve*\0fadeRemoved\0"
    "recordingFinished\0AudioClip*\0"
    "finish_recording\0finish_write_source\0"
    "set_left_edge\0TimeRef\0newLeftLocation\0"
    "set_right_edge\0newRightLocation\0"
    "track_audible_state_changed\0toggle_mute\0"
    "toggle_lock\0set_gain\0g\0get_gain\0"
    "Command*\0private_add_fade\0fade\0"
    "private_remove_fade\0get_capture_bus"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_AudioClip[] = {

 // content:
       7,       // revision
       0,       // classname
       6,   14, // classinfo
      25,   26, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       7,       // signalCount

 // classinfo: key, value
       1,    2,
       3,    4,
       5,    6,
       7,    8,
       9,   10,
      11,   12,

 // signals: name, argc, parameters, tag, flags
      13,    0,  151,   14, 0x06 /* Public */,
      15,    0,  152,   14, 0x06 /* Public */,
      16,    0,  153,   14, 0x06 /* Public */,
      17,    0,  154,   14, 0x06 /* Public */,
      18,    1,  155,   14, 0x06 /* Public */,
      20,    1,  158,   14, 0x06 /* Public */,
      21,    1,  161,   14, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      23,    0,  164,   14, 0x0a /* Public */,
      24,    0,  165,   14, 0x0a /* Public */,
      25,    1,  166,   14, 0x0a /* Public */,
      28,    1,  169,   14, 0x0a /* Public */,
      30,    0,  172,   14, 0x0a /* Public */,
      31,    0,  173,   14, 0x0a /* Public */,
      32,    0,  174,   14, 0x0a /* Public */,
      33,    1,  175,   14, 0x0a /* Public */,
      35,    0,  178,   14, 0x0a /* Public */,
       1,    0,  179,   14, 0x0a /* Public */,
       3,    0,  180,   14, 0x0a /* Public */,
       5,    0,  181,   14, 0x0a /* Public */,
       7,    0,  182,   14, 0x0a /* Public */,
       9,    0,  183,   14, 0x0a /* Public */,
      11,    0,  184,   14, 0x0a /* Public */,
      37,    1,  185,   14, 0x08 /* Private */,
      39,    1,  188,   14, 0x08 /* Private */,
      40,    0,  191,   14, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 19,   14,
    QMetaType::Void, 0x80000000 | 19,   14,
    QMetaType::Void, 0x80000000 | 22,   14,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 26,   27,
    QMetaType::Void, 0x80000000 | 26,   29,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Float,   34,
    QMetaType::Float,
    0x80000000 | 36,
    0x80000000 | 36,
    0x80000000 | 36,
    0x80000000 | 36,
    0x80000000 | 36,
    0x80000000 | 36,
    QMetaType::Void, 0x80000000 | 19,   38,
    QMetaType::Void, 0x80000000 | 19,   38,
    QMetaType::Void,

       0        // eod
};

void AudioClip::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        AudioClip *_t = static_cast<AudioClip *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->stateChanged(); break;
        case 1: _t->muteChanged(); break;
        case 2: _t->lockChanged(); break;
        case 3: _t->positionChanged(); break;
        case 4: _t->fadeAdded((*reinterpret_cast< FadeCurve*(*)>(_a[1]))); break;
        case 5: _t->fadeRemoved((*reinterpret_cast< FadeCurve*(*)>(_a[1]))); break;
        case 6: _t->recordingFinished((*reinterpret_cast< AudioClip*(*)>(_a[1]))); break;
        case 7: _t->finish_recording(); break;
        case 8: _t->finish_write_source(); break;
        case 9: _t->set_left_edge((*reinterpret_cast< TimeRef(*)>(_a[1]))); break;
        case 10: _t->set_right_edge((*reinterpret_cast< TimeRef(*)>(_a[1]))); break;
        case 11: _t->track_audible_state_changed(); break;
        case 12: _t->toggle_mute(); break;
        case 13: _t->toggle_lock(); break;
        case 14: _t->set_gain((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 15: { float _r = _t->get_gain();
            if (_a[0]) *reinterpret_cast< float*>(_a[0]) = std::move(_r); }  break;
        case 16: { Command* _r = _t->mute();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 17: { Command* _r = _t->reset_fade_in();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 18: { Command* _r = _t->reset_fade_out();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 19: { Command* _r = _t->reset_fade_both();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 20: { Command* _r = _t->normalize();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 21: { Command* _r = _t->lock();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 22: _t->private_add_fade((*reinterpret_cast< FadeCurve*(*)>(_a[1]))); break;
        case 23: _t->private_remove_fade((*reinterpret_cast< FadeCurve*(*)>(_a[1]))); break;
        case 24: _t->get_capture_bus(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 6:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< AudioClip* >(); break;
            }
            break;
        case 9:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< TimeRef >(); break;
            }
            break;
        case 10:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< TimeRef >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (AudioClip::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&AudioClip::stateChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (AudioClip::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&AudioClip::muteChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (AudioClip::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&AudioClip::lockChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (AudioClip::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&AudioClip::positionChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (AudioClip::*)(FadeCurve * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&AudioClip::fadeAdded)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (AudioClip::*)(FadeCurve * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&AudioClip::fadeRemoved)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (AudioClip::*)(AudioClip * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&AudioClip::recordingFinished)) {
                *result = 6;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject AudioClip::staticMetaObject = {
    { &ContextItem::staticMetaObject, qt_meta_stringdata_AudioClip.data,
      qt_meta_data_AudioClip,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *AudioClip::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *AudioClip::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_AudioClip.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "AudioProcessingItem"))
        return static_cast< AudioProcessingItem*>(this);
    if (!strcmp(_clname, "Snappable"))
        return static_cast< Snappable*>(this);
    return ContextItem::qt_metacast(_clname);
}

int AudioClip::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ContextItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 25)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 25;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 25)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 25;
    }
    return _id;
}

// SIGNAL 0
void AudioClip::stateChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void AudioClip::muteChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void AudioClip::lockChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void AudioClip::positionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void AudioClip::fadeAdded(FadeCurve * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void AudioClip::fadeRemoved(FadeCurve * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void AudioClip::recordingFinished(AudioClip * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
