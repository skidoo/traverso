/****************************************************************************
** Meta object code from reading C++ file 'Sheet.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/core/Sheet.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Sheet.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Sheet_t {
    QByteArrayData data[74];
    char stringdata0[1116];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Sheet_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Sheet_t qt_meta_stringdata_Sheet = {
    {
QT_MOC_LITERAL(0, 0, 5), // "Sheet"
QT_MOC_LITERAL(1, 6, 15), // "start_transport"
QT_MOC_LITERAL(2, 22, 4), // "Play"
QT_MOC_LITERAL(3, 27, 34), // "set_recordable_and_start_tran..."
QT_MOC_LITERAL(4, 62, 6), // "Record"
QT_MOC_LITERAL(5, 69, 14), // "work_next_edge"
QT_MOC_LITERAL(6, 84, 24), // "Workcursor: To next edge"
QT_MOC_LITERAL(7, 109, 18), // "work_previous_edge"
QT_MOC_LITERAL(8, 128, 28), // "Workcursor: To previous edge"
QT_MOC_LITERAL(9, 157, 4), // "undo"
QT_MOC_LITERAL(10, 162, 4), // "Undo"
QT_MOC_LITERAL(11, 167, 4), // "redo"
QT_MOC_LITERAL(12, 172, 4), // "Redo"
QT_MOC_LITERAL(13, 177, 11), // "toggle_snap"
QT_MOC_LITERAL(14, 189, 12), // "Snap: On/Off"
QT_MOC_LITERAL(15, 202, 11), // "toggle_solo"
QT_MOC_LITERAL(16, 214, 12), // "Solo: On/Off"
QT_MOC_LITERAL(17, 227, 11), // "toggle_mute"
QT_MOC_LITERAL(18, 239, 12), // "Mute: On/Off"
QT_MOC_LITERAL(19, 252, 10), // "toggle_arm"
QT_MOC_LITERAL(20, 263, 11), // "Arm: On/Off"
QT_MOC_LITERAL(21, 275, 16), // "set_editing_mode"
QT_MOC_LITERAL(22, 292, 10), // "Mode: Edit"
QT_MOC_LITERAL(23, 303, 16), // "set_effects_mode"
QT_MOC_LITERAL(24, 320, 11), // "Mode: Curve"
QT_MOC_LITERAL(25, 332, 13), // "prev_skip_pos"
QT_MOC_LITERAL(26, 346, 25), // "To previous snap position"
QT_MOC_LITERAL(27, 372, 13), // "next_skip_pos"
QT_MOC_LITERAL(28, 386, 21), // "To next snap position"
QT_MOC_LITERAL(29, 408, 12), // "trackRemoved"
QT_MOC_LITERAL(30, 421, 0), // ""
QT_MOC_LITERAL(31, 422, 6), // "Track*"
QT_MOC_LITERAL(32, 429, 10), // "trackAdded"
QT_MOC_LITERAL(33, 440, 12), // "hzoomChanged"
QT_MOC_LITERAL(34, 453, 16), // "transportStarted"
QT_MOC_LITERAL(35, 470, 16), // "transportStopped"
QT_MOC_LITERAL(36, 487, 17), // "workingPosChanged"
QT_MOC_LITERAL(37, 505, 15), // "transportPosSet"
QT_MOC_LITERAL(38, 521, 24), // "firstVisibleFrameChanged"
QT_MOC_LITERAL(39, 546, 24), // "lastFramePositionChanged"
QT_MOC_LITERAL(40, 571, 9), // "seekStart"
QT_MOC_LITERAL(41, 581, 11), // "snapChanged"
QT_MOC_LITERAL(42, 593, 17), // "tempFollowChanged"
QT_MOC_LITERAL(43, 611, 5), // "state"
QT_MOC_LITERAL(44, 617, 15), // "propertyChanged"
QT_MOC_LITERAL(45, 633, 15), // "setCursorAtEdge"
QT_MOC_LITERAL(46, 649, 17), // "masterGainChanged"
QT_MOC_LITERAL(47, 667, 11), // "modeChanged"
QT_MOC_LITERAL(48, 679, 21), // "recordingStateChanged"
QT_MOC_LITERAL(49, 701, 16), // "prepareRecording"
QT_MOC_LITERAL(50, 718, 13), // "seek_finished"
QT_MOC_LITERAL(51, 732, 26), // "audiodevice_client_removed"
QT_MOC_LITERAL(52, 759, 19), // "TAudioDeviceClient*"
QT_MOC_LITERAL(53, 779, 19), // "audiodevice_started"
QT_MOC_LITERAL(54, 799, 26), // "audiodevice_params_changed"
QT_MOC_LITERAL(55, 826, 8), // "set_gain"
QT_MOC_LITERAL(56, 835, 4), // "gain"
QT_MOC_LITERAL(57, 840, 17), // "set_transport_pos"
QT_MOC_LITERAL(58, 858, 7), // "TimeRef"
QT_MOC_LITERAL(59, 866, 8), // "location"
QT_MOC_LITERAL(60, 875, 8), // "get_gain"
QT_MOC_LITERAL(61, 884, 21), // "set_temp_follow_state"
QT_MOC_LITERAL(62, 906, 8), // "Command*"
QT_MOC_LITERAL(63, 915, 14), // "set_recordable"
QT_MOC_LITERAL(64, 930, 17), // "private_add_track"
QT_MOC_LITERAL(65, 948, 5), // "track"
QT_MOC_LITERAL(66, 954, 20), // "private_remove_track"
QT_MOC_LITERAL(67, 975, 33), // "handle_diskio_writebuffer_ove..."
QT_MOC_LITERAL(68, 1009, 33), // "handle_diskio_readbuffer_unde..."
QT_MOC_LITERAL(69, 1043, 17), // "prepare_recording"
QT_MOC_LITERAL(70, 1061, 23), // "clip_finished_recording"
QT_MOC_LITERAL(71, 1085, 10), // "AudioClip*"
QT_MOC_LITERAL(72, 1096, 4), // "clip"
QT_MOC_LITERAL(73, 1101, 14) // "config_changed"

    },
    "Sheet\0start_transport\0Play\0"
    "set_recordable_and_start_transport\0"
    "Record\0work_next_edge\0Workcursor: To next edge\0"
    "work_previous_edge\0Workcursor: To previous edge\0"
    "undo\0Undo\0redo\0Redo\0toggle_snap\0"
    "Snap: On/Off\0toggle_solo\0Solo: On/Off\0"
    "toggle_mute\0Mute: On/Off\0toggle_arm\0"
    "Arm: On/Off\0set_editing_mode\0Mode: Edit\0"
    "set_effects_mode\0Mode: Curve\0prev_skip_pos\0"
    "To previous snap position\0next_skip_pos\0"
    "To next snap position\0trackRemoved\0\0"
    "Track*\0trackAdded\0hzoomChanged\0"
    "transportStarted\0transportStopped\0"
    "workingPosChanged\0transportPosSet\0"
    "firstVisibleFrameChanged\0"
    "lastFramePositionChanged\0seekStart\0"
    "snapChanged\0tempFollowChanged\0state\0"
    "propertyChanged\0setCursorAtEdge\0"
    "masterGainChanged\0modeChanged\0"
    "recordingStateChanged\0prepareRecording\0"
    "seek_finished\0audiodevice_client_removed\0"
    "TAudioDeviceClient*\0audiodevice_started\0"
    "audiodevice_params_changed\0set_gain\0"
    "gain\0set_transport_pos\0TimeRef\0location\0"
    "get_gain\0set_temp_follow_state\0Command*\0"
    "set_recordable\0private_add_track\0track\0"
    "private_remove_track\0"
    "handle_diskio_writebuffer_overrun\0"
    "handle_diskio_readbuffer_underrun\0"
    "prepare_recording\0clip_finished_recording\0"
    "AudioClip*\0clip\0config_changed"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Sheet[] = {

 // content:
       7,       // revision
       0,       // classname
      14,   14, // classinfo
      46,   42, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      18,       // signalCount

 // classinfo: key, value
       1,    2,
       3,    4,
       5,    6,
       7,    8,
       9,   10,
      11,   12,
      13,   14,
      15,   16,
      17,   18,
      19,   20,
      21,   22,
      23,   24,
      25,   26,
      27,   28,

 // signals: name, argc, parameters, tag, flags
      29,    1,  272,   30, 0x06 /* Public */,
      32,    1,  275,   30, 0x06 /* Public */,
      33,    0,  278,   30, 0x06 /* Public */,
      34,    0,  279,   30, 0x06 /* Public */,
      35,    0,  280,   30, 0x06 /* Public */,
      36,    0,  281,   30, 0x06 /* Public */,
      37,    0,  282,   30, 0x06 /* Public */,
      38,    0,  283,   30, 0x06 /* Public */,
      39,    0,  284,   30, 0x06 /* Public */,
      40,    0,  285,   30, 0x06 /* Public */,
      41,    0,  286,   30, 0x06 /* Public */,
      42,    1,  287,   30, 0x06 /* Public */,
      44,    0,  290,   30, 0x06 /* Public */,
      45,    0,  291,   30, 0x06 /* Public */,
      46,    0,  292,   30, 0x06 /* Public */,
      47,    0,  293,   30, 0x06 /* Public */,
      48,    0,  294,   30, 0x06 /* Public */,
      49,    0,  295,   30, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      50,    0,  296,   30, 0x0a /* Public */,
      51,    1,  297,   30, 0x0a /* Public */,
      53,    0,  300,   30, 0x0a /* Public */,
      54,    0,  301,   30, 0x0a /* Public */,
      55,    1,  302,   30, 0x0a /* Public */,
      57,    1,  305,   30, 0x0a /* Public */,
      60,    0,  308,   30, 0x0a /* Public */,
      61,    1,  309,   30, 0x0a /* Public */,
      27,    0,  312,   30, 0x0a /* Public */,
      25,    0,  313,   30, 0x0a /* Public */,
       1,    0,  314,   30, 0x0a /* Public */,
      63,    0,  315,   30, 0x0a /* Public */,
       3,    0,  316,   30, 0x0a /* Public */,
       5,    0,  317,   30, 0x0a /* Public */,
       7,    0,  318,   30, 0x0a /* Public */,
      13,    0,  319,   30, 0x0a /* Public */,
      15,    0,  320,   30, 0x0a /* Public */,
      17,    0,  321,   30, 0x0a /* Public */,
      19,    0,  322,   30, 0x0a /* Public */,
      21,    0,  323,   30, 0x0a /* Public */,
      23,    0,  324,   30, 0x0a /* Public */,
      64,    1,  325,   30, 0x08 /* Private */,
      66,    1,  328,   30, 0x08 /* Private */,
      67,    0,  331,   30, 0x08 /* Private */,
      68,    0,  332,   30, 0x08 /* Private */,
      69,    0,  333,   30, 0x08 /* Private */,
      70,    1,  334,   30, 0x08 /* Private */,
      73,    0,  337,   30, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 31,   30,
    QMetaType::Void, 0x80000000 | 31,   30,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   43,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 52,   30,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Float,   56,
    QMetaType::Void, 0x80000000 | 58,   59,
    QMetaType::Float,
    QMetaType::Void, QMetaType::Bool,   43,
    0x80000000 | 62,
    0x80000000 | 62,
    0x80000000 | 62,
    0x80000000 | 62,
    0x80000000 | 62,
    0x80000000 | 62,
    0x80000000 | 62,
    0x80000000 | 62,
    0x80000000 | 62,
    0x80000000 | 62,
    0x80000000 | 62,
    0x80000000 | 62,
    0x80000000 | 62,
    QMetaType::Void, 0x80000000 | 31,   65,
    QMetaType::Void, 0x80000000 | 31,   65,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 71,   72,
    QMetaType::Void,

       0        // eod
};

void Sheet::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Sheet *_t = static_cast<Sheet *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->trackRemoved((*reinterpret_cast< Track*(*)>(_a[1]))); break;
        case 1: _t->trackAdded((*reinterpret_cast< Track*(*)>(_a[1]))); break;
        case 2: _t->hzoomChanged(); break;
        case 3: _t->transportStarted(); break;
        case 4: _t->transportStopped(); break;
        case 5: _t->workingPosChanged(); break;
        case 6: _t->transportPosSet(); break;
        case 7: _t->firstVisibleFrameChanged(); break;
        case 8: _t->lastFramePositionChanged(); break;
        case 9: _t->seekStart(); break;
        case 10: _t->snapChanged(); break;
        case 11: _t->tempFollowChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 12: _t->propertyChanged(); break;
        case 13: _t->setCursorAtEdge(); break;
        case 14: _t->masterGainChanged(); break;
        case 15: _t->modeChanged(); break;
        case 16: _t->recordingStateChanged(); break;
        case 17: _t->prepareRecording(); break;
        case 18: _t->seek_finished(); break;
        case 19: _t->audiodevice_client_removed((*reinterpret_cast< TAudioDeviceClient*(*)>(_a[1]))); break;
        case 20: _t->audiodevice_started(); break;
        case 21: _t->audiodevice_params_changed(); break;
        case 22: _t->set_gain((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 23: _t->set_transport_pos((*reinterpret_cast< TimeRef(*)>(_a[1]))); break;
        case 24: { float _r = _t->get_gain();
            if (_a[0]) *reinterpret_cast< float*>(_a[0]) = std::move(_r); }  break;
        case 25: _t->set_temp_follow_state((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 26: { Command* _r = _t->next_skip_pos();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 27: { Command* _r = _t->prev_skip_pos();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 28: { Command* _r = _t->start_transport();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 29: { Command* _r = _t->set_recordable();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 30: { Command* _r = _t->set_recordable_and_start_transport();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 31: { Command* _r = _t->work_next_edge();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 32: { Command* _r = _t->work_previous_edge();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 33: { Command* _r = _t->toggle_snap();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 34: { Command* _r = _t->toggle_solo();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 35: { Command* _r = _t->toggle_mute();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 36: { Command* _r = _t->toggle_arm();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 37: { Command* _r = _t->set_editing_mode();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 38: { Command* _r = _t->set_effects_mode();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 39: _t->private_add_track((*reinterpret_cast< Track*(*)>(_a[1]))); break;
        case 40: _t->private_remove_track((*reinterpret_cast< Track*(*)>(_a[1]))); break;
        case 41: _t->handle_diskio_writebuffer_overrun(); break;
        case 42: _t->handle_diskio_readbuffer_underrun(); break;
        case 43: _t->prepare_recording(); break;
        case 44: _t->clip_finished_recording((*reinterpret_cast< AudioClip*(*)>(_a[1]))); break;
        case 45: _t->config_changed(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 23:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< TimeRef >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (Sheet::*)(Track * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Sheet::trackRemoved)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (Sheet::*)(Track * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Sheet::trackAdded)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (Sheet::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Sheet::hzoomChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (Sheet::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Sheet::transportStarted)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (Sheet::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Sheet::transportStopped)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (Sheet::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Sheet::workingPosChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (Sheet::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Sheet::transportPosSet)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (Sheet::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Sheet::firstVisibleFrameChanged)) {
                *result = 7;
                return;
            }
        }
        {
            using _t = void (Sheet::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Sheet::lastFramePositionChanged)) {
                *result = 8;
                return;
            }
        }
        {
            using _t = void (Sheet::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Sheet::seekStart)) {
                *result = 9;
                return;
            }
        }
        {
            using _t = void (Sheet::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Sheet::snapChanged)) {
                *result = 10;
                return;
            }
        }
        {
            using _t = void (Sheet::*)(bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Sheet::tempFollowChanged)) {
                *result = 11;
                return;
            }
        }
        {
            using _t = void (Sheet::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Sheet::propertyChanged)) {
                *result = 12;
                return;
            }
        }
        {
            using _t = void (Sheet::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Sheet::setCursorAtEdge)) {
                *result = 13;
                return;
            }
        }
        {
            using _t = void (Sheet::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Sheet::masterGainChanged)) {
                *result = 14;
                return;
            }
        }
        {
            using _t = void (Sheet::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Sheet::modeChanged)) {
                *result = 15;
                return;
            }
        }
        {
            using _t = void (Sheet::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Sheet::recordingStateChanged)) {
                *result = 16;
                return;
            }
        }
        {
            using _t = void (Sheet::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Sheet::prepareRecording)) {
                *result = 17;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject Sheet::staticMetaObject = {
    { &ContextItem::staticMetaObject, qt_meta_stringdata_Sheet.data,
      qt_meta_data_Sheet,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *Sheet::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Sheet::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Sheet.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "AudioProcessingItem"))
        return static_cast< AudioProcessingItem*>(this);
    return ContextItem::qt_metacast(_clname);
}

int Sheet::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ContextItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 46)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 46;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 46)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 46;
    }
    return _id;
}

// SIGNAL 0
void Sheet::trackRemoved(Track * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Sheet::trackAdded(Track * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Sheet::hzoomChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void Sheet::transportStarted()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void Sheet::transportStopped()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void Sheet::workingPosChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void Sheet::transportPosSet()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}

// SIGNAL 7
void Sheet::firstVisibleFrameChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, nullptr);
}

// SIGNAL 8
void Sheet::lastFramePositionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 8, nullptr);
}

// SIGNAL 9
void Sheet::seekStart()
{
    QMetaObject::activate(this, &staticMetaObject, 9, nullptr);
}

// SIGNAL 10
void Sheet::snapChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 10, nullptr);
}

// SIGNAL 11
void Sheet::tempFollowChanged(bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 11, _a);
}

// SIGNAL 12
void Sheet::propertyChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 12, nullptr);
}

// SIGNAL 13
void Sheet::setCursorAtEdge()
{
    QMetaObject::activate(this, &staticMetaObject, 13, nullptr);
}

// SIGNAL 14
void Sheet::masterGainChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 14, nullptr);
}

// SIGNAL 15
void Sheet::modeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 15, nullptr);
}

// SIGNAL 16
void Sheet::recordingStateChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 16, nullptr);
}

// SIGNAL 17
void Sheet::prepareRecording()
{
    QMetaObject::activate(this, &staticMetaObject, 17, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
