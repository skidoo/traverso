/****************************************************************************
** Meta object code from reading C++ file 'TimeLine.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/core/TimeLine.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TimeLine.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_TimeLine_t {
    QByteArrayData data[10];
    char stringdata0[138];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_TimeLine_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_TimeLine_t qt_meta_stringdata_TimeLine = {
    {
QT_MOC_LITERAL(0, 0, 8), // "TimeLine"
QT_MOC_LITERAL(1, 9, 11), // "markerAdded"
QT_MOC_LITERAL(2, 21, 0), // ""
QT_MOC_LITERAL(3, 22, 7), // "Marker*"
QT_MOC_LITERAL(4, 30, 13), // "markerRemoved"
QT_MOC_LITERAL(5, 44, 21), // "markerPositionChanged"
QT_MOC_LITERAL(6, 66, 18), // "private_add_marker"
QT_MOC_LITERAL(7, 85, 6), // "marker"
QT_MOC_LITERAL(8, 92, 21), // "private_remove_marker"
QT_MOC_LITERAL(9, 114, 23) // "marker_position_changed"

    },
    "TimeLine\0markerAdded\0\0Marker*\0"
    "markerRemoved\0markerPositionChanged\0"
    "private_add_marker\0marker\0"
    "private_remove_marker\0marker_position_changed"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_TimeLine[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   44,    2, 0x06 /* Public */,
       4,    1,   47,    2, 0x06 /* Public */,
       5,    0,   50,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       6,    1,   51,    2, 0x08 /* Private */,
       8,    1,   54,    2, 0x08 /* Private */,
       9,    0,   57,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    2,
    QMetaType::Void, 0x80000000 | 3,    2,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    7,
    QMetaType::Void, 0x80000000 | 3,    7,
    QMetaType::Void,

       0        // eod
};

void TimeLine::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        TimeLine *_t = static_cast<TimeLine *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->markerAdded((*reinterpret_cast< Marker*(*)>(_a[1]))); break;
        case 1: _t->markerRemoved((*reinterpret_cast< Marker*(*)>(_a[1]))); break;
        case 2: _t->markerPositionChanged(); break;
        case 3: _t->private_add_marker((*reinterpret_cast< Marker*(*)>(_a[1]))); break;
        case 4: _t->private_remove_marker((*reinterpret_cast< Marker*(*)>(_a[1]))); break;
        case 5: _t->marker_position_changed(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (TimeLine::*)(Marker * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TimeLine::markerAdded)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (TimeLine::*)(Marker * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TimeLine::markerRemoved)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (TimeLine::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TimeLine::markerPositionChanged)) {
                *result = 2;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject TimeLine::staticMetaObject = {
    { &ContextItem::staticMetaObject, qt_meta_stringdata_TimeLine.data,
      qt_meta_data_TimeLine,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *TimeLine::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *TimeLine::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_TimeLine.stringdata0))
        return static_cast<void*>(this);
    return ContextItem::qt_metacast(_clname);
}

int TimeLine::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ContextItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 6;
    }
    return _id;
}

// SIGNAL 0
void TimeLine::markerAdded(Marker * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void TimeLine::markerRemoved(Marker * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void TimeLine::markerPositionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
