/****************************************************************************
** Meta object code from reading C++ file 'AudioClipManager.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/core/AudioClipManager.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'AudioClipManager.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_AudioClipManager_t {
    QByteArrayData data[16];
    char stringdata0[206];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_AudioClipManager_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_AudioClipManager_t qt_meta_stringdata_AudioClipManager = {
    {
QT_MOC_LITERAL(0, 0, 16), // "AudioClipManager"
QT_MOC_LITERAL(1, 17, 16), // "select_all_clips"
QT_MOC_LITERAL(2, 34, 10), // "Select all"
QT_MOC_LITERAL(3, 45, 21), // "invert_clip_selection"
QT_MOC_LITERAL(4, 67, 6), // "Invert"
QT_MOC_LITERAL(5, 74, 8), // "add_clip"
QT_MOC_LITERAL(6, 83, 0), // ""
QT_MOC_LITERAL(7, 84, 10), // "AudioClip*"
QT_MOC_LITERAL(8, 95, 4), // "clip"
QT_MOC_LITERAL(9, 100, 11), // "remove_clip"
QT_MOC_LITERAL(10, 112, 11), // "select_clip"
QT_MOC_LITERAL(11, 124, 15), // "toggle_selected"
QT_MOC_LITERAL(12, 140, 21), // "remove_from_selection"
QT_MOC_LITERAL(13, 162, 16), // "add_to_selection"
QT_MOC_LITERAL(14, 179, 17), // "update_last_frame"
QT_MOC_LITERAL(15, 197, 8) // "Command*"

    },
    "AudioClipManager\0select_all_clips\0"
    "Select all\0invert_clip_selection\0"
    "Invert\0add_clip\0\0AudioClip*\0clip\0"
    "remove_clip\0select_clip\0toggle_selected\0"
    "remove_from_selection\0add_to_selection\0"
    "update_last_frame\0Command*"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_AudioClipManager[] = {

 // content:
       7,       // revision
       0,       // classname
       2,   14, // classinfo
       9,   18, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // classinfo: key, value
       1,    2,
       3,    4,

 // slots: name, argc, parameters, tag, flags
       5,    1,   63,    6, 0x0a /* Public */,
       9,    1,   66,    6, 0x0a /* Public */,
      10,    1,   69,    6, 0x0a /* Public */,
      11,    1,   72,    6, 0x0a /* Public */,
      12,    1,   75,    6, 0x0a /* Public */,
      13,    1,   78,    6, 0x0a /* Public */,
      14,    0,   81,    6, 0x0a /* Public */,
       1,    0,   82,    6, 0x0a /* Public */,
       3,    0,   83,    6, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 7,    8,
    QMetaType::Void, 0x80000000 | 7,    8,
    QMetaType::Void, 0x80000000 | 7,    8,
    QMetaType::Void, 0x80000000 | 7,    8,
    QMetaType::Void, 0x80000000 | 7,    8,
    QMetaType::Void, 0x80000000 | 7,    8,
    QMetaType::Void,
    0x80000000 | 15,
    0x80000000 | 15,

       0        // eod
};

void AudioClipManager::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        AudioClipManager *_t = static_cast<AudioClipManager *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->add_clip((*reinterpret_cast< AudioClip*(*)>(_a[1]))); break;
        case 1: _t->remove_clip((*reinterpret_cast< AudioClip*(*)>(_a[1]))); break;
        case 2: _t->select_clip((*reinterpret_cast< AudioClip*(*)>(_a[1]))); break;
        case 3: _t->toggle_selected((*reinterpret_cast< AudioClip*(*)>(_a[1]))); break;
        case 4: _t->remove_from_selection((*reinterpret_cast< AudioClip*(*)>(_a[1]))); break;
        case 5: _t->add_to_selection((*reinterpret_cast< AudioClip*(*)>(_a[1]))); break;
        case 6: _t->update_last_frame(); break;
        case 7: { Command* _r = _t->select_all_clips();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 8: { Command* _r = _t->invert_clip_selection();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject AudioClipManager::staticMetaObject = {
    { &ContextItem::staticMetaObject, qt_meta_stringdata_AudioClipManager.data,
      qt_meta_data_AudioClipManager,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *AudioClipManager::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *AudioClipManager::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_AudioClipManager.stringdata0))
        return static_cast<void*>(this);
    return ContextItem::qt_metacast(_clname);
}

int AudioClipManager::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ContextItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 9;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
