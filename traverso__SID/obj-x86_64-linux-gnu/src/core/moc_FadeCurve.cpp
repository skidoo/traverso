/****************************************************************************
** Meta object code from reading C++ file 'FadeCurve.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/core/FadeCurve.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'FadeCurve.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_FadeCurve_t {
    QByteArrayData data[17];
    char stringdata0[213];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_FadeCurve_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_FadeCurve_t qt_meta_stringdata_FadeCurve = {
    {
QT_MOC_LITERAL(0, 0, 9), // "FadeCurve"
QT_MOC_LITERAL(1, 10, 13), // "toggle_bypass"
QT_MOC_LITERAL(2, 24, 13), // "Toggle Bypass"
QT_MOC_LITERAL(3, 38, 8), // "set_mode"
QT_MOC_LITERAL(4, 47, 11), // "Cycle Shape"
QT_MOC_LITERAL(5, 59, 5), // "reset"
QT_MOC_LITERAL(6, 65, 11), // "Remove Fade"
QT_MOC_LITERAL(7, 77, 13), // "toggle_raster"
QT_MOC_LITERAL(8, 91, 13), // "Toggle Raster"
QT_MOC_LITERAL(9, 105, 11), // "modeChanged"
QT_MOC_LITERAL(10, 117, 0), // ""
QT_MOC_LITERAL(11, 118, 16), // "bendValueChanged"
QT_MOC_LITERAL(12, 135, 20), // "strengthValueChanged"
QT_MOC_LITERAL(13, 156, 13), // "rasterChanged"
QT_MOC_LITERAL(14, 170, 12), // "rangeChanged"
QT_MOC_LITERAL(15, 183, 20), // "solve_node_positions"
QT_MOC_LITERAL(16, 204, 8) // "Command*"

    },
    "FadeCurve\0toggle_bypass\0Toggle Bypass\0"
    "set_mode\0Cycle Shape\0reset\0Remove Fade\0"
    "toggle_raster\0Toggle Raster\0modeChanged\0"
    "\0bendValueChanged\0strengthValueChanged\0"
    "rasterChanged\0rangeChanged\0"
    "solve_node_positions\0Command*"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_FadeCurve[] = {

 // content:
       7,       // revision
       0,       // classname
       4,   14, // classinfo
      10,   22, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // classinfo: key, value
       1,    2,
       3,    4,
       5,    6,
       7,    8,

 // signals: name, argc, parameters, tag, flags
       9,    0,   72,   10, 0x06 /* Public */,
      11,    0,   73,   10, 0x06 /* Public */,
      12,    0,   74,   10, 0x06 /* Public */,
      13,    0,   75,   10, 0x06 /* Public */,
      14,    0,   76,   10, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      15,    0,   77,   10, 0x0a /* Public */,
       1,    0,   78,   10, 0x0a /* Public */,
       3,    0,   79,   10, 0x0a /* Public */,
       5,    0,   80,   10, 0x0a /* Public */,
       7,    0,   81,   10, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    0x80000000 | 16,
    0x80000000 | 16,
    0x80000000 | 16,
    0x80000000 | 16,

       0        // eod
};

void FadeCurve::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        FadeCurve *_t = static_cast<FadeCurve *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->modeChanged(); break;
        case 1: _t->bendValueChanged(); break;
        case 2: _t->strengthValueChanged(); break;
        case 3: _t->rasterChanged(); break;
        case 4: _t->rangeChanged(); break;
        case 5: _t->solve_node_positions(); break;
        case 6: { Command* _r = _t->toggle_bypass();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 7: { Command* _r = _t->set_mode();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 8: { Command* _r = _t->reset();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 9: { Command* _r = _t->toggle_raster();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (FadeCurve::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&FadeCurve::modeChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (FadeCurve::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&FadeCurve::bendValueChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (FadeCurve::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&FadeCurve::strengthValueChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (FadeCurve::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&FadeCurve::rasterChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (FadeCurve::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&FadeCurve::rangeChanged)) {
                *result = 4;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject FadeCurve::staticMetaObject = {
    { &Curve::staticMetaObject, qt_meta_stringdata_FadeCurve.data,
      qt_meta_data_FadeCurve,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *FadeCurve::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *FadeCurve::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_FadeCurve.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "APILinkedListNode"))
        return static_cast< APILinkedListNode*>(this);
    return Curve::qt_metacast(_clname);
}

int FadeCurve::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Curve::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 10)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 10;
    }
    return _id;
}

// SIGNAL 0
void FadeCurve::modeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void FadeCurve::bendValueChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void FadeCurve::strengthValueChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void FadeCurve::rasterChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void FadeCurve::rangeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
