/****************************************************************************
** Meta object code from reading C++ file 'ResourcesManager.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/core/ResourcesManager.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'ResourcesManager.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_ResourcesManager_t {
    QByteArrayData data[11];
    char stringdata0[115];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ResourcesManager_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ResourcesManager_t qt_meta_stringdata_ResourcesManager = {
    {
QT_MOC_LITERAL(0, 0, 16), // "ResourcesManager"
QT_MOC_LITERAL(1, 17, 13), // "stateRestored"
QT_MOC_LITERAL(2, 31, 0), // ""
QT_MOC_LITERAL(3, 32, 11), // "clipRemoved"
QT_MOC_LITERAL(4, 44, 10), // "AudioClip*"
QT_MOC_LITERAL(5, 55, 4), // "clip"
QT_MOC_LITERAL(6, 60, 9), // "clipAdded"
QT_MOC_LITERAL(7, 70, 11), // "sourceAdded"
QT_MOC_LITERAL(8, 82, 11), // "ReadSource*"
QT_MOC_LITERAL(9, 94, 6), // "source"
QT_MOC_LITERAL(10, 101, 13) // "sourceRemoved"

    },
    "ResourcesManager\0stateRestored\0\0"
    "clipRemoved\0AudioClip*\0clip\0clipAdded\0"
    "sourceAdded\0ReadSource*\0source\0"
    "sourceRemoved"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ResourcesManager[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   39,    2, 0x06 /* Public */,
       3,    1,   40,    2, 0x06 /* Public */,
       6,    1,   43,    2, 0x06 /* Public */,
       7,    1,   46,    2, 0x06 /* Public */,
      10,    1,   49,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4,    5,
    QMetaType::Void, 0x80000000 | 4,    5,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, 0x80000000 | 8,    9,

       0        // eod
};

void ResourcesManager::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        ResourcesManager *_t = static_cast<ResourcesManager *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->stateRestored(); break;
        case 1: _t->clipRemoved((*reinterpret_cast< AudioClip*(*)>(_a[1]))); break;
        case 2: _t->clipAdded((*reinterpret_cast< AudioClip*(*)>(_a[1]))); break;
        case 3: _t->sourceAdded((*reinterpret_cast< ReadSource*(*)>(_a[1]))); break;
        case 4: _t->sourceRemoved((*reinterpret_cast< ReadSource*(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (ResourcesManager::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ResourcesManager::stateRestored)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (ResourcesManager::*)(AudioClip * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ResourcesManager::clipRemoved)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (ResourcesManager::*)(AudioClip * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ResourcesManager::clipAdded)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (ResourcesManager::*)(ReadSource * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ResourcesManager::sourceAdded)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (ResourcesManager::*)(ReadSource * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ResourcesManager::sourceRemoved)) {
                *result = 4;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject ResourcesManager::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_ResourcesManager.data,
      qt_meta_data_ResourcesManager,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *ResourcesManager::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ResourcesManager::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_ResourcesManager.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int ResourcesManager::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 5;
    }
    return _id;
}

// SIGNAL 0
void ResourcesManager::stateRestored()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void ResourcesManager::clipRemoved(AudioClip * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void ResourcesManager::clipAdded(AudioClip * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void ResourcesManager::sourceAdded(ReadSource * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void ResourcesManager::sourceRemoved(ReadSource * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
