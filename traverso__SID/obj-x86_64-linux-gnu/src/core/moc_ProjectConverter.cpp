/****************************************************************************
** Meta object code from reading C++ file 'ProjectConverter.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/core/ProjectConverter.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'ProjectConverter.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_ProjectConverter_t {
    QByteArrayData data[14];
    char stringdata0[213];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ProjectConverter_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ProjectConverter_t qt_meta_stringdata_ProjectConverter = {
    {
QT_MOC_LITERAL(0, 0, 16), // "ProjectConverter"
QT_MOC_LITERAL(1, 17, 8), // "progress"
QT_MOC_LITERAL(2, 26, 0), // ""
QT_MOC_LITERAL(3, 27, 18), // "actionProgressInfo"
QT_MOC_LITERAL(4, 46, 16), // "fileMergeStarted"
QT_MOC_LITERAL(5, 63, 4), // "file"
QT_MOC_LITERAL(6, 68, 17), // "fileMergeFinished"
QT_MOC_LITERAL(7, 86, 18), // "conversionFinished"
QT_MOC_LITERAL(8, 105, 7), // "message"
QT_MOC_LITERAL(9, 113, 19), // "conversion_finished"
QT_MOC_LITERAL(10, 133, 18), // "file_merge_started"
QT_MOC_LITERAL(11, 152, 19), // "file_merge_finished"
QT_MOC_LITERAL(12, 172, 21), // "finish_2_3_conversion"
QT_MOC_LITERAL(13, 194, 18) // "processing_stopped"

    },
    "ProjectConverter\0progress\0\0"
    "actionProgressInfo\0fileMergeStarted\0"
    "file\0fileMergeFinished\0conversionFinished\0"
    "message\0conversion_finished\0"
    "file_merge_started\0file_merge_finished\0"
    "finish_2_3_conversion\0processing_stopped"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ProjectConverter[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       6,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   69,    2, 0x06 /* Public */,
       3,    1,   72,    2, 0x06 /* Public */,
       4,    1,   75,    2, 0x06 /* Public */,
       6,    1,   78,    2, 0x06 /* Public */,
       7,    0,   81,    2, 0x06 /* Public */,
       8,    1,   82,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       9,    0,   85,    2, 0x08 /* Private */,
      10,    1,   86,    2, 0x08 /* Private */,
      11,    1,   89,    2, 0x08 /* Private */,
      12,    0,   92,    2, 0x08 /* Private */,
      13,    0,   93,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::QString,    5,
    QMetaType::Void, QMetaType::QString,    5,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    2,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    5,
    QMetaType::Void, QMetaType::QString,    5,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void ProjectConverter::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        ProjectConverter *_t = static_cast<ProjectConverter *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->progress((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->actionProgressInfo((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->fileMergeStarted((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 3: _t->fileMergeFinished((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 4: _t->conversionFinished(); break;
        case 5: _t->message((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 6: _t->conversion_finished(); break;
        case 7: _t->file_merge_started((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 8: _t->file_merge_finished((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 9: _t->finish_2_3_conversion(); break;
        case 10: _t->processing_stopped(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (ProjectConverter::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ProjectConverter::progress)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (ProjectConverter::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ProjectConverter::actionProgressInfo)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (ProjectConverter::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ProjectConverter::fileMergeStarted)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (ProjectConverter::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ProjectConverter::fileMergeFinished)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (ProjectConverter::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ProjectConverter::conversionFinished)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (ProjectConverter::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ProjectConverter::message)) {
                *result = 5;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject ProjectConverter::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_ProjectConverter.data,
      qt_meta_data_ProjectConverter,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *ProjectConverter::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ProjectConverter::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_ProjectConverter.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int ProjectConverter::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 11)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 11;
    }
    return _id;
}

// SIGNAL 0
void ProjectConverter::progress(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void ProjectConverter::actionProgressInfo(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void ProjectConverter::fileMergeStarted(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void ProjectConverter::fileMergeFinished(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void ProjectConverter::conversionFinished()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void ProjectConverter::message(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
