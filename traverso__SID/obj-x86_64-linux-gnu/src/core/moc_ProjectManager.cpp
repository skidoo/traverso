/****************************************************************************
** Meta object code from reading C++ file 'ProjectManager.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/core/ProjectManager.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'ProjectManager.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_ProjectManager_t {
    QByteArrayData data[20];
    char stringdata0[294];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ProjectManager_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ProjectManager_t qt_meta_stringdata_ProjectManager = {
    {
QT_MOC_LITERAL(0, 0, 14), // "ProjectManager"
QT_MOC_LITERAL(1, 15, 12), // "save_project"
QT_MOC_LITERAL(2, 28, 12), // "Save Project"
QT_MOC_LITERAL(3, 41, 4), // "exit"
QT_MOC_LITERAL(4, 46, 16), // "Exit application"
QT_MOC_LITERAL(5, 63, 13), // "projectLoaded"
QT_MOC_LITERAL(6, 77, 0), // ""
QT_MOC_LITERAL(7, 78, 8), // "Project*"
QT_MOC_LITERAL(8, 87, 13), // "aboutToDelete"
QT_MOC_LITERAL(9, 101, 6), // "Sheet*"
QT_MOC_LITERAL(10, 108, 24), // "currentProjectDirChanged"
QT_MOC_LITERAL(11, 133, 35), // "unsupportedProjectDirChangeDe..."
QT_MOC_LITERAL(12, 169, 24), // "projectDirChangeDetected"
QT_MOC_LITERAL(13, 194, 17), // "projectLoadFailed"
QT_MOC_LITERAL(14, 212, 26), // "projectFileVersionMismatch"
QT_MOC_LITERAL(15, 239, 8), // "Command*"
QT_MOC_LITERAL(16, 248, 4), // "undo"
QT_MOC_LITERAL(17, 253, 4), // "redo"
QT_MOC_LITERAL(18, 258, 27), // "project_dir_rename_detected"
QT_MOC_LITERAL(19, 286, 7) // "dirname"

    },
    "ProjectManager\0save_project\0Save Project\0"
    "exit\0Exit application\0projectLoaded\0"
    "\0Project*\0aboutToDelete\0Sheet*\0"
    "currentProjectDirChanged\0"
    "unsupportedProjectDirChangeDetected\0"
    "projectDirChangeDetected\0projectLoadFailed\0"
    "projectFileVersionMismatch\0Command*\0"
    "undo\0redo\0project_dir_rename_detected\0"
    "dirname"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ProjectManager[] = {

 // content:
       7,       // revision
       0,       // classname
       2,   14, // classinfo
      12,   18, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       7,       // signalCount

 // classinfo: key, value
       1,    2,
       3,    4,

 // signals: name, argc, parameters, tag, flags
       5,    1,   78,    6, 0x06 /* Public */,
       8,    1,   81,    6, 0x06 /* Public */,
      10,    0,   84,    6, 0x06 /* Public */,
      11,    0,   85,    6, 0x06 /* Public */,
      12,    0,   86,    6, 0x06 /* Public */,
      13,    2,   87,    6, 0x06 /* Public */,
      14,    2,   92,    6, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       1,    0,   97,    6, 0x0a /* Public */,
       3,    0,   98,    6, 0x0a /* Public */,
      16,    0,   99,    6, 0x0a /* Public */,
      17,    0,  100,    6, 0x0a /* Public */,
      18,    1,  101,    6, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 7,    6,
    QMetaType::Void, 0x80000000 | 9,    6,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,    6,    6,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,    6,    6,

 // slots: parameters
    0x80000000 | 15,
    0x80000000 | 15,
    0x80000000 | 15,
    0x80000000 | 15,
    QMetaType::Void, QMetaType::QString,   19,

       0        // eod
};

void ProjectManager::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        ProjectManager *_t = static_cast<ProjectManager *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->projectLoaded((*reinterpret_cast< Project*(*)>(_a[1]))); break;
        case 1: _t->aboutToDelete((*reinterpret_cast< Sheet*(*)>(_a[1]))); break;
        case 2: _t->currentProjectDirChanged(); break;
        case 3: _t->unsupportedProjectDirChangeDetected(); break;
        case 4: _t->projectDirChangeDetected(); break;
        case 5: _t->projectLoadFailed((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 6: _t->projectFileVersionMismatch((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 7: { Command* _r = _t->save_project();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 8: { Command* _r = _t->exit();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 9: { Command* _r = _t->undo();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 10: { Command* _r = _t->redo();
            if (_a[0]) *reinterpret_cast< Command**>(_a[0]) = std::move(_r); }  break;
        case 11: _t->project_dir_rename_detected((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (ProjectManager::*)(Project * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ProjectManager::projectLoaded)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (ProjectManager::*)(Sheet * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ProjectManager::aboutToDelete)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (ProjectManager::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ProjectManager::currentProjectDirChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (ProjectManager::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ProjectManager::unsupportedProjectDirChangeDetected)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (ProjectManager::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ProjectManager::projectDirChangeDetected)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (ProjectManager::*)(QString , QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ProjectManager::projectLoadFailed)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (ProjectManager::*)(QString , QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ProjectManager::projectFileVersionMismatch)) {
                *result = 6;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject ProjectManager::staticMetaObject = {
    { &ContextItem::staticMetaObject, qt_meta_stringdata_ProjectManager.data,
      qt_meta_data_ProjectManager,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *ProjectManager::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ProjectManager::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_ProjectManager.stringdata0))
        return static_cast<void*>(this);
    return ContextItem::qt_metacast(_clname);
}

int ProjectManager::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = ContextItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 12)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 12;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 12)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 12;
    }
    return _id;
}

// SIGNAL 0
void ProjectManager::projectLoaded(Project * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void ProjectManager::aboutToDelete(Sheet * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void ProjectManager::currentProjectDirChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void ProjectManager::unsupportedProjectDirChangeDetected()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void ProjectManager::projectDirChangeDetected()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void ProjectManager::projectLoadFailed(QString _t1, QString _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void ProjectManager::projectFileVersionMismatch(QString _t1, QString _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
